<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--NewPage-->
<HTML>
<HEAD>
<!-- Generated by javadoc (build 1.5.0_12) on Sat Oct 30 14:00:52 EEST 2010 -->
<TITLE>
Filter (OpenFaces 3.0.0 API Documentation)
</TITLE>

<META NAME="keywords" CONTENT="org.openfaces.component.filter.Filter class">

<LINK REL ="stylesheet" TYPE="text/css" HREF="../../../../stylesheet.css" TITLE="Style">

<SCRIPT type="text/javascript">
function windowTitle()
{
    parent.document.title="Filter (OpenFaces 3.0.0 API Documentation)";
}
</SCRIPT>
<NOSCRIPT>
</NOSCRIPT>

</HEAD>

<BODY BGCOLOR="white" onload="windowTitle();">


<!-- ========= START OF TOP NAVBAR ======= -->
<A NAME="navbar_top"><!-- --></A>
<A HREF="#skip-navbar_top" title="Skip navigation links"></A>
<TABLE BORDER="0" WIDTH="100%" CELLPADDING="1" CELLSPACING="0" SUMMARY="">
<TR>
<TD COLSPAN=2 BGCOLOR="#EEEEFF" CLASS="NavBarCell1">
<A NAME="navbar_top_firstrow"><!-- --></A>
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="3" SUMMARY="">
  <TR ALIGN="center" VALIGN="top">
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../../overview-summary.html"><FONT CLASS="NavBarFont1"><B>Overview</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="package-summary.html"><FONT CLASS="NavBarFont1"><B>Package</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#FFFFFF" CLASS="NavBarCell1Rev"> &nbsp;<FONT CLASS="NavBarFont1Rev"><B>Class</B></FONT>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="class-use/Filter.html"><FONT CLASS="NavBarFont1"><B>Use</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="package-tree.html"><FONT CLASS="NavBarFont1"><B>Tree</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../../deprecated-list.html"><FONT CLASS="NavBarFont1"><B>Deprecated</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../../index-all.html"><FONT CLASS="NavBarFont1"><B>Index</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../../help-doc.html"><FONT CLASS="NavBarFont1"><B>Help</B></FONT></A>&nbsp;</TD>
  </TR>
</TABLE>
</TD>
<TD ALIGN="right" VALIGN="top" ROWSPAN=3><EM>
</EM>
</TD>
</TR>

<TR>
<TD BGCOLOR="white" CLASS="NavBarCell2"><FONT SIZE="-2">
&nbsp;<A HREF="../../../../org/openfaces/component/filter/ExpressionFilterCriterion.html" title="class in org.openfaces.component.filter"><B>PREV CLASS</B></A>&nbsp;
&nbsp;<A HREF="../../../../org/openfaces/component/filter/FilterableComponentPropertyLocatorFactory.html" title="class in org.openfaces.component.filter"><B>NEXT CLASS</B></A></FONT></TD>
<TD BGCOLOR="white" CLASS="NavBarCell2"><FONT SIZE="-2">
  <A HREF="../../../../index.html?org/openfaces/component/filter/Filter.html" target="_top"><B>FRAMES</B></A>  &nbsp;
&nbsp;<A HREF="Filter.html" target="_top"><B>NO FRAMES</B></A>  &nbsp;
&nbsp;<SCRIPT type="text/javascript">
  <!--
  if(window==top) {
    document.writeln('<A HREF="../../../../allclasses-noframe.html"><B>All Classes</B></A>');
  }
  //-->
</SCRIPT>
<NOSCRIPT>
  <A HREF="../../../../allclasses-noframe.html"><B>All Classes</B></A>
</NOSCRIPT>


</FONT></TD>
</TR>
<TR>
<TD VALIGN="top" CLASS="NavBarCell3"><FONT SIZE="-2">
  SUMMARY:&nbsp;NESTED&nbsp;|&nbsp;<A HREF="#fields_inherited_from_class_javax.faces.component.UIComponent">FIELD</A>&nbsp;|&nbsp;<A HREF="#constructor_summary">CONSTR</A>&nbsp;|&nbsp;<A HREF="#method_summary">METHOD</A></FONT></TD>
<TD VALIGN="top" CLASS="NavBarCell3"><FONT SIZE="-2">
DETAIL:&nbsp;FIELD&nbsp;|&nbsp;<A HREF="#constructor_detail">CONSTR</A>&nbsp;|&nbsp;<A HREF="#method_detail">METHOD</A></FONT></TD>
</TR>
</TABLE>
<A NAME="skip-navbar_top"></A>
<!-- ========= END OF TOP NAVBAR ========= -->

<HR>
<!-- ======== START OF CLASS DATA ======== -->
<H2>
<FONT SIZE="-1">
org.openfaces.component.filter</FONT>
<BR>
Class Filter</H2>
<PRE>
java.lang.Object
  <IMG SRC="../../../../resources/inherit.gif" ALT="extended by ">javax.faces.component.UIComponent
      <IMG SRC="../../../../resources/inherit.gif" ALT="extended by ">javax.faces.component.UIComponentBase
          <IMG SRC="../../../../resources/inherit.gif" ALT="extended by "><A HREF="../../../../org/openfaces/component/OUIComponentBase.html" title="class in org.openfaces.component">org.openfaces.component.OUIComponentBase</A>
              <IMG SRC="../../../../resources/inherit.gif" ALT="extended by "><B>org.openfaces.component.filter.Filter</B>
</PRE>
<DL>
<DT><B>All Implemented Interfaces:</B> <DD>java.util.EventListener, javax.faces.component.PartialStateHolder, javax.faces.component.StateHolder, javax.faces.event.ComponentSystemEventListener, javax.faces.event.FacesListener, javax.faces.event.SystemEventListenerHolder, <A HREF="../../../../org/openfaces/component/OUIComponent.html" title="interface in org.openfaces.component">OUIComponent</A></DD>
</DL>
<DL>
<DT><B>Direct Known Subclasses:</B> <DD><A HREF="../../../../org/openfaces/component/filter/CompositeFilter.html" title="class in org.openfaces.component.filter">CompositeFilter</A>, <A HREF="../../../../org/openfaces/component/filter/ExpressionFilter.html" title="class in org.openfaces.component.filter">ExpressionFilter</A></DD>
</DL>
<HR>
<DL>
<DT><PRE>public abstract class <B>Filter</B><DT>extends <A HREF="../../../../org/openfaces/component/OUIComponentBase.html" title="class in org.openfaces.component">OUIComponentBase</A></DL>
</PRE>

<P>
<HR>

<P>
<!-- =========== FIELD SUMMARY =========== -->

<A NAME="field_summary"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#CCCCFF" CLASS="TableHeadingColor">
<TH ALIGN="left" COLSPAN="2"><FONT SIZE="+2">
<B>Field Summary</B></FONT></TH>
</TR>
</TABLE>
&nbsp;<A NAME="fields_inherited_from_class_javax.faces.component.UIComponent"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#EEEEFF" CLASS="TableSubHeadingColor">
<TH ALIGN="left"><B>Fields inherited from class javax.faces.component.UIComponent</B></TH>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD><CODE>BEANINFO_KEY, COMPOSITE_COMPONENT_TYPE_KEY, COMPOSITE_FACET_NAME, CURRENT_COMPONENT, CURRENT_COMPOSITE_COMPONENT, FACETS_KEY, VIEW_LOCATION_KEY</CODE></TD>
</TR>
</TABLE>
&nbsp;
<!-- ======== CONSTRUCTOR SUMMARY ======== -->

<A NAME="constructor_summary"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#CCCCFF" CLASS="TableHeadingColor">
<TH ALIGN="left" COLSPAN="2"><FONT SIZE="+2">
<B>Constructor Summary</B></FONT></TH>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD><CODE><B><A HREF="../../../../org/openfaces/component/filter/Filter.html#Filter()">Filter</A></B>()</CODE>

<BR>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
</TR>
</TABLE>
&nbsp;
<!-- ========== METHOD SUMMARY =========== -->

<A NAME="method_summary"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#CCCCFF" CLASS="TableHeadingColor">
<TH ALIGN="left" COLSPAN="2"><FONT SIZE="+2">
<B>Method Summary</B></FONT></TH>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD ALIGN="right" VALIGN="top" WIDTH="1%"><FONT SIZE="-1">
<CODE>&nbsp;<A HREF="../../../../org/openfaces/component/FilterableComponent.html" title="interface in org.openfaces.component">FilterableComponent</A></CODE></FONT></TD>
<TD><CODE><B><A HREF="../../../../org/openfaces/component/filter/Filter.html#getFilteredComponent()">getFilteredComponent</A></B>()</CODE>

<BR>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD ALIGN="right" VALIGN="top" WIDTH="1%"><FONT SIZE="-1">
<CODE>&nbsp;java.lang.String</CODE></FONT></TD>
<TD><CODE><B><A HREF="../../../../org/openfaces/component/filter/Filter.html#getFor()">getFor</A></B>()</CODE>

<BR>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD ALIGN="right" VALIGN="top" WIDTH="1%"><FONT SIZE="-1">
<CODE>abstract &nbsp;java.lang.Object</CODE></FONT></TD>
<TD><CODE><B><A HREF="../../../../org/openfaces/component/filter/Filter.html#getValue()">getValue</A></B>()</CODE>

<BR>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD ALIGN="right" VALIGN="top" WIDTH="1%"><FONT SIZE="-1">
<CODE>abstract &nbsp;boolean</CODE></FONT></TD>
<TD><CODE><B><A HREF="../../../../org/openfaces/component/filter/Filter.html#getWantsRowList()">getWantsRowList</A></B>()</CODE>

<BR>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD ALIGN="right" VALIGN="top" WIDTH="1%"><FONT SIZE="-1">
<CODE>&nbsp;boolean</CODE></FONT></TD>
<TD><CODE><B><A HREF="../../../../org/openfaces/component/filter/Filter.html#isAcceptingAllRecords()">isAcceptingAllRecords</A></B>()</CODE>

<BR>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD ALIGN="right" VALIGN="top" WIDTH="1%"><FONT SIZE="-1">
<CODE>&nbsp;void</CODE></FONT></TD>
<TD><CODE><B><A HREF="../../../../org/openfaces/component/filter/Filter.html#restoreState(javax.faces.context.FacesContext, java.lang.Object)">restoreState</A></B>(javax.faces.context.FacesContext&nbsp;context,
             java.lang.Object&nbsp;stateObj)</CODE>

<BR>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD ALIGN="right" VALIGN="top" WIDTH="1%"><FONT SIZE="-1">
<CODE>&nbsp;java.lang.Object</CODE></FONT></TD>
<TD><CODE><B><A HREF="../../../../org/openfaces/component/filter/Filter.html#saveState(javax.faces.context.FacesContext)">saveState</A></B>(javax.faces.context.FacesContext&nbsp;context)</CODE>

<BR>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD ALIGN="right" VALIGN="top" WIDTH="1%"><FONT SIZE="-1">
<CODE>&nbsp;void</CODE></FONT></TD>
<TD><CODE><B><A HREF="../../../../org/openfaces/component/filter/Filter.html#setFor(java.lang.String)">setFor</A></B>(java.lang.String&nbsp;aFor)</CODE>

<BR>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD ALIGN="right" VALIGN="top" WIDTH="1%"><FONT SIZE="-1">
<CODE>&nbsp;void</CODE></FONT></TD>
<TD><CODE><B><A HREF="../../../../org/openfaces/component/filter/Filter.html#setParent(javax.faces.component.UIComponent)">setParent</A></B>(javax.faces.component.UIComponent&nbsp;parent)</CODE>

<BR>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD ALIGN="right" VALIGN="top" WIDTH="1%"><FONT SIZE="-1">
<CODE>abstract &nbsp;void</CODE></FONT></TD>
<TD><CODE><B><A HREF="../../../../org/openfaces/component/filter/Filter.html#setValue(java.lang.Object)">setValue</A></B>(java.lang.Object&nbsp;value)</CODE>

<BR>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD ALIGN="right" VALIGN="top" WIDTH="1%"><FONT SIZE="-1">
<CODE>abstract &nbsp;void</CODE></FONT></TD>
<TD><CODE><B><A HREF="../../../../org/openfaces/component/filter/Filter.html#updateValueFromBinding(javax.faces.context.FacesContext)">updateValueFromBinding</A></B>(javax.faces.context.FacesContext&nbsp;context)</CODE>

<BR>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
</TR>
</TABLE>
&nbsp;<A NAME="methods_inherited_from_class_org.openfaces.component.OUIComponentBase"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#EEEEFF" CLASS="TableSubHeadingColor">
<TH ALIGN="left"><B>Methods inherited from class org.openfaces.component.<A HREF="../../../../org/openfaces/component/OUIComponentBase.html" title="class in org.openfaces.component">OUIComponentBase</A></B></TH>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD><CODE><A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getOnblur()">getOnblur</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getOnclick()">getOnclick</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getOncontextmenu()">getOncontextmenu</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getOndblclick()">getOndblclick</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getOnfocus()">getOnfocus</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getOnkeydown()">getOnkeydown</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getOnkeypress()">getOnkeypress</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getOnkeyup()">getOnkeyup</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getOnmousedown()">getOnmousedown</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getOnmousemove()">getOnmousemove</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getOnmouseout()">getOnmouseout</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getOnmouseover()">getOnmouseover</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getOnmouseup()">getOnmouseup</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getRolloverClass()">getRolloverClass</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getRolloverStyle()">getRolloverStyle</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getStyle()">getStyle</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#getStyleClass()">getStyleClass</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setOnblur(java.lang.String)">setOnblur</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setOnclick(java.lang.String)">setOnclick</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setOncontextmenu(java.lang.String)">setOncontextmenu</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setOndblclick(java.lang.String)">setOndblclick</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setOnfocus(java.lang.String)">setOnfocus</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setOnkeydown(java.lang.String)">setOnkeydown</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setOnkeypress(java.lang.String)">setOnkeypress</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setOnkeyup(java.lang.String)">setOnkeyup</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setOnmousedown(java.lang.String)">setOnmousedown</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setOnmousemove(java.lang.String)">setOnmousemove</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setOnmouseout(java.lang.String)">setOnmouseout</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setOnmouseover(java.lang.String)">setOnmouseover</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setOnmouseup(java.lang.String)">setOnmouseup</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setRolloverClass(java.lang.String)">setRolloverClass</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setRolloverStyle(java.lang.String)">setRolloverStyle</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setStyle(java.lang.String)">setStyle</A>, <A HREF="../../../../org/openfaces/component/OUIComponentBase.html#setStyleClass(java.lang.String)">setStyleClass</A></CODE></TD>
</TR>
</TABLE>
&nbsp;<A NAME="methods_inherited_from_class_javax.faces.component.UIComponentBase"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#EEEEFF" CLASS="TableSubHeadingColor">
<TH ALIGN="left"><B>Methods inherited from class javax.faces.component.UIComponentBase</B></TH>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD><CODE>addClientBehavior, broadcast, clearInitialState, decode, encodeBegin, encodeChildren, encodeEnd, findComponent, getAttributes, getChildCount, getChildren, getClientBehaviors, getClientId, getDefaultEventName, getEventNames, getFacet, getFacetCount, getFacets, getFacetsAndChildren, getId, getParent, getRendererType, getRendersChildren, getValueBinding, invokeOnComponent, isRendered, isTransient, markInitialState, processDecodes, processRestoreState, processSaveState, processUpdates, processValidators, queueEvent, restoreAttachedState, saveAttachedState, setId, setRendered, setRendererType, setTransient, setValueBinding</CODE></TD>
</TR>
</TABLE>
&nbsp;<A NAME="methods_inherited_from_class_javax.faces.component.UIComponent"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#EEEEFF" CLASS="TableSubHeadingColor">
<TH ALIGN="left"><B>Methods inherited from class javax.faces.component.UIComponent</B></TH>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD><CODE>encodeAll, getClientId, getCompositeComponentParent, getContainerClientId, getCurrentComponent, getCurrentCompositeComponent, getFamily, getListenersForEventClass, getNamingContainer, getResourceBundleMap, getValueExpression, initialStateMarked, isCompositeComponent, isInView, popComponentFromEL, processEvent, pushComponentToEL, setInView, setValueExpression, subscribeToEvent, unsubscribeFromEvent, visitTree</CODE></TD>
</TR>
</TABLE>
&nbsp;<A NAME="methods_inherited_from_class_java.lang.Object"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#EEEEFF" CLASS="TableSubHeadingColor">
<TH ALIGN="left"><B>Methods inherited from class java.lang.Object</B></TH>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD><CODE>equals, getClass, hashCode, notify, notifyAll, toString, wait, wait, wait</CODE></TD>
</TR>
</TABLE>
&nbsp;
<P>

<!-- ========= CONSTRUCTOR DETAIL ======== -->

<A NAME="constructor_detail"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#CCCCFF" CLASS="TableHeadingColor">
<TH ALIGN="left" COLSPAN="1"><FONT SIZE="+2">
<B>Constructor Detail</B></FONT></TH>
</TR>
</TABLE>

<A NAME="Filter()"><!-- --></A><H3>
Filter</H3>
<PRE>
public <B>Filter</B>()</PRE>
<DL>
</DL>

<!-- ============ METHOD DETAIL ========== -->

<A NAME="method_detail"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#CCCCFF" CLASS="TableHeadingColor">
<TH ALIGN="left" COLSPAN="1"><FONT SIZE="+2">
<B>Method Detail</B></FONT></TH>
</TR>
</TABLE>

<A NAME="saveState(javax.faces.context.FacesContext)"><!-- --></A><H3>
saveState</H3>
<PRE>
public java.lang.Object <B>saveState</B>(javax.faces.context.FacesContext&nbsp;context)</PRE>
<DL>
<DD><DL>
<DT><B>Specified by:</B><DD><CODE>saveState</CODE> in interface <CODE>javax.faces.component.StateHolder</CODE><DT><B>Overrides:</B><DD><CODE><A HREF="../../../../org/openfaces/component/OUIComponentBase.html#saveState(javax.faces.context.FacesContext)">saveState</A></CODE> in class <CODE><A HREF="../../../../org/openfaces/component/OUIComponentBase.html" title="class in org.openfaces.component">OUIComponentBase</A></CODE></DL>
</DD>
<DD><DL>
</DL>
</DD>
</DL>
<HR>

<A NAME="restoreState(javax.faces.context.FacesContext, java.lang.Object)"><!-- --></A><H3>
restoreState</H3>
<PRE>
public void <B>restoreState</B>(javax.faces.context.FacesContext&nbsp;context,
                         java.lang.Object&nbsp;stateObj)</PRE>
<DL>
<DD><DL>
<DT><B>Specified by:</B><DD><CODE>restoreState</CODE> in interface <CODE>javax.faces.component.StateHolder</CODE><DT><B>Overrides:</B><DD><CODE><A HREF="../../../../org/openfaces/component/OUIComponentBase.html#restoreState(javax.faces.context.FacesContext, java.lang.Object)">restoreState</A></CODE> in class <CODE><A HREF="../../../../org/openfaces/component/OUIComponentBase.html" title="class in org.openfaces.component">OUIComponentBase</A></CODE></DL>
</DD>
<DD><DL>
</DL>
</DD>
</DL>
<HR>

<A NAME="getFor()"><!-- --></A><H3>
getFor</H3>
<PRE>
public java.lang.String <B>getFor</B>()</PRE>
<DL>
<DD><DL>
</DL>
</DD>
</DL>
<HR>

<A NAME="setFor(java.lang.String)"><!-- --></A><H3>
setFor</H3>
<PRE>
public void <B>setFor</B>(java.lang.String&nbsp;aFor)</PRE>
<DL>
<DD><DL>
</DL>
</DD>
</DL>
<HR>

<A NAME="getValue()"><!-- --></A><H3>
getValue</H3>
<PRE>
public abstract java.lang.Object <B>getValue</B>()</PRE>
<DL>
<DD><DL>
</DL>
</DD>
</DL>
<HR>

<A NAME="setValue(java.lang.Object)"><!-- --></A><H3>
setValue</H3>
<PRE>
public abstract void <B>setValue</B>(java.lang.Object&nbsp;value)</PRE>
<DL>
<DD><DL>
</DL>
</DD>
</DL>
<HR>

<A NAME="updateValueFromBinding(javax.faces.context.FacesContext)"><!-- --></A><H3>
updateValueFromBinding</H3>
<PRE>
public abstract void <B>updateValueFromBinding</B>(javax.faces.context.FacesContext&nbsp;context)</PRE>
<DL>
<DD><DL>
</DL>
</DD>
</DL>
<HR>

<A NAME="getWantsRowList()"><!-- --></A><H3>
getWantsRowList</H3>
<PRE>
public abstract boolean <B>getWantsRowList</B>()</PRE>
<DL>
<DD><DL>
</DL>
</DD>
</DL>
<HR>

<A NAME="isAcceptingAllRecords()"><!-- --></A><H3>
isAcceptingAllRecords</H3>
<PRE>
public boolean <B>isAcceptingAllRecords</B>()</PRE>
<DL>
<DD><DL>
</DL>
</DD>
</DL>
<HR>

<A NAME="getFilteredComponent()"><!-- --></A><H3>
getFilteredComponent</H3>
<PRE>
public <A HREF="../../../../org/openfaces/component/FilterableComponent.html" title="interface in org.openfaces.component">FilterableComponent</A> <B>getFilteredComponent</B>()</PRE>
<DL>
<DD><DL>
</DL>
</DD>
</DL>
<HR>

<A NAME="setParent(javax.faces.component.UIComponent)"><!-- --></A><H3>
setParent</H3>
<PRE>
public void <B>setParent</B>(javax.faces.component.UIComponent&nbsp;parent)</PRE>
<DL>
<DD><DL>
<DT><B>Overrides:</B><DD><CODE>setParent</CODE> in class <CODE>javax.faces.component.UIComponentBase</CODE></DL>
</DD>
<DD><DL>
</DL>
</DD>
</DL>
<!-- ========= END OF CLASS DATA ========= -->
<HR>


<!-- ======= START OF BOTTOM NAVBAR ====== -->
<A NAME="navbar_bottom"><!-- --></A>
<A HREF="#skip-navbar_bottom" title="Skip navigation links"></A>
<TABLE BORDER="0" WIDTH="100%" CELLPADDING="1" CELLSPACING="0" SUMMARY="">
<TR>
<TD COLSPAN=2 BGCOLOR="#EEEEFF" CLASS="NavBarCell1">
<A NAME="navbar_bottom_firstrow"><!-- --></A>
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="3" SUMMARY="">
  <TR ALIGN="center" VALIGN="top">
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../../overview-summary.html"><FONT CLASS="NavBarFont1"><B>Overview</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="package-summary.html"><FONT CLASS="NavBarFont1"><B>Package</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#FFFFFF" CLASS="NavBarCell1Rev"> &nbsp;<FONT CLASS="NavBarFont1Rev"><B>Class</B></FONT>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="class-use/Filter.html"><FONT CLASS="NavBarFont1"><B>Use</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="package-tree.html"><FONT CLASS="NavBarFont1"><B>Tree</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../../deprecated-list.html"><FONT CLASS="NavBarFont1"><B>Deprecated</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../../index-all.html"><FONT CLASS="NavBarFont1"><B>Index</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../../help-doc.html"><FONT CLASS="NavBarFont1"><B>Help</B></FONT></A>&nbsp;</TD>
  </TR>
</TABLE>
</TD>
<TD ALIGN="right" VALIGN="top" ROWSPAN=3><EM>
</EM>
</TD>
</TR>

<TR>
<TD BGCOLOR="white" CLASS="NavBarCell2"><FONT SIZE="-2">
&nbsp;<A HREF="../../../../org/openfaces/component/filter/ExpressionFilterCriterion.html" title="class in org.openfaces.component.filter"><B>PREV CLASS</B></A>&nbsp;
&nbsp;<A HREF="../../../../org/openfaces/component/filter/FilterableComponentPropertyLocatorFactory.html" title="class in org.openfaces.component.filter"><B>NEXT CLASS</B></A></FONT></TD>
<TD BGCOLOR="white" CLASS="NavBarCell2"><FONT SIZE="-2">
  <A HREF="../../../../index.html?org/openfaces/component/filter/Filter.html" target="_top"><B>FRAMES</B></A>  &nbsp;
&nbsp;<A HREF="Filter.html" target="_top"><B>NO FRAMES</B></A>  &nbsp;
&nbsp;<SCRIPT type="text/javascript">
  <!--
  if(window==top) {
    document.writeln('<A HREF="../../../../allclasses-noframe.html"><B>All Classes</B></A>');
  }
  //-->
</SCRIPT>
<NOSCRIPT>
  <A HREF="../../../../allclasses-noframe.html"><B>All Classes</B></A>
</NOSCRIPT>


</FONT></TD>
</TR>
<TR>
<TD VALIGN="top" CLASS="NavBarCell3"><FONT SIZE="-2">
  SUMMARY:&nbsp;NESTED&nbsp;|&nbsp;<A HREF="#fields_inherited_from_class_javax.faces.component.UIComponent">FIELD</A>&nbsp;|&nbsp;<A HREF="#constructor_summary">CONSTR</A>&nbsp;|&nbsp;<A HREF="#method_summary">METHOD</A></FONT></TD>
<TD VALIGN="top" CLASS="NavBarCell3"><FONT SIZE="-2">
DETAIL:&nbsp;FIELD&nbsp;|&nbsp;<A HREF="#constructor_detail">CONSTR</A>&nbsp;|&nbsp;<A HREF="#method_detail">METHOD</A></FONT></TD>
</TR>
</TABLE>
<A NAME="skip-navbar_bottom"></A>
<!-- ======== END OF BOTTOM NAVBAR ======= -->

<HR>
<i>Copyright &#169; 1998-2010 TeamDev Ltd. All Rights Reserved.</i>
</BODY>
</HTML>
