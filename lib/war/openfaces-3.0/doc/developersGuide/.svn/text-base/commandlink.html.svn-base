
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE>CommandLink &mdash; OpenFaces documentation</TITLE>
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<LINK rel="icon" href="favicon.ico" type="image/x-icon">
<LINK rel="shortcut icon" href="favicon.ico" type="image/x-icon">
</HEAD>

<BODY>
<DIV id="logo"><A href="index.html"><IMG src="images/documentation_logo.gif" alt="OpenFaces Documentation" border="0"></A><BR><SPAN style="margin-top:0;">Version 3.0.0</SPAN></DIV>
<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
  <TR>
    <TD width="240px" align="right" valign="bottom" style="border-bottom:1px solid #CCCCCC;">&nbsp;</TD>
    <TD height="30px" style="border-bottom:1px solid #CCCCCC;">&nbsp;</TD>
  </TR>
  <TR>
    <TD colspan="2" valign="top"><H1>CommandLink</H1></TD>
  </TR>
  <TR>
    <TD valign="top" style="background: url(images/line.gif) #fbfbfc repeat-y right; padding:0px 0px 0px 0px; width:240px; margin: 0px 0px 0px 0px;">
	<TABLE width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
	<TR>
	<TD>
        <DIV class="contents index" style="background-color:#FFFFFF; width:260px;"><UL><LI><A href="#CommandLink-KeyFeatures">Key Features</A></LI>
    <LI><A href="#CommandLink-GeneralInformation">General Information</A></LI>
    <LI><A href="#CommandLink-KnownIssues">Known Issues</A></LI></UL></DIV>
	</TD>
	</TR>
	</TABLE>
	<DIV class="components_links">
				<A href="index.html">Developer&rsquo;s Guide Home</A><BR>
		  <A href="installation-and-configuration.html">Installation and Configuration</A><BR>
		  <A href="common-concepts.html">Common Concepts</A><BR>
          <A href="components-index.html" style="padding-bottom:0.5em;">Components Index</A>
                    <HR noshade="noshade" size="1px" color="#d5d6d7" width="85%">
                    <A href="borderlayoutpanel.html">Border Layout Panel</A><BR>
                    <A href="calendar.html">Calendar</A><BR>
                    <A href="chart.html">Chart</A><BR>
                    <A href="commandbutton.html">Command Button</A><BR>
                    <A href="commandlink.html">Command Link</A><BR>
                    <A href="compositefilter.html">Composite Filter</A><BR>
                    <A href="confirmation.html">Confirmation</A><BR>
                    <A href="datatable.html">Data Table</A><BR>
                    <A href="datechooser.html">Date Chooser</A><BR>
                    <A href="daytable.html">Day Table</A><BR>
                    <A href="dropdownfield.html">Drop Down Field</A><BR>
                    <A href="dynamicimage.html">Dynamic Image</A><BR>
                    <A href="foldingpanel.html">Folding Panel</A><BR>
                    <A href="foreach.html">For Each</A><BR>
                    <A href="graphictext.html">Graphic Text</A><BR>
                    <A href="hintlabel.html">Hint Label</A><BR>
                    <A href="inputtext.html">Input Text</A><BR>
                    <A href="inputtextarea.html">Input Textarea</A><BR>
                    <A href="layeredpane.html">Layered Pane</A><BR>
                    <A href="levelindicator.html">Level Indicator</A><BR>
                    <A href="popuplayer.html">Popup Layer</A><BR>
                    <A href="popupmenu.html">Popup Menu</A><BR>
                    <A href="selectbooleancheckbox.html">Select Boolean Checkbox</A><BR>
                    <A href="selectmanycheckbox.html">Select Many Checkbox</A><BR>
                    <A href="selectoneradio.html">Select One Radio</A><BR>
                    <A href="spinner.html">Spinner</A><BR>
		            <A href="suggestionfield.html">Suggestion Field</A><BR>
                    <A href="tabbedpane.html">Tabbed Pane</A><BR>
                    <A href="tabset.html">Tab Set</A><BR>
                    <A href="treetable.html">Tree Table</A><BR>
                    <A href="twolistselection.html">Two List Selection</A><BR>
                    <A href="window.html" style="padding-bottom:0.5em;">Window</A>
                    <HR noshade="noshade" size="1px" color="#d5d6d7" width="85%">
                    <A href="focus.html">Focus</A><BR>
                    <A href="loadbundle.html">Load Bundle</A><BR>
                    <A href="scrollposition.html" style="padding-bottom:0.5em;">Scroll Position</A>
                    <HR noshade="noshade" size="1px" color="#d5d6d7" width="85%">
                    <A href="ajax-framework.html">Ajax Framework</A>
                    <UL>
                        <LI><A href="ajax.html">Ajax Component</A><BR></LI>
                        <LI><A href="ajax-framework.html#AjaxFramework-PageWideConfiguration">Ajax Settings Component</A><BR></LI>
                    </UL>
                    <A href="validation-framework.html">Validation Framework</A>
                    <HR noshade="noshade" size="1px" color="#d5d6d7" width="85%">
                    <A href="../tagReference/index.html">Tag Reference</A><BR>
                    <A href="../apiReference/index.html">API Reference</A>
	<DIV style=" width:240px; height:0px; padding: 0px; margin:0px;"></DIV>
		</DIV>
	</TD>

		<TD style="padding-bottom:1em; padding-left:2em;"><P>

<TABLE  border="0" cellpadding="5" cellspacing="0" width="100%"><TBODY><TR>
<TD valign="top" width="60%"><P><BR>
This is an analog of the standard <SPAN class="tag">&lt;h:commandLink&gt;</SPAN> component extended with Ajax features making it      possible to use Ajax request instead of form submission to execute an action and/or reload components. </P></TD>
<TD valign="top" width="40%">
<P><IMG src="components-index.data/commandlink.png" align="absmiddle" border="0"><BR>
<A href="../apiReference/org/openfaces/component/command/CommandLink.html" rel="nofollow">API Reference</A> &#124; <A href="../tagReference/o/commandLink.html" rel="nofollow">Tag Reference</A><BR>
<A href="http://www.openfaces.org/demo/commandlink/" rel="nofollow">Online Demo</A></P></TD></TR></TBODY></TABLE>

<H2><A name="CommandLink-KeyFeatures"></A>Key Features</H2>

<UL>
	<LI>All features of the standard CommandLink component</LI>
	<LI>Support for Ajax action execution</LI>
	<LI>Support for Ajax component submission/reloading</LI>
</UL>


<H2><A name="CommandLink-GeneralInformation"></A>General Information</H2>

<P>The <SPAN class="tag">&lt;o:commandLink&gt;</SPAN> component is API-compatible with the standard <SPAN class="tag">&lt;h:commandLink&gt;</SPAN> component and if no extended options are specified it behaves in the same way as its standard analog.</P>

<P>You can turn on the Ajax mode and configure the link to reload the specified set of components instead of reloading the whole page by specifying its <SPAN class="attribute">render</SPAN> attribute. This attribute is specified as a space-separated list of component Ids in the same way as the <SPAN class="attribute">render</SPAN> attribute of the <CITE><A href="ajax.html" title="Ajax">Ajax</A></CITE> component. Specifying this attribute will also make <SPAN class="attribute">action</SPAN> and/or <SPAN class="attribute">actionListener</SPAN> specified for the link to be executed during the Ajax request as well. If the Ajax mode is turned on for the link it doesn't submit data for form's components for processing on the server by default, so if you'd like to include some particular components into the Ajax request, for their data to be available during action execution or be saved through bindings, you can specify the <SPAN class="attribute">execute</SPAN> attribute. Like the <SPAN class="attribute">action</SPAN> attribute, this attribute is specified as a space-delimited list of component Ids in the same way as for the <CITE><A href="ajax.html" title="Ajax">Ajax</A></CITE> component. Here's a simple example:</P>
<PRE class="code_area">&lt;h:inputText id=<SPAN class="code_area_values">&quot;firstName&quot;</SPAN> value=<SPAN class="code_area_values">&quot;#{MyBean.firstName}&quot;</SPAN>/&gt;
&lt;h:inputText id=<SPAN class="code_area_values">&quot;lastName&quot;</SPAN> value=<SPAN class="code_area_values">&quot;#{MyBean.lastName}&quot;</SPAN>/&gt;
&lt;o:commandLink value=<SPAN class="code_area_values">&quot;Execute&quot;</SPAN> 
        execute=<SPAN class="code_area_values">&quot;firstName lastName&quot;</SPAN> 
        action=<SPAN class="code_area_values">&quot;#{MyBean.processName}&quot;</SPAN>
        render=<SPAN class="code_area_values">&quot;fullName&quot;</SPAN>/&gt;
&lt;h:outputText id=<SPAN class="code_area_values">&quot;fullName&quot;</SPAN> value=<SPAN class="code_area_values">&quot;#{MyBean.fullName}&quot;</SPAN>/&gt;</PRE> 

<P>When in the Ajax mode, <CITE>CommandLink</CITE> provides the Ajax request lifecycle notifications through its <SPAN class="attribute">onajaxstart</SPAN>, <SPAN class="attribute">onajaxend</SPAN>, and <SPAN class="attribute">onerror</SPAN> events.</P>
<H2><A name="CommandLink-KnownIssues"></A>Known Issues</H2>

<UL class="alternate" type="square">
	<LI>The <SPAN class="tag">&lt;f:param&gt;</SPAN> tags placed into the <SPAN class="tag">&lt;o:commandLink&gt;</SPAN> tag are ignored currently.</LI>
</UL>

    </TD>

  </TR>

  <TR>
    <TD height="50" align="left" class="copyright" style="border-top:1px solid #CCCCCC;">&copy; 2010 <A href="http://www.teamdev.com/">TeamDev Ltd</A>.</TD>
    <TD height="50" align="right" style="border-top:1px solid #CCCCCC;"><A style="border-bottom: none;" href="http://www.openfaces.org"><IMG src="images/logo.gif" alt="OpenFaces" width="124" height="28" border="0"></A></TD>
  </TR>
</TABLE>
</BODY>

</HTML>
