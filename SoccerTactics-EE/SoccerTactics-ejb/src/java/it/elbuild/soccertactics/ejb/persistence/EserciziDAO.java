/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.persistence;

import el.persistence.db.JpaDAO;
import it.elbuild.soccertactics.lib.entities.Esercizio;
import it.elbuild.soccertactics.lib.entities.Esercizio_;
import it.elbuild.soccertactics.lib.utils.RicercaEsercizi;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Ale
 */
public class EserciziDAO extends JpaDAO<Integer, Esercizio> {

    public EserciziDAO(EntityManagerFactory emf) {
        super(emf);
    }
    
    public List<Esercizio> findByIdUtente(Integer idUtente){
        List<Esercizio> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findByIdUtente");
            q.setParameter("idUtente", idUtente);
            res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res;
    }
    
    public List<Esercizio> findEserciziTitoloDesc(Integer idUtente, String text, List<Integer> uids) {
        List<Esercizio> res = null;

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Esercizio> cq = cb.createQuery(Esercizio.class);
        Root<Esercizio> s = cq.from(Esercizio.class);

        List<Predicate> predicati = new ArrayList<Predicate>();
        
        Predicate p = cb.isFalse(s.get(Esercizio_.eliminato));
        predicati.add(p);
        
        if(idUtente!=null){
            p = cb.equal(s.get(Esercizio_.idUtente), idUtente);
            predicati.add(p);
        }
        if(uids!=null && !uids.isEmpty()){
            p = s.get(Esercizio_.idUtente).in(uids);
            predicati.add(p);
        }
        
        List<Predicate> predicatiOR = new ArrayList<Predicate>();        
        p = cb.like(cb.lower(s.get(Esercizio_.titolo)), "%" + text.toLowerCase() + "%");
        predicatiOR.add(p);
        p = cb.like(cb.lower(s.get(Esercizio_.descrizione)), "%" + text.toLowerCase() + "%");
        predicatiOR.add(p);
        Predicate orp = cb.or(predicatiOR.get(0), predicatiOR.get(1));
        predicati.add(orp);
        
        Predicate[] pred = new Predicate[predicati.size()];
        cq.where(predicati.toArray(pred)).distinct(true);

        cq.orderBy(cb.desc(s.get(Esercizio_.id)));

        cq.distinct(true);
        TypedQuery<Esercizio> tq = em.createQuery(cq);
        res = tq.getResultList();
        
        return res;
    }
    
    public List<Esercizio> ricercaAvanzataEsercizi(RicercaEsercizi re){
    
        javax.persistence.criteria.CriteriaBuilder cb =em.getCriteriaBuilder();
        CriteriaQuery<Esercizio> cq = cb.createQuery(Esercizio.class);
        Root<Esercizio> s = cq.from(Esercizio.class);
        
        re.buildPredicates(cb, s);
        
        List<Predicate> predicati = re.getPredicati();
        
        Predicate[] pred = new Predicate[predicati.size()];
        cq.where(predicati.toArray(pred)).distinct(true);
//        if (orderBy != null) {
//            cq.orderBy(asc ? cb.asc(s.get(orderBy)) : cb.desc(s.get(orderBy)));
//        } else {
            cq.orderBy(cb.desc(s.get(Esercizio_.titolo)));
//        }
        TypedQuery<Esercizio> tq = em.createQuery(cq);
//        tq.setFirstResult(start);
//        tq.setMaxResults(stop - start);
        List<Esercizio> results = tq.getResultList();
        close();

        return results;
    }
    
    public List<Esercizio> findByIdUtenti(List<Integer> uids){
        List<Esercizio> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findByIdUtenti");
            q.setParameter("idUList", uids);
            res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res;
    }
}
