/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans;

import it.elbuild.soccertactics.ejb.persistence.DAOFactory;
import it.elbuild.soccertactics.lib.entities.Utente;
import it.elbuild.soccertactics.lib.interfaces.UtentiManager;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Ale
 */
@Stateless
public class UtentiManagerBean implements UtentiManager{
    
    @EJB
    DAOFactory factory;

    @Override
    public Utente findByEmail(String email) {
        return factory.getUtenteDAO().findByEmail(email);
    }

    @Override
    public Utente insertUpdateUtente(Utente utente) {
        return factory.getUtenteDAO().insertOrUpdate(utente);
    }

    @Override
    public List<Utente> findAllUtenti() {
        return factory.getUtenteDAO().findAll();
    }

    @Override
    public Utente findById(Integer idUtente) {
        return factory.getUtenteDAO().findById(idUtente);
    }

    @Override
    public Utente findByCF(String cf) {
        return factory.getUtenteDAO().findByCF(cf);
    }
    
    
}
