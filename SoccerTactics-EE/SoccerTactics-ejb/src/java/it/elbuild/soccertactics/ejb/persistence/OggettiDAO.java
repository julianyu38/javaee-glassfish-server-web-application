/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.persistence;

import el.persistence.db.JpaDAO;
import it.elbuild.soccertactics.lib.entities.Oggetti;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Ale
 */
public class OggettiDAO extends JpaDAO<Integer, Oggetti> {

    public OggettiDAO(EntityManagerFactory emf) {
        super(emf);
    }
    
    public List<Oggetti> findAllOggetti(){
        List<Oggetti> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findAll");
                res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res;

    }
}
