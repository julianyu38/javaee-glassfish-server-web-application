/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.persistence;

import el.persistence.db.JpaDAO;
import it.elbuild.soccertactics.lib.entities.Societa;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Ale
 */
public class SocietaDAO extends JpaDAO<Integer, Societa> {

    public SocietaDAO(EntityManagerFactory emf) {
        super(emf);
    }
    
}
