/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.persistence;

import el.persistence.db.JpaDAO;
import it.elbuild.soccertactics.lib.entities.Mesociclo;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Ale
 */
public class MesocicloDAO extends JpaDAO<Integer, Mesociclo> {

    public MesocicloDAO(EntityManagerFactory emf) {
        super(emf);
    }
    
    public List<Mesociclo> findByIdStagioneIdUtente(Integer idStagione, Integer idUtente){
        List<Mesociclo> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findByIdStagioneIdUtente");
            q.setParameter("idUtente", idUtente);
            q.setParameter("idStagione", idStagione);
            res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res;
    }
    
    public List<Mesociclo> findByIdStagioneIdUtenteSettimanaAnno(Integer idStagione, Integer idUtente, Integer settimana, Integer anno){
        List<Mesociclo> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findByIdStagioneIdUtenteSettimanaAnno");
            q.setParameter("idUtente", idUtente);
            q.setParameter("idStagione", idStagione);
            q.setParameter("settimana", settimana);
            q.setParameter("anno", anno);
            res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res.isEmpty() ? null : res ;
    }
    
    public Mesociclo findByIdStagioneIdUtenteMese(Integer idStagione, Integer idUtente, Integer mese, Integer anno){
        List<Mesociclo> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findByIdStagioneIdUtenteMese");
            q.setParameter("idUtente", idUtente);
            q.setParameter("idStagione", idStagione);
            q.setParameter("mese", mese);
            q.setParameter("anno", anno);
            res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res!=null && !res.isEmpty() ? res.get(0) : null;
    }
    
}
