/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans.fatture;

import it.elbuild.soccertactics.ejb.persistence.DAOFactory;
import it.elbuild.soccertactics.lib.entities.fatture.DocumentoFatturazione;
import it.elbuild.soccertactics.lib.entities.fatture.Fattura;
import it.elbuild.soccertactics.lib.entities.fatture.FatturaSemplice;
import it.elbuild.soccertactics.lib.entities.fatture.IntestatarioFattura;
import it.elbuild.soccertactics.lib.entities.fatture.VoceFattura;
import it.elbuild.soccertactics.lib.interfaces.fatture.GestoreFatture;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 *
 * @author Ale
 */
@Stateless
public class GestoreFattureBean implements GestoreFatture {
    
    @EJB
    DAOFactory factory;
  
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    
    private SimpleDateFormat sdfanno = new SimpleDateFormat("yyyy");
    
    public static Date getYearStart(Date date) {
        Date d = null;
        if (date != null) {
            Calendar c = Calendar.getInstance(TimeZone.getTimeZone("CET"));
            c.setTime(date);
            Calendar c2 = Calendar.getInstance(TimeZone.getTimeZone("CET"));
            int year = c.get(Calendar.YEAR);
            c2.set(Calendar.YEAR, year);
            c2.set(Calendar.MONTH, 0);
            c2.set(Calendar.DAY_OF_MONTH, 1);
            c2.set(Calendar.HOUR_OF_DAY, 0);
            c2.set(Calendar.MINUTE, 0);
            c2.set(Calendar.SECOND, 0);
            c2.set(Calendar.MILLISECOND, 0);
            d = c2.getTime();
        }
        return d;
    }

    public static Date getYearEnd(Date date) {
        Date d = null;
        if (date != null) {
            Calendar c = Calendar.getInstance(TimeZone.getTimeZone("CET"));
            c.setTime(date);
            Calendar c2 = Calendar.getInstance(TimeZone.getTimeZone("CET"));
            int year = c.get(Calendar.YEAR);
            c2.set(Calendar.YEAR, year);
            c2.set(Calendar.MONTH, 11);
            c2.set(Calendar.DAY_OF_MONTH, 31);
            c2.set(Calendar.HOUR_OF_DAY, 23);
            c2.set(Calendar.MINUTE, 59);
            c2.set(Calendar.SECOND, 59);
            c2.set(Calendar.MILLISECOND, 0);
            d = c2.getTime();
        }
        return d;
    }

    @Override
    public Fattura inserisciOAggiornaFattura(Fattura f) {
        try {
       // System.out.println("intestatario fattura "+f.getIntestatario().toString());
            f.setDataUltimaModifica(new Date(System.currentTimeMillis()));
            return factory.getFatturaDAO().insertOrUpdate(f);
        } catch (Exception ex) {
            Logger.getLogger(GestoreFattureBean.class.getName()).log(Level.SEVERE, null, ex);
//            notifiche.inviaNotificaErrore(ex);
        }
        return null;
    }
     
    @Override
    public Integer trovaUltimoNumeroFattura(Date date) throws Exception {
        try {
//            Date date = null;
//            date = new Date(System.currentTimeMillis());
            Date d1 = getYearStart(date);
            Date d2 = getYearEnd(date);
            //    System.out.println("Estremi anno :"+d1+" "+d2);        
            Integer i = factory.getFatturaDAO().findLastNumero(d1, d2);
            if (i == null || i < 0) {
                return 0;
            } else {
                return i;
            }
        } catch (Exception ex) {
            Logger.getLogger(GestoreFattureBean.class.getName()).log(Level.SEVERE, null, ex);
//            notifiche.inviaNotificaErrore(ex);
            throw new Exception(ex);
        }
    }
    
    
    @Override
    public boolean numeroFatturaInUso(Integer numero) throws Exception {
        try {
            Date date = null;
            date = new Date(System.currentTimeMillis());
            Date d1 = getYearStart(date);
            Date d2 = getYearEnd(date);
            Fattura i = factory.getFatturaDAO().findByNumeroInDates(numero, d1, d2);
            if (i == null) {
                return false;
            } else {
                return true;
            }
        } catch (Exception ex) {
            Logger.getLogger(GestoreFattureBean.class.getName()).log(Level.SEVERE, null, ex);
//            notifiche.inviaNotificaErrore(ex);
            throw new Exception(ex);
        }
    }
    
    @Override
    public Fattura emettiFattura(Fattura fattura) throws Exception {
        try {
            fattura.emetti();
            Integer i = trovaUltimoNumeroFattura(fattura.getData());
            boolean u = numeroFatturaInUso(fattura.getNumero());
            if (u) {
                throw new Exception("Numero fattura in uso: " + i);
            } else {
                return inserisciOAggiornaFattura(fattura);
            }
        } catch (Exception ex) {
            Logger.getLogger(GestoreFattureBean.class.getName()).log(Level.SEVERE, null, ex);
//            notifiche.inviaNotificaErrore(ex);
            throw new Exception(ex);
        }
    }
    
             
    @Override
    public List<Fattura> trovaTutteFatture() {
        try {
            return factory.getFatturaDAO().findAll();
        } catch (Exception ex) {
            Logger.getLogger(GestoreFattureBean.class.getName()).log(Level.SEVERE, null, ex);
//            notifiche.inviaNotificaErrore(ex);
        }
        return new ArrayList<Fattura>();
    }

    @Override
    public void eliminaDocumentoFatturazione(DocumentoFatturazione f) {
        try {
            factory.getFatturaDAO().delete(f.getId());
        } catch (Exception ex) {
            Logger.getLogger(GestoreFattureBean.class.getName()).log(Level.SEVERE, null, ex);
//            notifiche.inviaNotificaErrore(ex);
        }
    }

    @Override
    public List<Fattura> trovaUltimeFatture(int i) {
        try {
            return factory.getFatturaDAO().findLast(i);
        } catch (Exception ex) {
            Logger.getLogger(GestoreFattureBean.class.getName()).log(Level.SEVERE, null, ex);
//            notifiche.inviaNotificaErrore(ex);
        }
        return new ArrayList<Fattura>();
    }

    @Override
    public OutputStream generaPDF(DocumentoFatturazione f, String templatePath, String  savePath) throws Exception {
        if (savePath == null || templatePath == null) {
            throw new Exception("Non e' possibile generare il pdf perche' i dati sui percorsi del template e della directory di destinazione non sono completi");
        }        
        OutputStream output = null;
        try {
            File reportFile = new File(templatePath);
            IntestatarioFattura intestatario = f.getIntestatario();

            Map parameters = new HashMap();
            parameters.put("cognomenome", f.getIntestatario().getCognome()+" "+f.getIntestatario().getNome());
            parameters.put("ragionesociale", f.getIntestatario().getRagioneSociale());
            parameters.put("luogonascita", f.getIntestatario().getLuogoNascita());
            parameters.put("datanascita", f.getIntestatario().getDataNascita()!=null ? sdf.format(f.getIntestatario().getDataNascita()) : "");
            parameters.put("residenza", f.getIntestatario().getLuogoResidenza()!=null ? f.getIntestatario().getLuogoResidenza() : "");
            parameters.put("cap", f.getIntestatario().getCap()!=null ? f.getIntestatario().getCap() : "");
            parameters.put("pv", f.getIntestatario().getPv()!=null ? f.getIntestatario().getPv() : "");
            parameters.put("indirizzo", f.getIntestatario().getIndirizzo()!=null ? f.getIntestatario().getIndirizzo() : "");
            parameters.put("nc", f.getIntestatario().getNumCivico()!=null ? f.getIntestatario().getNumCivico() : "");
            parameters.put("codiceFiscale", f.getIntestatario().getCodiceFiscale()!=null ? f.getIntestatario().getCodiceFiscale() : "");
            
            
            parameters.put("numero", f.getEtichettaNumero());
            parameters.put("note", f.getNote());
            parameters.put("data", sdf.format(f.getData()));
            parameters.put("riferimento", f.getRiferimentoOrdine());
            parameters.put("codiceFiscale", intestatario.getCodiceFiscale()!=null && !intestatario.getCodiceFiscale().isEmpty() ? intestatario.getCodiceFiscale() : "");
            parameters.put("partitaIva", intestatario.getPartitaIva()!=null && !intestatario.getPartitaIva().isEmpty() ? "P.IVA "+intestatario.getPartitaIva() : "");
            parameters.put("spedizione", f.getSpeseSpedizione());
            parameters.put("imponibile", f.getImponibile());
            if (f.getIva() == 0) {
                parameters.put("iva", "Esente art.10 com.1 , n.20 , DPR 633/1972");
                parameters.put("codart", "9");
                parameters.put("codiva", "E");
                parameters.put("aliquota", "0,00 %");
                parameters.put("importoIva", 0d);
            } else{
                parameters.put("iva", "IVA " + (f.getIva() == null || f.getIva() == 0 ? "" : f.getIva() + "%"));
                parameters.put("codart", "3");
                parameters.put("codiva", "22");
                parameters.put("aliquota", f.getIva() == null || f.getIva() == 0 ? "" : f.getIva().toString().replace(".", ",") + "%");
                parameters.put("importoIva", f.getImportoIva());
            }
            parameters.put("totale", f.getTotale());
            parameters.put("sconto", (f.getSconto() == null || f.getSconto() == 0 ? f.getSconto() : -1 * f.getSconto()));
            parameters.put("modalita", f.getModalitaPagamento());
            parameters.put("banca", f.getBancaAppoggio());
            parameters.put("scadenza", f.getScadenza());
            parameters.put("pagato", f.getPagato());
//            if(f.getIva()==0){
//                parameters.put("note", "Operazione esente iva ai sensi dell'art.10 D.P.R.633/72 e dell'art.14 c.10 L.537/93");
//            }

            if (f.getSpeseIncasso()!=null && f.getSpeseIncasso()>0)
                parameters.put("incasso", f.getSpeseIncasso());
            
                if (f instanceof FatturaSemplice){
                FatturaSemplice fs=(FatturaSemplice) f;
                if (fs.getDocumentoTrasporto()!=null && "emesso".equals(fs.getDocumentoTrasporto().getStato()))
                parameters.put("docTrasporto", fs.getDocumentoTrasporto().getNumero());
            }            
                
            Integer anno = Integer.parseInt(f.getAnno());
            parameters.put("descart", "Quota associativa anno "+anno+"/"+(anno+1));
           
            List<VoceFattura> voci=new ArrayList<VoceFattura>();                      
            voci.add(null);//va messo in testa uno nullo perche' il primo se lo mangia
            
            for (VoceFattura voce: f.getVoci()){
                voci.add(new VoceFattura(voce));
            }
            
            parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALY);

            JasperPrint jasperPrint = JasperFillManager.fillReport(reportFile.getPath(), parameters, new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource(voci));

            String file=f.getNomePDF();
            File filepdf =new File(savePath);
            
            filepdf.mkdirs();
            
            output = new FileOutputStream(new File (savePath+"/"+file));
            JasperExportManager.exportReportToPdfStream(jasperPrint, output);

        } catch (JRException ex) {
            Logger.getLogger(GestoreFattureBean.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception(ex);
        } finally {
            try {
                if (output!=null)
                output.close();
            } catch (IOException ex) {
                Logger.getLogger(GestoreFattureBean.class.getName()).log(Level.SEVERE, null, ex);
                throw new Exception(ex);
            }
        }
        return output;
    }

    @Override
    public List<Fattura> findFattureDate(Date start, Date stop) {
        return factory.getFatturaDAO().findFattureDate(start, stop);
    }

  

    
}
