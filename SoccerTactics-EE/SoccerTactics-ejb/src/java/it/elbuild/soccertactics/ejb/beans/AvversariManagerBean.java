/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans;

import it.elbuild.soccertactics.ejb.persistence.DAOFactory;
import it.elbuild.soccertactics.lib.entities.Avversario;
import it.elbuild.soccertactics.lib.interfaces.AvversariManager;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Ale
 */
@Stateless
public class AvversariManagerBean implements AvversariManager{
    
    @EJB
    DAOFactory factory;

    @Override
    public List<Avversario> findAvversariStagione(Integer idStagione) {
        return factory.getAvversariDAO().findByIdIdStagione(idStagione);
    }

    @Override
    public Avversario insertUpdateAvversario(Avversario avversario) {
        return factory.getAvversariDAO().insertOrUpdate(avversario);
    }

    @Override
    public Avversario findById(Integer idAvversario) {
        return factory.getAvversariDAO().findById(idAvversario);
    }
}
