/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.persistence;

import el.persistence.db.JpaDAO;
import it.elbuild.soccertactics.lib.entities.Pacchetto;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Ale
 */
public class PacchettoDAO extends JpaDAO<Integer, Pacchetto>{

    public PacchettoDAO(EntityManagerFactory emf) {
        super(emf);
    }
    
    public List<Pacchetto> findByTipo(String tipo){
        List<Pacchetto> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findByTipo");
            q.setParameter("tipo", tipo);
            res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res;
    }
    
}
