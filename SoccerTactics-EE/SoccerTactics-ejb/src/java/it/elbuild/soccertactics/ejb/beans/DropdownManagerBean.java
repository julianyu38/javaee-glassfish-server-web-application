/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans;

import it.elbuild.soccertactics.ejb.persistence.DAOFactory;
import it.elbuild.soccertactics.lib.entities.DropdownSt;
import it.elbuild.soccertactics.lib.interfaces.DropdownManager;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.model.SelectItem;

/**
 *
 * @author Ale
 */
@Stateless
public class DropdownManagerBean implements DropdownManager{
    
    @EJB
    DAOFactory factory;

    @Override
    public List<SelectItem> getListTecniche(String macro) {
        return factory.getDropdownStDAO().getListTecniche(macro);
    }

    @Override
    public List<SelectItem> getListClassificazioni(String macro, String tecnica) {
        return factory.getDropdownStDAO().getListClassificazioni(macro, tecnica);
    }

    @Override
    public List<SelectItem> getListSpecifiche(String macro, String tecnica, String classificazione) {
        return factory.getDropdownStDAO().getListSpecifiche(macro, tecnica, classificazione);
    }

    
    
}
