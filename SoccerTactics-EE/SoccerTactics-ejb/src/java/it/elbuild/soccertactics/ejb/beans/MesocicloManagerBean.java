/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans;

import it.elbuild.soccertactics.ejb.persistence.DAOFactory;
import it.elbuild.soccertactics.lib.entities.Mesociclo;
import it.elbuild.soccertactics.lib.interfaces.MesocicloManager;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Ale
 */
@Stateless
public class MesocicloManagerBean implements MesocicloManager{
    
    @EJB
    DAOFactory factory;

    @Override
    public List<Mesociclo> findAllMesocicli(Integer idStagione, Integer idUtente) {
        return factory.getMesocicloDAO().findByIdStagioneIdUtente(idStagione, idUtente);
    }

    @Override
    public Mesociclo insertUpdateMesociclo(Mesociclo mesociclo) {
        return factory.getMesocicloDAO().insertOrUpdate(mesociclo);
    }

    @Override
    public void insertUpdateSettimanaMesociclo(List<Mesociclo> settimana) {
        factory.getMesocicloDAO().insertOrUpdateAll(settimana);
    }

    @Override
    public List<Mesociclo> findByIdStagioneIdUtenteSettimanaAnno(Integer idStagione, Integer idUtente, Integer settimana, Integer anno) {
        return factory.getMesocicloDAO().findByIdStagioneIdUtenteSettimanaAnno(idStagione, idUtente, settimana, anno);
    }

    @Override
    public Mesociclo findByIdStagioneIdUtenteMese(Integer idStagione, Integer idUtente, Integer mese, Integer anno) {
        return factory.getMesocicloDAO().findByIdStagioneIdUtenteMese(idStagione, idUtente, mese, anno);
    }

    @Override
    public Mesociclo findById(Integer id) {
        return factory.getMesocicloDAO().findById(id);
    }
    
}
