/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans;

import it.elbuild.soccertactics.ejb.persistence.DAOFactory;
import it.elbuild.soccertactics.lib.entities.Allenamento;
import it.elbuild.soccertactics.lib.interfaces.AllenamentiManager;
import it.elbuild.soccertactics.lib.utils.RicercaAllenamento;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Ale
 */
@Stateless
public class AllenamentiManagerBean implements AllenamentiManager{

    @EJB
    DAOFactory factory;
    
    @Override
    public List<Allenamento> findAllenamentiByDate(Date inizio, Date fine, Integer idStagione) {
        return factory.getAllenamentoDAO().findAllenamentiDate(inizio, fine, idStagione);
    }

    @Override
    public Allenamento findById(Integer idAllenamento) {
        return factory.getAllenamentoDAO().findById(idAllenamento);
    }

    @Override
    public Allenamento insertUpdateAllenamento(Allenamento allenamento) {
        return factory.getAllenamentoDAO().insertOrUpdate(allenamento);
    }

    @Override
    public List<Allenamento> findAllenamentiStagione(Integer idStagione) {
        return factory.getAllenamentoDAO().findByIdIdStagione(idStagione);
    }

    @Override
    public List<Allenamento> findByMeseAnno(Integer idStagione, Integer mese, Integer anno) {
        return factory.getAllenamentoDAO().findByMeseAnno(idStagione, mese, anno);
    }

    @Override
    public List<Allenamento> ricercaAvanzataAllenamenti(RicercaAllenamento re) {
        return factory.getAllenamentoDAO().ricercaAvanzataAllenamenti(re);
    }
    
}
