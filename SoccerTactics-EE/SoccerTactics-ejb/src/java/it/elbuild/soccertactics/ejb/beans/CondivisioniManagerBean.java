/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans;

import it.elbuild.soccertactics.ejb.persistence.DAOFactory;
import it.elbuild.soccertactics.lib.entities.Condivisione;
import it.elbuild.soccertactics.lib.interfaces.CondivisioniManager;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Ale
 */
@Stateless
public class CondivisioniManagerBean implements CondivisioniManager{
    
    @EJB
    DAOFactory factory;

    @Override
    public Condivisione findCondivisione(String token) {
        return factory.getCondivisioneDAO().findCondivisione(token);
    }

    @Override
    public Condivisione inserisciAggiornaCondivisione(Condivisione condivisione) {
        return factory.getCondivisioneDAO().insertOrUpdate(condivisione);
    }
    
}
