/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans;

import it.elbuild.soccertactics.ejb.persistence.DAOFactory;
import it.elbuild.soccertactics.lib.entities.Staff;
import it.elbuild.soccertactics.lib.interfaces.StaffManager;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Ale
 */
@Stateless
public class StaffManagerBean implements StaffManager{
    
    @EJB
    DAOFactory factory;

    @Override
    public List<Staff> findStaffStagione(Integer idStagione) {
        return factory.getStaffDAO().findByIdStagione(idStagione);
    }

    @Override
    public Staff insertupdateStaff(Staff staff) {
        return factory.getStaffDAO().insertOrUpdate(staff);
    }

    @Override
    public Staff findStaffById(Integer idStaff) {
        return factory.getStaffDAO().findById(idStaff);
    }
    
}
