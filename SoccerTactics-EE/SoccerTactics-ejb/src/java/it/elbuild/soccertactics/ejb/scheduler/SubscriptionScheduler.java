/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.scheduler;

import it.elbuild.soccertactics.ejb.persistence.DAOFactory;
import it.elbuild.soccertactics.lib.entities.Subscription;
import it.elbuild.soccertactics.lib.entities.Utente;
import it.elbuild.soccertactics.lib.interfaces.NotificheManager;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;

/**
 *
 * @author Ale
 */
@Singleton
public class SubscriptionScheduler {
    
    private final static long MSDAY = 24 * 60 * 60 * 1000l;
    private final static long DELTA_30 = 30 * MSDAY;
    private final static long DELTA_15 = 15 * MSDAY;
    private final static long DELTA_7 = 7 * MSDAY;
    private final static long DELTA_3 = 3 * MSDAY;
    
    @EJB
    DAOFactory factory;
    
    @EJB
    NotificheManager notificheManager;
    
    // invia notifiche abbonamento in scadenza....
    @Schedule(minute = "0", hour = "8")
    public void sendSubscriptionAlert() {
        
        try {            
            // ricavo tutte le subscription attive e non free
            List<Subscription> allSubscriptions = factory.getSubscriptionDAO().findSubscriptionAttive();
            
            if(allSubscriptions!=null && !allSubscriptions.isEmpty()){
                // creo una mappa con per ogni utente la subscription con scadenza più lontana 
                Map<Integer, Subscription> mapUserSubscr = new HashMap<>();
                for (Subscription s : allSubscriptions) {
                    if(mapUserSubscr.containsKey(s.getUserId())){
                        // se scadenza è successiva metto quella
                        if(s.getValidUntil().after(mapUserSubscr.get(s.getUserId()).getValidUntil())){
                            mapUserSubscr.put(s.getUserId(), s);
                        }
                    }else{
                        mapUserSubscr.put(s.getUserId(), s);
                    }
                }
                
                // scorro le subscription e se diff in days è 30 o 3 o 1 mando la mail 
                Subscription sub = null;
                long diffDays = 0;
                Date currTime = Calendar.getInstance(TimeZone.getTimeZone("CET")).getTime();
                for (Map.Entry<Integer, Subscription> entrySet : mapUserSubscr.entrySet()) {
                    sub = entrySet.getValue();
                    diffDays = diffInDays(currTime, sub.getValidUntil());
                    
                    if(diffDays==30 || diffDays==3 || diffDays==1){
                        notificheManager.inviaMailScadenzaAbbonamento(sub);
                    }
                }
                
            }
            
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Exception while checking subscription state sendSubscriptionAlert(). " + "Exception {0} with message {1} catched to avoid bean disruption.", new Object[]{ex.getClass().getName(), ex.getMessage()});
        }
    }
    
    public long diffInDays(Date date1, Date date2){
        Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone("CET"), Locale.ITALIAN);
        cal1.setTime(date1);
        cal1.set(Calendar.HOUR, 0);
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);
        Calendar cal2 = Calendar.getInstance(TimeZone.getTimeZone("CET"), Locale.ITALIAN);
        cal2.setTime(date2);
        cal2.set(Calendar.HOUR, 0);
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.SECOND, 0);
        cal2.set(Calendar.MILLISECOND, 0);
        
        long diff = cal2.getTime().getTime() - cal1.getTime().getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }
}
