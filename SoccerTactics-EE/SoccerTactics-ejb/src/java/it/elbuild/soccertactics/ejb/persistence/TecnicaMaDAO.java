/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.persistence;

import el.persistence.db.JpaDAO;
import it.elbuild.soccertactics.lib.entities.TecnicaMa;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Ale
 */
public class TecnicaMaDAO extends JpaDAO<Integer, TecnicaMa> {

    public TecnicaMaDAO(EntityManagerFactory emf) {
        super(emf);
    }
    
    public List<SelectItem> findCategorie1(){
        List<SelectItem> retmap = new ArrayList<>();
        Query q = em.createNativeQuery("SELECT distinct CATEGORIA_1 FROM TECNICA_MA WHERE CATEGORIA_1 IS NOT NULL");
        List<Object> l;
        try {
            l = q.getResultList();
            for (Object oo : l) {

                retmap.add(new SelectItem(String.valueOf(oo)));
            }
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return retmap;
    }
    
    public List<SelectItem> findCategorie2(String categoria1){
        List<SelectItem> retmap = new ArrayList<>();
        Query q = em.createNativeQuery("SELECT distinct CATEGORIA_2 FROM TECNICA_MA WHERE CATEGORIA_1 = ? AND CATEGORIA_2 IS NOT NULL");
        q.setParameter(1, categoria1);
        List<Object> l;
        try {
            l = q.getResultList();
            for (Object oo : l) {

                retmap.add(new SelectItem(String.valueOf(oo)));
            }
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return retmap;
    }
    
    public List<SelectItem> findCategorie3(String categoria1, String categoria2){
        List<SelectItem> retmap = new ArrayList<>();
        Query q = em.createNativeQuery("SELECT distinct CATEGORIA_3 FROM TECNICA_MA WHERE CATEGORIA_1 = ? AND CATEGORIA_2 = ? AND CATEGORIA_3 IS NOT NULL");
        q.setParameter(1, categoria1);
        q.setParameter(2, categoria2);
        List<Object> l;
        try {
            l = q.getResultList();
            for (Object oo : l) {

                retmap.add(new SelectItem(String.valueOf(oo)));
            }
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return retmap;
    }
    
    public List<SelectItem> findTecniche(String categoria1, String categoria2, String categoria3){
        List<SelectItem> retmap = new ArrayList<SelectItem>();
        Query q = em.createNativeQuery("SELECT ID, TECNICA FROM TECNICA_MA WHERE CATEGORIA_1 = ? AND CATEGORIA_2 = ? AND CATEGORIA_3 = ? AND TECNICA IS NOT NULL");
        q.setParameter(1, categoria1);
        q.setParameter(2, categoria2);
        q.setParameter(3, categoria3);
        List<Object> l;
        try {
            l = q.getResultList();
            for (Object oo : l){
                Object[] oarr = (Object[])oo;
                retmap.add(new SelectItem(Integer.parseInt(String.valueOf(oarr[0])), String.valueOf(oarr[1])));
            }
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return retmap;
    }
    
    public List<TecnicaMa> findByCategorie(String categoria1, String categoria2, String categoria3){
        List<TecnicaMa> res = null;
        String queryString = "SELECT * FROM TECNICA_MA WHERE CATEGORIA_1 = ? AND CATEGORIA_2 = ? ";
        if(categoria3!=null && !categoria3.isEmpty()){
            queryString = queryString + "AND CATEGORIA_3 = ? ";
        }else{
            queryString = queryString + "AND CATEGORIA_3 IS NULL ";
        }
        Query q = em.createNativeQuery(queryString, TecnicaMa.class);
        q.setParameter(1, categoria1);
        q.setParameter(2, categoria2);
        if(categoria3!=null && !categoria3.isEmpty()){
            q.setParameter(3, categoria3);
        }
        try {
            res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
}
