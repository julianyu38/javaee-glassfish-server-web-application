/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans;

import it.elbuild.soccertactics.ejb.persistence.DAOFactory;
import it.elbuild.soccertactics.lib.entities.Avversario;
import it.elbuild.soccertactics.lib.entities.Partita;
import it.elbuild.soccertactics.lib.entities.TecnicaMa;
import it.elbuild.soccertactics.lib.interfaces.PartiteManager;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.model.SelectItem;

/**
 *
 * @author Ale
 */
@Stateless
public class PartiteManagerBean implements PartiteManager{
  
    @EJB
    DAOFactory factory;

    @Override
    public List<Partita> findPartiteByDate(Date inizio, Date fine, Integer idSquadra) {
        return factory.getPartitaDAO().findPartiteDate(inizio, fine, idSquadra);
    }

    @Override
    public Partita findById(Integer idPartita) {
        return factory.getPartitaDAO().findById(idPartita);
    }

    @Override
    public Partita insertUpdatePartita(Partita partita) {
        return factory.getPartitaDAO().insertOrUpdate(partita);
    }

    @Override
    public List<TecnicaMa> findAllTecnicheMa() {
        return factory.getTecnicaMaDAO().findAll();
    }

    @Override
    public List<Partita> findPartiteStagione(Integer idStagione) {
        return factory.getPartitaDAO().findByIdStagione(idStagione);
    }

    @Override
    public List<SelectItem> findCategorie1() {
        return factory.getTecnicaMaDAO().findCategorie1();
    }

    @Override
    public List<SelectItem> findCategorie2(String categoria1) {
        return factory.getTecnicaMaDAO().findCategorie2(categoria1);
    }

    @Override
    public List<SelectItem> findCategorie3(String categoria1, String categoria2) {
        return factory.getTecnicaMaDAO().findCategorie3(categoria1, categoria2);
    }

    @Override
    public List<SelectItem> findTecniche(String categoria1, String categoria2, String categoria3) {
        return factory.getTecnicaMaDAO().findTecniche(categoria1, categoria2, categoria3);
    }

    @Override
    public TecnicaMa findTecnicaMaByID(Integer tecid) {
        return factory.getTecnicaMaDAO().findById(tecid);
    }

    @Override
    public List<TecnicaMa> findByCategorie(String categoria1, String categoria2, String categoria3) {
        return factory.getTecnicaMaDAO().findByCategorie(categoria1, categoria2, categoria3);
    }

    @Override
    public boolean deletePartita(Partita partita) {
        return factory.getPartitaDAO().delete(partita.getId());
    }
    
}
