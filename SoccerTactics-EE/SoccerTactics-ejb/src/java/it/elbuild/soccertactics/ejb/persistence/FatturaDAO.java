/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.persistence;

import el.persistence.db.JpaDAO;
import it.elbuild.soccertactics.lib.entities.fatture.Fattura;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Ale
 */
public class FatturaDAO  extends JpaDAO<Integer, Fattura>{
    
    FatturaDAO(EntityManagerFactory unitName) {
        super (unitName);
    }

    public Integer findLastNumero(Date d1, Date d2) {        
        Integer i=null;
        open();
        try {
            Query q = em.createNamedQuery("Fattura.findLastNumero");
            q.setParameter("start", d1);
             q.setParameter("end", d2);
            i = (Integer) q.getSingleResult();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
            //FIXME serve il try catch? con entityManager.getTransaction().rollback() in caso di eccezione
        } finally {
            close();
        }
        return i;
    }
    public List<Fattura> findLast(int i) {
        List<Fattura> l=null;
        open();
        try {
            Query q = em.createNamedQuery("Fattura.findAllOrderByEditTime");
            q.setMaxResults(i);
            l = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
            //FIXME serve il try catch? con entityManager.getTransaction().rollback() in caso di eccezione
        } finally {
            close();
        }
        return l;
    }

    public Fattura findByNumero(Integer numero) {          
        Fattura i=null;
        open();
        try {
            Query q = em.createNamedQuery("Fattura.findByNumero");
            q.setParameter("numero", numero);
            i = (Fattura) q.getSingleResult();
        } catch (Exception ex) {
          //  Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
            //FIXME serve il try catch? con entityManager.getTransaction().rollback() in caso di eccezione
        } finally {
            close();
        }
        return i;
    }
    
     public Fattura findByNumeroInDates(Integer numero, Date d1, Date d2) {
         Fattura i=null;
        open();
        try {
            Query q = em.createNamedQuery("Fattura.findByNumeroInDates");
            
            q.setParameter("numero", numero);
             q.setParameter("start", d1);
             q.setParameter("end", d2);
            i = (Fattura) q.getSingleResult();
        } catch (Exception ex) {
            //Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
            //FIXME serve il try catch? con entityManager.getTransaction().rollback() in caso di eccezione
        } finally {
            close();
        }
        return i;
    }
    
    public List<Fattura> findFattureDate(Date start, Date stop) {
        List<Fattura> l=null;
        open();
        try {
            Query q = em.createNamedQuery("Fattura.findFattureDate");
            q.setParameter("start", start);
            q.setParameter("stop", stop);
            l = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
            //FIXME serve il try catch? con entityManager.getTransaction().rollback() in caso di eccezione
        } finally {
            close();
        }
        return l;
    } 
     
}