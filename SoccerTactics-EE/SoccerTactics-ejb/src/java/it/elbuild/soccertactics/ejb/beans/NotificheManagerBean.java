/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans;

import it.elbuild.mail.lib.entities.Mail;
import it.elbuild.mail.lib.interfaces.MailGenerator;
import it.elbuild.soccertactics.lib.email.utils.EmailText;
import it.elbuild.soccertactics.lib.entities.Condivisione;
import it.elbuild.soccertactics.lib.entities.Ordine;
import it.elbuild.soccertactics.lib.entities.Subscription;
import it.elbuild.soccertactics.lib.entities.Utente;
import it.elbuild.soccertactics.lib.interfaces.ConfManager;
import it.elbuild.soccertactics.lib.interfaces.NotificheManager;
import it.elbuild.soccertactics.lib.utils.Constants;
import java.net.URL;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Ale
 */
@Stateless
public class NotificheManagerBean implements NotificheManager{
    
    private SimpleDateFormat sdfData = new SimpleDateFormat("dd/MM/yyyy");
    
    @EJB
    MailGenerator mailGenerator;
    @EJB
    ConfManager confMng;

    @PostConstruct
    public void init() {
    }

    private void inviaMessage(Mail m) {
        m.setTime(new Date(System.currentTimeMillis()));
        m.setSent(false);
        m.setIdSito("soccertactics");//FIXME setta singolo progetto
        // m=gestoreMail.inserisciERitornaMail(m);
        mailGenerator.inviaMessage(m);
    }

    @Override
    public void inviaMailCondivisione(Condivisione condivisione, String url) throws Exception {
//    try {
//            String mittente = confMng.findByLabel(Constants.EmailConstants.MAIL_MITTENTE);
//            
//            String soggettoUtente = "Nuova condivisione";
//            //utente
//            
//            String testoCompleto = condivisione.getTesto()!=null ? condivisione.getTesto()+"\n \n" : "";
//            testoCompleto = testoCompleto+url;
//            
//            Mail m = new Mail();
//            m.setMittente(mittente);
//            m.setSoggetto(soggettoUtente);
//            m.setTesto(testoCompleto);
//            m.setPriorita(Mail.LOW_PRIORITY);
//            String [] destinatari = condivisione.getListaMail().split(",");
//            if(destinatari!=null){
//                for (String d : destinatari) {
//                    m.setDestinatario(d.trim());
//                    this.inviaMessage(m);
//                }
//            }
//            
//
//        } catch (Exception ex) {
//            throw new Exception("errore nell'invio della mail");
//        }
    }

    @Override
    public void inviaMailScadenzaAbbonamento(Subscription s) throws Exception {
//        try {
//            URL f = this.getClass().getResource("/email/scadenza-abbonamento.txt");
//            URL fhtml = this.getClass().getResource("/email/scadenza-abbonamento.html");
//            String testoUtente = EmailText.getTestoFromFile(f.getPath());
//            String testoUtenteHtml = EmailText.getTestoFromFile(fhtml.getPath());
//
//            String mittente = confMng.findByLabel(Constants.EmailConstants.MAIL_MITTENTE);
//            String soggettoUtente = "SOFTKEEPER | Abbonamento in scadenza";
//
//            //utente
//            MessageFormat formatter = new MessageFormat("");
//            formatter.applyPattern(testoUtente);
//            Object[] messageArguments = {(s.getUtente().getNome() != null ? s.getUtente().getNome() : "") + " "
//                + s.getUtente().getCognome() != null ? s.getUtente().getCognome() : "",
//                sdfData.format(s.getValidUntil())};
//
//            String testoCompleto = formatter.format(messageArguments);
//            Mail m = new Mail();
//            m.setMittente(mittente);
//            m.setDestinatario(s.getUtente().getEmail());
//            m.setSoggetto(soggettoUtente);
//            m.setTesto(testoCompleto);
//            if (testoUtenteHtml != null) {
//                MessageFormat formatter2 = new MessageFormat("");
//                formatter2.applyPattern(testoUtenteHtml);
//                String testoCompletoUtenteHtml = formatter2.format(messageArguments);
//                m.setHtml(true);
//                m.setTesto(testoCompletoUtenteHtml);
//                m.setTestoPlain(testoUtente);
//            }
//            this.inviaMessage(m);
//
//        } catch (Exception ex) {
//            throw new Exception("errore nell'invio della mail");
//        }
    }

    @Override
    public void inviaEmailPagamentoRicevuto(Ordine ordine, Utente u) throws Exception {
//        try {
//            URL f = this.getClass().getResource("/email/nuovo-abbonamento.txt");
//            URL fhtml = this.getClass().getResource("/email/nuovo-abbonamento.html");
//            String testoUtente = EmailText.getTestoFromFile(f.getPath());
//            String testoUtenteHtml = EmailText.getTestoFromFile(fhtml.getPath());
//
//            String mittente = confMng.findByLabel(Constants.EmailConstants.MAIL_MITTENTE);
//            String soggettoUtente = "SOFTKEEPER | Pagamento completato";
//
//            String pathRicevute = confMng.findByLabel(Constants.Conf.PATH_RICEVUTE);
//            
//            //utente
//            MessageFormat formatter = new MessageFormat("");
//            formatter.applyPattern(testoUtente);
//            Object[] messageArguments = {(u.getNome() != null ? u.getNome() : "")+" "+(u.getCognome() != null ? u.getCognome() : ""),
//                u.getEmail(), "Password: "+u.getPswUtente()};
//
//            String testoCompleto = formatter.format(messageArguments);
//            Mail m = new Mail();
//            m.setMittente(mittente);
//            m.setDestinatario(u.getEmail());
//            m.setSoggetto(soggettoUtente);
//            m.setTesto(testoCompleto);
//            if(ordine.getFattura()!=null){
//                m.setAllegato(pathRicevute+ordine.getFattura().getNomePDF());
//            }
//            if (testoUtenteHtml != null) {
//                MessageFormat formatter2 = new MessageFormat("");
//                formatter2.applyPattern(testoUtenteHtml);
//                String testoCompletoUtenteHtml = formatter2.format(messageArguments);
//                m.setHtml(true);
//                m.setTesto(testoCompletoUtenteHtml);
//                m.setTestoPlain(testoUtente);
//            }
//            this.inviaMessage(m);
//            
//            String emailAdmin = confMng.findByLabel(Constants.EmailConstants.MAIL_ADMIN);                    
//            
//            m = new Mail();
//            m.setMittente(mittente);
//            m.setDestinatario(emailAdmin);
//            m.setSoggetto("Pagamento ricevuto");
//            m.setTesto(u.getNome()+" "+u.getCognome()+" ha eseguito il pagamento di "+ordine.getTotale()+" per "+ordine.getDescrizione());
//            if(ordine.getFattura()!=null){               
//                m.setAllegato(pathRicevute+ordine.getFattura().getNomePDF());
//            }
//            this.inviaMessage(m);
//
//        } catch (Exception ex) {
//            throw new Exception("errore nell'invio della mail");
//        }
        
    }
    
    @Override
    public void mailEsempio() throws Exception {
//        try {
//            String testoUtente = EmailText.getTesto("/email/file.txt");
//            String testoUtenteHtml = EmailText.getTesto("/email/file.html");
//            String testoAdmin = EmailText.getTesto("/email/file-admin.txt");
//
//            String mittente = confMng.findByLabel(Constants.EmailConstants.MAIL_MITTENTE);
//            String emailAdmin = confMng.findByLabel(Constants.EmailConstants.MAIL_ADMIN);
//            String emailDestintario = "";
//
//            String soggettoUtente = "soggetto utente";
//            String soggettoAdmin = "soggetto admin";
//
//            //utente
//            MessageFormat formatter = new MessageFormat("");
//            formatter.applyPattern(testoUtente);
//
//            Object[] messageArguments = {"0", "1"};
//            String testoCompleto = formatter.format(messageArguments);
//            Mail m = new Mail();
//            m.setMittente(mittente);
//            m.setDestinatario(emailDestintario);
//            m.setSoggetto(soggettoUtente);
//            m.setTesto(testoCompleto);
//
//            if (testoUtenteHtml != null) {
//                MessageFormat formatter2 = new MessageFormat("");
//                //     System.out.println(testoUtenteHtml);
//                formatter2.applyPattern(testoUtenteHtml);
//                String testoCompletoUtenteHtml = formatter2.format(messageArguments);
//                m.setHtml(true);
//                m.setTesto(testoCompletoUtenteHtml);
//                m.setTestoPlain(testoUtente);
//            }
//
//            this.inviaMessage(m);
//
//            //admin
//            MessageFormat formatterA = new MessageFormat("");
//            formatterA.applyPattern(testoAdmin);
//            Object[] messageArgumentsAdmin = {"0", "1"};
//            String testoCompletoAdmin = formatterA.format(messageArgumentsAdmin);
//
//            Mail m2 = new Mail();
//            m2.setMittente(mittente);
//            m2.setDestinatario(emailAdmin);
//            m2.setSoggetto(soggettoAdmin);
//            m2.setTesto(testoCompletoAdmin);
//
//            this.inviaMessage(m2);
//
//        } catch (Exception ex) {
//            throw new Exception("errore nell'invio della mail");
//        }
    }
    
}
