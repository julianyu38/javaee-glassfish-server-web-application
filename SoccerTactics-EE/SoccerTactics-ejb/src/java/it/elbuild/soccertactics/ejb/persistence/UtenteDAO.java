/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.persistence;

import el.persistence.db.JpaDAO;
import it.elbuild.soccertactics.lib.entities.Utente;
import it.elbuild.soccertactics.lib.utils.Constants;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Ale
 */
public class UtenteDAO extends JpaDAO<Integer, Utente>{

    public UtenteDAO(EntityManagerFactory emf) {
        super(emf);
    }
    
    public Utente findByEmail(String email){
        List<Utente> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findByEmail");
                q.setParameter("email", email);
                q.setParameter("ruolo", Constants.Ruoli.DELETED);
                res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res!=null && !res.isEmpty() ? res.get(0) : null;

    }
    
    @Override
    public List<Utente> findAll(){
        List<Utente> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findAll");
                q.setParameter("ruolo", Constants.Ruoli.DELETED);
                res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res!=null && !res.isEmpty() ? res : null;
    }
    
    @Override
    public Utente findById(Integer id){
        List<Utente> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findById");
                q.setParameter("id", id);
                q.setParameter("ruolo", Constants.Ruoli.DELETED);
                res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res!=null && !res.isEmpty() ? res.get(0) : null;
    }
    
    public Utente findByCF(String cf){
        List<Utente> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findByCF");
                q.setParameter("cf", cf);
                q.setParameter("ruolo", Constants.Ruoli.DELETED);
                res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res!=null && !res.isEmpty() ? res.get(0) : null;

    }
}
