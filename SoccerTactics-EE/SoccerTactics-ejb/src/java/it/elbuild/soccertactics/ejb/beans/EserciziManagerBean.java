/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans;

import it.elbuild.soccertactics.ejb.persistence.DAOFactory;
import it.elbuild.soccertactics.lib.entities.Esercizio;
import it.elbuild.soccertactics.lib.entities.Oggetti;
import it.elbuild.soccertactics.lib.interfaces.EserciziManager;
import it.elbuild.soccertactics.lib.utils.RicercaEsercizi;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Ale
 */
@Stateless
public class EserciziManagerBean implements EserciziManager{
    
    @EJB
    DAOFactory factory;

    @Override
    public Esercizio findById(Integer id) {
        return factory.getEserciziDAO().findById(id);
    }

    @Override
    public Esercizio insertUpdateEsercizio(Esercizio esercizio) {
        return factory.getEserciziDAO().insertOrUpdate(esercizio);
    }

    @Override
    public List<Oggetti> findAllOggetti() {
        return factory.getOggettiDAO().findAllOggetti();
    }

    @Override
    public List<Esercizio> findEserciziUtente(Integer idutente) {
        return factory.getEserciziDAO().findByIdUtente(idutente);
    }

    @Override
    public List<Esercizio> findEserciziTitoloDescrizione(String text, Integer idUtente, List<Integer> uids) {
        return factory.getEserciziDAO().findEserciziTitoloDesc(idUtente, text, uids);
    }

    @Override
    public List<Esercizio> ricercaAvanzataEsercizi(RicercaEsercizi criteri) {
        return factory.getEserciziDAO().ricercaAvanzataEsercizi(criteri);
    }

    @Override
    public List<Esercizio> findByIdUtenti(List<Integer> uids) {
        return factory.getEserciziDAO().findByIdUtenti(uids);
    }
    
}
