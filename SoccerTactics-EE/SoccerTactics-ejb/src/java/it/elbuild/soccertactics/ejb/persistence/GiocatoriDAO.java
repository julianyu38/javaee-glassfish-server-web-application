/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.persistence;

import el.persistence.db.JpaDAO;
import it.elbuild.soccertactics.lib.entities.Giocatore;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Ale
 */
public class GiocatoriDAO extends JpaDAO<Integer, Giocatore> {

    public GiocatoriDAO(EntityManagerFactory emf) {
        super(emf);
    }
    
    public List<Giocatore> findGiocatoriBySquadra(Integer idSquadra){
        List<Giocatore> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findByIdSquadra");
                q.setParameter("idSquadra", idSquadra);
                res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res;

    }
    
    public List<Giocatore> findByIdStagione(Integer idStagione){
        List<Giocatore> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findByIdStagione");
                q.setParameter("idStagione", idStagione);
                res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res;

    }
    
}
