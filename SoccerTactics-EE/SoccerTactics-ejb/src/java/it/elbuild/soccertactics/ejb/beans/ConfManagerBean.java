/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans;

import it.elbuild.soccertactics.ejb.persistence.DAOFactory;
import it.elbuild.soccertactics.lib.entities.Conf;
import it.elbuild.soccertactics.lib.interfaces.ConfManager;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Ale
 */
@Stateless
public class ConfManagerBean implements ConfManager{
    
    @EJB
    DAOFactory factory;

    @Override
    public String findByLabel(String label) throws Exception {
        Conf res = factory.getConfDAO().findByLabel(label);
        if(res==null){
            throw new Exception("Conf item non trovato");
        }
        return res.getValue();
    }
    
}
