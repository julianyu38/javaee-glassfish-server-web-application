/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import it.elbuild.soccertactics.lib.esercizio.model.ElementoEx;
import it.elbuild.soccertactics.lib.interfaces.JSONFactory;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;

/**
 *
 * @author Ale
 */
@Singleton
public class JSONFactoryBean implements JSONFactory{

    private Gson gson;
    @PostConstruct
    private void init(){
        gson = new Gson();
    }
    @Override
    public String toJSON(List<ElementoEx> ex) {
        if(gson!=null){
            return gson.toJson(ex);
        } else
        {
            return "GSON FAILURE";
        }            
    }

    @Override
    public List<ElementoEx> toList(String json) {
        List<ElementoEx> data = gson.fromJson(json, new TypeToken<ArrayList<ElementoEx>>(){}.getType());
        return data;
    }
    
    @Override
    public String toJSON(Object o) {
        if(gson!=null){
            return gson.toJson(o);
        } else
        {
            return "GSON FAILURE";
        }            
    }
    
}
