/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.persistence;

import el.persistence.db.JpaDAO;
import it.elbuild.soccertactics.lib.entities.DropdownSt;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Ale
 */
public class DropdownStDAO extends JpaDAO<Integer, DropdownSt> {

    public DropdownStDAO(EntityManagerFactory emf) {
        super(emf);
    }
    
    public List<SelectItem> getListTecniche(String macro) {
    
        List<SelectItem> res = new ArrayList<>();
        Query q = em.createNativeQuery("SELECT DISTINCT TECNICA FROM DROPDOWN_ST WHERE MACRO = ? AND TECNICA IS NOT NULL");
        q.setParameter(1, macro);
        
        List<String> l;
        try {
            l = q.getResultList();
            for (String s : l) {
                res.add(new SelectItem(s));
            }
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    public List<SelectItem> getListClassificazioni(String macro, String tecnica) {
        List<SelectItem> res = new ArrayList<>();
        Query q = em.createNativeQuery("SELECT DISTINCT CLASSIFICAZIONE FROM DROPDOWN_ST WHERE MACRO = ? AND TECNICA = ? AND CLASSIFICAZIONE IS NOT NULL");
        q.setParameter(1, macro);
        q.setParameter(2, tecnica);
        
        List<String> l;
        try {
            l = q.getResultList();
            for (String s : l) {
                res.add(new SelectItem(s));
            }
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    public List<SelectItem> getListSpecifiche(String macro, String tecnica, String classificazione) {
        List<SelectItem> res = new ArrayList<>();
        Query q = em.createNativeQuery("SELECT DISTINCT SPECIFICA FROM DROPDOWN_ST WHERE MACRO = ? AND TECNICA = ? AND CLASSIFICAZIONE = ? AND SPECIFICA IS NOT NULL");
        q.setParameter(1, macro);
        q.setParameter(2, tecnica);
        q.setParameter(3, classificazione);
        
        List<String> l;
        try {
            l = q.getResultList();
            for (String s : l) {
                res.add(new SelectItem(s));
            }
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
}
