/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans;

import it.elbuild.soccertactics.ejb.persistence.DAOFactory;
import it.elbuild.soccertactics.lib.entities.Societa;
import it.elbuild.soccertactics.lib.entities.Stagione;
import it.elbuild.soccertactics.lib.interfaces.StagioniManager;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Ale
 */
@Stateless
public class StagioniManagerBean implements StagioniManager{
    
    @EJB
    DAOFactory factory;

    @Override
    public List<Stagione> findAllStagioni() {
        return factory.getStagioneDAO().findAll();
    }

    @Override
    public Stagione insertUpdateStagione(Stagione stagione) {
        return factory.getStagioneDAO().insertOrUpdate(stagione);
    }

    @Override
    public Societa findSocietaById(Integer id) {
        return factory.getSocietaDAO().findById(id);
    }

    @Override
    public Societa insertUpdateSocieta(Societa societa) {
        return factory.getSocietaDAO().insertOrUpdate(societa);
    }

    @Override
    public List<Societa> findAllSocieta() {
        return factory.getSocietaDAO().findAll();
    }
    
}
