/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.persistence;

import el.persistence.db.JpaDAO;
import it.elbuild.soccertactics.lib.entities.Stagione;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Ale
 */
public class StagioneDAO extends JpaDAO<Integer, Stagione>{

    public StagioneDAO(EntityManagerFactory emf) {
        super(emf);
    }
    
}
