/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.persistence;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Ale
 */
@Singleton
public class DAOFactory {
    
    public final String PERSISTENCE_UNIT = "SoccerTactics-PU";
    private EntityManagerFactory emfa;
    
    @PostConstruct
    public void init() {
        emfa = null;
    }

    @PreDestroy
    public void clean() {
        if (emfa != null && emfa.isOpen()) {
            emfa.close();
        }
    }

    private void createEmfa() {
        if (emfa == null) {
            emfa = javax.persistence.Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
        }
    }
    
    public StagioneDAO getStagioneDAO(){
        createEmfa();
        return new StagioneDAO(emfa);
    }
    
    public GiocatoriDAO getGiocatoriDAO(){
        createEmfa();
        return new GiocatoriDAO(emfa);
    }
    
    public ConfDAO getConfDAO(){
        createEmfa();
        return new ConfDAO(emfa);
    }
        
    public SocietaDAO getSocietaDAO(){
        createEmfa();
        return new SocietaDAO(emfa);
    }
    
    public UtenteDAO getUtenteDAO(){
        createEmfa();
        return new UtenteDAO(emfa);
    }
    
    public OggettiDAO getOggettiDAO(){
        createEmfa();
        return new OggettiDAO(emfa);
    }
    
    public EserciziDAO getEserciziDAO(){
        createEmfa();
        return new EserciziDAO(emfa);
    }
    
    public AllenamentoDAO getAllenamentoDAO(){
        createEmfa();
        return new AllenamentoDAO(emfa);
    }
    
    public PartitaDAO getPartitaDAO(){
        createEmfa();
        return new PartitaDAO(emfa);
    }
    
    public AvversariDAO getAvversariDAO(){
        createEmfa();
        return new AvversariDAO(emfa);
    }
    
    public StaffDAO getStaffDAO(){
        createEmfa();
        return new StaffDAO(emfa);
    }
    
    public DropdownStDAO getDropdownStDAO(){
        createEmfa();
        return new DropdownStDAO(emfa);
    }
    
    public TecnicaMaDAO getTecnicaMaDAO(){
        createEmfa();
        return new TecnicaMaDAO(emfa);
    }
    
    public MesocicloDAO getMesocicloDAO(){
        createEmfa();
        return new MesocicloDAO(emfa);
    }
    
    public CondivisioneDAO getCondivisioneDAO(){
        createEmfa();
        return new CondivisioneDAO(emfa);
    }
    
    public PacchettoDAO getPacchettoDAO(){
        createEmfa();
        return new PacchettoDAO(emfa);
    }
    
    public OrdineDAO getOrdineDAO(){
        createEmfa();
        return new OrdineDAO(emfa);
    }
    
    public SubscriptionDAO getSubscriptionDAO(){
        createEmfa();
        return new SubscriptionDAO(emfa);
    }
    
    public FatturaDAO getFatturaDAO(){
        createEmfa();
        return new FatturaDAO(emfa);
    }
}
