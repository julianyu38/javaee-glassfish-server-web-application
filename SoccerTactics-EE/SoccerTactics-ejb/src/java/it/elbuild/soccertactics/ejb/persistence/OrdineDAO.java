/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.persistence;

import el.persistence.db.JpaDAO;
import it.elbuild.soccertactics.lib.entities.Ordine;
import it.elbuild.soccertactics.lib.entities.Pacchetto;
import it.elbuild.soccertactics.lib.utils.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Ale
 */
public class OrdineDAO extends JpaDAO<Integer, Ordine>{

    public OrdineDAO(EntityManagerFactory emf) {
        super(emf);
    }
    
    public List<Integer> findPacchettiEserciziUtente(Integer uid){
        List<Integer> res = new ArrayList<>();
        Query q = em.createNativeQuery("select distinct PACCHETTO_ES from ORDINE"
                + " where ID_UTENTE = ? AND STATO_ORDINE = ? ");
        q.setParameter(1, uid);
        q.setParameter(2, Constants.StatiOrdine.AUTHORIZED);
        List<Object> l;
        try {
            l = q.getResultList();
            if(l!=null && !l.isEmpty()){
                for (Object oo : l) {
                    res.add((Integer)oo);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return res!=null && !res.isEmpty() ? res : null;
    }
    
    public List<Integer> findUidsPacchettiEserciziUtente(Integer uid){
        List<Integer> res = new ArrayList<>();
        Query q = em.createNativeQuery("SELECT DISTINCT PACCHETTO.UID, PACCHETTO.UID2 "
                + "FROM ORDINE JOIN PACCHETTO ON ORDINE.PACCHETTO_ES = PACCHETTO.ID "
                + "WHERE ID_UTENTE = ? AND STATO_ORDINE = ?");
        q.setParameter(1, uid);
        q.setParameter(2, Constants.StatiOrdine.AUTHORIZED);
        List<Object> l;
        try {
            l = q.getResultList();
            Integer value = null;
            Object[] oarr = null;
            if(l!=null && !l.isEmpty()){
                for (Object oo : l) {
                    oarr = (Object[]) oo;
                    if(oarr[0]!=null){
                        value = Integer.parseInt(String.valueOf(oarr[0]));
                        if(!res.contains(value)){
                            res.add(value);
                        }
                    }
                    if(oarr[1]!=null){
                        value = Integer.parseInt(String.valueOf(oarr[1]));
                        if(!res.contains(value)){
                            res.add(value);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return res!=null && !res.isEmpty() ? res : null;
    }
}
