/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans;

import it.elbuild.soccertactics.ejb.persistence.DAOFactory;
import it.elbuild.soccertactics.lib.entities.Giocatore;
import it.elbuild.soccertactics.lib.interfaces.GiocatoriManager;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Ale
 */
@Stateless
public class GiocatoriManagerBean implements GiocatoriManager{
    
    @EJB
    DAOFactory factory;

    @Override
    public List<Giocatore> findAllGiocatori() {
        return factory.getGiocatoriDAO().findAll();
    }

    @Override
    public Giocatore insertUpdateGiocatore(Giocatore giocatore) {
        return factory.getGiocatoriDAO().insertOrUpdate(giocatore);
    }

    @Override
    public Giocatore findGiocatoreById(Integer idGiocatore) {
        return factory.getGiocatoriDAO().findById(idGiocatore);
    }

//    @Override
//    public List<Giocatore> findGiocatoriByIdSquadra(Integer idSquadra) {
//        return factory.getGiocatoriDAO().findGiocatoriBySquadra(idSquadra);
//    }

    @Override
    public List<Giocatore> findGiocatoriByIdStagione(Integer idStagione) {
        return factory.getGiocatoriDAO().findByIdStagione(idStagione);
    }

    @Override
    public Boolean eliminaGiocatore(Giocatore giocatore) {
        return factory.getGiocatoriDAO().delete(giocatore.getId());
    }
    
    
}
