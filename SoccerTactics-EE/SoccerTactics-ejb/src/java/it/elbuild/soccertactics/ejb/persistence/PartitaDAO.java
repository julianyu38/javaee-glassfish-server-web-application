/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.persistence;

import el.persistence.db.JpaDAO;
import it.elbuild.soccertactics.lib.entities.Partita;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Ale
 */
public class PartitaDAO extends JpaDAO<Integer, Partita> {

    public PartitaDAO(EntityManagerFactory emf) {
        super(emf);
    }
    
    public List<Partita> findPartiteDate(Date inizio, Date fine, Integer idStagione){
        List<Partita> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findByDate");
                q.setParameter("inizio", inizio);
                q.setParameter("fine", fine);
                q.setParameter("idStagione", idStagione);
                res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res;

    }
    
    public List<Partita> findByIdStagione(Integer idStagione){
        List<Partita> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findByIdStagione");
                q.setParameter("idStagione", idStagione);
                res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res;

    }
    
}
