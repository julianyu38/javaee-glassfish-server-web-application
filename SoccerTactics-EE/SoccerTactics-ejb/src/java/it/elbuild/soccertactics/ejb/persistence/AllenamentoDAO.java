/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.persistence;

import el.persistence.db.JpaDAO;
import it.elbuild.soccertactics.lib.entities.Allenamento;
import it.elbuild.soccertactics.lib.entities.Allenamento_;
import it.elbuild.soccertactics.lib.utils.RicercaAllenamento;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Ale
 */
public class AllenamentoDAO extends JpaDAO<Integer, Allenamento> {

    public AllenamentoDAO(EntityManagerFactory emf) {
        super(emf);
    }
    
     public List<Allenamento> findAllenamentiDate(Date inizio, Date fine, Integer idStagione){
        List<Allenamento> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findByDate");
                q.setParameter("inizio", inizio);
                q.setParameter("fine", fine);
                q.setParameter("idStagione", idStagione);
                res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res;

    }
     
     public List<Allenamento> findByIdIdStagione(Integer idStagione){
        List<Allenamento> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findByIdStagione");
                q.setParameter("idStagione", idStagione);
                res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res;

    }
     
     public List<Allenamento> findByMeseAnno(Integer idStagione, Integer mese, Integer anno){
        List<Allenamento> res = null;
        open();
        try {
            Query q = em.createNamedQuery(entityClass.getSimpleName() + ".findByMeseAnno");
                q.setParameter("idStagione", idStagione);
                q.setParameter("mese", mese+1);
                q.setParameter("anno", anno);
                res = q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(JpaDAO.class.getName()).log(Level.SEVERE, null, ex);
            em.getTransaction().rollback();
        } finally {
            close();
        }
        return res;

    }
     
     public List<Allenamento> ricercaAvanzataAllenamenti(RicercaAllenamento re){
    
        javax.persistence.criteria.CriteriaBuilder cb =em.getCriteriaBuilder();
        CriteriaQuery<Allenamento> cq = cb.createQuery(Allenamento.class);
        Root<Allenamento> s = cq.from(Allenamento.class);
        
        re.buildPredicates(cb, s);
        
        List<Predicate> predicati = re.getPredicati();
        
        Predicate[] pred = new Predicate[predicati.size()];
        cq.where(predicati.toArray(pred)).distinct(true);
//        if (orderBy != null) {
//            cq.orderBy(asc ? cb.asc(s.get(orderBy)) : cb.desc(s.get(orderBy)));
//        } else {
            cq.orderBy(cb.desc(s.get(Allenamento_.titolo)));
//        }
        TypedQuery<Allenamento> tq = em.createQuery(cq);
//        tq.setFirstResult(start);
//        tq.setMaxResults(stop - start);
        List<Allenamento> results = tq.getResultList();
        close();

        return results;
    }
}
