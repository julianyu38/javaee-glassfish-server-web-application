/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.ejb.beans;

import it.elbuild.soccertactics.ejb.persistence.DAOFactory;
import it.elbuild.soccertactics.lib.entities.Ordine;
import it.elbuild.soccertactics.lib.entities.Pacchetto;
import it.elbuild.soccertactics.lib.entities.Subscription;
import it.elbuild.soccertactics.lib.interfaces.PacchettiManager;
import it.elbuild.soccertactics.lib.utils.Constants;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Ale
 */
@Stateless
public class PacchettiManagerBean implements PacchettiManager{
    
    @EJB
    DAOFactory factory;

    @Override
    public List<Pacchetto> findPacchettiEsercizi() {
        return factory.getPacchettoDAO().findByTipo(Constants.TipoPacchetto.ESERCIZI);
    }

    @Override
    public List<Pacchetto> findPacchettiAbbonamenti() {
        return factory.getPacchettoDAO().findByTipo(Constants.TipoPacchetto.ABBONAMENTO);
    }

    @Override
    public Ordine insertUpdateOrdine(Ordine ordine) {
        return factory.getOrdineDAO().insertOrUpdate(ordine);
    }

    @Override
    public Subscription trovaSubscriptionAttiva(Integer idUtente, Integer idPacchetto) {
        return factory.getSubscriptionDAO().findSubscriptionAttivaUser(idUtente, idPacchetto);
    }

    @Override
    public Subscription insertUpdateSubscription(Subscription subscription) {
        return factory.getSubscriptionDAO().insertOrUpdate(subscription);
    }

    @Override
    public Subscription trovaSubscriptionAttiva(Integer idUtente) {
        return factory.getSubscriptionDAO().findSubscriptionAttivaUser(idUtente);
    }

    @Override
    public List<Ordine> findAllOrdini() {
        return factory.getOrdineDAO().findAll();
    }

    @Override
    public List<Subscription> findAllSubscriptions() {
        return factory.getSubscriptionDAO().findAll();
    }

    @Override
    public List<Subscription> findAllSubscriptionUser(Integer idUtente) {
        return factory.getSubscriptionDAO().findAllSubscriptionUser(idUtente);
    }

    @Override
    public List<Integer> findPacchettiEserciziUtente(Integer uid) {
        return factory.getOrdineDAO().findPacchettiEserciziUtente(uid);
    }

    @Override
    public List<Integer> findUidsPacchettiEserciziUtente(Integer uid) {
        return factory.getOrdineDAO().findUidsPacchettiEserciziUtente(uid);
    }
    
}
