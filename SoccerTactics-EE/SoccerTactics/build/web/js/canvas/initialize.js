/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var canvas, context, canvaso, contexto;
var drawing;

var elementi = [];  
                 
// inizializza il canvas e disegna il muro e gli strati
function initialize(){
    
    
    
    // Find the canvas element.
    canvaso = document.getElementById('imageView');
                    
    // Get the 2D canvas context.
    contexto = canvaso.getContext('2d'); 
          
    if(canvas==null){
    // Add the temporary canvas.
    var container = canvaso.parentNode;
    canvas = document.createElement('canvas');
                
    canvas.id     = 'imageTemp';
    canvas.width  = canvaso.width;
    canvas.height = canvaso.height;
    container.appendChild(canvas);
                                
    context = canvas.getContext('2d');
    }
    // pulisco lo sfondo 
    contexto.clearRect(0, 0, canvaso.width, canvaso.height);
    // disegno lo sfondo scelto e gli oggetti
    caricaSfondo();
//    alert('background '+background);          
         
    
};



// aggiorna il disegno nel canvas
function img_update () {
    //alert('image update')
    contexto.drawImage(canvas, 0, 0);
    context.clearRect(0, 0, canvas.width, canvas.height);
};

            
// ricava le coordinate del punto di click          
function calcolaCoordinateClick(ev){
    if (ev.layerX || ev.layerX == 0) { // Firefox
        ev._x = ev.layerX;
        ev._y = ev.layerY;
    } else if (ev.offsetX || ev.offsetX == 0) { // Opera
        ev._x = ev.offsetX;
        ev._y = ev.offsetY;
    }
    return ev;
};


function drawArrow(context, fromx, fromy, tox, toy){
    var headlen = 10;   // length of head in pixels
    var angle = Math.atan2(toy-fromy,tox-fromx);
//    context.moveTo(fromx, fromy);
    context.lineTo(tox, toy);
    context.lineTo(tox-headlen*Math.cos(angle-Math.PI/6),toy-headlen*Math.sin(angle-Math.PI/6));
    context.moveTo(tox, toy);
    context.lineTo(tox-headlen*Math.cos(angle+Math.PI/6),toy-headlen*Math.sin(angle+Math.PI/6));
}

function drawDashedLine(x1, y1, x2, y2, dashLen) {
    context.beginPath();
    if (dashLen == undefined) dashLen = 2;
    context.moveTo(x1, y1);

    var dX = x2 - x1;
    var dY = y2 - y1;
    var dashes = Math.floor(Math.sqrt(dX * dX + dY * dY) / dashLen);
    var dashX = dX / dashes;
    var dashY = dY / dashes;

    var q = 0;
    while (q++ < dashes) {
        x1 += dashX;
        y1 += dashY;
        context[q % 2 == 0 ? 'moveTo' : 'lineTo'](x1, y1);
    }
    context[q % 2 == 0 ? 'moveTo' : 'lineTo'](x2, y2);
    context.stroke();
};

function disegnaElementi(){
    
    var vertici = null;
    for (var i in elementi){
        vertici = elementi[i].vertici;
//        alert('disegnaElementi '+i+' tipo '+elementi[i].tipo);
        if(elementi[i].tipo == 'linea'){
            context.beginPath();
            context.lineTo(vertici[0].x,vertici[0].y);
            context.lineTo(vertici[1].x,vertici[1].y);                            
            drawArrow(context,vertici[0].x,vertici[0].y,vertici[1].x,vertici[1].y);                            
            context.stroke();
        }else if(elementi[i].tipo == 'linedashed'){
            drawDashedLine(vertici[0].x,vertici[0].y,vertici[1].x,vertici[1].y, 3)
            context.beginPath();                     
            drawArrow(context,vertici[0].x,vertici[0].y,vertici[1].x,vertici[1].y);                            
            context.stroke();
        }else if(elementi[i].tipo == 'curva'){
            // faccio anche due linee bianche per mostrare il terzo punto della curva 
            context.beginPath();
            context.moveTo(vertici[0].x, vertici[0].y);
            context.lineTo(vertici[1].x,vertici[1].y);
            context.lineTo(vertici[2].x, vertici[2].y);
            context.lineWidth= 0.5;
            context.strokeStyle = 'white';
            context.stroke();
            context.lineWidth = 1.8;
            context.strokeStyle = 'black';
            // disegno la freccia curva
            context.beginPath();
            context.moveTo(vertici[0].x, vertici[0].y);
            context.quadraticCurveTo(vertici[1].x,vertici[1].y,vertici[2].x,vertici[2].y);                            
            drawArrow(context,vertici[1].x,vertici[1].y,vertici[2].x,vertici[2].y);                            
            context.stroke();
            
        }else if(elementi[i].tipo == 'imgcustom'){    
             disegnaImmagineCustom(elementi[i].file, vertici[0].x, vertici[0].y, true);
        }else{              
           var image = document.getElementById(elementi[i].tipo);
           context.drawImage(image, vertici[0].x-(image.naturalWidth/2), vertici[0].y-(image.naturalHeight));
           disegnaPuntoAncoraggio(vertici[0].x, vertici[0].y);
        }        
    }
    contexto.drawImage(canvas, 0, 0);
    context.clearRect(0, 0, canvas.width, canvas.height);
}

// verifica se è stato eseguito un click su un elemento
function controllaClick(ev){
    // se non sta disegnando controllo se il punto è vicino a uno dei vertici 
    for (var i in elementi){
        var vertici = elementi[i].vertici;
        for (var j in vertici){
            var p = vertici[j];                            
            var x_min = p.x-delta;
            var x_max = p.x+delta;
            var y_min = p.y-delta;
            var y_max = p.y+delta;
//            alert('selezionato punto '+ev._x+' '+ev._y+'controllo '+elementi[i].tipo+' punto '+p.x+' '+p.y);  
            // se il punto è vicino a uno dei vertici inseriti prendo il punto precedente e quello successivo nel path
            // salvo l'indice del path e del punto e imposto aggiorna=true
            //<![CDATA[
            if((ev._x>x_min && ev._x<x_max) && (ev._y>y_min && ev._y<y_max)){
//                alert('selezionato elemento '+i+' punto '+j);                                
                selectedElementIndex = i;
                selectedPointIndex = j;
                return true;                                
            }
        //]]>
        }
    }
    return false;
};

function ridisegnaElementi(ev){
    // se è necessario un aggiornamento ridisegno gli elementi saltando quello selezionato 
    contexto.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(background, 0, 0);
    console.log('ridisegnaElementi '+elementi);
    for (var i in elementi){  
        vertici = elementi[i].vertici;
        if(i != selectedElementIndex){
            if(elementi[i].tipo == 'linea'){
                context.beginPath();
                context.lineTo(vertici[0].x,vertici[0].y);
                context.lineTo(vertici[1].x,vertici[1].y);                            
                drawArrow(context,vertici[0].x,vertici[0].y,vertici[1].x,vertici[1].y);                            
                context.stroke();
            }else if(elementi[i].tipo == 'linedashed'){
                drawDashedLine(vertici[0].x,vertici[0].y,vertici[1].x,vertici[1].y, 3)
                context.beginPath();                     
                drawArrow(context,vertici[0].x,vertici[0].y,vertici[1].x,vertici[1].y);                            
                context.stroke();
            }else if(elementi[i].tipo == 'curva'){
                context.beginPath();
                context.moveTo(vertici[0].x, vertici[0].y);
                context.quadraticCurveTo(vertici[1].x,vertici[1].y,vertici[2].x,vertici[2].y);                            
                drawArrow(context,vertici[1].x,vertici[1].y,vertici[2].x,vertici[2].y);                            
                context.stroke();
            }else if(elementi[i].tipo == 'imgcustom'){   
                console.log('disegno immagine '+elementi[i].file);
                disegnaImmagineCustom(elementi[i].file, vertici[0].x, vertici[0].y, false);
            }else{              
               var image = document.getElementById(elementi[i].tipo);
               context.drawImage(image, vertici[0].x-(image.naturalWidth/2), vertici[0].y-(image.naturalHeight));
            }      
        }
    }
    img_update();
    drawing = false;
    updating = true;
}

function puntiVicini(x ,y , ev){
  
    var x_min = oldx-delta;
    var x_max = oldx+delta;
    var y_min = oldy-delta;
    var y_max = oldy+delta;
    //<![CDATA[
    if((ev._x>x_min && ev._x<x_max) && (ev._y>y_min && ev._y<y_max)){
        return true;
    }else{
        return false;
    }
    //]]>     
}

function disegnaImmagineCustom(file, x, y, ancoraggio){
    var img = new Image();
    img.src = basePath+file;
    img.onload = function(){ 
        console.log('disegnata immagine '+file);
        context.drawImage(img, x-(img.naturalWidth/2), y-(img.naturalHeight));
        if(ancoraggio){
            disegnaPuntoAncoraggio(x, y);
        }
        img_update()
    };
}

//function disegnaImmagine(image, x, y, r, g, b){
//   
//    if(image.className == 'custom'){
////        var tmpimage = new Image();
////            tmpimage.onload = function () {
//            // mette il colore scelto al posto di(255,255,255)
//            recolorImage(context, image, 255, 255, 255, r, g, b, x, y, true);
////        }
////        tmpimage.src = image.src;
//    }else{
//        context.drawImage(image, x-(image.naturalWidth/2), y-(image.naturalHeight/2));
//        disegnaPuntoAncoraggio(x,y);
//    }
//}

function disegnaPuntoAncoraggio(x,y){
    var image = document.getElementById("crocesel");
    context.drawImage(image, x-(image.naturalWidth/2), y-(image.naturalHeight/2));
//    context.beginPath();
//    context.arc(x, y, 2, 0, Math.PI*2, true); 
//    context.closePath();
//    context.fillStyle="white";
//    context.fill();  
//    context.beginPath();
//    context.lineWidth = 4;
//    context.moveTo(x-12, y);
//    context.lineTo(x+12, y);
//    context.moveTo(x, y-12);
//    context.lineTo(x, y+12);
//    context.strokeStyle="white";
//    context.stroke();
//    context.lineWidth = 1;
//    
//    context.strokeStyle="black";
    context.lineWidth = 1;
    
    context.strokeStyle="black";
}

//function recolorImage(context, img, oldRed, oldGreen, oldBlue, newRed, newGreen, newBlue, x, y, disegnaPunto) {
//                
//    var c = document.createElement('canvas');
//    var ctx = c.getContext("2d");
//    var w = img.width;
//    var h = img.height;                
//    c.width = w;
//    c.height = h;                
//    // draw the image on the temporary canvas
//    ctx.drawImage(img, 0, 0, w, h);                
//    // pull the entire image into an array of pixel data
//    var imageData = ctx.getImageData(0, 0, w, h);                
//    // examine every pixel, 
//    // change any old rgb to the new-rgb
//    //<![CDATA[
//    for (var i = 0; i < imageData.data.length; i += 4) {
//        // is this pixel the old rgb?
//        if (imageData.data[i] == oldRed && imageData.data[i + 1] == oldGreen && imageData.data[i + 2] == oldBlue) {
//            // change to your new rgb
//            imageData.data[i] = newRed;
//            imageData.data[i + 1] = newGreen;
//            imageData.data[i + 2] = newBlue;
//        }
//    }  
//    //]]>     
//      
//    // put the altered data back on the canvas  
//    ctx.putImageData(imageData, 0, 0);
//    var img1 = new Image();
//    img1.onload = function () {
//        context.drawImage(img1,  x-(img1.naturalWidth/2), y-(img1.naturalHeight/2));
//        img_update();  
//        if(disegnaPunto){
//        disegnaPuntoAncoraggio(x,y);
//        }
//    }
//    img1.src = c.toDataURL('image/png');
//    
//}

function drawDashedLineC2(context2, x1, y1, x2, y2, dashLen) {
    context2.beginPath();
    if (dashLen == undefined) dashLen = 2;
    context2.moveTo(x1, y1);

    var dX = x2 - x1;
    var dY = y2 - y1;
    var dashes = Math.floor(Math.sqrt(dX * dX + dY * dY) / dashLen);
    var dashX = dX / dashes;
    var dashY = dY / dashes;

    var q = 0;
    while (q++ < dashes) {
        x1 += dashX;
        y1 += dashY;
        context2[q % 2 == 0 ? 'moveTo' : 'lineTo'](x1, y1);
    }
    context2[q % 2 == 0 ? 'moveTo' : 'lineTo'](x2, y2);
    context2.stroke();
};

var imgToLoad;
var imgLoaded;
var canvas2;

// ridisegna tutto in un canvas nascosto e crea la string in codifica base64 dell'immagine 
// (NB: non si può salvare direttamente dagli altri canvas perchè canvas è provvisorio e in canvaso c'è solo il background)
function loadDataToSave(){
//    alert('creo immagine per salvataggio');
    canvas2 = document.getElementById('imageViewSave');
                    
    var context2 = canvas2.getContext('2d');
    
    context2.clearRect(0, 0, canvas2.width, canvas2.height);  
       
    context2.drawImage(background, 0, 0);
    
    context2.lineWidth = 1.8;
    context2.lineJoin = 'round';
    
    var vertici = null;
    imgToLoad = 0;
    imgLoaded = 0;
    for (var i in elementi){
        vertici = elementi[i].vertici;
        if(elementi[i].tipo == 'linea'){
            context2.beginPath();
            context2.lineTo(vertici[0].x,vertici[0].y);
            context2.lineTo(vertici[1].x,vertici[1].y);                            
            drawArrow(context2,vertici[0].x,vertici[0].y,vertici[1].x,vertici[1].y);                            
            context2.stroke();
        }else if(elementi[i].tipo == 'linedashed'){
            drawDashedLineC2(context2,vertici[0].x,vertici[0].y,vertici[1].x,vertici[1].y, 3)
            context2.beginPath();                     
            drawArrow(context2,vertici[0].x,vertici[0].y,vertici[1].x,vertici[1].y);                            
            context2.stroke();
        }else if(elementi[i].tipo == 'curva'){
            context2.beginPath();
            context2.moveTo(vertici[0].x, vertici[0].y);
            context2.quadraticCurveTo(vertici[1].x,vertici[1].y,vertici[2].x,vertici[2].y);                            
            drawArrow(context2,vertici[1].x,vertici[1].y,vertici[2].x,vertici[2].y);                            
            context2.stroke();
        }else if(elementi[i].tipo == 'imgcustom'){  
            imgToLoad++;
            disegnaImmagineCustomPerSalvataggio(elementi[i].file, vertici[0].x, vertici[0].y, context2);
        }else{              
            var image = document.getElementById(elementi[i].tipo);
            context2.drawImage(image, vertici[0].x-(image.naturalWidth/2), vertici[0].y-(image.naturalHeight));
        }   
            
//        }else{     
//            var image = document.getElementById(elementi[i].tipo);
////            alert('aggiungo immagine '+image.className);
//            if(image.className == 'custom'){
//                imgToLoad++;
////                alert('ciclo '+i+' imgToLoad '+imgToLoad+' imgLoaded '+imgLoaded);
////                var tmpimage = new Image();
////                tmpimage.onload = function () {
//                    // mette il colore scelto al posto di(255,255,255)
////                    alert('inserisco immagine '+elementi[i].r+' '+elementi[i].g+' '+elementi[i].b);
////                    recolorImageSave(context2, tmpimage, 255, 255, 255, elementi[i].r, elementi[i].g, elementi[i].b, elementi[i].vertici[0].x, elementi[i].vertici[0].y);
//                    
////                    alert('caricata immagime ciclo '+i+' imgToLoad '+imgToLoad+' imgLoaded '+imgLoaded);
////                }
////                tmpimage.src = image.src;
//
//                recolorImageSave(context2, image, 255, 255, 255, elementi[i].r, elementi[i].g, elementi[i].b, elementi[i].vertici[0].x, elementi[i].vertici[0].y); 
////                alert();
//            }else{
//                context2.drawImage(image, vertici[0].x-(image.naturalWidth/2), vertici[0].y-(image.naturalHeight/2));
//            }
//        }        
    }
    
//    var pngData = canvas2.toDataURL('image/png');
//    document.getElementById('main-form:base64img').value = pngData;  
//    
    console.log('Salvataggio completato');
//    alert('Salvataggio completato');
    
    check();    
}

function disegnaImmagineCustomPerSalvataggio(file, x, y, context2){
    var img = new Image();
    img.src = basePath+file;
    img.onload = function(){ 
        console.log('disegnata immagine per salvataggio'+file);
        context2.drawImage(img, x-(img.naturalWidth/2), y-(img.naturalHeight));
        imgLoaded++;
    };
}

var check = function(){
//    console.log('check imgToLoad '+imgToLoad+' imgLoaded '+imgLoaded);
    if(imgToLoad==imgLoaded){
//        alert('caricate tutte le immagini');
        var pngData = canvas2.toDataURL('image/png');
//       console.log(pngData);
        document.getElementById('main-form:base64img').value = pngData;  
        salvataggioCompletato = true;
    }else{
//         alert('set timeout')
        setTimeout(check, 1000); // check again in a second
    }
}



function recolorImageSave(context, img, oldRed, oldGreen, oldBlue, newRed, newGreen, newBlue, x, y) {
                
    var c = document.createElement('canvas');
    var ctx = c.getContext("2d");
    var w = img.width;
    var h = img.height;                
    c.width = w;
    c.height = h;                
    // draw the image on the temporary canvas
    ctx.drawImage(img, 0, 0, w, h);                
    // pull the entire image into an array of pixel data
    var imageData = ctx.getImageData(0, 0, w, h);                
    // examine every pixel, 
    // change any old rgb to the new-rgb
    //<![CDATA[
    for (var i = 0; i < imageData.data.length; i += 4) {
        // is this pixel the old rgb?
        if (imageData.data[i] == oldRed && imageData.data[i + 1] == oldGreen && imageData.data[i + 2] == oldBlue) {
            // change to your new rgb
            imageData.data[i] = newRed;
            imageData.data[i + 1] = newGreen;
            imageData.data[i + 2] = newBlue;
        }
    }  
    //]]>     
      
    // put the altered data back on the canvas  
    ctx.putImageData(imageData, 0, 0);
    var img1 = new Image();
    img1.onload = function () {
        context.drawImage(img1,  x-(img1.naturalWidth/2), y-(img1.naturalHeight/2));
        imgLoaded++;
        console.log('caricata immagine custom');
    }
    img1.src = c.toDataURL('image/png');
   
    
}
