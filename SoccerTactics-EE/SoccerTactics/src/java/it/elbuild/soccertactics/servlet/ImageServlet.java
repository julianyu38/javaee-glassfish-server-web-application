/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.servlet;

import it.elbuild.soccertactics.lib.interfaces.ConfManager;
import it.elbuild.soccertactics.lib.utils.Constants;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ale
 */
public class ImageServlet extends HttpServlet {
    
    private static final int DEFAULT_BUFFER_SIZE = 10240; // 10KB.
    
    private static final String SCHEMA = "schema";
    
    @EJB
    ConfManager confManager;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            // parametri 
            // file = nome file
            // type = schema se richiede l'immagine di uno schema 
            
            String type = request.getParameter("type");
            
            String filename = request.getParameter("file");
            
            String filePath = "";
            if(type == null){
                filePath = confManager.findByLabel(Constants.ServletConstants.FILE_PATH);
            }else if(type.equals(SCHEMA)){
                filePath = confManager.findByLabel(Constants.Conf.PATH_SCHEMI);
            }
            
            if (filename == null) {
                response.getWriter().write(Constants.ServletConstants.MISSING_PARAMETER);
                response.getWriter().close();
                return;
            }else{
                response.setContentType(getContentType(filename));
                response.setHeader("Content-Disposition", "inline; filename=\""+filename+"\"");
                // Prepare streams.
                BufferedInputStream input = null;
                BufferedOutputStream output = null;

                try {
                    File file = new File(filePath, filename);
                    // Open streams.
                    input = new BufferedInputStream(new FileInputStream(file), DEFAULT_BUFFER_SIZE);
                    output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);

                    // Write file contents to response.
                    byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
                    int length;
                    while ((length = input.read(buffer)) > 0) {
                        output.write(buffer, 0, length);
                    }
                } finally {
                    // Gently close streams.
                    close(output);
                    close(input);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ImageServlet.class.getName()).log(Level.SEVERE, null, ex);
        }


    }
    
    public String getContentType(String filePath) {
        if (filePath.endsWith("png")) {
            return "image/png";
        } else if (filePath.endsWith("jpeg")) {
            return "image/jpeg";
        } else if (filePath.endsWith("jpg")) {
            return "image/jpg";
        } else if (filePath.endsWith("gif")) {
            return "image/gif";
        }
        return "";
    }
    
    private static void close(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException e) {
                // Do your thing with the exception. Print it, log it or mail it.
                e.printStackTrace();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
