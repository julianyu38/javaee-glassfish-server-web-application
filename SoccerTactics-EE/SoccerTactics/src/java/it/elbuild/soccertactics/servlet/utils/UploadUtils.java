/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.servlet.utils;

import java.io.File;

/**
 *
 * @author Ale
 */
public class UploadUtils {
    
    // genera nome della cartella in cui salvare le foto...
    // in modo da non sovrascrivere se ci sono file con lo stesso nome... 
    public static String createFileName(String file, String path) {

        String fileName = file;
        boolean addSlash = false;

        if (file.endsWith(File.separator)) {
            fileName = file.substring(0, file.length() - 1);
            addSlash = true;
        }

        fileName = fileName.replace(" ", "");

        int i = fileName.lastIndexOf(".");

        String ext = "";
        String name = fileName;

        if (i != -1) {
            ext = fileName.substring(i);
            name = fileName.substring(0, i);
        }
        File f = new File(path + fileName);
        String newName = fileName;
        int c = 1;
        while (f.exists()) {

            newName = name + c + ext;

            f = new File(path + newName);
            c++;
        }

        if (addSlash) {
            newName = newName + File.separator;
        }
        return newName;

    }
    
}
