/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.servlet.utils;

/**
 *
 * @author Ale
 */
public class JSONEventoCalendario {
    
    private Integer id;
    private String title;
    private String url;
    private String qwertyuiop;
    private Long start;
    private Long end;

    public JSONEventoCalendario(Integer id, String title, String url, Long start, Long end, String qwertyuiop) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.start = start;
        this.end = end;
        this.qwertyuiop = qwertyuiop;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public String getQwertyuiop() {
        return qwertyuiop;
    }

    public void setQwertyuiop(String qwertyuiop) {
        this.qwertyuiop = qwertyuiop;
    }
    
}
