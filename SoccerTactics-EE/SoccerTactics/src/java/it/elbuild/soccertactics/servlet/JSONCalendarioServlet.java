/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.servlet;

import com.sun.corba.se.impl.orbutil.closure.Constant;
import it.elbuild.soccertactics.lib.entities.Allenamento;
import it.elbuild.soccertactics.lib.entities.Partita;
import it.elbuild.soccertactics.lib.interfaces.AllenamentiManager;
import it.elbuild.soccertactics.lib.interfaces.JSONFactory;
import it.elbuild.soccertactics.lib.interfaces.PartiteManager;
import it.elbuild.soccertactics.lib.utils.Constants;
import it.elbuild.soccertactics.servlet.utils.JSONEventoCalendario;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ale
 */
public class JSONCalendarioServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    @EJB
    JSONFactory jsonFactory; 
    
    @EJB
    AllenamentiManager allenamentiManager;
    
    @EJB
    PartiteManager partiteManager;
    
    SimpleDateFormat sdfOre = new SimpleDateFormat("HH:mm");
    SimpleDateFormat sdfData = new SimpleDateFormat("dd/MM/yyyy");
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String visualizzazione = request.getParameter(Constants.ServletConstants.VISUALIZZAZIONE);
            String inizio = request.getParameter(Constants.ServletConstants.DATA_INIZIO);
            String fine = request.getParameter(Constants.ServletConstants.DATA_FINE);
            String idStagione = request.getParameter(Constants.ServletConstants.STAGIONE);

            Calendar dataInizio = Calendar.getInstance(TimeZone.getTimeZone("CET"));
            Calendar dataFine = Calendar.getInstance(TimeZone.getTimeZone("CET"));

            if ((inizio == null || inizio.equals(""))) {
                // errore
                return;
            } else {
                dataInizio.setTimeInMillis(Long.parseLong(inizio));
            }
            if ((fine == null || fine.equals(""))) {
                // errore
                return;
            } else {
                dataFine.setTimeInMillis(Long.parseLong(fine));
            }
            if (idStagione == null) {
                // errore
                return;
            }

            
            
            List<JSONEventoCalendario> jsonApp = new ArrayList<JSONEventoCalendario>();            
            String desc = "";
            // carica lista eventi
            List<Allenamento> allenamenti = allenamentiManager.findAllenamentiByDate(dataInizio.getTime(), dataFine.getTime(), Integer.parseInt(idStagione));
            if(allenamenti!=null){
                for(Allenamento a : allenamenti){
                   desc = visualizzazione==null || (visualizzazione!=null && !visualizzazione.equals("giorno")) 
                           ? sdfData.format(a.getDataInizio())+" "+sdfOre.format(a.getDataInizio())+" - "+sdfOre.format(a.getDataFine())+" "+a.getTitolo()
                           : a.getTitolo();                   
                    jsonApp.add(new JSONEventoCalendario(a.getId(), desc, "/SoccerTactics/faces/staff/scheda-allenamento.xhtml?idAllenamento="+a.getId(), a.getDataInizio().getTime(), a.getDataFine().getTime(), Constants.ServletConstants.EVENT_SUCCESS));
                }
            }
            
            List<Partita> partite = partiteManager.findPartiteByDate(dataInizio.getTime(), dataFine.getTime(), Integer.parseInt(idStagione));
            if(partite!=null){
                for(Partita p : partite){
                    desc = visualizzazione==null || (visualizzazione!=null && !visualizzazione.equals("giorno")) 
                           ? sdfData.format(p.getDataInizio())+" "+sdfOre.format(p.getDataInizio())+" Partita contro "+p.getAvversario().getNome()
                           : "Partita contro "+p.getAvversario().getNome();
                    jsonApp.add(new JSONEventoCalendario(p.getId(), desc, "/SoccerTactics/faces/staff/scheda-partita.xhtml?idPartita="+p.getId(), p.getDataInizio().getTime(), p.getDataFine().getTime(), Constants.ServletConstants.EVENT_IMPORTANT));
                }
            }
            
            out.println(jsonFactory.toJSON(jsonApp).replace("qwertyuiop", "class"));
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
