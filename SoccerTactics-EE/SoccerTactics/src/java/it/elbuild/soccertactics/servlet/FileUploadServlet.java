/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.servlet;

import it.elbuild.soccertactics.lib.interfaces.ConfManager;
import it.elbuild.soccertactics.lib.utils.Constants;
import it.elbuild.soccertactics.servlet.utils.UploadUtils;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Ale
 */
@MultipartConfig
public class FileUploadServlet extends HttpServlet {
    
    @EJB
    ConfManager confManager;
    
    private static final String SCHEMA = "schema";

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        
        String type = request.getParameter("type");
        
        Part file = request.getPart("file"); 
        String filename = getFilename(file);  
        int dot = filename.lastIndexOf('.');
        String ext = filename.substring(dot + 1); 
            
        if (!(ext.equalsIgnoreCase(Constants.ServletConstants.JPEG)
                    || ext.equalsIgnoreCase(Constants.ServletConstants.JPG)
                    || ext.equalsIgnoreCase(Constants.ServletConstants.PNG)
                    || ext.equalsIgnoreCase(Constants.ServletConstants.GIF))) {
                response.getWriter().write(Constants.ServletConstants.IMAGE_FORMAT_ERROR);
                response.getWriter().close();
                return;
        }                
        InputStream filecontent = file.getInputStream();
                
        try{
            String path = uploadFile(type, filename, filecontent);
            response.getWriter().write(path);   
            response.getWriter().close();
        }catch(Exception ex){
            response.getWriter().write(Constants.ServletConstants.GENERIC_ERROR);
            response.getWriter().close();
            return;
        }finally{
            filecontent.close();
        }
    }
    
    private static String getFilename(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }
    
    public String uploadFile(String type, String nomefile, InputStream fileContent) throws FileNotFoundException, IOException, Exception {
        OutputStream output = null;
        
        String path = "";
        if(type == null){
            path = confManager.findByLabel(Constants.ServletConstants.FILE_PATH);
        }else if(type.equals(SCHEMA)){
            path = confManager.findByLabel(Constants.Conf.PATH_SCHEMI);
        }
        
        String nomeFile = nomefile.replace("/", "");
        nomeFile = nomeFile.replace("\\", "");
        nomeFile = nomeFile.replace(" ", "");
        nomeFile = nomeFile.replace(":", "");
        nomeFile = nomeFile.replace("'", "");
        String foldername = UploadUtils.createFileName(nomeFile, path);
        
        String filename = path.concat(foldername);
        output = new FileOutputStream(filename);
        output.write(IOUtils.toByteArray(fileContent));
        output.close();
        return foldername;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
