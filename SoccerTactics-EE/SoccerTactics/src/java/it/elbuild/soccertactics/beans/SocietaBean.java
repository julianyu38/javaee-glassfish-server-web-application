/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans;

import it.elbuild.soccertactics.beans.LoginBean;
import it.elbuild.soccertactics.lib.entities.Societa;
import it.elbuild.soccertactics.lib.interfaces.StagioniManager;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "SocietaBean")
@SessionScoped
public class SocietaBean implements Serializable{
    
    @EJB
    StagioniManager stagioniManager;
    
    private Societa societa;
    
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean;
    
    @PostConstruct
    public void caricaSocieta(){
        societa = stagioniManager.findSocietaById(loginBean.getUtente().getIdSocieta());
    }
    
    public void updateSocieta() {
        societa = stagioniManager.insertUpdateSocieta(societa);
        if (societa != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Dati società correttamente modificati"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nella modifica dei dati della società", ""));
        }
    }

    public Societa getSocieta() {
        return societa;
    }

    public void setSocieta(Societa societa) {
        this.societa = societa;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
    
}
