/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.admin;

import elbuild.utils.DigestUtil;
import elbuild.utils.ELbuildValidators;
import elbuild.utils.ValidationResult;
import it.elbuild.soccertactics.beans.validator.EmailFieldValidator;
import it.elbuild.soccertactics.lib.entities.Pacchetto;
import it.elbuild.soccertactics.lib.entities.Societa;
import it.elbuild.soccertactics.lib.entities.Stagione;
import it.elbuild.soccertactics.lib.entities.Subscription;
import it.elbuild.soccertactics.lib.entities.Utente;
import it.elbuild.soccertactics.lib.interfaces.PacchettiManager;
import it.elbuild.soccertactics.lib.interfaces.StagioniManager;
import it.elbuild.soccertactics.lib.interfaces.UtentiManager;
import it.elbuild.soccertactics.lib.utils.Constants;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "UtenteBean")
@ViewScoped
public class UtenteBean implements Serializable{
        
    @EJB
    StagioniManager stagioniManager;
    
    @EJB
    UtentiManager utentiManager;
    
    @EJB
    PacchettiManager pacchettiManager;
    
    private Utente utente;
    private Stagione stagione;
    
    private String userPassword;
    
    private Integer idUtente;
    
    private List<Subscription> subscriptions;
    
    private List<Pacchetto> abbonamenti;   
    private Subscription subscription;
    
    public void initNuovoUtente(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            utente = new Utente();
            stagione = new Stagione();
            userPassword = null;
        }
    }
             
    public void checkPassword(FacesContext context, UIComponent component, Object value) {
        String psw = (String) value;
        if (userPassword != null && !(userPassword.equals(psw))) {
            ((UIInput) component).setValid(false);
            userPassword = null;
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Le due password inserite non coincidono", "");
            context.addMessage(component.getClientId(context), message);
        }
    }
    
    public void checkMail(FacesContext context, UIComponent component, Object value) {
        String email = (String) value;
        try {
            if (new EmailFieldValidator().validateEmail(context, component, value)) {
                Utente u = utentiManager.findByEmail(email);
                if (u != null) {
                    ((UIInput) component).setValid(false);
                    email = "";
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email già presente", "");
                    context.addMessage(component.getClientId(context), message);
                }
            } else {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Formato email non valido", "");
                ((UIInput) component).setValid(false);
                context.addMessage(component.getClientId(context), message);
            }
        } catch (Exception ex) {
            Logger.getLogger(UtenteBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore durante il controllo dell'email.", ""));
        }
    }
    
    public void validateCF(FacesContext context, UIComponent component, Object value) {
        String cf = (String) value;
        try{
            ValidationResult res = ELbuildValidators.ControllaCF(cf);
            if(!res.getValid()){
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, res.getMessage(), "");
                ((UIInput) component).setValid(false);
                context.addMessage(component.getClientId(context), message);
            }else{
                Utente u = utentiManager.findByCF(cf);
                if (u != null) {
                    ((UIInput) component).setValid(false);
                    cf = "";
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Codice fiscale già presente", "");
                    context.addMessage(component.getClientId(context), message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(UtenteBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore durante il controllo del codice fiscale.", ""));
        }
    }
    
    public String inserisciUtente(){
        utente.setPassword(DigestUtil.digestMD5(userPassword));
        utente.setDataInserimento(Calendar.getInstance(TimeZone.getTimeZone("CET")).getTime());
        utente.setRuolo(Constants.Ruoli.STAFF);
        // inserisco l'utente 
        utente = utentiManager.insertUpdateUtente(utente);
        if (utente != null) {
            // inserisco la stagione 
            stagione.setIdUtente(utente.getId());
            stagione = stagioniManager.insertUpdateStagione(stagione);
            if(stagione!=null){
                utente.setIdStagione(stagione.getId());
                utente = utentiManager.insertUpdateUtente(utente);
                if (utente != null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Utente correttamente inserito. "));
                    return "/protected/utenti.xhtml?faces-redirect=true";
                }
            }            
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nell'inserimento dell'utente", ""));
            return "";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nell'inserimento dell'utente", ""));
        return "";
    }    
    
    public void initModificaUtente(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            if(idUtente!=null){
                utente = utentiManager.findById(idUtente);
                subscriptions = pacchettiManager.findAllSubscriptionUser(idUtente);
            }
        }
    }
    
    public void modificaUtente(){
        // inserisco l'utente 
        utente = utentiManager.insertUpdateUtente(utente);
        if (utente != null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Utente correttamente modificato. "));
//                    return "/protected/utenti.xhtml?faces-redirect=true";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nella modifica dell'utente", ""));
//            return "";
        }
    }    
    
    public void modificaPasswordUtente() {
        utente.setPassword(DigestUtil.digestMD5(userPassword));
        utente = utentiManager.insertUpdateUtente(utente);
        if (utente != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Password correttamente modificata. "));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nella modifica della password", ""));
        }
    }
    
    public void mostraInserisciAbbonamento(){
        subscription = new Subscription();
        subscription.setActive(true);
        subscription.setMaxUsers(1);
        subscription.setUtente(utente);
        subscription.setUserId(utente.getId());
        subscription.setValidFrom(Calendar.getInstance(TimeZone.getTimeZone("CET")).getTime());
        abbonamenti = pacchettiManager.findPacchettiAbbonamenti();
        
    }
    
    public void nascondiInserisciAbbonamento(){
        subscription = null;       
    }
    
    public void inserisciAbbonamento(){
        if(subscription.getValidFrom()!=null && subscription.getValidUntil()!=null && subscription.getValidUntil().after(subscription.getValidFrom())){
          subscription.setDataInserimento(Calendar.getInstance(TimeZone.getTimeZone("CET")).getTime());
            if(pacchettiManager.insertUpdateSubscription(subscription)!=null){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Abbonamento correttamente inserito."));
              subscription = null;
              // refresh subscription utente 
              subscriptions = pacchettiManager.findAllSubscriptionUser(idUtente);
          }
          
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRORE: Controllare le date inserite.", ""));
                    
        }
        
    }
    
    public void eliminaAbbonamento(Subscription s) {
        s.setActive(false);
        if (pacchettiManager.insertUpdateSubscription(s) != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Abbonamento correttamente elinimato."));
            // refresh subscription utente 
            subscriptions = pacchettiManager.findAllSubscriptionUser(idUtente);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore", ""));

        }
    }

    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Stagione getStagione() {
        return stagione;
    }

    public void setStagione(Stagione stagione) {
        this.stagione = stagione;
    }

    public Integer getIdUtente() {
        return idUtente;
    }

    public void setIdUtente(Integer idUtente) {
        this.idUtente = idUtente;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public List<Pacchetto> getAbbonamenti() {
        return abbonamenti;
    }

    public void setAbbonamenti(List<Pacchetto> abbonamenti) {
        this.abbonamenti = abbonamenti;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    
}
