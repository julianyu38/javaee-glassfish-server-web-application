/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans;

import elbuild.utils.DigestUtil;
import it.elbuild.soccertactics.beans.validator.EmailFieldValidator;
import it.elbuild.soccertactics.lib.entities.Subscription;
import it.elbuild.soccertactics.lib.entities.Utente;
import it.elbuild.soccertactics.lib.interfaces.PacchettiManager;
import it.elbuild.soccertactics.lib.interfaces.UtentiManager;
import it.elbuild.soccertactics.lib.utils.Constants;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "LoginBean")
@SessionScoped
public class LoginBean implements Serializable{
    
    @EJB
    UtentiManager utentiManager;
    
    @EJB
    PacchettiManager pacchettiManager;
    
    private Utente utente;
    
    private String email;    
    private String password;
    
    private String userPassword;
    
    private Subscription subscription;
    
    public String logout() {
        if (utente != null) {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            if (session != null) {
                session.invalidate();
                utente =null;
            }
        }
        return "/index?faces-redirect=true";

    }
    
    public String login() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            request.login(email, password);
            Principal p = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
            if (p != null) {
                utente = utentiManager.findByEmail(email);
                refreshSubscription();
                if(utente!=null){
                    return utente.getRuolo().equals(Constants.Ruoli.ADMIN) ? "/protected/index.xhtml?faces-redirect=true" : "/staff/index.xhtml?faces-redirect=true";
                }
            }
        } catch (ServletException ex) {
            Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, " L'utente non ha i diritti\n"
                    + "       per accedere a quest'area.\n"
                    + " Controllare che username e password siano corretti.", ""));
        }
        return "/index?faces-redirect=true";
    }

    public Utente getUtente() {
        if(utente == null){
            Principal p = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
            if (p != null) {
                utente = utentiManager.findByEmail(p.getName());
                refreshSubscription();
            }
        }
        return utente;
    }
    
    public void initModificaUtente(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            userPassword = null;
        }
    }
    
    public void checkPassword(FacesContext context, UIComponent component, Object value) {
        String psw = (String) value;
        if(psw!=null && psw.isEmpty()){ psw = null; }
        if(utente.getId()!=null){
            if (userPassword == null && psw == null) {
                return;
            } else if (userPassword == null || psw == null) {
                ((UIInput) component).setValid(false);
                userPassword = null;
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Per modificare la password inserire la nuova password e ripeterla", "");
                context.addMessage(component.getClientId(context), message);
            } else if (!(userPassword.equals(psw))) {
                ((UIInput) component).setValid(false);
                userPassword = null;
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Le due password inserite non coincidono", "");
                context.addMessage(component.getClientId(context), message);
            }
        } else if (userPassword!=null && !(userPassword.equals(psw))) {
            ((UIInput) component).setValid(false);
            userPassword=null;
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Le due password inserite non coincidono","");
            context.addMessage(component.getClientId(context), message);
            }
    }
    
    public void checkNewMail(FacesContext context, UIComponent component, Object value) {
        String email = (String) value;
        try{
            if(new EmailFieldValidator().validateEmail(context, component, value)){
                Utente u = utentiManager.findByEmail(email);
                if (u != null && !u.getId().equals(utente.getId())) {
                    ((UIInput) component).setValid(false);
                    email = "";
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Email già presente","");
                    context.addMessage(component.getClientId(context), message);
                }
            }else{
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Formato email non valido","");
                ((UIInput) component).setValid(false);
                context.addMessage(component.getClientId(context), message);
            }
        } catch (Exception ex) {
            Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Si è verificato un errore durante il controllo dell'email.",""));
        }
    }
    
    public void modificaUtente() throws NoSuchAlgorithmException{
        if(userPassword!=null){
            String md5 = DigestUtil.digestMD5(userPassword);
            utente.setPassword(md5);
        }
        utente = utentiManager.insertUpdateUtente(utente);
        if(utente == null){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Si è verificato un errore nell'aggiornamento dei dati dell'utente",""));
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Dati utente correttamente modificati."));
        }
    }
    
    public void modificaSocieta(){
        utente = utentiManager.insertUpdateUtente(utente);
        if(utente == null){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Si è verificato un errore nell'aggiornamento dei dati della società",""));
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Dati società correttamente modificati."));
        }
    }
    
    public void refreshSubscription(){
        // recupreo la subscription attiva per l'utente
        if(utente!=null && utente.getRuolo().equals(Constants.Ruoli.STAFF)){
            subscription = pacchettiManager.trovaSubscriptionAttiva(utente.getId());
        }
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }
    
}
