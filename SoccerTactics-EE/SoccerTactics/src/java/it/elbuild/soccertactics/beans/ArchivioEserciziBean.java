/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans;

import it.elbuild.soccertactics.lib.entities.Esercizio;
import it.elbuild.soccertactics.lib.entities.Pacchetto;
import it.elbuild.soccertactics.lib.interfaces.EserciziManager;
import it.elbuild.soccertactics.lib.interfaces.PacchettiManager;
import it.elbuild.soccertactics.lib.utils.RicercaEsercizi;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ale
 */
@ManagedBean(name="ArchivioEserciziBean")
@SessionScoped
public class ArchivioEserciziBean implements Serializable{
    
    @EJB
    EserciziManager eserciziManager;
    
    @EJB
    PacchettiManager pacchettiManager;
    
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean;
    
    private List<Esercizio> esercizi;
    
    private RicercaEsercizi criteri;
    
    private Boolean ricercaAvanzata;
    
    private String text;
    
    private List<Integer> uids; // lista id utenti pacchetti acquistati dall'utente de cui recuperare gli esercizi
    
    @ManagedProperty(value = "#{DropdownBean}")
    private DropdownBean dropdownBean;
    
    @PostConstruct
    public void init(){
        ricercaAvanzata = Boolean.FALSE;
        criteri = new RicercaEsercizi();
        text = null;
    }
    
    public void toggleRicercaAvanzata(){
        ricercaAvanzata = !ricercaAvanzata;
        if(!ricercaAvanzata){
            criteri.resetCriteri();
            refreshEsercizi();   
        }
    }
    
    public void updateDropdownDifesa(){
        dropdownBean.caricaDropdowndDifesa(criteri.getObDifI(), criteri.getObDifII());
    }
    
    public void updateDropdownAttacco(){
        dropdownBean.caricaDropdowndAttacco(criteri.getObAttI());
    }
    
    public void caricaEserciziArchivio(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            uids = pacchettiManager.findUidsPacchettiEserciziUtente(loginBean.getUtente().getId());
            refreshEsercizi();          
        }        
    }
    
    private void refreshEsercizi() {
        if (uids != null && !uids.isEmpty()) {
            esercizi = eserciziManager.findByIdUtenti(uids);
        } else {
            esercizi = null;
        }
    }
    
    public void ricercaAvanzataEsercizi(){
        if(uids!=null && !uids.isEmpty()){
            criteri.setUids(uids);
            esercizi = eserciziManager.ricercaAvanzataEsercizi(criteri);
        }
    }
    
    public void resetCampi(){
        text = null;
        criteri.resetCriteri();
//        dropdownBean.ricaricaObiettivi(null, null);
        refreshEsercizi();   
    }
    
    public void ricercaLiberaEsercizi(){
        if(text!=null && !text.equals("") && uids!=null && !uids.isEmpty()){
            esercizi = eserciziManager.findEserciziTitoloDescrizione(text, null, uids);
        }
    }
    
    public String duplicaEsercizio(Esercizio es){
        if(es!=null){
            Esercizio escopy = new Esercizio(es, loginBean.getUtente());
            escopy.setIdUtente(loginBean.getUtente().getId());
            escopy.setUtente(loginBean.getUtente());
            escopy = eserciziManager.insertUpdateEsercizio(escopy);
            if(escopy!=null){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Esercizio correttamente inserito nell'archivio personale. "));
                return "modifica-esercizio.xhtml?faces-redirect=true&idEsercizio="+escopy.getId();
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore", ""));
            }
        }
        return "";
    }
    
    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public List<Esercizio> getEsercizi() {
        return esercizi;
    }

    public void setEsercizi(List<Esercizio> esercizi) {
        this.esercizi = esercizi;
    }

    public Boolean getRicercaAvanzata() {
        return ricercaAvanzata;
    }

    public void setRicercaAvanzata(Boolean ricercaAvanzata) {
        this.ricercaAvanzata = ricercaAvanzata;
    }

    public RicercaEsercizi getCriteri() {
        return criteri;
    }

    public void setCriteri(RicercaEsercizi criteri) {
        this.criteri = criteri;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public DropdownBean getDropdownBean() {
        return dropdownBean;
    }

    public void setDropdownBean(DropdownBean dropdownBean) {
        this.dropdownBean = dropdownBean;
    }
    
    
}
