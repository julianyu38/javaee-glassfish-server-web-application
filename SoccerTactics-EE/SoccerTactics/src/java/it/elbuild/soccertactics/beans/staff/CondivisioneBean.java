/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.staff;

import it.elbuild.soccertactics.lib.entities.Condivisione;
import it.elbuild.soccertactics.lib.entities.Esercizio;
import it.elbuild.soccertactics.lib.interfaces.CondivisioniManager;
import it.elbuild.soccertactics.lib.interfaces.EserciziManager;
import it.elbuild.soccertactics.lib.interfaces.NotificheManager;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "CondivisioneBean")
@ViewScoped
public class CondivisioneBean implements Serializable{
    
    @EJB
    CondivisioniManager condivisioniManager;
    
    @EJB
    NotificheManager notificheManager;
    
    @EJB
    EserciziManager eserciziManager;
    
    private Condivisione condivisione;
    private Esercizio esercizio;
    
    private String token;
    
    @PostConstruct
    public void init(){
        condivisione = null;
    }
    
    public void mostraCondividiEsercizio(Esercizio e){
        if(e!=null){
            esercizio = e;
            condivisione = new Condivisione(e);
        }
    }
    
    public void nascondiCondividiEsercizio(){
        condivisione = null;
    }
    
    public void condividiEsercizio(){
        if(condivisione!=null){
            if(condivisione.getListaMail()!=null && !condivisione.getListaMail().isEmpty()){
                
                if(condivisioniManager.inserisciAggiornaCondivisione(condivisione)!=null){
                    HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
                    String url = request.getRequestURL().toString();
                    url = url.substring(0, url.indexOf("staff"));
                    url = url+ "esercizio-condiviso.xhtml?token=" + condivisione.getToken();         
                    try {
                        notificheManager.inviaMailCondivisione(condivisione, url);
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage( "Condivisione correttamente eseguita"));
                        nascondiCondividiEsercizio();
                    } catch (Exception ex) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nell' invio della mail. ", ""));
                        Logger.getLogger(CondivisioneBean.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else{
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nella condivisione. ", ""));
                }
                
                
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Specificare i destinatari della condivisione. ", ""));
            }
        }
    }
    
    public void caricaEsercizioCondiviso(){
        if(!FacesContext.getCurrentInstance().isPostback() && token!=null && !token.isEmpty()){
            condivisione = condivisioniManager.findCondivisione(token);
            if(condivisione!=null){
                esercizio = eserciziManager.findById(condivisione.getIdEsercizio());
            }
        }
    }

    public Condivisione getCondivisione() {
        return condivisione;
    }

    public void setCondivisione(Condivisione condivisione) {
        this.condivisione = condivisione;
    }

    public Esercizio getEsercizio() {
        return esercizio;
    }

    public void setEsercizio(Esercizio esercizio) {
        this.esercizio = esercizio;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    
    
}
