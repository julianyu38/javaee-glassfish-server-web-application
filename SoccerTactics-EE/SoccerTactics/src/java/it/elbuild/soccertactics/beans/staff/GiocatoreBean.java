/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.staff;

import it.elbuild.soccertactics.beans.LoginBean;
import it.elbuild.soccertactics.lib.entities.Giocatore;
import it.elbuild.soccertactics.lib.interfaces.GiocatoriManager;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "GiocatoreBean")
@ViewScoped
public class GiocatoreBean implements Serializable{

    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean;
    
    @EJB
    GiocatoriManager giocatoriManager;
        
    private Giocatore giocatore;
    private Integer idGiocatore;
    
    public void initGiocatore() {
        if (!FacesContext.getCurrentInstance().isPostback()) {
            giocatore = new Giocatore();
        }
    }

    public String insertGiocatore() {
        if (giocatore.getFoto() != null && !giocatore.getFoto().isEmpty()) {
            giocatore.setIdStagione(loginBean.getUtente().getIdStagione()); 
            giocatore = giocatoriManager.insertUpdateGiocatore(giocatore);
            if (giocatore != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Giocatore correttamente inserito"));
                giocatore = null;
                return "giocatori.xhtml?faces-redirect=true";
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nell'inserimento del giocatore", ""));
                return "";
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Caricare la foto del giocatore"));
            return "";
        }
    }

    public void initUpdateGiocatore() {
        if (!FacesContext.getCurrentInstance().isPostback()) {
            if (idGiocatore != null) {
                giocatore = giocatoriManager.findGiocatoreById(idGiocatore);
            } else {
                giocatore = null;
            }
        }
    }

    public String updateGiocatore() {
        if (giocatore.getFoto() != null && !giocatore.getFoto().isEmpty()) {
            giocatore = giocatoriManager.insertUpdateGiocatore(giocatore);
            if (giocatore != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Giocatore correttamente modificato"));
                giocatore = null;
                return "giocatori.xhtml?faces-redirect=true";
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nella modifica del giocatore", ""));
                return "";
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Caricare la foto del giocatore"));
            return "";
        }
    }
    
    public void initSchedaGiocatore() {
        if (!FacesContext.getCurrentInstance().isPostback()) {
            if (idGiocatore != null) {
                giocatore = giocatoriManager.findGiocatoreById(idGiocatore);
            } else {
                giocatore = null;
            }
        }
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public Giocatore getGiocatore() {
        return giocatore;
    }

    public void setGiocatore(Giocatore giocatore) {
        this.giocatore = giocatore;
    }

    public Integer getIdGiocatore() {
        return idGiocatore;
    }

    public void setIdGiocatore(Integer idGiocatore) {
        this.idGiocatore = idGiocatore;
    }

    
}
