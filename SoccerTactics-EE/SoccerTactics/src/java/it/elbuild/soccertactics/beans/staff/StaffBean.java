/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.staff;

import it.elbuild.soccertactics.beans.LoginBean;
import it.elbuild.soccertactics.beans.validator.EmailFieldValidator;
import it.elbuild.soccertactics.lib.entities.Staff;
import it.elbuild.soccertactics.lib.interfaces.StaffManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "StaffBean")
@ViewScoped
public class StaffBean implements Serializable{
    
    @EJB
    StaffManager staffManager;
    
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean;
    
    private Staff staff;
    
    private Integer idStaff;
    
    private List<SelectItem> mansioni; 
    
    @PostConstruct
    public void init(){
        mansioni = new ArrayList<SelectItem>();
        mansioni.add(new SelectItem("Allenatore"));
        mansioni.add(new SelectItem("II Allenatore"));
        mansioni.add(new SelectItem("Preparatore atletico"));
        mansioni.add(new SelectItem("Preparatore portieri"));
        mansioni.add(new SelectItem("Altro"));
    }
    
    public void initNuovoStaff(){
        if(!FacesContext.getCurrentInstance().isPostback() && loginBean.getUtente().getIdStagione()!=null){
            staff = new Staff();
        }
    }
    
    public void checkEmail(FacesContext context, UIComponent component, Object value) {
        String email = (String) value;
        try{
            if(!new EmailFieldValidator().validateEmail(context, component, value)){
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Formato email non valido","");
                ((UIInput) component).setValid(false);
                context.addMessage(component.getClientId(context), message);
            }
        } catch (Exception ex) {
            Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Si è verificato un errore durante il controllo dell'email.",""));
        }
    }
    
    public String inserisciStaff() {
        staff.setIdStagione(loginBean.getUtente().getIdStagione());
        if (staffManager.insertupdateStaff(staff) != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Staff correttamente inserito"));
            return "staff.xhtml?faces-redirect=true";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nell'inserimento dello staff", ""));
            return "";
        }
    }
    
    public void initModificaStaff(){
        if(!FacesContext.getCurrentInstance().isPostback() && idStaff!=null){
            staff = staffManager.findStaffById(idStaff);
        }
    }
    
    public String modificaStaff() {
        if (staffManager.insertupdateStaff(staff) != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Staff correttamente modficato"));
            return "staff.xhtml?faces-redirect=true";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nella modifica dello staff", ""));
            return "";
        }
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public Integer getIdStaff() {
        return idStaff;
    }

    public void setIdStaff(Integer idStaff) {
        this.idStaff = idStaff;
    }

    public List<SelectItem> getMansioni() {
        return mansioni;
    }

    public void setMansioni(List<SelectItem> mansioni) {
        this.mansioni = mansioni;
    }
    
}
