/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;


@FacesValidator("emailFieldValidator")

public class EmailFieldValidator implements Validator{
 
	private static final String EMAIL_PATTERN = "[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}";
 
	private Pattern pattern;
	private Matcher matcher;
 
	public EmailFieldValidator(){
		  pattern = Pattern.compile(EMAIL_PATTERN);
	}
 
	@Override
	public void validate(FacesContext context, UIComponent component,Object value) throws ValidatorException {
            if(value==null || value.toString().equals("")){
                return;
            }
            	matcher = pattern.matcher(value.toString());
		if(!matcher.matches()){
                    FacesMessage message = new FacesMessage();
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    message.setSummary(component.getId()+": formato  non valido");
                    throw new ValidatorException(message);
		}
 
	}
        
        public boolean validateEmail(FacesContext context, UIComponent component,Object value) throws ValidatorException {
 
		matcher = pattern.matcher(value.toString());
		if(!matcher.matches()){
                    return false;
                }
                return true;
 
	}
}