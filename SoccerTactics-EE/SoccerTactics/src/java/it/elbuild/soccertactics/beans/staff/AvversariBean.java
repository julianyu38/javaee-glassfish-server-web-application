/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.staff;

import it.elbuild.soccertactics.beans.LoginBean;
import it.elbuild.soccertactics.lib.entities.Avversario;
import it.elbuild.soccertactics.lib.interfaces.AvversariManager;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "AvversariBean")
@SessionScoped
public class AvversariBean implements Serializable{
    
    @EJB
    AvversariManager avversariManager;
    
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean;
    
    private List<Avversario> avversari; 
    
    public void caricaAvversari(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            avversari = avversariManager.findAvversariStagione(loginBean.getUtente().getIdStagione());
        }
    }

    public List<Avversario> getAvversari() {
        return avversari;
    }

    public void setAvversari(List<Avversario> avversari) {
        this.avversari = avversari;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
    
}
