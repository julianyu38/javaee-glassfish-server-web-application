/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.staff;

import it.elbuild.soccertactics.beans.LoginBean;
import it.elbuild.soccertactics.lib.entities.Giocatore;
import it.elbuild.soccertactics.lib.interfaces.GiocatoriManager;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "GiocatoriBean")
@SessionScoped
public class GiocatoriBean implements Serializable{
    
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean;
    
    @EJB
    GiocatoriManager giocatoriManager;
    
    private List<Giocatore> giocatori;
    
    public void loadGiocatori(){
        if(!FacesContext.getCurrentInstance().isPostback() && loginBean.getUtente().getIdStagione()!=null){
            giocatori = giocatoriManager.findGiocatoriByIdStagione(loginBean.getUtente().getIdStagione());
        }
    }
    
    public void eliminaGiocatore(Giocatore g){
        if(g!=null){
            if (giocatoriManager.eliminaGiocatore(g) != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Giocatore correttamente eliminato"));
                giocatori = giocatoriManager.findGiocatoriByIdStagione(loginBean.getUtente().getIdStagione());
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nell'eliminazione del giocatore", ""));
            }
        }
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public List<Giocatore> getGiocatori() {
        return giocatori;
    }

    public void setGiocatori(List<Giocatore> giocatori) {
        this.giocatori = giocatori;
    }
    
}
