/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans;

import it.elbuild.soccertactics.lib.entities.DropdownSt;
import it.elbuild.soccertactics.lib.entities.Esercizio;
import it.elbuild.soccertactics.lib.entities.Oggetti;
import it.elbuild.soccertactics.lib.esercizio.model.ElementoEx;
import it.elbuild.soccertactics.lib.interfaces.ConfManager;
import it.elbuild.soccertactics.lib.interfaces.DropdownManager;
import it.elbuild.soccertactics.lib.interfaces.EserciziManager;
import it.elbuild.soccertactics.lib.interfaces.JSONFactory;
import it.elbuild.soccertactics.lib.utils.Constants;
import it.elbuild.soccertactics.servlet.utils.UploadUtils;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.imageio.ImageIO;
import sun.misc.BASE64Decoder;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "EsercizioBean")
@SessionScoped
public class EsercizioBean implements Serializable{
    
    @EJB
    ConfManager confManager;
    
    @EJB
    EserciziManager eserciziManager;
    
    @EJB
    JSONFactory jSONFactory;
    
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean;
    
    private Esercizio esercizio;
    
    private List<Oggetti> oggetti;
    
    private String encodedImage; 
    
    private String updateUrl;
    private String updateUrlSfondo;
    
    private Integer idEsercizio;
    
    // setta il colore iniziale
    private String defaultColor = "blu";
    
    private List<String> listaMateriali;
    
    @ManagedProperty(value = "#{DropdownBean}")
    private DropdownBean dropdownBean;
    
    private List<SelectItem> colori;
    
    @PostConstruct
    public void init(){
        try {
            colori = new ArrayList<>();
            colori.add(new SelectItem("arancione"));
            colori.add(new SelectItem("azzurro"));
            colori.add(new SelectItem("blu"));
            colori.add(new SelectItem("giallo"));
            colori.add(new SelectItem("grigio"));
            colori.add(new SelectItem("porpora"));
            colori.add(new SelectItem("rosso"));
            colori.add(new SelectItem("verde"));
            colori.add(new SelectItem("verdeacqua"));
            colori.add(new SelectItem("viola"));
            
            updateUrl = confManager.findByLabel(Constants.Conf.UPDATE_URL);
            updateUrlSfondo = confManager.findByLabel(Constants.Conf.UPDATE_SFONDO_URL);
            listaMateriali = Constants.MATERIALI_ESERCIZIO;
        } catch (Exception ex) {
            Logger.getLogger(EsercizioBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String apriNuovoEsercizio() {
        oggetti = eserciziManager.findAllOggetti();
        esercizio = new Esercizio();
//        esercizio.addMateriale();
        encodedImage = null;
        dropdownBean.resetDropdown();
        return "nuovo-esercizio.xhtml?faces-redirect=true";
    }
    
    public String getModel(String type, Double x0, Double y0, Double x1, Double y1, Double x2, Double y2, Integer selectedElementIndex, Integer selectedPointIndex, Integer deleteElementIndex, String file){
        if(type!=null && !type.isEmpty()){
                if(type.equals(ElementoEx.LINEA)){
                    if(x0!=null && x1!=null && y0!=null && y1!=null){
                        esercizio.getElementi().add(new ElementoEx(type, x0, y0, x1, y1));
                    }
                }else if(type.equals(ElementoEx.LINEDASHED)){
                    if(x0!=null && x1!=null && y0!=null && y1!=null){
                        esercizio.getElementi().add(new ElementoEx(type, x0, y0, x1, y1));
                    }
                }else if(type.equals(ElementoEx.CURVA)){
                    if(x0!=null && x1!=null && x2!=null && y0!=null && y1!=null && y2!=null){
                        esercizio.getElementi().add(new ElementoEx(type, x0, y0, x1, y1, x2, y2));
                    }
                }else{
                    if(x0!=null && y0!=null){
                        if(type.equals(ElementoEx.IMGCUSTOM)){
                            esercizio.getElementi().add(new ElementoEx(type, x0, y0, file.contains("MISTER") ? file+".png" : file+"_"+defaultColor+".png"));
                        }else{
                            esercizio.getElementi().add(new ElementoEx(type, x0, y0));
                        }
                    }
                }
            }else if(selectedElementIndex!=null && selectedPointIndex!=null){
                esercizio.getElementi().get(selectedElementIndex).getPunti().get(selectedPointIndex).setX(x0);
                esercizio.getElementi().get(selectedElementIndex).getPunti().get(selectedPointIndex).setY(y0);
            }else if(selectedElementIndex!=null && file!=null && !file.isEmpty()){
                esercizio.getElementi().get(selectedElementIndex).setFile(file);
            }else if(deleteElementIndex!=null){
                esercizio.getElementi().remove(esercizio.getElementi().get(deleteElementIndex));
            }
        String json = jSONFactory.toJSON(esercizio.getElementi());
        return json;
    }
    
    public String getSfondo(String sfondo) {
        if (sfondo != null && !sfondo.isEmpty()) {
            esercizio.setSfondo(sfondo);
        }
        return esercizio.getSfondo();
    }
     
    public void updateDropdownDifesa(){
        dropdownBean.caricaDropdowndDifesa(esercizio.getObDifI(), esercizio.getObDifII());
    }
    
    public void updateDropdownDifesa2(){
        dropdownBean.caricaDropdowndDifesaII(esercizio.getDifesa2(), esercizio.getDifesa2II());
    }
    
    public void updateDropdownAttacco(){
        dropdownBean.caricaDropdowndAttacco(esercizio.getObAttI());
    }
      
    public void updateDropdownAttacco2(){
        dropdownBean.caricaDropdowndAttaccoII(esercizio.getAttacco2());
    }
    
    public String inserisciEsercizio(){
        esercizio.setIdUtente(loginBean.getUtente().getId());
//        if ((esercizio.getElementi() != null && !esercizio.getElementi().isEmpty()) || (esercizio.getFileCaricato() != null && !esercizio.getFileCaricato().isEmpty())) {
            if(esercizio.getElementi()!=null && !esercizio.getElementi().isEmpty()){
                // creo il json e lo salvo 
                esercizio.setModello(jSONFactory.toJSON(esercizio.getElementi()));
                // salvo l'immagine del modello
                esercizio.setImmagine(salvaImmagineEsercizio());
            }else if(esercizio.getFileCaricato()==null || esercizio.getFileCaricato().isEmpty()){
                esercizio.setImmagine(salvaImmagineEsercizio());
            }
            // inserisco l'esercizio
            if(eserciziManager.insertUpdateEsercizio(esercizio)!=null){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Esercizio correttamente inserito. "));
                esercizio = null;
                encodedImage = null;
                return "esercizi.xhtml?faces-redirect=true";
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nell'inserimento dell'esercizio", ""));
                return "nuovo-esercizio.xhtml?faces-redirect=true";
            }
//        }else{
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Esercizio non inserito. Per aggiungere l'esercizio definire uno schema o caricare un'immagine dello schema", ""));
//            return "nuovo-esercizio.xhtml?faces-redirect=true";
//        }        
    }
    
    // la chiamo quando viene fatto inserisci esercizio oppure quando viene fatto modidifca esercizio se la stringa json dell'es è cambiata!
    private String salvaImmagineEsercizio(){
        if(encodedImage!=null && encodedImage.length()>0){
            try {
                int i = (new String("data:image/png;base64,")).length();
                String imagebase64 = encodedImage.substring(i);
                imagebase64 = imagebase64.replaceAll(" ", "+");
                BASE64Decoder decoder = new BASE64Decoder();
                byte[] decodedBytes = decoder.decodeBuffer(imagebase64);
                BufferedImage imag=ImageIO.read(new ByteArrayInputStream(decodedBytes));

                String path = confManager.findByLabel(Constants.Conf.PATH_SCHEMI);
                String foldername = UploadUtils.createFileName("esercizio.png", path);

                String filename = path.concat(foldername);

                ImageIO.write(imag, "png", new File(filename));
                
                return foldername;
                
            } catch (Exception ex) {
                Logger.getLogger(EsercizioBean.class.getName()).log(Level.SEVERE, null, ex);  
            }
        }
        return null;
    }
    
    public void caricaEsericizio(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            if(idEsercizio!=null){
                esercizio = eserciziManager.findById(idEsercizio);
                if(esercizio!=null){
                    if(esercizio.getModello()!=null && !esercizio.getModello().isEmpty()){
                        esercizio.setElementi(jSONFactory.toList(esercizio.getModello()));
                    }else{
                        esercizio.setElementi(new ArrayList<ElementoEx>());
                    }
                    oggetti = eserciziManager.findAllOggetti();
                    encodedImage = null;
                    updateDropdownDifesa();
                    updateDropdownDifesa2();
                    updateDropdownAttacco();
                    updateDropdownAttacco2();
                }
                
            }else{
                esercizio = null;
            }
        }
    }
    
    public String modificaEsercizio(){
//        if ((esercizio.getElementi() != null && !esercizio.getElementi().isEmpty()) || (esercizio.getFileCaricato() != null && !esercizio.getFileCaricato().isEmpty())) {
            if (esercizio.getElementi() != null && !esercizio.getElementi().isEmpty()) {
                String json = jSONFactory.toJSON(esercizio.getElementi());
                // se è stato cambiato qualcosa aggiorno il modello e l'immagine
                if (!json.equals(esercizio.getModello())) {
                    // creo il json e lo salvo 
                    esercizio.setModello(json);
                    // salvo l'immagine del modello
                    esercizio.setImmagine(salvaImmagineEsercizio());
                }
            }else if(esercizio.getFileCaricato()==null || esercizio.getFileCaricato().isEmpty()){
                esercizio.setImmagine(salvaImmagineEsercizio());
            }
            // inserisco l'esercizio
            if(eserciziManager.insertUpdateEsercizio(esercizio)!=null){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Esercizio correttamente modificato. "));
                esercizio = null;
                encodedImage = null;
                return "esercizi.xhtml?faces-redirect=true";
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nella modifica dell'esercizio", ""));
                return "modifica-esercizio.xhtml?faces-redirect=true";
            }
//        }else{
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore: definire uno schema o caricare un'immagine dello schema", ""));
//            return "modifica-esercizio.xhtml?faces-redirect=true";
//        }
        
    }
    
    public void cancellaTutto(){
        esercizio.setElementi(new ArrayList<ElementoEx>());
    }
    
    public void caricaVisualizzaEsercizio(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            if(idEsercizio!=null){
                esercizio = eserciziManager.findById(idEsercizio);
                
            }else{
                esercizio = null;
            }
        }
    }
    
    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public Esercizio getEsercizio() {
        return esercizio;
    }

    public void setEsercizio(Esercizio esercizio) {
        this.esercizio = esercizio;
    }

    public List<Oggetti> getOggetti() {
        return oggetti;
    }

    public void setOggetti(List<Oggetti> oggetti) {
        this.oggetti = oggetti;
    }

    public String getEncodedImage() {
        return encodedImage;
    }

    public void setEncodedImage(String encodedImage) {
        this.encodedImage = encodedImage;
    }

    public String getUpdateUrl() {
        return updateUrl;
    }

    public void setUpdateUrl(String updateUrl) {
        this.updateUrl = updateUrl;
    }

    public Integer getIdEsercizio() {
        return idEsercizio;
    }

    public void setIdEsercizio(Integer idEsercizio) {
        this.idEsercizio = idEsercizio;
    }

    public String getDefaultColor() {
        return defaultColor;
    }

    public void setDefaultColor(String defaultColor) {
        this.defaultColor = defaultColor;
    }

    public List<String> getListaMateriali() {
        return listaMateriali;
    }

    public void setListaMateriali(List<String> listaMateriali) {
        this.listaMateriali = listaMateriali;
    }

    public String getUpdateUrlSfondo() {
        return updateUrlSfondo;
    }

    public void setUpdateUrlSfondo(String updateUrlSfondo) {
        this.updateUrlSfondo = updateUrlSfondo;
    }

    public DropdownBean getDropdownBean() {
        return dropdownBean;
    }

    public void setDropdownBean(DropdownBean dropdownBean) {
        this.dropdownBean = dropdownBean;
    }

    public List<SelectItem> getColori() {
        return colori;
    }

    public void setColori(List<SelectItem> colori) {
        this.colori = colori;
    }

}
