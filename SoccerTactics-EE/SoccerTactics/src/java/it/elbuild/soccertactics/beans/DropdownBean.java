/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans;

import it.elbuild.soccertactics.lib.entities.DropdownSt;
import it.elbuild.soccertactics.lib.interfaces.DropdownManager;
import it.elbuild.soccertactics.lib.utils.Constants;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author Ale
 */
@ManagedBean(name="DropdownBean")
@SessionScoped
public class DropdownBean implements Serializable{
    
    @EJB
    DropdownManager dropdownManager;
    
    private List<SelectItem> tecDif;
    private List<SelectItem> classDif;
    private List<SelectItem> specDif;  
    private List<SelectItem> classDifII;
    private List<SelectItem> specDifII;
    
    private List<SelectItem> tecAtt;
    private List<SelectItem> classAtt;
    private List<SelectItem> classAttII;
    
    private List<SelectItem> tecTat;
    
    private List<SelectItem> tecCoord;       
    private List<SelectItem> tecCond;
        
    @PostConstruct
    public void init(){
        tecDif = dropdownManager.getListTecniche(Constants.ObiettiviEs.TECNICA_DIFESA);
        tecAtt = dropdownManager.getListTecniche(Constants.ObiettiviEs.TECNICA_ATTACCO);
        tecTat = dropdownManager.getListTecniche(Constants.ObiettiviEs.OBIETTIVO_TATTICO);      
        tecCoord = dropdownManager.getListTecniche(Constants.ObiettiviEs.OBIETTIVO_COORDINATIVO);
        tecCond = dropdownManager.getListTecniche(Constants.ObiettiviEs.OBIETTIVO_CONDIZIONALE);
    }
    
    public void caricaDropdowndDifesa(String tecnica, String classificazione){
        if(tecnica!=null && !tecnica.isEmpty()){
            classDif = dropdownManager.getListClassificazioni(Constants.ObiettiviEs.TECNICA_DIFESA, tecnica);
        }else{
            classDif = null;
            specDif = null;
        }
        if(classificazione!=null && !classificazione.isEmpty()){
            specDif = dropdownManager.getListSpecifiche(Constants.ObiettiviEs.TECNICA_DIFESA, tecnica, classificazione);
        }else{
            specDif = null;
        }
    }
    
    public void caricaDropdowndDifesaII(String tecnica, String classificazione){
        if(tecnica!=null && !tecnica.isEmpty()){
            classDifII = dropdownManager.getListClassificazioni(Constants.ObiettiviEs.TECNICA_DIFESA, tecnica);
        }else{
            classDifII = null;
            specDifII = null;
        }
        if(classificazione!=null && !classificazione.isEmpty()){
            specDifII = dropdownManager.getListSpecifiche(Constants.ObiettiviEs.TECNICA_DIFESA, tecnica, classificazione);
        }else{
            specDifII = null;
        }
    }
    
    public void caricaDropdowndAttacco(String tecnica){
        if(tecnica!=null && !tecnica.isEmpty()){
            classAtt = dropdownManager.getListClassificazioni(Constants.ObiettiviEs.TECNICA_ATTACCO, tecnica);
        }else{
            classAtt = null;
        }
    }
    
    public void caricaDropdowndAttaccoII(String tecnica){
        if(tecnica!=null && !tecnica.isEmpty()){
            classAttII = dropdownManager.getListClassificazioni(Constants.ObiettiviEs.TECNICA_ATTACCO, tecnica);
        }else{
            classAttII = null;
        }
    }
    
    public void resetDropdown(){
        classDif = null;
        specDif = null;
        classDifII = null;
        specDifII = null;
        classAtt = null;
        classAttII = null;
    }
    
    public DropdownManager getDropdownManager() {
        return dropdownManager;
    }

    public void setDropdownManager(DropdownManager dropdownManager) {
        this.dropdownManager = dropdownManager;
    }

    public List<SelectItem> getTecDif() {
        return tecDif;
    }

    public void setTecDif(List<SelectItem> tecDif) {
        this.tecDif = tecDif;
    }

    public List<SelectItem> getClassDif() {
        return classDif;
    }

    public void setClassDif(List<SelectItem> classDif) {
        this.classDif = classDif;
    }

    public List<SelectItem> getSpecDif() {
        return specDif;
    }

    public void setSpecDif(List<SelectItem> specDif) {
        this.specDif = specDif;
    }

    public List<SelectItem> getClassDifII() {
        return classDifII;
    }

    public void setClassDifII(List<SelectItem> classDifII) {
        this.classDifII = classDifII;
    }

    public List<SelectItem> getSpecDifII() {
        return specDifII;
    }

    public void setSpecDifII(List<SelectItem> specDifII) {
        this.specDifII = specDifII;
    }

    public List<SelectItem> getTecAtt() {
        return tecAtt;
    }

    public void setTecAtt(List<SelectItem> tecAtt) {
        this.tecAtt = tecAtt;
    }

    public List<SelectItem> getClassAtt() {
        return classAtt;
    }

    public void setClassAtt(List<SelectItem> classAtt) {
        this.classAtt = classAtt;
    }

    public List<SelectItem> getClassAttII() {
        return classAttII;
    }

    public void setClassAttII(List<SelectItem> classAttII) {
        this.classAttII = classAttII;
    }

    public List<SelectItem> getTecTat() {
        return tecTat;
    }

    public void setTecTat(List<SelectItem> tecTat) {
        this.tecTat = tecTat;
    }

    public List<SelectItem> getTecCoord() {
        return tecCoord;
    }

    public void setTecCoord(List<SelectItem> tecCoord) {
        this.tecCoord = tecCoord;
    }

    public List<SelectItem> getTecCond() {
        return tecCond;
    }

    public void setTecCond(List<SelectItem> tecCond) {
        this.tecCond = tecCond;
    }
    
}
