/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.staff;

import it.elbuild.soccertactics.beans.LoginBean;
import it.elbuild.soccertactics.lib.entities.Partita;
import it.elbuild.soccertactics.lib.interfaces.AllenamentiManager;
import it.elbuild.soccertactics.lib.interfaces.PartiteManager;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "PartiteBean")
@SessionScoped
public class PartiteBean implements Serializable{
    
    @EJB
    PartiteManager partiteManager;
    
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean;
    
    private List<Partita> partite;
    
    public void caricaPartite(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            partite = partiteManager.findPartiteStagione(loginBean.getUtente().getIdStagione());
        }
    }
    
    public void eliminaPartita(Partita a){
        if(a!=null){
            if(partiteManager.deletePartita(a)){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Partita correttamente eliminata. "));
                partite = partiteManager.findPartiteStagione(loginBean.getUtente().getIdStagione());
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore", ""));
            }
        }
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public List<Partita> getPartite() {
        return partite;
    }

    public void setPartite(List<Partita> partite) {
        this.partite = partite;
    }

}
