/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.admin;

import it.elbuild.soccertactics.lib.entities.Societa;
import it.elbuild.soccertactics.lib.entities.Utente;
import it.elbuild.soccertactics.lib.interfaces.StagioniManager;
import it.elbuild.soccertactics.lib.interfaces.UtentiManager;
import it.elbuild.soccertactics.lib.utils.Constants;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "UtentiBean")
@SessionScoped
public class UtentiBean implements Serializable{
    
    @EJB
    UtentiManager utentiManager;
        
    List<Utente> utenti;
    
    public void caricaUtenti(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            utenti = utentiManager.findAllUtenti();
        }
    }
    
    public void bloccaSbloccaUtente(Utente u){
        if(u.getRuolo().equals(Constants.Ruoli.BLOCKED)){
            u.setRuolo(Constants.Ruoli.STAFF);
            if (utentiManager.insertUpdateUtente(u) != null) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("L'utente "+u.getNome()+" "+u.getCognome()+" è stato sbloccato. "));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nello sbloccare l'utente", ""));
            }
        }else{
            u.setRuolo(Constants.Ruoli.BLOCKED);
            if (utentiManager.insertUpdateUtente(u) != null) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("L'utente "+u.getNome()+" "+u.getCognome()+" è stato bloccato. "));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nel bloccare l'utente", ""));
            }
        }
    } 
    
    public void eliminaUtente(Utente u) {
        u.setRuolo(Constants.Ruoli.DELETED);
        if (utentiManager.insertUpdateUtente(u) != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("L'utente " + u.getNome() + " " + u.getCognome() + " è stato eliminato. "));
            utenti = utentiManager.findAllUtenti();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nell'eliminazione dell'utente", ""));
        }
    }

    public List<Utente> getUtenti() {
        return utenti;
    }

    public void setUtenti(List<Utente> utenti) {
        this.utenti = utenti;
    }
    
}
