/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.ecommerce;

import elbuild.jsf.utils.FacesUtils;
import it.elbuild.soccertactics.lib.entities.Ordine;
import it.elbuild.soccertactics.lib.entities.Utente;
import it.elbuild.soccertactics.lib.entities.fatture.Fattura;
import it.elbuild.soccertactics.lib.entities.fatture.FatturaSemplice;
import it.elbuild.soccertactics.lib.interfaces.ConfManager;
import it.elbuild.soccertactics.lib.interfaces.fatture.GestoreFatture;
import it.elbuild.soccertactics.lib.utils.Constants;
import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ale
 */
@ManagedBean(name="FatturazioneBean")
@ApplicationScoped
public class FatturazioneBean {
    
    @EJB
    private GestoreFatture gestoreFatture;
    
    @EJB
    ConfManager confManager;
            
    public void downloadPDF(Fattura fattura) {
        try {
            String f = null;
            if (fattura != null) {
                f = fattura.getNomePDF();
            }
            
            String path = confManager.findByLabel(Constants.Conf.PATH_RICEVUTE);

            if (f != null && path != null) {
                final FacesContext facesContext = FacesContext.getCurrentInstance();
                HttpServletResponse r = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
                FacesUtils.writeContentToResponse(r, new File(path, f), f, "application/pdf");
                facesContext.responseComplete();
            }
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Errore nel recupero della fattura"));
            Logger.getLogger(FatturazioneBean.class.getName()).log(Level.SEVERE, null, ex);
        }      
    }
    
    private boolean faiPdf(Fattura fattura) throws Exception {
        
        String templatePath = ((ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext()).getRealPath("/fattura/fattura-semplice.jasper");
//            
        String savePath = confManager.findByLabel(Constants.Conf.PATH_RICEVUTE);

        if (savePath == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Errore di configurazione. Parametro mancante: path.fatture"));
            return false;
        }
        
        gestoreFatture.generaPDF(fattura, templatePath, savePath);

        return true;

    }
    
    public Fattura creaFattura(double iva, Ordine order, Utente u) {
        try {
            Fattura f = new FatturaSemplice(iva, order, u);
            if (gestoreFatture.trovaUltimoNumeroFattura(f.getData()) != null) {
                f.setNumero(gestoreFatture.trovaUltimoNumeroFattura(f.getData()) + 1);
            } else {
                f.setNumero(1);
            }
            f= gestoreFatture.emettiFattura(f);
            f.calcolaTotale();   
            boolean r = faiPdf(f);
            if (!r) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Errore nella generazione della fattura"));
                return null;
            } else {
                return f;
            }
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Errore nella generazione della ricevuta"));
            Logger.getLogger(FatturazioneBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
    public void rigeneraFattura(Fattura f) {
        if (f != null) {
            try {
                f.calcolaTotale();
                boolean r = faiPdf(f);
                gestoreFatture.inserisciOAggiornaFattura(f);

                if (!r) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Errore nella generazione della fattura"));

                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("PDF rigenerato correttamente"));
                }
            } catch (Exception ex) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Errore nella generazione della fattura"));
                Logger.getLogger(FatturazioneBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
        
}
