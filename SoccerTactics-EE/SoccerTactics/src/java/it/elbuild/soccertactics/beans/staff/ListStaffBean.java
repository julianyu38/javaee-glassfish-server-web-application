/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.staff;

import it.elbuild.soccertactics.beans.LoginBean;
import it.elbuild.soccertactics.lib.entities.Staff;
import it.elbuild.soccertactics.lib.interfaces.StaffManager;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "ListStaffBean")
@SessionScoped
public class ListStaffBean implements Serializable{
    
    @EJB
    StaffManager staffManager;
    
    private List<Staff> staff; 
    
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean;
    
    public void caricaListaStaff(){
        if(!FacesContext.getCurrentInstance().isPostback() && loginBean.getUtente().getIdStagione()!=null){
            staff = staffManager.findStaffStagione(loginBean.getUtente().getIdStagione());
        }
    }
    
    public String staffMailTo() {
        if (staff != null && !staff.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (Staff s : staff) {
                if (s.getEmail() != null && !s.getEmail().isEmpty()) {
                    sb.append((sb.length() > 0) ? ";" : "");
                    sb.append(s.getEmail().trim());
                }
            }
            return sb.toString();
        }
        return null;
    }

    public List<Staff> getStaff() {
        return staff;
    }

    public void setStaff(List<Staff> staff) {
        this.staff = staff;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
    
}
