/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.staff;

import it.elbuild.soccertactics.beans.LoginBean;
import it.elbuild.soccertactics.lib.entities.Avversario;
import it.elbuild.soccertactics.lib.interfaces.AvversariManager;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "AvversarioBean")
@ViewScoped
public class AvversarioBean implements Serializable{
    
    @EJB
    AvversariManager avversariManager;
    
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean;
    
    private Avversario avversario;
    
    private Integer idAvversario;
    
    public void initNuovoAvversario(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            avversario = new Avversario();
        }
    }
        
    public String inserisciAvversario() {
        avversario.setIdStagione(loginBean.getUtente().getIdStagione());
        if (avversariManager.insertUpdateAvversario(avversario) != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Avversario correttamente inserito"));
            return "avversari.xhtml?faces-redirect=true";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nell'inserimento dell'avversario", ""));
            return "";
        }
    }
    
    public void initModificaAvversario(){
        if(!FacesContext.getCurrentInstance().isPostback() && idAvversario!=null){
            avversario = avversariManager.findById(idAvversario);
        }
    }
    
    public String modificaAvversario() {
        if (avversariManager.insertUpdateAvversario(avversario) != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Avversario correttamente modficato"));
            return "avversari.xhtml?faces-redirect=true";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nella modifica dell'avversario", ""));
            return "";
        }
    }

    public Avversario getAvversario() {
        return avversario;
    }

    public void setAvversario(Avversario avversario) {
        this.avversario = avversario;
    }

    public Integer getIdAvversario() {
        return idAvversario;
    }

    public void setIdAvversario(Integer idAvversario) {
        this.idAvversario = idAvversario;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
    
}
