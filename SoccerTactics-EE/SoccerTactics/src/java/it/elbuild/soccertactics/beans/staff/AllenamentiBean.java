/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.staff;

import it.elbuild.soccertactics.beans.LoginBean;
import it.elbuild.soccertactics.lib.entities.Allenamento;
import it.elbuild.soccertactics.lib.interfaces.AllenamentiManager;
import it.elbuild.soccertactics.lib.utils.RicercaAllenamento;
import it.elbuild.soccertactics.lib.utils.RicercaEsercizi;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "AllenamentiBean")
@SessionScoped
public class AllenamentiBean implements Serializable{
    
    @EJB
    AllenamentiManager allenamentiManager;
    
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean;
    
    private List<Allenamento> allenamenti;
    
    private RicercaAllenamento criteri;
    
    private Boolean ricercaAvanzata;
    
    @PostConstruct
    public void init(){
        ricercaAvanzata = Boolean.TRUE;
        criteri = new RicercaAllenamento();
    }
    
    public void toggleRicercaAvanzata(){
        ricercaAvanzata = !ricercaAvanzata;
        if(!ricercaAvanzata){
            criteri.resetCriteri();
            allenamenti = allenamentiManager.findAllenamentiStagione(loginBean.getUtente().getIdStagione());
        }
    }
    
    public void caricaAllenamenti(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            allenamenti = allenamentiManager.findAllenamentiStagione(loginBean.getUtente().getIdStagione());
        }
    }
    
    public void ricercaAvanzataAllenamenti(){
        criteri.setIdStagione(loginBean.getUtente().getIdStagione());
        allenamenti = allenamentiManager.ricercaAvanzataAllenamenti(criteri);
    }
    
    public void resetCampi(){
        criteri.resetCriteri();
        allenamenti = allenamentiManager.findAllenamentiStagione(loginBean.getUtente().getIdStagione());
    }
    
    public void eliminaAllenamento(Allenamento a){
        if(a!=null){
            a.setEliminato(true);
            if(allenamentiManager.insertUpdateAllenamento(a)!=null){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Allenamento correttamente eliminato. "));
                allenamenti = allenamentiManager.findAllenamentiStagione(loginBean.getUtente().getIdStagione());
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore", ""));
            }
        }
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public List<Allenamento> getAllenamenti() {
        return allenamenti;
    }

    public void setAllenamenti(List<Allenamento> allenamenti) {
        this.allenamenti = allenamenti;
    }

    public RicercaAllenamento getCriteri() {
        return criteri;
    }

    public void setCriteri(RicercaAllenamento criteri) {
        this.criteri = criteri;
    }

    public Boolean getRicercaAvanzata() {
        return ricercaAvanzata;
    }

    public void setRicercaAvanzata(Boolean ricercaAvanzata) {
        this.ricercaAvanzata = ricercaAvanzata;
    }
    
}
