/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.ecommerce;

import elbuild.utils.DigestUtil;
import it.elbuild.soccertactics.beans.LoginBean;
import it.elbuild.soccertactics.lib.entities.Ordine;
import it.elbuild.soccertactics.lib.entities.Pacchetto;
import it.elbuild.soccertactics.lib.entities.Stagione;
import it.elbuild.soccertactics.lib.entities.Subscription;
import it.elbuild.soccertactics.lib.entities.Utente;
import it.elbuild.soccertactics.lib.interfaces.ConfManager;
import it.elbuild.soccertactics.lib.interfaces.NotificheManager;
import it.elbuild.soccertactics.lib.interfaces.PacchettiManager;
import it.elbuild.soccertactics.lib.interfaces.StagioniManager;
import it.elbuild.soccertactics.lib.interfaces.UtentiManager;
import it.elbuild.soccertactics.lib.utils.Constants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentReq;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentRequestType;
import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutReq;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutRequestType;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import urn.ebay.apis.eBLBaseComponents.DoExpressCheckoutPaymentRequestDetailsType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsItemType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;
import urn.ebay.apis.eBLBaseComponents.SetExpressCheckoutRequestDetailsType;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "AcquistoPacchettiBean")
@SessionScoped
public class AcquistoPacchettiBean implements Serializable{
    
    @EJB
    PacchettiManager pacchettiManager;
    
    @EJB
    UtentiManager utentiManager;
    
    @EJB
    StagioniManager stagioniManager;
    
    @EJB
    ConfManager confManager;
    
    @EJB
    NotificheManager notificheManager;
    
    @ManagedProperty(value = "#{FatturazioneBean}")
    private FatturazioneBean fatturazioneBean;
    
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean;
    
    private List<Pacchetto> packAbb;
    private List<Pacchetto> packEs;
    
    // per inserimento utente se non ce n'è uno già loggato 
    private Utente utente;
    private Stagione stagione;
    private String userPassword;
    
    private Pacchetto packAbbSel;
    private Pacchetto packEsSel;
    
    private Double totale;
    
    private Ordine ordine;
    private String message;
    private String token;
    private String payerID;
    
        
    private PayPalAPIInterfaceServiceService service;
        
    private List<Integer> uidsPackEsUtente; // uid pacchetti esercizi già acquistati dall'utente
    
    private Boolean trattamentoDati;
    private Boolean terminiCondizioni;
    
    public void initAcquistaPacchetti(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            if(loginBean.getUtente()!=null){
                utente = loginBean.getUtente();
                uidsPackEsUtente = pacchettiManager.findUidsPacchettiEserciziUtente(loginBean.getUtente().getId());
            }else{
                utente = new Utente();
                stagione = new Stagione();
                uidsPackEsUtente = null;
            }
            packAbbSel = null;
            packEsSel = null;
            totale = 0d;
            trattamentoDati = false;
            terminiCondizioni = false;
            
            packAbb = pacchettiManager.findPacchettiAbbonamenti();
            packEs = pacchettiManager.findPacchettiEsercizi();
        }
    }
    
    public void selezionaAbbonamento(Pacchetto p){
        packAbbSel = p;
        refreshTotale();
    }
    
    public Boolean packEsAcquistabile(Pacchetto p){
        if(uidsPackEsUtente!=null && !uidsPackEsUtente.isEmpty()){
            if(p.getUid()!= null && p.getUid2()!= null){
                return !(uidsPackEsUtente.contains(p.getUid()) && uidsPackEsUtente.contains(p.getUid2()));
            }else if(p.getUid()!= null){
                return !(uidsPackEsUtente.contains(p.getUid()));
            }else if(p.getUid2()!= null ){
                return !(uidsPackEsUtente.contains(p.getUid2()));
            }
        }       
        return true;        
    }
    
    public void selezionaPackEsercizi(Pacchetto p){
        packEsSel = p;
        refreshTotale();
    }
    
    public void deselezionaPackEsercizi(Pacchetto p){
        packEsSel = null;
        refreshTotale();
    }
    
    public void refreshTotale(){
        totale = 0d;
        if(packAbbSel!=null){
            totale = totale + packAbbSel.getPrezzo();
        }
        if(packEsSel!=null){
            totale = totale + packEsSel.getPrezzo();
        }
    }
    
    public void checkPassword(FacesContext context, UIComponent component, Object value) {
        String psw = (String) value;
        if (userPassword != null && !(userPassword.equals(psw))) {
            ((UIInput) component).setValid(false);
            userPassword = null;
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Le due password inserite non coincidono", "");
            context.addMessage(component.getClientId(context), message);
        }
    }
    
    public void inserisciUtente() {
        utente.setPassword(DigestUtil.digestMD5(userPassword));
        utente.setDataInserimento(Calendar.getInstance(TimeZone.getTimeZone("CET")).getTime());
        utente.setRuolo(Constants.Ruoli.STAFF);
        utente.setPswUtente(userPassword);
        // inserisco l'utente 
        utente = utentiManager.insertUpdateUtente(utente);
        if (utente != null) {
            // inserisco la stagione 
            // di default inizio sempre oggi 
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("CET"));
            stagione.setNome("Stagione "+cal.get(Calendar.YEAR));
            stagione.setInizio(cal.getTime());
            cal.set(Calendar.MONTH, Calendar.JUNE);
            cal.set(Calendar.DAY_OF_MONTH, 30);
            if(cal.getTime().after(stagione.getInizio())){
                stagione.setFine(cal.getTime());
            }else{
                cal.add(Calendar.YEAR, 1);
                stagione.setFine(cal.getTime());
            }
            stagione.setIdUtente(utente.getId());
            stagione = stagioniManager.insertUpdateStagione(stagione);
            if (stagione != null) {
                utente.setIdStagione(stagione.getId());
                utente = utentiManager.insertUpdateUtente(utente);
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nell'inserimento dell'utente", ""));
        }
    }
    
    public void indirizzaAPagamento() {       
        if(terminiCondizioni==null || !terminiCondizioni){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRORE: Per continuare confermare di aver letto e di accettare i termini e le condizioni.", ""));
            return;
        }
        
        boolean nuovoUtente = utente.getId()==null;
        if(nuovoUtente && (trattamentoDati==null || !trattamentoDati)){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRORE: Per continuare è necessario autorizzare il trattamento dei dati personali.", ""));
            return;
        }
        // se è un nuovo utente controllo che sia stato selezionato un abbonamento
        if(nuovoUtente){
            if(packAbbSel==null){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Selezionare l'abbonamento da attivare", ""));
                return;
            }
        }        
        if(nuovoUtente){
            
            inserisciUtente();
            if(utente==null){
                return;
            }
        }        
        try {            
            StringBuilder sb = new StringBuilder();
            if(packAbbSel!=null){
                sb.append("- Abbonamento annuale '"+packAbbSel.getNome()+"'\n");
            }
            if (packEsSel!=null ) {
                sb.append("- Pacchetto esercizi '"+packEsSel.getNome()+"'\n");
            }            
            ordine = new Ordine(Constants.StatiOrdine.CREATED, sb.toString(), totale, utente.getId(), packAbbSel, packEsSel);
            ordine.setUtente(utente);
            ordine = pacchettiManager.insertUpdateOrdine(ordine);            
            if (ordine == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore. Si prega di riprovare.", ""));
                return;
            }
            
            String environment = confManager.findByLabel(Constants.Conf.PAYPAL_ENVIRONMENT);
            Map<String, String> configurationMap = new HashMap<String, String>();
            configurationMap.put("mode", environment);
            configurationMap.put("acct1.UserName", confManager.findByLabel(Constants.Conf.PAYPAL_USERNAME));
            configurationMap.put("acct1.Password", confManager.findByLabel(Constants.Conf.PAYPAL_PASSWORD));
            configurationMap.put("acct1.Signature", confManager.findByLabel(Constants.Conf.PAYPAL_SIGNATURE));
            service = new PayPalAPIInterfaceServiceService(configurationMap);
            
            SetExpressCheckoutRequestType setExpressCheckoutReq = new SetExpressCheckoutRequestType();
            SetExpressCheckoutRequestDetailsType details = new SetExpressCheckoutRequestDetailsType();
            
            details.setReturnURL(confManager.findByLabel(Constants.Conf.PAYPAL_RETURNURL));
            details.setCancelURL(confManager.findByLabel(Constants.Conf.PAYPAL_CANCELURL));
                        
            List<PaymentDetailsItemType> lineItems = new ArrayList<PaymentDetailsItemType>();

            PaymentDetailsItemType item = new PaymentDetailsItemType();
            BasicAmountType amt = new BasicAmountType();
            amt.setCurrencyID(CurrencyCodeType.EUR);
            amt.setValue(Double.toString(totale));
            item.setQuantity(new Integer("1"));
            item.setName(sb.toString());
            item.setAmount(amt);            
//            item.setItemCategory(ItemCategoryType.PHYSICAL);
            item.setDescription(sb.toString());
            lineItems.add(item);
            
            List<PaymentDetailsType> payDetails = new ArrayList<PaymentDetailsType>();
            PaymentDetailsType paydtl = new PaymentDetailsType();
            paydtl.setPaymentAction(urn.ebay.apis.eBLBaseComponents.PaymentActionCodeType.SALE);          
            
            paydtl.setOrderDescription(sb.toString());
            
            BasicAmountType itemsTotal = new BasicAmountType();
            itemsTotal.setValue(Double.toString(totale));
            itemsTotal.setCurrencyID(CurrencyCodeType.EUR);
            paydtl.setOrderTotal(new BasicAmountType(CurrencyCodeType.EUR, Double.toString(totale)));
            paydtl.setPaymentDetailsItem(lineItems);
            paydtl.setItemTotal(itemsTotal);
            payDetails.add(paydtl);
            details.setPaymentDetails(payDetails);
            details.setReqConfirmShipping("0");
            details.setNoShipping("noShipping");            
            setExpressCheckoutReq.setSetExpressCheckoutRequestDetails(details);
            SetExpressCheckoutReq expressCheckoutReq = new SetExpressCheckoutReq();
            expressCheckoutReq.setSetExpressCheckoutRequest(setExpressCheckoutReq);

            urn.ebay.api.PayPalAPI.SetExpressCheckoutResponseType setExpressCheckoutResponse = service.setExpressCheckout(expressCheckoutReq);            
            if (setExpressCheckoutResponse != null) {
                System.out.println("setExpressCheckoutResponse ACK "+setExpressCheckoutResponse.getAck());
                System.out.println("Pagamento: TOTALE ("+totale+")");
                if(setExpressCheckoutResponse.getErrors()!=null && !setExpressCheckoutResponse.getErrors().isEmpty()){
                    System.out.println("setExpressCheckoutResponse Error "+setExpressCheckoutResponse.getErrors().get(0).getLongMessage());
                }
                if (setExpressCheckoutResponse.getAck().toString().contains("SUCCESS")) {                    
                    HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
                    String redirectUrl = environment.equals("sandbox") ? "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token="+setExpressCheckoutResponse.getToken() : "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token="+setExpressCheckoutResponse.getToken();
                    response.sendRedirect(redirectUrl);
                    FacesContext.getCurrentInstance().responseComplete();
                }else{
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore. Si prega di riprovare.", ""));
                    return;
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(AcquistoPacchettiBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nel reindirizzamento a paypal", ""));
        }
    }
    
    
    

    /**
     * 
     * Eseguito al ritorno da paypal. Aggiorna lo stato dell'ordine e attiva la subscription
     *
     * @param 
     * @return  
     */
    public void confermaPagamento() {        
//        pagamentoOk = false;
            if (token != null && !token.equals("") && payerID != null && !payerID.equals("") && ordine!=null && ordine.getStatoOrdine().equals(Constants.StatiOrdine.CREATED)) {
                try {
                    DoExpressCheckoutPaymentRequestType doCheckoutPaymentRequestType = new DoExpressCheckoutPaymentRequestType();
                    DoExpressCheckoutPaymentRequestDetailsType details = new DoExpressCheckoutPaymentRequestDetailsType();
                    details.setToken(token);
                    details.setPayerID(payerID);
                    details.setPaymentAction(urn.ebay.apis.eBLBaseComponents.PaymentActionCodeType.SALE);

                    PaymentDetailsType paymentDetails = new PaymentDetailsType();
                    BasicAmountType orderTotal = new BasicAmountType();
                    orderTotal.setValue(Double.toString(totale));
                    //PayPal uses 3-character ISO-4217 codes for specifying currencies in fields and variables.
                    orderTotal.setCurrencyID(CurrencyCodeType.EUR);
                    paymentDetails.setOrderTotal(orderTotal);
                    
                    BasicAmountType itemTotal = new BasicAmountType();
                    itemTotal.setValue(Double.toString(totale));
                    itemTotal.setCurrencyID(CurrencyCodeType.EUR);
                    paymentDetails.setItemTotal(itemTotal);

                    List<PaymentDetailsItemType> paymentItems = new ArrayList<PaymentDetailsItemType>();
                    PaymentDetailsItemType paymentItem = new PaymentDetailsItemType();
                    paymentItem.setName(ordine.getDescrizione());
                    paymentItem.setQuantity(1);
                    
                    BasicAmountType amount = new BasicAmountType();
                    amount.setValue(Double.toString(totale));
                    amount.setCurrencyID(CurrencyCodeType.EUR);
                    paymentItem.setAmount(amount);
                    paymentItems.add(paymentItem);
                    paymentDetails.setPaymentDetailsItem(paymentItems);

                    List<PaymentDetailsType> payDetailType = new ArrayList<PaymentDetailsType>();
                    payDetailType.add(paymentDetails);
                    details.setPaymentDetails(payDetailType);
                    doCheckoutPaymentRequestType.setDoExpressCheckoutPaymentRequestDetails(details);
                    DoExpressCheckoutPaymentReq doExpressCheckoutPaymentReq = new DoExpressCheckoutPaymentReq();
                    doExpressCheckoutPaymentReq.setDoExpressCheckoutPaymentRequest(doCheckoutPaymentRequestType);

                    urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentResponseType doCheckoutPaymentResponseType = service.doExpressCheckoutPayment(doExpressCheckoutPaymentReq);

                    if (doCheckoutPaymentResponseType != null) {
                        if (doCheckoutPaymentResponseType.getAck().toString().contains("SUCCESS")) {
                            //		  aggiorno ordine in stato AUTHORIZED
                            // creo la ricevuta
                            ordine.setFattura(fatturazioneBean.creaFattura(0, ordine, ordine.getUtente()));
                            aggiornaOrdine(null, Constants.StatiOrdine.AUTHORIZED, doCheckoutPaymentResponseType.getAck().toString()); 

                            message = "Pagamento correttamente eseguito";
                            // iserisco subscription 
                            Subscription s = inserisciSubscription();
                            
                            caricaPacchettiEsercizi();
                            
                            // invio mail notifica utente e amministratore
                            if(s!=null){
                                notificheManager.inviaEmailPagamentoRicevuto(ordine, utente);
                            }
                            token = null;
                            payerID = null;
                            
                            
                        } else {
                            String error = "";
                            for (urn.ebay.apis.eBLBaseComponents.ErrorType et : doCheckoutPaymentResponseType.getErrors()) {
                                error = error + et.getLongMessage();
                            }
                            // errore nell'esecuzione di doExpressCheckout aggiorno ordine in stato NOT_AUTHORIZED 
                            aggiornaOrdine(null, Constants.StatiOrdine.NOT_AUTHORIZED, error);
                            //notifica errore
                            message = "Si è verificato un errore nel processare il tuo pagamento";
                        }
                    }
                } catch (Exception ex) {
                    try {
                        aggiornaOrdine(null, Constants.StatiOrdine.NOT_AUTHORIZED, "");
                    } catch (Exception ex1) {
                        Logger.getLogger(AcquistoPacchettiBean.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    Logger.getLogger(AcquistoPacchettiBean.class.getName()).log(Level.SEVERE, null, ex);
                    message = "Si è verificato un errore nel processare il tuo pagamento";
                }

            }

    }
    
    /**
     * 
     * Annulla il pagamento e aggiorna lo stato dell'ordine 
     *
     * @param 
     * @return  
     */
    public void annullaPagamento(){
        if (token != null && !token.equals("")){
            try {
                aggiornaOrdine(null, Constants.StatiOrdine.CANCELLED, "annullato dall'utente");
                message = "Come richiesto il pagamento è stato annullato";
            } catch (Exception ex) {
                Logger.getLogger(AcquistoPacchettiBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
 
    //aggiorna stato ordine
    public void aggiornaOrdine(String token, String statoOrdine, String statoPaypal) throws Exception {
        if (token != null) ordine.setIdOrdine(token);

        ordine.setStatoOrdine(statoOrdine);
        ordine.setStatoPaypal(statoPaypal);
        ordine = pacchettiManager.insertUpdateOrdine(ordine);
    }
    
    public Subscription inserisciSubscription() {
        // se nell'ordine c'è un abbonamento inserisco la subscription
        Pacchetto abbonamento = ordine.getPackAbb();
        if (abbonamento != null) {
            try {
                Date startDate = dataInizioSubscription(abbonamento);
                Subscription s = new Subscription(utente.getId(), abbonamento, 1, startDate);
                s = pacchettiManager.insertUpdateSubscription(s);
                if (s == null) {
                    message = message + "<br/> Si è verificato un errore nell'inserimento dell'abbonamento <br/> Si prega di contattare l'assistenza.";
                    return null;
                } else {
                    message = message + "<br/> L'abbonamento è stato correttamente attivato"
                            + "<br/> Esegui l'accesso con le credenziali scelte per cominciare ad utilizzare Softkeeper!";
                    
                    // se l'utente era già loggato aggiorno i dati sull'abbonamento caricato
                    if (loginBean.getUtente() != null) {
                        loginBean.refreshSubscription();
                    }
                    
                    return s;
                }
            } catch (Exception ex) {
                Logger.getLogger(AcquistoPacchettiBean.class.getName()).log(Level.SEVERE, null, ex);
                message = message + "<br/> Si è verificato un errore nell'inserimento dell'abbonamento <br/> Si prega di contattare l'assistenza.";
                return null;
            }
        }else{
            return null;
        }
    }
    
    public void caricaPacchettiEsercizi(){
        // TODO
//        se nell'ordine ci sono dei pacchetti di esercizi carico gli esercizi del pacchetto nell'utente
//        if (ordine.getPacchetti() != null) {
//            for (Pacchetto o : ordine.getPacchetti()) {
//                if (o.getTipo().equals(Constants.TipoPacchetto.ESERCIZI)) {
//                    carica es nell'utente
//                }
//            }
//        }   
    }
    
    /**
     * 
     * calcola la data di inizio della subscription. 
     * Carica le subscription dell'utente e se ce n'è una attiva setta la data d'inizio della nuova subscription uguale alla data di fine della subscription attiva 
     *
     */
    public Date dataInizioSubscription(Pacchetto abbonamento){
        Subscription subsc = pacchettiManager.trovaSubscriptionAttiva(utente.getId(), abbonamento.getId());
        if(subsc!=null){
            return subsc.getValidUntil();
        }else{
            return Calendar.getInstance(TimeZone.getTimeZone("CET")).getTime();
        }
    }

    public List<Pacchetto> getPackAbb() {
        return packAbb;
    }

    public void setPackAbb(List<Pacchetto> packAbb) {
        this.packAbb = packAbb;
    }

    public List<Pacchetto> getPackEs() {
        return packEs;
    }

    public void setPackEs(List<Pacchetto> packEs) {
        this.packEs = packEs;
    }

    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public Pacchetto getPackAbbSel() {
        return packAbbSel;
    }

    public void setPackAbbSel(Pacchetto packAbbSel) {
        this.packAbbSel = packAbbSel;
    }

    public Pacchetto getPackEsSel() {
        return packEsSel;
    }

    public void setPackEsSel(Pacchetto packEsSel) {
        this.packEsSel = packEsSel;
    }

    public Double getTotale() {
        return totale;
    }

    public void setTotale(Double totale) {
        this.totale = totale;
    }

    public Stagione getStagione() {
        return stagione;
    }

    public void setStagione(Stagione stagione) {
        this.stagione = stagione;
    }

    public Ordine getOrdine() {
        return ordine;
    }

    public void setOrdine(Ordine ordine) {
        this.ordine = ordine;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPayerID() {
        return payerID;
    }

    public void setPayerID(String payerID) {
        this.payerID = payerID;
    }

    public PayPalAPIInterfaceServiceService getService() {
        return service;
    }

    public void setService(PayPalAPIInterfaceServiceService service) {
        this.service = service;
    }

    public Boolean getTrattamentoDati() {
        return trattamentoDati;
    }

    public void setTrattamentoDati(Boolean trattamentoDati) {
        this.trattamentoDati = trattamentoDati;
    }

    public Boolean getTerminiCondizioni() {
        return terminiCondizioni;
    }

    public void setTerminiCondizioni(Boolean terminiCondizioni) {
        this.terminiCondizioni = terminiCondizioni;
    }

    public FatturazioneBean getFatturazioneBean() {
        return fatturazioneBean;
    }

    public void setFatturazioneBean(FatturazioneBean fatturazioneBean) {
        this.fatturazioneBean = fatturazioneBean;
    }
    
    
}
