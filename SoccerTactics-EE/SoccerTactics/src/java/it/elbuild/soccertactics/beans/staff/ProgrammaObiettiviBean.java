/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.staff;

import com.sun.org.apache.xerces.internal.util.AttributesProxy;
import it.elbuild.soccertactics.beans.LoginBean;
import it.elbuild.soccertactics.lib.entities.Mesociclo;
import it.elbuild.soccertactics.lib.entities.Stagione;
import it.elbuild.soccertactics.lib.interfaces.MesocicloManager;
import it.elbuild.soccertactics.lib.interfaces.StagioniManager;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "ProgrammaObiettiviBean")
@SessionScoped
public class ProgrammaObiettiviBean implements Serializable{
    
    @EJB
    MesocicloManager mesocicloManager;
    
    @EJB
    StagioniManager stagioniManager;
    
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean; 
    
    private List<Mesociclo> progMesi;
    
    private Integer pid;
    private Mesociclo programma;
    
    public void caricaProgrammi(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            if(loginBean.getUtente().getStagione().programmaInserito()){
                progMesi = mesocicloManager.findAllMesocicli(loginBean.getUtente().getStagione().getId(), loginBean.getUtente().getId());
            }
        }
    }   
    
    public void inserisciPercentualiStagione(){
        Stagione s = loginBean.getUtente().getStagione();
        // se sono stati inseriti tutti gli obiettivi e la somma fa 100 inserisco la stagione
        if(s.programmaInserito()){
            // inserisco la stagione
            s = stagioniManager.insertUpdateStagione(s);
            if(s!=null){
                Mesociclo mesociclo = null;
                loginBean.getUtente().setStagione(s);
                // inserisco le righe relative a ogni mese della stagione
                Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("CET"), Locale.ITALIAN);
                calendar.setTime(s.getInizio());
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                // scorro da data inizio a data fine stagione e inserisco percentuale
                while(calendar.getTime().before(s.getFine())){
                    mesociclo = new Mesociclo(loginBean.getUtente(), calendar);
                    mesocicloManager.insertUpdateMesociclo(mesociclo);
                    calendar.add(Calendar.MONTH, 1);
                } 
                
                progMesi = mesocicloManager.findAllMesocicli(loginBean.getUtente().getStagione().getId(), loginBean.getUtente().getId());
                
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore.", ""));
            }
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Controllare di aver inserito tutte le percentuali e che la somma faccia 100", ""));
        }
    }
    
    public void caricaProgrammaMese(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            if(pid!=null){
                programma = mesocicloManager.findById(pid);
            }else{
                programma = null;
            }
        }
    }
    
    public String modificaPercentualiProgrammaMese(){
        // se il totale è 100 salvo le nuove percentuali e ricalcolo le percentuali della stagione
        if(programma.controllaPercentuali()){
            programma = mesocicloManager.insertUpdateMesociclo(programma);
            if(programma!=null){
                // ricalcolo percentuali stagione
                progMesi = mesocicloManager.findAllMesocicli(loginBean.getUtente().getStagione().getId(), loginBean.getUtente().getId());
                Stagione s = loginBean.getUtente().getStagione();
                s.azzeraPercentuali();
                for(Mesociclo m : progMesi){
                    s.aggiungiPercentuale(m.getAttacco(), m.getDifesa(), m.getCoordinativo(), m.getCondizionale());
                }               
                s.calcolaPercentuali(progMesi.size());
                                
                s = stagioniManager.insertUpdateStagione(s);
                
                return "programma.xhtml?faces-redirect=true";
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore.", ""));
                return "";
            }
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Controllare di aver inserito tutte le percentuali e che la somma faccia 100", ""));
            return "";
        }
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public List<Mesociclo> getProgMesi() {
        return progMesi;
    }

    public void setProgMesi(List<Mesociclo> progMesi) {
        this.progMesi = progMesi;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Mesociclo getProgramma() {
        return programma;
    }

    public void setProgramma(Mesociclo programma) {
        this.programma = programma;
    }
    
    
}
