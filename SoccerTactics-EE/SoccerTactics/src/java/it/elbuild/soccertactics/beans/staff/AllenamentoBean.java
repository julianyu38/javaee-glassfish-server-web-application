/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.staff;

import it.elbuild.soccertactics.beans.LoginBean;
import it.elbuild.soccertactics.lib.entities.Allenamento;
import it.elbuild.soccertactics.lib.entities.Esercizio;
import it.elbuild.soccertactics.lib.entities.Giocatore;
import it.elbuild.soccertactics.lib.interfaces.AllenamentiManager;
import it.elbuild.soccertactics.lib.interfaces.EserciziManager;
import it.elbuild.soccertactics.lib.interfaces.GiocatoriManager;
import it.elbuild.soccertactics.lib.utils.Constants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author Ale
 */
@ManagedBean(name="AllenamentoBean")
@ViewScoped
public class AllenamentoBean implements Serializable{
    
    @EJB
    AllenamentiManager allenamentiManager;
    
    @EJB
    GiocatoriManager giocatoriManager;
    
    @EJB
    EserciziManager eserciziManager;    
    
    private List<SelectItem> orari;
    
    private Allenamento allenamento;
    
    private List<Giocatore> giocatori;
    
    private List<Esercizio> esercizi;
    
    private Integer idAllenamento;
    
    private String fase;
    
    private String text;
       
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean;
    
    @PostConstruct
    public void init(){
        orari = new ArrayList<SelectItem>();
        orari.add(new SelectItem("07:00"));
        orari.add(new SelectItem("07:30"));
        orari.add(new SelectItem("08:00"));
        orari.add(new SelectItem("08:30"));
        orari.add(new SelectItem("09:00"));
        orari.add(new SelectItem("09:30"));
        orari.add(new SelectItem("10:00"));
        orari.add(new SelectItem("10:30"));
        orari.add(new SelectItem("11:00"));
        orari.add(new SelectItem("11:30"));
        orari.add(new SelectItem("12:00"));
        orari.add(new SelectItem("12:30"));
        orari.add(new SelectItem("13:00"));
        orari.add(new SelectItem("13:30"));
        orari.add(new SelectItem("14:00"));
        orari.add(new SelectItem("14:30"));
        orari.add(new SelectItem("15:00"));
        orari.add(new SelectItem("15:30"));
        orari.add(new SelectItem("16:00"));
        orari.add(new SelectItem("16:30"));
        orari.add(new SelectItem("17:00"));
        orari.add(new SelectItem("17:30"));
        orari.add(new SelectItem("18:00"));
        orari.add(new SelectItem("18:30"));
        orari.add(new SelectItem("19:00"));
        orari.add(new SelectItem("19:30"));
        orari.add(new SelectItem("20:00"));
        orari.add(new SelectItem("20:30"));
        orari.add(new SelectItem("21:00"));
        orari.add(new SelectItem("22:30"));
        orari.add(new SelectItem("23:00"));
        orari.add(new SelectItem("23:30"));
        orari.add(new SelectItem("24:00"));        
    }
    
    public void initNuovoAllenamento(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            if(loginBean.getUtente().getIdStagione()!=null){
                allenamento = new Allenamento();
                giocatori = giocatoriManager.findGiocatoriByIdStagione(loginBean.getUtente().getIdStagione());
                esercizi = eserciziManager.findEserciziUtente(loginBean.getUtente().getId());
                text = null;
            }
        }
    }
    
    public void mostraDialogEserciziFase(Integer num){
        if(num==null){
            fase = null;
        }
        if(num.equals(1)){
            fase = Constants.FasiAllenamento.FASE1;
        }else if(num.equals(2)){
            fase = Constants.FasiAllenamento.FASE2;
        }else if(num.equals(3)){
            fase = Constants.FasiAllenamento.FASE3;
        }else if(num.equals(4)){
            fase = Constants.FasiAllenamento.FASE4;
        }else{
            fase = null;
        }
    }
    
    public void aggiungiEsercizioFase(Esercizio e){
        allenamento.addEsercizio(e, fase);
        fase = null;
    }
    
    public void nascondiDialogEserciziFase(){
        fase = null;
    }
    
    public void filtraEsercizi(){
        if(text!=null && !text.equals("")){
            esercizi = eserciziManager.findEserciziTitoloDescrizione(text, loginBean.getUtente().getId(), null);
        }else{
            text = null;
            esercizi = eserciziManager.findEserciziUtente(loginBean.getUtente().getId());
        }
    }
    
    public String inserisciAllenamento() {
        allenamento.setIdStagione(loginBean.getUtente().getIdStagione());
        //imposto le date e controllo che la data di fine sia successiva a quella di inizio
        if (!allenamento.impostaDate()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Controllare orari di inizio e fine allenamento", ""));
            return "";
        }
        
        //inserici allenamento
        if (allenamentiManager.insertUpdateAllenamento(allenamento) == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nell'inserimento dell'appuntamento", ""));
            return "";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Allenamento correttamente inserito"));
            return "/staff/calendario.xhtml?faces-redirect=true";
        }
    }
    
    public void initVisualizzaAllenamento(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            if(idAllenamento!=null){
                allenamento = allenamentiManager.findById(idAllenamento);
                allenamento.updateListEsercizi();
                allenamento.updateMaterialiAllenamento();
             }
        }
    }
    
    public void initModificaAllenamento(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            if(idAllenamento!=null){
                allenamento = allenamentiManager.findById(idAllenamento);
                if(allenamento!=null){
                    allenamento.impostaOrariTransient(); 
                    allenamento.updateListEsercizi();
                    giocatori = giocatoriManager.findGiocatoriByIdStagione(loginBean.getUtente().getIdStagione());
                    esercizi = eserciziManager.findEserciziUtente(loginBean.getUtente().getId());
                    text = null;
                }
            }
        }
    }    
    
    public String modificaAllenamento() {
        //imposto le date e controllo che la data di fine sia successiva a quella di inizio
        if (!allenamento.impostaDate()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Controllare orari di inizio e fine allenamento", ""));
            return "";
        }
        
        //inserici allenamento
        if (allenamentiManager.insertUpdateAllenamento(allenamento) == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nella modifica dell'allenamento", ""));
            return "";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Allenamento correttamente modificato"));
            return "/staff/calendario.xhtml?faces-redirect=true";
        }
    }
    
    public void eliminaAllenamento(){
        if(allenamento!=null){
            allenamento.setEliminato(true);
            if(allenamentiManager.insertUpdateAllenamento(allenamento)!=null){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Allenamento correttamente eliminato. "));
                allenamento = null;
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore", ""));
            }
        }
    }

    public Allenamento getAllenamento() {
        return allenamento;
    }

    public void setAllenamento(Allenamento allenamento) {
        this.allenamento = allenamento;
    }

    public Integer getIdAllenamento() {
        return idAllenamento;
    }

    public void setIdAllenamento(Integer idAllenamento) {
        this.idAllenamento = idAllenamento;
    }

    public List<Giocatore> getGiocatori() {
        return giocatori;
    }

    public void setGiocatori(List<Giocatore> giocatori) {
        this.giocatori = giocatori;
    }

    public List<Esercizio> getEsercizi() {
        return esercizi;
    }

    public void setEsercizi(List<Esercizio> esercizi) {
        this.esercizi = esercizi;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public List<SelectItem> getOrari() {
        return orari;
    }

    public void setOrari(List<SelectItem> orari) {
        this.orari = orari;
    }

    public String getFase() {
        return fase;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    
    
}
