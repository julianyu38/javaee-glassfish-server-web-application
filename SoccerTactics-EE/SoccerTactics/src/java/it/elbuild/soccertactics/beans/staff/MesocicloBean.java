/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.staff;

import it.elbuild.soccertactics.beans.LoginBean;
import it.elbuild.soccertactics.lib.entities.Allenamento;
import it.elbuild.soccertactics.lib.entities.AllenamentoEsercizio;
import it.elbuild.soccertactics.lib.entities.Esercizio;
import it.elbuild.soccertactics.lib.entities.Mesociclo;
import it.elbuild.soccertactics.lib.interfaces.AllenamentiManager;
import it.elbuild.soccertactics.lib.interfaces.MesocicloManager;
import it.elbuild.soccertactics.lib.utils.MesocicloAllenamento;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.jfree.data.time.Year;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "MesocicloBean")
@ViewScoped
public class MesocicloBean implements Serializable{
    
    @EJB
    AllenamentiManager allenamentiManager;
    
    @EJB
    MesocicloManager mesocicloManager;
    
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean; 
    
    private Mesociclo progMese; 
    
    // per far scegliere la settimana per cui inserire il programma
    private String dataS;
    private List<SelectItem> settimaneStagione;
    SimpleDateFormat sdfDay = new SimpleDateFormat("dd MMMMMMMMMMM");
    SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
    
    private Integer settimana;
    private Integer anno;
    
    private Integer mese;
    
    private String settimana1;
    private Mesociclo allSett1Macro;
    private MesocicloAllenamento allSett1Micro;
    private String settimana2;
    private Mesociclo allSett2Macro;
    private MesocicloAllenamento allSett2Micro;
    private String settimana3;
    private Mesociclo allSett3Macro;
    private MesocicloAllenamento allSett3Micro;
    private String settimana4;
    private Mesociclo allSett4Macro;
    private MesocicloAllenamento allSett4Micro;
    private String settimana5;
    private Mesociclo allSett5Macro;
    private MesocicloAllenamento allSett5Micro;
    
    private Mesociclo totAllMacro;    
    private MesocicloAllenamento totAllMicro;    
    
    public void initVisualizzaMeseMesociclo() {
        if(!FacesContext.getCurrentInstance().isPostback() && mese!=null){
            progMese = mesocicloManager.findByIdStagioneIdUtenteMese(loginBean.getUtente().getStagione().getId(), loginBean.getUtente().getId(), mese, anno);
            if(progMese!=null ){
                
                totAllMacro = new Mesociclo();
                totAllMacro.initTotale();
                totAllMicro = new MesocicloAllenamento();
                totAllMicro.initTotale();
                // carico gli allenamenti della settimana
                List<Allenamento> allenamenti = allenamentiManager.findByMeseAnno(loginBean.getUtente().getStagione().getId(), mese, anno);
                if(allenamenti!=null && !allenamenti.isEmpty()){
                    for(Allenamento a : allenamenti){
                        if(a.getEsercizi()!=null && !a.getEsercizi().isEmpty()){
                            for(AllenamentoEsercizio e : a.getEsercizi()){
                                if(e.getEsercizio()!=null && e.getEsercizio().getDurata()!=null){
                                    if(e.getEsercizio().getObAttI()!=null && !e.getEsercizio().getObAttI().isEmpty()){
                                        totAllMacro.setAttacco(totAllMacro.getAttacco() + e.getEsercizio().getDurata().doubleValue());
                                        totAllMicro.addDurataObiettivo(e.getEsercizio().getObAttI(), e.getEsercizio().getDurata());
                                    }
                                    if(e.getEsercizio().getObCoord()!=null && !e.getEsercizio().getObCoord().isEmpty()){
                                        totAllMacro.setCoordinativo(totAllMacro.getCoordinativo() + e.getEsercizio().getDurata().doubleValue());
                                        totAllMicro.addDurataObiettivo(e.getEsercizio().getObCoord(), e.getEsercizio().getDurata());
                                    }
                                    if(e.getEsercizio().getObDifI()!=null && !e.getEsercizio().getObDifI().isEmpty()){
                                        totAllMacro.setDifesa(totAllMacro.getDifesa() + e.getEsercizio().getDurata().doubleValue());
                                        totAllMicro.addDurataObiettivo(e.getEsercizio().getObDifI(), e.getEsercizio().getDurata());
                                    }
                                    if(e.getEsercizio().getObCond()!=null && !e.getEsercizio().getObCond().isEmpty()){
                                        totAllMacro.setCondizionale(totAllMacro.getCondizionale() + e.getEsercizio().getDurata().doubleValue());
                                        totAllMicro.addDurataObiettivo(e.getEsercizio().getObCond(), e.getEsercizio().getDurata());
                                    } 
                                    if(e.getEsercizio().getObTatI()!=null && !e.getEsercizio().getObTatI().isEmpty()){
                                        totAllMacro.setTattico(totAllMacro.getTattico() + e.getEsercizio().getDurata().doubleValue());
                                    } 
                                }
                            }
                        }
                    }
                }
                mostraDettagliSettimane();
            }
        
        }
    }
    
    public void mostraDettagliSettimane(){
        settimana1 = null;
        settimana2 = null;
        settimana3 = null;
        settimana4 = null;
        settimana5 = null;
        
        // popolo il dropdown delle settimane
            Calendar data = Calendar.getInstance(TimeZone.getTimeZone("CET"), Locale.ITALIAN);
            data.setTime(progMese.getData());
            data.setFirstDayOfWeek(Calendar.MONDAY);
            // la settimana comincia sempre dal lunedì 
            while(data.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY){
                data.add(Calendar.DAY_OF_YEAR, -1);
            }
            Date inizio = null;
            Date fine = null;
            Integer mesePrec = mese.equals(0) ? 11 : mese-1 ;
            while (mesePrec.equals(data.get(Calendar.MONTH)) || mese.equals(data.get(Calendar.MONTH))) {
                inizio = data.getTime();
                data.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                fine = data.getTime();
                if(settimana1==null){
                    settimana1 = "Settimana dal "+sdfDay.format(inizio)+" al "+sdfDay.format(fine);
                    allSett1Macro = new Mesociclo();
                    allSett1Micro = new MesocicloAllenamento();        
                    caricaStatisticheAllenamentiSettimana(inizio, fine, allSett1Macro, allSett1Micro);
                }else if(settimana2==null){
                    settimana2 = "Settimana "+sdfDay.format(inizio)+" al "+sdfDay.format(fine);
                    allSett2Macro = new Mesociclo();
                    allSett2Micro = new MesocicloAllenamento(); 
                    caricaStatisticheAllenamentiSettimana(inizio, fine, allSett2Macro, allSett2Micro);
                }else if(settimana3==null){
                    settimana3 = "Settimana "+sdfDay.format(inizio)+" al "+sdfDay.format(fine);
                    allSett3Macro = new Mesociclo();
                    allSett3Micro = new MesocicloAllenamento(); 
                    caricaStatisticheAllenamentiSettimana(inizio, fine, allSett3Macro, allSett3Micro);
                }else if(settimana4==null){
                    allSett4Macro = new Mesociclo();
                    allSett4Micro = new MesocicloAllenamento(); 
                    settimana4 = "Settimana "+sdfDay.format(inizio)+" al "+sdfDay.format(fine);
                    caricaStatisticheAllenamentiSettimana(inizio, fine, allSett4Macro, allSett4Micro);
                }else if(settimana5==null){
                    settimana5 = "Settimana "+sdfDay.format(inizio)+" al "+sdfDay.format(fine);
                    allSett5Macro = new Mesociclo();
                    allSett5Micro = new MesocicloAllenamento(); 
                    caricaStatisticheAllenamentiSettimana(inizio, fine, allSett5Macro, allSett5Micro);
                }                
                data.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);                
                data.add(Calendar.WEEK_OF_YEAR, 1);
                System.out.println("Data "+sdfDay.format(data.getTime())+"  -  "+sdf.format(data.getTime()));
                System.out.println("mese "+mese+" calendar month "+data.get(Calendar.MONTH)+" mese precedente "+mesePrec);
            }
    }
    
    
    private void caricaStatisticheAllenamentiSettimana(Date inizio, Date fine, Mesociclo settMacro, MesocicloAllenamento settMicro) {
        settMacro.initTotale();
        settMicro.initTotale();
        // carico gli allenamenti della settimana
        List<Allenamento> allenamenti = allenamentiManager.findAllenamentiByDate(inizio, fine, loginBean.getUtente().getStagione().getId());
        if (allenamenti != null && !allenamenti.isEmpty()) {
            for (Allenamento a : allenamenti) {
                if (a.getEsercizi() != null && !a.getEsercizi().isEmpty()) {
                    for (AllenamentoEsercizio e : a.getEsercizi()) {
                        if (e.getEsercizio() != null && e.getEsercizio().getDurata() != null) {
                            if (e.getEsercizio().getObAttI() != null && !e.getEsercizio().getObAttI().isEmpty()) {
                                settMacro.setAttacco(settMacro.getAttacco() + e.getEsercizio().getDurata().doubleValue());
                                settMicro.addDurataObiettivo(e.getEsercizio().getObAttI(), e.getEsercizio().getDurata());
                            }
                            if (e.getEsercizio().getObCoord() != null && !e.getEsercizio().getObCoord().isEmpty()) {
                                settMacro.setCoordinativo(settMacro.getCoordinativo() + e.getEsercizio().getDurata().doubleValue());
                                settMicro.addDurataObiettivo(e.getEsercizio().getObCoord(), e.getEsercizio().getDurata());
                            }
                            if (e.getEsercizio().getObDifI() != null && !e.getEsercizio().getObDifI().isEmpty()) {
                                settMacro.setDifesa(settMacro.getDifesa() + e.getEsercizio().getDurata().doubleValue());
                                settMicro.addDurataObiettivo(e.getEsercizio().getObDifI(), e.getEsercizio().getDurata());
                            }
                            if (e.getEsercizio().getObCond() != null && !e.getEsercizio().getObCond().isEmpty()) {
                                settMacro.setCondizionale(settMacro.getCondizionale() + e.getEsercizio().getDurata().doubleValue());
                                settMicro.addDurataObiettivo(e.getEsercizio().getObCond(), e.getEsercizio().getDurata());
                            }
                            if (e.getEsercizio().getObTatI()!= null && !e.getEsercizio().getObTatI().isEmpty()) {
                                settMacro.setTattico(settMacro.getTattico() + e.getEsercizio().getDurata().doubleValue());
                            }
                        }
                    }
                }
            }
        }
    }


    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public String getDataS() {
        return dataS;
    }

    public void setDataS(String dataS) {
        this.dataS = dataS;
    }
    
    public List<SelectItem> getSettimaneStagione() {
        return settimaneStagione;
    }

    public void setSettimaneStagione(List<SelectItem> settimaneStagione) {
        this.settimaneStagione = settimaneStagione;
    }

    public Integer getSettimana() {
        return settimana;
    }

    public void setSettimana(Integer settimana) {
        this.settimana = settimana;
    }

    public Integer getAnno() {
        return anno;
    }

    public void setAnno(Integer anno) {
        this.anno = anno;
    }

    public Integer getMese() {
        return mese;
    }

    public void setMese(Integer mese) {
        this.mese = mese;
    }

    public Mesociclo getProgMese() {
        return progMese;
    }

    public void setProgMese(Mesociclo progMese) {
        this.progMese = progMese;
    }

    public String getSettimana1() {
        return settimana1;
    }

    public void setSettimana1(String settimana1) {
        this.settimana1 = settimana1;
    }

    public String getSettimana2() {
        return settimana2;
    }

    public void setSettimana2(String settimana2) {
        this.settimana2 = settimana2;
    }
    
    public String getSettimana3() {
        return settimana3;
    }

    public void setSettimana3(String settimana3) {
        this.settimana3 = settimana3;
    }

    public String getSettimana4() {
        return settimana4;
    }

    public void setSettimana4(String settimana4) {
        this.settimana4 = settimana4;
    }

    public String getSettimana5() {
        return settimana5;
    }

    public void setSettimana5(String settimana5) {
        this.settimana5 = settimana5;
    }

    public Mesociclo getAllSett1Macro() {
        return allSett1Macro;
    }

    public void setAllSett1Macro(Mesociclo allSett1Macro) {
        this.allSett1Macro = allSett1Macro;
    }

    public MesocicloAllenamento getAllSett1Micro() {
        return allSett1Micro;
    }

    public void setAllSett1Micro(MesocicloAllenamento allSett1Micro) {
        this.allSett1Micro = allSett1Micro;
    }

    public Mesociclo getAllSett2Macro() {
        return allSett2Macro;
    }

    public void setAllSett2Macro(Mesociclo allSett2Macro) {
        this.allSett2Macro = allSett2Macro;
    }

    public MesocicloAllenamento getAllSett2Micro() {
        return allSett2Micro;
    }

    public void setAllSett2Micro(MesocicloAllenamento allSett2Micro) {
        this.allSett2Micro = allSett2Micro;
    }

    public Mesociclo getAllSett3Macro() {
        return allSett3Macro;
    }

    public void setAllSett3Macro(Mesociclo allSett3Macro) {
        this.allSett3Macro = allSett3Macro;
    }

    public MesocicloAllenamento getAllSett3Micro() {
        return allSett3Micro;
    }

    public void setAllSett3Micro(MesocicloAllenamento allSett3Micro) {
        this.allSett3Micro = allSett3Micro;
    }

    public Mesociclo getAllSett4Macro() {
        return allSett4Macro;
    }

    public void setAllSett4Macro(Mesociclo allSett4Macro) {
        this.allSett4Macro = allSett4Macro;
    }

    public MesocicloAllenamento getAllSett4Micro() {
        return allSett4Micro;
    }

    public void setAllSett4Micro(MesocicloAllenamento allSett4Micro) {
        this.allSett4Micro = allSett4Micro;
    }

    public Mesociclo getAllSett5Macro() {
        return allSett5Macro;
    }

    public void setAllSett5Macro(Mesociclo allSett5Macro) {
        this.allSett5Macro = allSett5Macro;
    }

    public MesocicloAllenamento getAllSett5Micro() {
        return allSett5Micro;
    }

    public void setAllSett5Micro(MesocicloAllenamento allSett5Micro) {
        this.allSett5Micro = allSett5Micro;
    }

    public Mesociclo getTotAllMacro() {
        return totAllMacro;
    }

    public void setTotAllMacro(Mesociclo totAllMacro) {
        this.totAllMacro = totAllMacro;
    }

    public MesocicloAllenamento getTotAllMicro() {
        return totAllMicro;
    }

    public void setTotAllMicro(MesocicloAllenamento totAllMicro) {
        this.totAllMicro = totAllMicro;
    }

}
