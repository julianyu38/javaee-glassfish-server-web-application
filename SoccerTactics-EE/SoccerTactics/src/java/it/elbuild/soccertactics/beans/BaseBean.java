/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans;

import com.sun.corba.se.impl.orbutil.closure.Constant;
import it.elbuild.soccertactics.lib.entities.Esercizio;
import it.elbuild.soccertactics.lib.interfaces.ConfManager;
import it.elbuild.soccertactics.lib.utils.Constants;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "BaseBean")
@ApplicationScoped
public class BaseBean implements Serializable{
    
    @EJB
    ConfManager confManager;
    
    private String urlImmagini;
    
    @PostConstruct 
    public void init(){
        try {
            urlImmagini = confManager.findByLabel(Constants.ServletConstants.URL_IMMAGINI);
        } catch (Exception ex) {
            Logger.getLogger(BaseBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String urlSchemaEs(Esercizio esercizio){
        return urlImmagini+(esercizio.getImmagine()!=null ? esercizio.getImmagine() : esercizio.getFileCaricato())+"&type=schema";
    }
    
    public String getUrlImmagini() {
        return urlImmagini;
    }

    public void setUrlImmagini(String urlImmagini) {
        this.urlImmagini = urlImmagini;
    }
    
    
    
}
