/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans;

import it.elbuild.soccertactics.lib.entities.Esercizio;
import it.elbuild.soccertactics.lib.interfaces.EserciziManager;
import it.elbuild.soccertactics.lib.utils.RicercaEsercizi;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ale
 */
@ManagedBean(name="EserciziBean")
@SessionScoped
public class EserciziBean implements Serializable{

    @EJB
    EserciziManager eserciziManager;
    
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean;
    
    private List<Esercizio> esercizi;
    
    private RicercaEsercizi criteri;
    
    private Boolean ricercaAvanzata;
    
    private String text;
    
    @ManagedProperty(value = "#{DropdownBean}")
    private DropdownBean dropdownBean;
    
    @PostConstruct
    public void init(){
        ricercaAvanzata = Boolean.FALSE;
        criteri = new RicercaEsercizi();
        text = null;
    }
    
    public void toggleRicercaAvanzata(){
        ricercaAvanzata = !ricercaAvanzata;
        if(!ricercaAvanzata){
            criteri.resetCriteri();
            esercizi = eserciziManager.findEserciziUtente(loginBean.getUtente().getId());
        }
    }
    
    public void updateDropdownDifesa(){
        dropdownBean.caricaDropdowndDifesa(criteri.getObDifI(), criteri.getObDifII());
    }
    
    public void updateDropdownAttacco(){
        dropdownBean.caricaDropdowndAttacco(criteri.getObAttI());
    }
    
    public void caricaEserciziUtente(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            esercizi = eserciziManager.findEserciziUtente(loginBean.getUtente().getId());
        }        
    }
    
    public void ricercaAvanzataEsercizi(){
        criteri.setIdUtente(loginBean.getUtente().getId());
        esercizi = eserciziManager.ricercaAvanzataEsercizi(criteri);
    }
    
    public void resetCampi(){
        text = null;
        criteri.resetCriteri();
//        dropdownBean.ricaricaObiettivi(null, null);
        esercizi = eserciziManager.findEserciziUtente(loginBean.getUtente().getId());
    }
    
    public void ricercaLiberaEsercizi(){
        if(text!=null && !text.equals("")){
            esercizi = eserciziManager.findEserciziTitoloDescrizione(text, loginBean.getUtente().getId(), null);
        }
    }
    
    public void duplicaEsercizio(Esercizio es){
        if(es!=null){
            Esercizio escopy = new Esercizio(es, loginBean.getUtente());
            escopy = eserciziManager.insertUpdateEsercizio(escopy);
            if(escopy!=null){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Esercizio correttamente duplicato. "));
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("/SoccerTactics/faces/staff/modifica-esercizio.xhtml?idEsercizio=" + escopy.getId());
                } catch (IOException ex) {
                    Logger.getLogger(EserciziBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore", ""));
            }
        }
    }
    
    public void eliminaEsercizio(Esercizio e){
        if(e!=null){
            e.setEliminato(true);
            if(eserciziManager.insertUpdateEsercizio(e)!=null){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Esercizio correttamente eliminato. "));
                esercizi = eserciziManager.findEserciziUtente(loginBean.getUtente().getId());
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore", ""));
            }
        }
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public List<Esercizio> getEsercizi() {
        return esercizi;
    }

    public void setEsercizi(List<Esercizio> esercizi) {
        this.esercizi = esercizi;
    }

    public Boolean getRicercaAvanzata() {
        return ricercaAvanzata;
    }

    public void setRicercaAvanzata(Boolean ricercaAvanzata) {
        this.ricercaAvanzata = ricercaAvanzata;
    }

    public RicercaEsercizi getCriteri() {
        return criteri;
    }

    public void setCriteri(RicercaEsercizi criteri) {
        this.criteri = criteri;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public DropdownBean getDropdownBean() {
        return dropdownBean;
    }

    public void setDropdownBean(DropdownBean dropdownBean) {
        this.dropdownBean = dropdownBean;
    }
    
}
