/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.admin;

import it.elbuild.soccertactics.lib.entities.Subscription;
import it.elbuild.soccertactics.lib.interfaces.PacchettiManager;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "AbbonamentiBean")
@SessionScoped
public class AbbonamentiBean implements Serializable{
    
    @EJB
    PacchettiManager pacchettiManager;
    
    public List<Subscription> subscriptions;
    
    public void loadSubscriptions(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            subscriptions = pacchettiManager.findAllSubscriptions();
        }
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

}
