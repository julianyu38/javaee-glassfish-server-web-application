/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.staff;

import it.elbuild.soccertactics.beans.LoginBean;
import it.elbuild.soccertactics.lib.entities.Avversario;
import it.elbuild.soccertactics.lib.entities.Giocatore;
import it.elbuild.soccertactics.lib.entities.GiocatoreAvversario;
import it.elbuild.soccertactics.lib.entities.Partita;
import it.elbuild.soccertactics.lib.entities.PartitaAvversario;
import it.elbuild.soccertactics.lib.entities.PartitaGiocatore;
import it.elbuild.soccertactics.lib.entities.ResocontoPG;
import it.elbuild.soccertactics.lib.entities.TecnicaMa;
import it.elbuild.soccertactics.lib.interfaces.AvversariManager;
import it.elbuild.soccertactics.lib.interfaces.GiocatoriManager;
import it.elbuild.soccertactics.lib.interfaces.PartiteManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author Ale
 */
@ManagedBean(name="PartitaBean")
@ViewScoped
public class PartitaBean implements Serializable{
    
    @EJB
    PartiteManager partiteManager;
    
    @EJB
    GiocatoriManager giocatoriManager;
    
    @EJB
    AvversariManager avversariManager;
        
    private List<SelectItem> orari;
    
    private Partita partita;
    
    private List<Giocatore> giocatori;
    
    private List<Avversario> avversari;
    
    private Integer idPartita;
    
    @ManagedProperty(value = "#{LoginBean}")
    private LoginBean loginBean; 
    
    private String squadra;
    
    private List<SelectItem> categorieI;
    private List<SelectItem> categorieII;
    private List<SelectItem> categorieIII;
    private List<SelectItem> tecniche;
    
    private String cat1;
    private String cat2;
    private String cat3;
    private Integer tecid;
    private Integer idGiocatore;
    
    @PostConstruct
    public void init(){
        orari = new ArrayList<SelectItem>();
        orari.add(new SelectItem("07:00"));
        orari.add(new SelectItem("07:30"));
        orari.add(new SelectItem("08:00"));
        orari.add(new SelectItem("08:30"));
        orari.add(new SelectItem("09:00"));
        orari.add(new SelectItem("09:30"));
        orari.add(new SelectItem("10:00"));
        orari.add(new SelectItem("10:30"));
        orari.add(new SelectItem("11:00"));
        orari.add(new SelectItem("11:30"));
        orari.add(new SelectItem("12:00"));
        orari.add(new SelectItem("12:30"));
        orari.add(new SelectItem("13:00"));
        orari.add(new SelectItem("13:30"));
        orari.add(new SelectItem("14:00"));
        orari.add(new SelectItem("14:30"));
        orari.add(new SelectItem("15:00"));
        orari.add(new SelectItem("15:30"));
        orari.add(new SelectItem("16:00"));
        orari.add(new SelectItem("16:30"));
        orari.add(new SelectItem("17:00"));
        orari.add(new SelectItem("17:30"));
        orari.add(new SelectItem("18:00"));
        orari.add(new SelectItem("18:30"));
        orari.add(new SelectItem("19:00"));
        orari.add(new SelectItem("19:30"));
        orari.add(new SelectItem("20:00"));
        orari.add(new SelectItem("20:30"));
        orari.add(new SelectItem("21:00"));
        orari.add(new SelectItem("22:30"));
        orari.add(new SelectItem("23:00"));
        orari.add(new SelectItem("23:30"));
        orari.add(new SelectItem("24:00"));        
        
        categorieI = partiteManager.findCategorie1();
    }
    
    public void initNuovaPartita(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            if(loginBean.getUtente().getIdStagione()!=null){
                partita = new Partita();
                giocatori = giocatoriManager.findGiocatoriByIdStagione(loginBean.getUtente().getIdStagione());
                avversari = avversariManager.findAvversariStagione(loginBean.getUtente().getIdStagione());
            }
        }
    }
    
    public String inserisciPartita() {
        partita.setIdStagione(loginBean.getUtente().getIdStagione());
        //imposto le date e controllo che la data di fine sia successiva a quella di inizio
        if (!partita.impostaDate()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Controllare data e orario di inizio della partita", ""));
            return "";
        }
        
        //inserici partita
        if (partiteManager.insertUpdatePartita(partita) == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nell'inserimento della partita", ""));
            return "";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Partita correttamente inserita"));
            return "/staff/calendario.xhtml?faces-redirect=true";
        }
    }
    
    public void initVisualizzaPartita(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            if(idPartita!=null){
                partita = partiteManager.findById(idPartita);
             }
        }
    }
    
    public void initModificaPartita(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            if(idPartita!=null){
                partita = partiteManager.findById(idPartita);
                if(partita!=null){
                    partita.impostaOrariTransient(); 
                    giocatori = giocatoriManager.findGiocatoriByIdStagione(loginBean.getUtente().getIdStagione());
                    avversari = avversariManager.findAvversariStagione(loginBean.getUtente().getIdStagione());
                }
            }
        }
    }    
    
    public String modificaPartita() {
        //imposto le date e controllo che la data di fine sia successiva a quella di inizio
        if (!partita.impostaDate()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Controllare data e orario di inizio della partita", ""));
            return "";
        }
        
        //inserici allenamento
        if (partiteManager.insertUpdatePartita(partita) == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nella modifica della partita", ""));
            return "";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Partita correttamente modificata"));
            return "/staff/calendario.xhtml?faces-redirect=true";
        }
    }
    
    public void initInserisciResocontoPartita() {
        if (!FacesContext.getCurrentInstance().isPostback()) {
            if (idPartita != null) {
                partita = partiteManager.findById(idPartita);
                if (partita != null) {
                    partita.impostaOrariTransient();
                    giocatori = giocatoriManager.findGiocatoriByIdStagione(loginBean.getUtente().getIdStagione());
                    avversari = avversariManager.findAvversariStagione(loginBean.getUtente().getIdStagione());

                    associaResocontoGiocatoriAvversari();
                    // associa resoconto al singolo giocatore
//                    List<TecnicaMa> tecniche = partiteManager.findAllTecnicheMa();
//                    if (tecniche != null && !tecniche.isEmpty() && partita.getGiocatori() != null && !partita.getGiocatori().isEmpty()) {
//                        //associo i dettagli del match analysis a ogni giocatore se non sono stati inseriti
//                        for (PartitaGiocatore pg : partita.getGiocatori()) {
//                            if (pg.getResoconti() == null || (pg.getResoconti() != null && pg.getResoconti().isEmpty())) {
//                                pg.addResoconti(tecniche);
//                            }
//                        }
//                    }
                    nascondiAggiungiTecnica();                    
                }
            }
        }
    }
    
    public String salvaResocontoPartita(){
        partita.setResocontoInserito(Boolean.TRUE);
        if (partiteManager.insertUpdatePartita(partita) == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore nel salvataggio del resoconto della partita", ""));
            return "";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Resoconto partita correttamente salvato"));
            return "/staff/scheda-partita.xhtml?faces-redirect=true&idPartita="+partita.getId();
        }
    }
    
    public void associaResocontoGiocatoriAvversari(){
        if(partita.getIdAvversario()!=null){
            Avversario avversario = avversariManager.findById(partita.getIdAvversario());
            PartitaAvversario pa;
            // se ci sono dei giocatori nell'avversario scelto scorro la lista di PartitaAvversario 
            // e controllo se sono già presenti se no li inserisco
            if(avversario.getGiocatori()!=null && !avversario.getGiocatori().isEmpty()){
                // se ci sono dei giocatori controllo se ci sono dei dati da eliminare
                if(partita.getAvversari()!=null && !partita.getAvversari().isEmpty()){
                    // aggiungo i giocatori che non ci sono 
                    for(GiocatoreAvversario ga : avversario.getGiocatori()){
                        if(!listAvversariPartitaContains(partita.getAvversari(), idPartita)){
                            partita.getAvversari().add(new PartitaAvversario(ga));
                        }
                    }
                    // elimino eventuali giocatori di troppo 
                    Iterator<PartitaAvversario> ipa = partita.getAvversari().iterator();
                    while(ipa.hasNext()){
                        pa = ipa.next();
                        if(!listGiocatoriAvversarioContains(avversario.getGiocatori(), pa.getIdGiocatoreAvversario())){
                            ipa.remove();;
                        }
                    }
                }else{
                    // se la lista nella partita è nulla la inizializzo e inserisco tutti i giocatori
                    partita.setAvversari(new ArrayList<PartitaAvversario>());
                    // aggiungo i giocatori che non ci sono 
                    for(GiocatoreAvversario ga : avversario.getGiocatori()){
                        if(!listAvversariPartitaContains(partita.getAvversari(), idPartita)){
                            partita.getAvversari().add(new PartitaAvversario(ga));
                        }
                    }                
                }
            }else{
                // se non ci sono giocatori nell'avversario scelto devo eliminare tutti gli avversari inseriti nella partita
                if(partita.getAvversari()!=null && !partita.getAvversari().isEmpty()){
                    partita.getAvversari().clear();
                }            
            }
        }
    }
    
    public boolean listAvversariPartitaContains(List<PartitaAvversario> listaAvv, Integer idAvv ){
        for(PartitaAvversario pa : listaAvv){
            if(pa.getIdGiocatoreAvversario()!=null && pa.getIdGiocatoreAvversario().equals(idAvv)){
                return true;
            }
        }
        return false;
    }
    
    public boolean listGiocatoriAvversarioContains(List<GiocatoreAvversario> listaAvv, Integer idAvv ){
        for(GiocatoreAvversario g : listaAvv){
            if(g.getId()!=null && g.getId().equals(idAvv)){
                return true;
            }
        }
        return false;
    }
    
    public void nascondiAggiungiTecnica(){
        idGiocatore = null;
        cat1 = null;
        cat2 = null;
        cat3 = null;
        tecid = null;
    }
    
    public void mostraAggiungiTecnica(Integer gid){
        idGiocatore = gid;
        cat1 = null;
        cat2 = null;
        cat3 = null;
        tecid = null;
    }
    
    public void refreshMenuTecniche(){
        if (cat1 != null && !cat1.isEmpty()) {
            categorieII = partiteManager.findCategorie2(cat1);
            if (cat2 != null && !cat2.isEmpty()) {
                categorieIII = partiteManager.findCategorie3(cat1, cat2);
                if (cat3 != null && !cat3.isEmpty()) {
                    tecniche = partiteManager.findTecniche(cat1, cat2, cat3);
                } else {
                    tecniche = null;
                    tecid = null;
                }
            } else {
                categorieIII = null;
                tecniche = null;
                cat3 = null;
                tecid = null;
            }
        } else {
            categorieII = null;
            categorieIII = null;
            tecniche = null;
            cat2 = null;
            cat3 = null;
            tecid = null;
        }
    }

    public void aggiungiTecnicaResconto(){
        TecnicaMa tecnica = null;
        if (tecid == null) {
            if (cat1 != null && !cat1.isEmpty() && cat2 != null && !cat2.isEmpty()) {
                List<TecnicaMa> listTec = partiteManager.findByCategorie(cat1, cat2, cat3);
                tecnica = (listTec != null && listTec.size() == 1) ? listTec.get(0) : null;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Selezionare la tecnica da aggiungere", ""));
            }

        } else {
            tecnica = partiteManager.findTecnicaMaByID(tecid);
        }
        if (tecnica != null) {
            for (PartitaGiocatore pg : partita.getGiocatori()) {
                if (pg.getIdGiocatore().equals(idGiocatore)) {
                    pg.addResoconto(tecnica);
                }
            }
            nascondiAggiungiTecnica();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore tecnica non trovata", ""));
        }
    }
    
    public void eliminaPartita(){
        if(partita!=null){
            if(partiteManager.deletePartita(partita)){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Partita correttamente eliminata. "));
                partita = null;
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Si è verificato un errore", ""));
            }
        }
    }

    public List<SelectItem> getOrari() {
        return orari;
    }

    public void setOrari(List<SelectItem> orari) {
        this.orari = orari;
    }

    public Partita getPartita() {
        return partita;
    }

    public void setPartita(Partita partita) {
        this.partita = partita;
    }

    public List<Giocatore> getGiocatori() {
        return giocatori;
    }

    public void setGiocatori(List<Giocatore> giocatori) {
        this.giocatori = giocatori;
    }

    public Integer getIdPartita() {
        return idPartita;
    }

    public void setIdPartita(Integer idPartita) {
        this.idPartita = idPartita;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public List<Avversario> getAvversari() {
        return avversari;
    }

    public void setAvversari(List<Avversario> avversari) {
        this.avversari = avversari;
    }

    public String getSquadra() {
        return squadra;
    }

    public void setSquadra(String squadra) {
        this.squadra = squadra;
    }

    public List<SelectItem> getCategorieI() {
        return categorieI;
    }

    public void setCategorieI(List<SelectItem> categorieI) {
        this.categorieI = categorieI;
    }

    public List<SelectItem> getCategorieII() {
        return categorieII;
    }

    public void setCategorieII(List<SelectItem> categorieII) {
        this.categorieII = categorieII;
    }

    public List<SelectItem> getCategorieIII() {
        return categorieIII;
    }

    public void setCategorieIII(List<SelectItem> categorieIII) {
        this.categorieIII = categorieIII;
    }

    public List<SelectItem> getTecniche() {
        return tecniche;
    }

    public void setTecniche(List<SelectItem> tecniche) {
        this.tecniche = tecniche;
    }

    public String getCat1() {
        return cat1;
    }

    public void setCat1(String cat1) {
        this.cat1 = cat1;
    }

    public String getCat2() {
        return cat2;
    }

    public void setCat2(String cat2) {
        this.cat2 = cat2;
    }

    public String getCat3() {
        return cat3;
    }

    public void setCat3(String cat3) {
        this.cat3 = cat3;
    }

    public Integer getTecid() {
        return tecid;
    }

    public void setTecid(Integer tecid) {
        this.tecid = tecid;
    }

    public Integer getIdGiocatore() {
        return idGiocatore;
    }

    public void setIdGiocatore(Integer idGiocatore) {
        this.idGiocatore = idGiocatore;
    }

    
}
