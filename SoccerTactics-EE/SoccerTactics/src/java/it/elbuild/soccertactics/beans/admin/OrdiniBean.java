/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.beans.admin;

import it.elbuild.soccertactics.lib.entities.Ordine;
import it.elbuild.soccertactics.lib.interfaces.PacchettiManager;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ale
 */
@ManagedBean(name = "OrdiniBean")
@SessionScoped
public class OrdiniBean implements Serializable{
    
    @EJB
    PacchettiManager pacchettiManager;
    
    public List<Ordine> ordini;
    
    public void loadOrdini(){
        if(!FacesContext.getCurrentInstance().isPostback()){
            ordini = pacchettiManager.findAllOrdini();
        }
    }

    public List<Ordine> getOrdini() {
        return ordini;
    }

    public void setOrdini(List<Ordine> ordini) {
        this.ordini = ordini;
    }
    
}
