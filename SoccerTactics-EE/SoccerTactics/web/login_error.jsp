<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xml:lang="it" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>SOFTKEEPER - Errore Login</title>

<link rel="stylesheet" type="text/css" href="<%out.print(request.getContextPath());%>/css/style.css"></link>

 <link rel="icon" href="<%out.print(request.getContextPath());%>/images/favicon.ico" type="image/x-icon"  />



</head>

    <body >
        <div class="wrapper">
        <div align="center" id="login" style="padding-top: 60px">
            
            <img class="logo-home" src="<%out.print(request.getContextPath());%>/images/logo.png"/>

            <div class="bordo-left"></div>
        <div class="sfondo-centro">
            
            <br/><br/>
            <div class="login_message">
                <div align="center">
       L'utente non ha i diritti
       per accedere a quest'area.<br/>
                    <br/>Controllare che username e password<br/>siano corretti.<br/><br/>

                    <a href="<%=session.getAttribute("path2")%>"> indietro</a>
                    <br/>
                    </div>
                    </div>
                    </div>
                    <div class="bordo-right"></div>
                    </div>

</div>
        <div class="post-wrapper"></div>
    

</body>
</html>
