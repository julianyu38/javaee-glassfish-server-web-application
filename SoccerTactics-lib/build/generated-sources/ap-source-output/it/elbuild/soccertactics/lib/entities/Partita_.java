package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.entities.Avversario;
import it.elbuild.soccertactics.lib.entities.PartitaAvversario;
import it.elbuild.soccertactics.lib.entities.PartitaGiocatore;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Partita.class)
public class Partita_ { 

    public static volatile SingularAttribute<Partita, Integer> possessoPallaII;
    public static volatile SingularAttribute<Partita, Integer> possessoPallaTot;
    public static volatile SingularAttribute<Partita, Integer> possessoPallaI;
    public static volatile SingularAttribute<Partita, String> luogo;
    public static volatile SingularAttribute<Partita, Integer> golFattiAzione;
    public static volatile SingularAttribute<Partita, Integer> golSubitiAzione;
    public static volatile ListAttribute<Partita, PartitaGiocatore> giocatori;
    public static volatile SingularAttribute<Partita, Boolean> resocontoInserito;
    public static volatile SingularAttribute<Partita, Integer> idStagione;
    public static volatile SingularAttribute<Partita, Date> dataInizio;
    public static volatile SingularAttribute<Partita, Double> golTiri;
    public static volatile SingularAttribute<Partita, Double> golTiriAvversari;
    public static volatile SingularAttribute<Partita, Avversario> avversario;
    public static volatile SingularAttribute<Partita, Date> dataFine;
    public static volatile SingularAttribute<Partita, Integer> idAvversario;
    public static volatile SingularAttribute<Partita, Integer> golSubitiPalla;
    public static volatile SingularAttribute<Partita, Integer> id;
    public static volatile ListAttribute<Partita, PartitaAvversario> avversari;
    public static volatile SingularAttribute<Partita, Integer> golFattiPalla;

}