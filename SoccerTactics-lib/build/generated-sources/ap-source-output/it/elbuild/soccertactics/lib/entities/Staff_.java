package it.elbuild.soccertactics.lib.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Staff.class)
public class Staff_ { 

    public static volatile SingularAttribute<Staff, String> mansione;
    public static volatile SingularAttribute<Staff, String> cognome;
    public static volatile SingularAttribute<Staff, String> cellulare;
    public static volatile SingularAttribute<Staff, String> categoria;
    public static volatile SingularAttribute<Staff, String> residenzaVia;
    public static volatile SingularAttribute<Staff, String> nome;
    public static volatile SingularAttribute<Staff, String> residenzaProvincia;
    public static volatile SingularAttribute<Staff, Integer> idStagione;
    public static volatile SingularAttribute<Staff, String> residenzaCap;
    public static volatile SingularAttribute<Staff, String> residenzaRegione;
    public static volatile SingularAttribute<Staff, String> residenzaCitta;
    public static volatile SingularAttribute<Staff, String> foto;
    public static volatile SingularAttribute<Staff, Integer> id;
    public static volatile SingularAttribute<Staff, String> email;

}