package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.entities.GiocatoreAvversario;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Avversario.class)
public class Avversario_ { 

    public static volatile SingularAttribute<Avversario, String> stemma;
    public static volatile ListAttribute<Avversario, GiocatoreAvversario> giocatori;
    public static volatile SingularAttribute<Avversario, String> nome;
    public static volatile SingularAttribute<Avversario, Integer> id;
    public static volatile SingularAttribute<Avversario, Integer> idStagione;

}