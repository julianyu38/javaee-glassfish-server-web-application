package it.elbuild.soccertactics.lib.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Condivisione.class)
public class Condivisione_ { 

    public static volatile SingularAttribute<Condivisione, Integer> id;
    public static volatile SingularAttribute<Condivisione, Integer> idEsercizio;
    public static volatile SingularAttribute<Condivisione, String> listaMail;
    public static volatile SingularAttribute<Condivisione, String> token;
    public static volatile SingularAttribute<Condivisione, String> testo;

}