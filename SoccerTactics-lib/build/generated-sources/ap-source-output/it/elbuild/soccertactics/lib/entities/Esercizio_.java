package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.entities.Materiale;
import it.elbuild.soccertactics.lib.entities.Utente;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Esercizio.class)
public class Esercizio_ { 

    public static volatile SingularAttribute<Esercizio, String> obTatI;
    public static volatile SingularAttribute<Esercizio, Boolean> lavoroGruppo;
    public static volatile SingularAttribute<Esercizio, String> obCoord;
    public static volatile SingularAttribute<Esercizio, String> condizionale;
    public static volatile SingularAttribute<Esercizio, Boolean> generale;
    public static volatile SingularAttribute<Esercizio, Boolean> globale;
    public static volatile SingularAttribute<Esercizio, Boolean> atleticoMotorio;
    public static volatile SingularAttribute<Esercizio, Boolean> recuperoInfortunio;
    public static volatile SingularAttribute<Esercizio, Integer> id;
    public static volatile SingularAttribute<Esercizio, Integer> idUtente;
    public static volatile SingularAttribute<Esercizio, Boolean> pubblico;
    public static volatile SingularAttribute<Esercizio, Integer> portieri;
    public static volatile SingularAttribute<Esercizio, Integer> durata;
    public static volatile SingularAttribute<Esercizio, String> obTatII;
    public static volatile SingularAttribute<Esercizio, Boolean> pulcini;
    public static volatile SingularAttribute<Esercizio, Boolean> preAllenamento;
    public static volatile SingularAttribute<Esercizio, String> fileCaricato;
    public static volatile SingularAttribute<Esercizio, String> obDifII;
    public static volatile SingularAttribute<Esercizio, Boolean> juniores;
    public static volatile SingularAttribute<Esercizio, String> obDifI;
    public static volatile SingularAttribute<Esercizio, Boolean> giocoTema;
    public static volatile SingularAttribute<Esercizio, String> obAttII;
    public static volatile SingularAttribute<Esercizio, Integer> stazioniGruppi;
    public static volatile SingularAttribute<Esercizio, String> immagine;
    public static volatile ListAttribute<Esercizio, Materiale> materiali;
    public static volatile SingularAttribute<Esercizio, Integer> ripetizioni;
    public static volatile SingularAttribute<Esercizio, Boolean> finalizzato;
    public static volatile SingularAttribute<Esercizio, Utente> utente;
    public static volatile SingularAttribute<Esercizio, String> titolo;
    public static volatile SingularAttribute<Esercizio, Boolean> eliminato;
    public static volatile SingularAttribute<Esercizio, Boolean> analitico;
    public static volatile SingularAttribute<Esercizio, Boolean> esordienti;
    public static volatile SingularAttribute<Esercizio, Boolean> giocoIniziale;
    public static volatile SingularAttribute<Esercizio, String> difesa2;
    public static volatile SingularAttribute<Esercizio, String> modello;
    public static volatile SingularAttribute<Esercizio, Boolean> situazionale;
    public static volatile SingularAttribute<Esercizio, Boolean> tutti;
    public static volatile SingularAttribute<Esercizio, String> obAttI;
    public static volatile SingularAttribute<Esercizio, Boolean> primaSquadra;
    public static volatile SingularAttribute<Esercizio, Boolean> allievi;
    public static volatile SingularAttribute<Esercizio, Boolean> percorsoStazioni;
    public static volatile SingularAttribute<Esercizio, Integer> recupero;
    public static volatile SingularAttribute<Esercizio, String> difesa2II;
    public static volatile SingularAttribute<Esercizio, String> descrizione;
    public static volatile SingularAttribute<Esercizio, Boolean> giovanissimi;
    public static volatile SingularAttribute<Esercizio, String> difesa2III;
    public static volatile SingularAttribute<Esercizio, String> attacco2II;
    public static volatile SingularAttribute<Esercizio, String> sfondo;
    public static volatile SingularAttribute<Esercizio, String> coordinativa;
    public static volatile SingularAttribute<Esercizio, Integer> serie;
    public static volatile SingularAttribute<Esercizio, Boolean> primiCalci;
    public static volatile SingularAttribute<Esercizio, String> obCond;
    public static volatile SingularAttribute<Esercizio, String> obDifIII;
    public static volatile SingularAttribute<Esercizio, String> attacco2;

}