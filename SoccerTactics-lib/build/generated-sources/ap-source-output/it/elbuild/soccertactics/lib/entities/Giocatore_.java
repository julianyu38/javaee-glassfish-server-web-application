package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.entities.Stagione;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Giocatore.class)
public class Giocatore_ { 

    public static volatile SingularAttribute<Giocatore, Double> gambaSx;
    public static volatile SingularAttribute<Giocatore, Double> sicurezzaInsicurezza;
    public static volatile SingularAttribute<Giocatore, String> gruppo;
    public static volatile SingularAttribute<Giocatore, String> residenzaVia;
    public static volatile SingularAttribute<Giocatore, String> luogoNascita;
    public static volatile SingularAttribute<Giocatore, Double> torace;
    public static volatile SingularAttribute<Giocatore, Double> cosciaMaxSx;
    public static volatile SingularAttribute<Giocatore, String> residenzaCitta;
    public static volatile SingularAttribute<Giocatore, Double> unoVSunoSx;
    public static volatile SingularAttribute<Giocatore, Double> posturaAttDx;
    public static volatile SingularAttribute<Giocatore, Double> uscitaAltaSx;
    public static volatile SingularAttribute<Giocatore, Double> problemSolving;
    public static volatile SingularAttribute<Giocatore, Double> uscitaBassaDx;
    public static volatile SingularAttribute<Giocatore, Integer> id;
    public static volatile SingularAttribute<Giocatore, Double> reazEventErrore;
    public static volatile SingularAttribute<Giocatore, Double> raccoltaDx;
    public static volatile SingularAttribute<Giocatore, Double> tuffoStrisDx;
    public static volatile SingularAttribute<Giocatore, Double> cosciaMinSx;
    public static volatile SingularAttribute<Giocatore, Double> cosciaMediaDx;
    public static volatile SingularAttribute<Giocatore, Double> posturaAttSx;
    public static volatile SingularAttribute<Giocatore, Double> presaDx;
    public static volatile SingularAttribute<Giocatore, Double> peso;
    public static volatile SingularAttribute<Giocatore, String> nome;
    public static volatile SingularAttribute<Giocatore, String> foto;
    public static volatile SingularAttribute<Giocatore, Double> efficacia;
    public static volatile SingularAttribute<Giocatore, Double> tuffoTraietAltSx;
    public static volatile SingularAttribute<Giocatore, String> valCapCondiz;
    public static volatile SingularAttribute<Giocatore, String> note;
    public static volatile SingularAttribute<Giocatore, Date> dataNascita;
    public static volatile SingularAttribute<Giocatore, String> cognome;
    public static volatile SingularAttribute<Giocatore, Double> uscitaBassaSx;
    public static volatile SingularAttribute<Giocatore, Double> tuffoTraietAltDx;
    public static volatile SingularAttribute<Giocatore, Integer> idStagione;
    public static volatile SingularAttribute<Giocatore, String> residenzaCap;
    public static volatile SingularAttribute<Giocatore, Double> raccoltaSx;
    public static volatile SingularAttribute<Giocatore, Double> tuffoStrisSx;
    public static volatile SingularAttribute<Giocatore, String> valCapCoord;
    public static volatile SingularAttribute<Giocatore, String> residenzaRegione;
    public static volatile SingularAttribute<Giocatore, Double> gestPresGara;
    public static volatile SingularAttribute<Giocatore, Double> gambaDx;
    public static volatile SingularAttribute<Giocatore, String> piede;
    public static volatile SingularAttribute<Giocatore, Double> comunicazione;
    public static volatile SingularAttribute<Giocatore, Double> partecipazioneGioco;
    public static volatile SingularAttribute<Giocatore, Double> cosciaMaxDx;
    public static volatile SingularAttribute<Giocatore, String> email;
    public static volatile SingularAttribute<Giocatore, Double> altezza;
    public static volatile SingularAttribute<Giocatore, Double> uscitaAltaDx;
    public static volatile SingularAttribute<Giocatore, String> cellulare;
    public static volatile SingularAttribute<Giocatore, String> residenzaProvincia;
    public static volatile SingularAttribute<Giocatore, Double> cosciaMinDx;
    public static volatile SingularAttribute<Giocatore, Double> unoVSunoDx;
    public static volatile SingularAttribute<Giocatore, Double> cosciaMediaSx;
    public static volatile SingularAttribute<Giocatore, Double> presaSx;
    public static volatile SingularAttribute<Giocatore, String> ruolo;
    public static volatile SingularAttribute<Giocatore, Double> altezzaSeduto;
    public static volatile SingularAttribute<Giocatore, Stagione> stagione;

}