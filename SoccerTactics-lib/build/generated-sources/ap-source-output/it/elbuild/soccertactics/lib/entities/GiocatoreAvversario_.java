package it.elbuild.soccertactics.lib.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(GiocatoreAvversario.class)
public class GiocatoreAvversario_ { 

    public static volatile SingularAttribute<GiocatoreAvversario, String> ruolo;
    public static volatile SingularAttribute<GiocatoreAvversario, Date> dataNascita;
    public static volatile SingularAttribute<GiocatoreAvversario, String> cognome;
    public static volatile SingularAttribute<GiocatoreAvversario, String> nazionalita;
    public static volatile SingularAttribute<GiocatoreAvversario, String> giudizio;
    public static volatile SingularAttribute<GiocatoreAvversario, Integer> idAvversario;
    public static volatile SingularAttribute<GiocatoreAvversario, String> nome;
    public static volatile SingularAttribute<GiocatoreAvversario, Integer> id;

}