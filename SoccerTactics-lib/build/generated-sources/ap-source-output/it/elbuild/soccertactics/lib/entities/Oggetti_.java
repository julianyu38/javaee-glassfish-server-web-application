package it.elbuild.soccertactics.lib.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Oggetti.class)
public class Oggetti_ { 

    public static volatile SingularAttribute<Oggetti, String> path;
    public static volatile SingularAttribute<Oggetti, String> tipo;
    public static volatile SingularAttribute<Oggetti, String> categoria;
    public static volatile SingularAttribute<Oggetti, String> nome;
    public static volatile SingularAttribute<Oggetti, Integer> id;

}