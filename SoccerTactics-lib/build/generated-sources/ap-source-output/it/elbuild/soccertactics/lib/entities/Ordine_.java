package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.entities.Pacchetto;
import it.elbuild.soccertactics.lib.entities.Utente;
import it.elbuild.soccertactics.lib.entities.fatture.Fattura;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Ordine.class)
public class Ordine_ { 

    public static volatile SingularAttribute<Ordine, Utente> utente;
    public static volatile SingularAttribute<Ordine, Pacchetto> packEs;
    public static volatile SingularAttribute<Ordine, String> metodoPagamento;
    public static volatile SingularAttribute<Ordine, Double> totale;
    public static volatile SingularAttribute<Ordine, Integer> fatturaId;
    public static volatile SingularAttribute<Ordine, String> statoOrdine;
    public static volatile SingularAttribute<Ordine, Integer> idPackAbb;
    public static volatile SingularAttribute<Ordine, Pacchetto> packAbb;
    public static volatile SingularAttribute<Ordine, Date> dataOrdine;
    public static volatile SingularAttribute<Ordine, String> statoPaypal;
    public static volatile SingularAttribute<Ordine, Integer> idPackEs;
    public static volatile SingularAttribute<Ordine, String> descrizione;
    public static volatile SingularAttribute<Ordine, Fattura> fattura;
    public static volatile SingularAttribute<Ordine, String> idOrdine;
    public static volatile SingularAttribute<Ordine, Integer> id;
    public static volatile SingularAttribute<Ordine, Integer> idUtente;

}