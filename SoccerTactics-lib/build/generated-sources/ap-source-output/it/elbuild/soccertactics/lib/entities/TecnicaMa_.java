package it.elbuild.soccertactics.lib.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(TecnicaMa.class)
public class TecnicaMa_ { 

    public static volatile SingularAttribute<TecnicaMa, String> categoria3;
    public static volatile SingularAttribute<TecnicaMa, String> categoria2;
    public static volatile SingularAttribute<TecnicaMa, String> categoria1;
    public static volatile SingularAttribute<TecnicaMa, String> tecnica;
    public static volatile SingularAttribute<TecnicaMa, Integer> id;

}