package it.elbuild.soccertactics.lib.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Materiale.class)
public class Materiale_ { 

    public static volatile SingularAttribute<Materiale, String> nome;
    public static volatile SingularAttribute<Materiale, Integer> id;
    public static volatile SingularAttribute<Materiale, Integer> quantita;
    public static volatile SingularAttribute<Materiale, Integer> idEsercizio;

}