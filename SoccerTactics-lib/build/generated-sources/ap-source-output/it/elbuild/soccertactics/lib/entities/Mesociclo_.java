package it.elbuild.soccertactics.lib.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Mesociclo.class)
public class Mesociclo_ { 

    public static volatile SingularAttribute<Mesociclo, Double> coordinativo;
    public static volatile SingularAttribute<Mesociclo, Double> attacco;
    public static volatile SingularAttribute<Mesociclo, Integer> anno;
    public static volatile SingularAttribute<Mesociclo, Double> condizionale;
    public static volatile SingularAttribute<Mesociclo, Date> data;
    public static volatile SingularAttribute<Mesociclo, Integer> mese;
    public static volatile SingularAttribute<Mesociclo, Double> tattico;
    public static volatile SingularAttribute<Mesociclo, Integer> id;
    public static volatile SingularAttribute<Mesociclo, Integer> idUtente;
    public static volatile SingularAttribute<Mesociclo, Integer> idStagione;
    public static volatile SingularAttribute<Mesociclo, Double> difesa;

}