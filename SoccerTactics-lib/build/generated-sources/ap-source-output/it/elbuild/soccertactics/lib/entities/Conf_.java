package it.elbuild.soccertactics.lib.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Conf.class)
public class Conf_ { 

    public static volatile SingularAttribute<Conf, Integer> id;
    public static volatile SingularAttribute<Conf, String> label;
    public static volatile SingularAttribute<Conf, String> value;

}