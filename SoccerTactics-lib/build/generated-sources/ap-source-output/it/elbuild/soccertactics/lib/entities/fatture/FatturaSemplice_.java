package it.elbuild.soccertactics.lib.entities.fatture;

import it.elbuild.soccertactics.lib.entities.fatture.DocumentoTrasporto;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(FatturaSemplice.class)
public class FatturaSemplice_ extends Fattura_ {

    public static volatile SingularAttribute<FatturaSemplice, DocumentoTrasporto> documentoTrasporto;

}