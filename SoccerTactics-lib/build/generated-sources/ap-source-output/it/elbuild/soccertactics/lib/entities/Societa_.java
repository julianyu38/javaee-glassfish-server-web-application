package it.elbuild.soccertactics.lib.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Societa.class)
public class Societa_ { 

    public static volatile SingularAttribute<Societa, String> coloreIMaglia;
    public static volatile SingularAttribute<Societa, String> stemma;
    public static volatile SingularAttribute<Societa, String> campoAllenamento;
    public static volatile SingularAttribute<Societa, String> coloreIiMaglia;
    public static volatile SingularAttribute<Societa, String> nome;
    public static volatile SingularAttribute<Societa, Integer> id;
    public static volatile SingularAttribute<Societa, String> campoGara;
    public static volatile SingularAttribute<Societa, String> sedeSociale;
    public static volatile SingularAttribute<Societa, String> presidente;

}