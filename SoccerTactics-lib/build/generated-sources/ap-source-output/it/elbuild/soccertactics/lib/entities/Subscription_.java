package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.entities.Pacchetto;
import it.elbuild.soccertactics.lib.entities.Utente;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Subscription.class)
public class Subscription_ { 

    public static volatile SingularAttribute<Subscription, Utente> utente;
    public static volatile SingularAttribute<Subscription, Integer> maxUsers;
    public static volatile SingularAttribute<Subscription, Date> nextReset;
    public static volatile SingularAttribute<Subscription, Date> dataInserimento;
    public static volatile SingularAttribute<Subscription, Pacchetto> pacchetto;
    public static volatile SingularAttribute<Subscription, Boolean> active;
    public static volatile SingularAttribute<Subscription, Date> validUntil;
    public static volatile SingularAttribute<Subscription, Date> validFrom;
    public static volatile SingularAttribute<Subscription, Integer> id;
    public static volatile SingularAttribute<Subscription, Integer> pacchettoId;
    public static volatile SingularAttribute<Subscription, Integer> userId;
    public static volatile SingularAttribute<Subscription, Integer> currentUsers;

}