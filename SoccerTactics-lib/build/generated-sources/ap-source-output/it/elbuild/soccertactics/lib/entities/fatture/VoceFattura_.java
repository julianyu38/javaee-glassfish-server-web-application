package it.elbuild.soccertactics.lib.entities.fatture;

import it.elbuild.soccertactics.lib.entities.fatture.DocumentoFatturazione;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(VoceFattura.class)
public class VoceFattura_ { 

    public static volatile SingularAttribute<VoceFattura, String> codiceArticolo;
    public static volatile SingularAttribute<VoceFattura, Integer> ordine;
    public static volatile SingularAttribute<VoceFattura, Double> scontoImporto;
    public static volatile SingularAttribute<VoceFattura, String> descrizione;
    public static volatile SingularAttribute<VoceFattura, DocumentoFatturazione> documentoFatturazione;
    public static volatile SingularAttribute<VoceFattura, Double> prezzoUnitario;
    public static volatile SingularAttribute<VoceFattura, Integer> id;
    public static volatile SingularAttribute<VoceFattura, Integer> quantita;
    public static volatile SingularAttribute<VoceFattura, Double> sconto;

}