package it.elbuild.soccertactics.lib.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Stagione.class)
public class Stagione_ { 

    public static volatile SingularAttribute<Stagione, Date> inizio;
    public static volatile SingularAttribute<Stagione, Double> attacco;
    public static volatile SingularAttribute<Stagione, Double> coordinativo;
    public static volatile SingularAttribute<Stagione, Double> condizionale;
    public static volatile SingularAttribute<Stagione, Date> fine;
    public static volatile SingularAttribute<Stagione, Double> tattico;
    public static volatile SingularAttribute<Stagione, String> nome;
    public static volatile SingularAttribute<Stagione, Integer> id;
    public static volatile SingularAttribute<Stagione, Integer> idUtente;
    public static volatile SingularAttribute<Stagione, Double> difesa;

}