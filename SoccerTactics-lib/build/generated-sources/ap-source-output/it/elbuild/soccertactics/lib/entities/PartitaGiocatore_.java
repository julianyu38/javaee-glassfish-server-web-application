package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.entities.Giocatore;
import it.elbuild.soccertactics.lib.entities.ResocontoPG;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(PartitaGiocatore.class)
public class PartitaGiocatore_ { 

    public static volatile SingularAttribute<PartitaGiocatore, Integer> colPr;
    public static volatile ListAttribute<PartitaGiocatore, ResocontoPG> resoconti;
    public static volatile SingularAttribute<PartitaGiocatore, Integer> colPp;
    public static volatile SingularAttribute<PartitaGiocatore, Integer> colFf;
    public static volatile SingularAttribute<PartitaGiocatore, Integer> colFs;
    public static volatile SingularAttribute<PartitaGiocatore, Integer> colGs;
    public static volatile SingularAttribute<PartitaGiocatore, Integer> colE;
    public static volatile SingularAttribute<PartitaGiocatore, Giocatore> giocatore;
    public static volatile SingularAttribute<PartitaGiocatore, Integer> colA;
    public static volatile SingularAttribute<PartitaGiocatore, Integer> idPartita;
    public static volatile SingularAttribute<PartitaGiocatore, Integer> colTf;
    public static volatile SingularAttribute<PartitaGiocatore, Integer> colGf;
    public static volatile SingularAttribute<PartitaGiocatore, Double> colVoto;
    public static volatile SingularAttribute<PartitaGiocatore, Integer> idGiocatore;
    public static volatile SingularAttribute<PartitaGiocatore, Integer> id;
    public static volatile SingularAttribute<PartitaGiocatore, Integer> colTp;
    public static volatile SingularAttribute<PartitaGiocatore, Integer> colMing;

}