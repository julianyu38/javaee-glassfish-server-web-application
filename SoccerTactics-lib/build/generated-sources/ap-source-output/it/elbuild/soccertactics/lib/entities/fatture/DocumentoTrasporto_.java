package it.elbuild.soccertactics.lib.entities.fatture;

import it.elbuild.soccertactics.lib.entities.fatture.FatturaSemplice;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(DocumentoTrasporto.class)
public class DocumentoTrasporto_ extends DocumentoFatturazione_ {

    public static volatile SingularAttribute<DocumentoTrasporto, String> trasportatoDa;
    public static volatile SingularAttribute<DocumentoTrasporto, String> vettore;
    public static volatile SingularAttribute<DocumentoTrasporto, FatturaSemplice> fattura;
    public static volatile SingularAttribute<DocumentoTrasporto, String> resa;
    public static volatile SingularAttribute<DocumentoTrasporto, Double> peso;
    public static volatile SingularAttribute<DocumentoTrasporto, Integer> colli;
    public static volatile SingularAttribute<DocumentoTrasporto, String> aspetto;
    public static volatile SingularAttribute<DocumentoTrasporto, String> causaleTrasporto;
    public static volatile SingularAttribute<DocumentoTrasporto, String> noteTrasporto;
    public static volatile SingularAttribute<DocumentoTrasporto, Date> dataRitiro;

}