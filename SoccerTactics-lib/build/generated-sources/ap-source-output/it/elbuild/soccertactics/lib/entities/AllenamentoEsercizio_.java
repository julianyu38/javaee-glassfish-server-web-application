package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.entities.Esercizio;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(AllenamentoEsercizio.class)
public class AllenamentoEsercizio_ { 

    public static volatile SingularAttribute<AllenamentoEsercizio, String> fase;
    public static volatile SingularAttribute<AllenamentoEsercizio, Esercizio> esercizio;
    public static volatile SingularAttribute<AllenamentoEsercizio, Integer> idAllenamento;
    public static volatile SingularAttribute<AllenamentoEsercizio, Integer> id;
    public static volatile SingularAttribute<AllenamentoEsercizio, Integer> idEsercizio;

}