package it.elbuild.soccertactics.lib.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Pacchetto.class)
public class Pacchetto_ { 

    public static volatile SingularAttribute<Pacchetto, Integer> uid;
    public static volatile SingularAttribute<Pacchetto, String> descrizione;
    public static volatile SingularAttribute<Pacchetto, String> tipo;
    public static volatile SingularAttribute<Pacchetto, Integer> uid2;
    public static volatile SingularAttribute<Pacchetto, Double> prezzo;
    public static volatile SingularAttribute<Pacchetto, String> nome;
    public static volatile SingularAttribute<Pacchetto, Integer> id;

}