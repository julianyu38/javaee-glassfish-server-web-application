package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.entities.Societa;
import it.elbuild.soccertactics.lib.entities.Stagione;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Utente.class)
public class Utente_ { 

    public static volatile SingularAttribute<Utente, String> numCivico;
    public static volatile SingularAttribute<Utente, String> mansione;
    public static volatile SingularAttribute<Utente, String> cf;
    public static volatile SingularAttribute<Utente, Date> dataNascita;
    public static volatile SingularAttribute<Utente, String> cognome;
    public static volatile SingularAttribute<Utente, String> pv;
    public static volatile SingularAttribute<Utente, String> indirizzo;
    public static volatile SingularAttribute<Utente, String> nome;
    public static volatile SingularAttribute<Utente, Societa> societa;
    public static volatile SingularAttribute<Utente, String> luogoNascita;
    public static volatile SingularAttribute<Utente, Integer> idStagione;
    public static volatile SingularAttribute<Utente, Integer> idSocieta;
    public static volatile SingularAttribute<Utente, String> piva;
    public static volatile SingularAttribute<Utente, String> password;
    public static volatile SingularAttribute<Utente, String> ruolo;
    public static volatile SingularAttribute<Utente, Date> dataInserimento;
    public static volatile SingularAttribute<Utente, String> cap;
    public static volatile SingularAttribute<Utente, String> pswUtente;
    public static volatile SingularAttribute<Utente, Stagione> stagione;
    public static volatile SingularAttribute<Utente, String> ragioneSociale;
    public static volatile SingularAttribute<Utente, Integer> id;
    public static volatile SingularAttribute<Utente, String> email;
    public static volatile SingularAttribute<Utente, String> luogoResidenza;

}