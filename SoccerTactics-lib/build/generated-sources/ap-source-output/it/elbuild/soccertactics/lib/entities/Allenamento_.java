package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.entities.AllenamentoEsercizio;
import it.elbuild.soccertactics.lib.entities.Giocatore;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(Allenamento.class)
public class Allenamento_ { 

    public static volatile ListAttribute<Allenamento, AllenamentoEsercizio> esercizi;
    public static volatile SingularAttribute<Allenamento, String> titolo;
    public static volatile SingularAttribute<Allenamento, Boolean> eliminato;
    public static volatile ListAttribute<Allenamento, Giocatore> giocatori;
    public static volatile SingularAttribute<Allenamento, Integer> idStagione;
    public static volatile SingularAttribute<Allenamento, Date> dataInizio;
    public static volatile SingularAttribute<Allenamento, String> descrizione;
    public static volatile SingularAttribute<Allenamento, String> obFis;
    public static volatile SingularAttribute<Allenamento, String> obTec;
    public static volatile SingularAttribute<Allenamento, Date> dataFine;
    public static volatile SingularAttribute<Allenamento, String> obPsi;
    public static volatile SingularAttribute<Allenamento, Integer> id;
    public static volatile SingularAttribute<Allenamento, String> obTat;

}