package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.entities.GiocatoreAvversario;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(PartitaAvversario.class)
public class PartitaAvversario_ { 

    public static volatile SingularAttribute<PartitaAvversario, GiocatoreAvversario> giocatoreAvversario;
    public static volatile SingularAttribute<PartitaAvversario, Integer> idGiocatoreAvversario;
    public static volatile SingularAttribute<PartitaAvversario, Integer> colPr;
    public static volatile SingularAttribute<PartitaAvversario, Integer> colPp;
    public static volatile SingularAttribute<PartitaAvversario, Integer> colFf;
    public static volatile SingularAttribute<PartitaAvversario, Integer> colFs;
    public static volatile SingularAttribute<PartitaAvversario, Integer> colGs;
    public static volatile SingularAttribute<PartitaAvversario, Integer> colE;
    public static volatile SingularAttribute<PartitaAvversario, Integer> colA;
    public static volatile SingularAttribute<PartitaAvversario, Integer> idPartita;
    public static volatile SingularAttribute<PartitaAvversario, Integer> colTf;
    public static volatile SingularAttribute<PartitaAvversario, Integer> colGf;
    public static volatile SingularAttribute<PartitaAvversario, Double> colVoto;
    public static volatile SingularAttribute<PartitaAvversario, Integer> id;
    public static volatile SingularAttribute<PartitaAvversario, Integer> colTp;
    public static volatile SingularAttribute<PartitaAvversario, Integer> colMing;

}