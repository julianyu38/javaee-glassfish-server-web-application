package it.elbuild.soccertactics.lib.entities.fatture;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(IntestatarioFattura.class)
public class IntestatarioFattura_ { 

    public static volatile SingularAttribute<IntestatarioFattura, String> numCivico;
    public static volatile SingularAttribute<IntestatarioFattura, Date> dataNascita;
    public static volatile SingularAttribute<IntestatarioFattura, String> cognome;
    public static volatile SingularAttribute<IntestatarioFattura, String> pv;
    public static volatile SingularAttribute<IntestatarioFattura, String> indirizzo;
    public static volatile SingularAttribute<IntestatarioFattura, String> nome;
    public static volatile SingularAttribute<IntestatarioFattura, String> luogoNascita;
    public static volatile SingularAttribute<IntestatarioFattura, String> codiceFiscale;
    public static volatile SingularAttribute<IntestatarioFattura, String> cap;
    public static volatile SingularAttribute<IntestatarioFattura, String> partitaIva;
    public static volatile SingularAttribute<IntestatarioFattura, String> ragioneSociale;
    public static volatile SingularAttribute<IntestatarioFattura, Integer> id;
    public static volatile SingularAttribute<IntestatarioFattura, String> luogoResidenza;

}