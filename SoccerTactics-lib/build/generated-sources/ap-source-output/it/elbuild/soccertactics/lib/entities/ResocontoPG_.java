package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.entities.TecnicaMa;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(ResocontoPG.class)
public class ResocontoPG_ { 

    public static volatile SingularAttribute<ResocontoPG, Integer> ordine;
    public static volatile SingularAttribute<ResocontoPG, TecnicaMa> tecnicaMa;
    public static volatile SingularAttribute<ResocontoPG, Integer> idPartitaGiocatore;
    public static volatile SingularAttribute<ResocontoPG, Integer> totale;
    public static volatile SingularAttribute<ResocontoPG, Integer> idTecnicaMa;
    public static volatile SingularAttribute<ResocontoPG, Integer> tempoI;
    public static volatile SingularAttribute<ResocontoPG, Integer> id;
    public static volatile SingularAttribute<ResocontoPG, Integer> tempoIi;

}