package it.elbuild.soccertactics.lib.entities.fatture;

import it.elbuild.soccertactics.lib.entities.fatture.IntestatarioFattura;
import it.elbuild.soccertactics.lib.entities.fatture.VoceFattura;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-16T18:33:53")
@StaticMetamodel(DocumentoFatturazione.class)
public class DocumentoFatturazione_ { 

    public static volatile SingularAttribute<DocumentoFatturazione, String> note;
    public static volatile SingularAttribute<DocumentoFatturazione, String> bancaAppoggio;
    public static volatile SingularAttribute<DocumentoFatturazione, Boolean> pagato;
    public static volatile SingularAttribute<DocumentoFatturazione, Integer> numero;
    public static volatile SingularAttribute<DocumentoFatturazione, Date> data;
    public static volatile SingularAttribute<DocumentoFatturazione, Date> dataCreazione;
    public static volatile SingularAttribute<DocumentoFatturazione, Date> scadenza;
    public static volatile ListAttribute<DocumentoFatturazione, VoceFattura> voci;
    public static volatile SingularAttribute<DocumentoFatturazione, Double> speseSpedizione;
    public static volatile SingularAttribute<DocumentoFatturazione, String> modalitaPagamento;
    public static volatile SingularAttribute<DocumentoFatturazione, Double> sconto;
    public static volatile SingularAttribute<DocumentoFatturazione, String> stato;
    public static volatile SingularAttribute<DocumentoFatturazione, Boolean> usaPiva;
    public static volatile SingularAttribute<DocumentoFatturazione, Double> speseIncasso;
    public static volatile SingularAttribute<DocumentoFatturazione, Double> iva;
    public static volatile SingularAttribute<DocumentoFatturazione, IntestatarioFattura> intestatario;
    public static volatile SingularAttribute<DocumentoFatturazione, Integer> id;
    public static volatile SingularAttribute<DocumentoFatturazione, Date> dataUltimaModifica;
    public static volatile SingularAttribute<DocumentoFatturazione, String> riferimentoOrdine;
    public static volatile SingularAttribute<DocumentoFatturazione, String> nomePDF;

}