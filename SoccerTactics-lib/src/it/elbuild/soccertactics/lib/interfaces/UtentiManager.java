/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.interfaces;

import it.elbuild.soccertactics.lib.entities.Societa;
import it.elbuild.soccertactics.lib.entities.Utente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Ale
 */
@Local
public interface UtentiManager {
    
    public Utente findByEmail(String email);
    
    public Utente insertUpdateUtente(Utente utente);
    
    public List<Utente> findAllUtenti();
    
    public Utente findById(Integer idUtente);
    
    public Utente findByCF(String cf);
    
}
