/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.interfaces;

import it.elbuild.soccertactics.lib.entities.Avversario;
import it.elbuild.soccertactics.lib.entities.Partita;
import it.elbuild.soccertactics.lib.entities.TecnicaMa;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.faces.model.SelectItem;

/**
 *
 * @author Ale
 */
@Local
public interface PartiteManager {
    
    public List<Partita> findPartiteByDate(Date inizio, Date fine, Integer idSquadra);
    
    public Partita findById(Integer idPartita);
    
    public Partita insertUpdatePartita(Partita partita);   
    
    public List<TecnicaMa> findAllTecnicheMa();
    
    public List<Partita> findPartiteStagione(Integer idStagione);
    
    public List<SelectItem> findCategorie1();
    public List<SelectItem> findCategorie2(String categoria1);
    public List<SelectItem> findCategorie3(String categoria1, String categoria2);
    public List<SelectItem> findTecniche(String categoria1, String categoria2, String categoria3);
    public TecnicaMa findTecnicaMaByID(Integer tecid);
    public List<TecnicaMa> findByCategorie(String categoria1, String categoria2, String categoria3);
    
    public boolean deletePartita(Partita partita);
}
