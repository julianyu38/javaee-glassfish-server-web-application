/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.interfaces;

import it.elbuild.soccertactics.lib.entities.Mesociclo;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Ale
 */
@Local
public interface MesocicloManager {
    
    public List<Mesociclo> findAllMesocicli(Integer idStagione, Integer idUtente);
    
    public Mesociclo insertUpdateMesociclo(Mesociclo mesociclo);
    
    public void insertUpdateSettimanaMesociclo(List<Mesociclo> settimana);
    
    public List<Mesociclo> findByIdStagioneIdUtenteSettimanaAnno(Integer idStagione, Integer idUtente, Integer settimana, Integer anno);
    
    public Mesociclo findByIdStagioneIdUtenteMese(Integer idStagione, Integer idUtente, Integer mese, Integer anno);
    
    public Mesociclo findById(Integer id);
    
}
