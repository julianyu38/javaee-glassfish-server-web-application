/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.interfaces;

import it.elbuild.soccertactics.lib.entities.Esercizio;
import it.elbuild.soccertactics.lib.entities.Oggetti;
import it.elbuild.soccertactics.lib.utils.RicercaEsercizi;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Ale
 */
@Local
public interface EserciziManager {
    
    public Esercizio findById(Integer id);
    
    public Esercizio insertUpdateEsercizio(Esercizio esercizio);
    
    public List<Oggetti> findAllOggetti();
    
    public List<Esercizio> findEserciziUtente(Integer idutente);
    
    public List<Esercizio> findEserciziTitoloDescrizione(String text, Integer idUtente, List<Integer> uids);
    
    public List<Esercizio> ricercaAvanzataEsercizi(RicercaEsercizi criteri);
    
    public List<Esercizio> findByIdUtenti(List<Integer> uids);
}
