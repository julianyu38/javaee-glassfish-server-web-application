/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.interfaces;

import it.elbuild.soccertactics.lib.entities.Giocatore;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Ale
 */
@Local
public interface GiocatoriManager {
 
    public List<Giocatore> findAllGiocatori();
    
    public Giocatore insertUpdateGiocatore(Giocatore giocatore);
    
    public Giocatore findGiocatoreById(Integer idGiocatore);
    
//    public List<Giocatore> findGiocatoriByIdSquadra(Integer idSquadra);
    
    public List<Giocatore> findGiocatoriByIdStagione(Integer idStagione);
    
    public Boolean eliminaGiocatore(Giocatore giocatore);
    
}
