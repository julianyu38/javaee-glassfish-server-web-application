/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.interfaces;

import it.elbuild.soccertactics.lib.entities.Ordine;
import it.elbuild.soccertactics.lib.entities.Pacchetto;
import it.elbuild.soccertactics.lib.entities.Subscription;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Ale
 */
@Local
public interface PacchettiManager {
    
    public List<Pacchetto> findPacchettiEsercizi();
    
    public List<Pacchetto> findPacchettiAbbonamenti();
    
    public Ordine insertUpdateOrdine(Ordine ordine);
    
    public Subscription trovaSubscriptionAttiva(Integer idUtente, Integer idPacchetto);
    
    public Subscription insertUpdateSubscription(Subscription subscription);
    
    public Subscription trovaSubscriptionAttiva(Integer idUtente);
    
    public List<Ordine> findAllOrdini();
    
    public List<Subscription> findAllSubscriptions();
    
    public List<Subscription> findAllSubscriptionUser(Integer idUtente);
    
    public List<Integer> findPacchettiEserciziUtente(Integer uid);
    
    public List<Integer> findUidsPacchettiEserciziUtente(Integer uid);
}
