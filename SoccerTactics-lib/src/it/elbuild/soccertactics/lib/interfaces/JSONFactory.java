/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.interfaces;

import it.elbuild.soccertactics.lib.esercizio.model.ElementoEx;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Ale
 */
@Local
public interface JSONFactory {
    
    public String toJSON(List<ElementoEx> ex);
    
    public List<ElementoEx> toList(String json);
    
    public String toJSON(Object o);
    
}