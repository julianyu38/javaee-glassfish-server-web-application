/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.interfaces.fatture;

import it.elbuild.soccertactics.lib.entities.fatture.DocumentoFatturazione;
import it.elbuild.soccertactics.lib.entities.fatture.Fattura;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Ale
 */
@Local
public interface GestoreFatture {
    
    Fattura inserisciOAggiornaFattura(Fattura f);

    public java.lang.Integer trovaUltimoNumeroFattura(Date date) throws Exception;

    public Fattura emettiFattura(Fattura f) throws Exception;

    public List<Fattura> trovaTutteFatture();

    public void eliminaDocumentoFatturazione(DocumentoFatturazione f);

    public List<Fattura> trovaUltimeFatture(int i);

    public boolean numeroFatturaInUso(Integer numero) throws Exception;
    
    public OutputStream generaPDF(DocumentoFatturazione f, String templatePath, String  savePath) throws Exception;
 
    public List<Fattura> findFattureDate(Date start, Date stop);
    
}
