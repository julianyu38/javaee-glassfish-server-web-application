/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.interfaces;

import it.elbuild.soccertactics.lib.entities.Staff;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Ale
 */
@Local
public interface StaffManager {
    
    public List<Staff> findStaffStagione(Integer idStagione);
    
    public Staff insertupdateStaff(Staff staff);
    
    public Staff findStaffById(Integer idStaff);
    
}
