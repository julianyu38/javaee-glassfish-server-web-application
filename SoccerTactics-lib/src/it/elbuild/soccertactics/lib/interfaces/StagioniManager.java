/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.interfaces;

import it.elbuild.soccertactics.lib.entities.Societa;
import it.elbuild.soccertactics.lib.entities.Stagione;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Ale
 */
@Local
public interface StagioniManager {
    
    public List<Stagione> findAllStagioni();
    
    public Stagione insertUpdateStagione(Stagione stagione);
    
    public Societa findSocietaById(Integer id);
    
    public Societa insertUpdateSocieta(Societa societa);
    
    public List<Societa> findAllSocieta();
    
}
