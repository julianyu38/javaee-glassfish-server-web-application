/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.interfaces;

import it.elbuild.soccertactics.lib.entities.Condivisione;
import it.elbuild.soccertactics.lib.entities.Ordine;
import it.elbuild.soccertactics.lib.entities.Subscription;
import it.elbuild.soccertactics.lib.entities.Utente;
import javax.ejb.Local;

/**
 *
 * @author Ale
 */
@Local
public interface NotificheManager {
    
    public void inviaMailCondivisione(Condivisione condivisione, String url) throws Exception;
    
    public void inviaEmailPagamentoRicevuto(Ordine ordine, Utente utente) throws Exception;
    
    public void inviaMailScadenzaAbbonamento(Subscription subscription) throws Exception;
    
    
    public void mailEsempio() throws Exception;
    
}
