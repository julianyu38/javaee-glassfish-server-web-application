/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.interfaces;

import it.elbuild.soccertactics.lib.entities.Condivisione;
import javax.ejb.Local;

/**
 *
 * @author Ale
 */
@Local
public interface CondivisioniManager {
    
    public Condivisione findCondivisione(String token);
    public Condivisione inserisciAggiornaCondivisione(Condivisione condivisione);
    
}
