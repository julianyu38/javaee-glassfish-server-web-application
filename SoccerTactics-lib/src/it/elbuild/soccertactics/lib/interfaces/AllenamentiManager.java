/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.interfaces;

import it.elbuild.soccertactics.lib.entities.Allenamento;
import it.elbuild.soccertactics.lib.utils.RicercaAllenamento;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Ale
 */
@Local
public interface AllenamentiManager {
    
    public List<Allenamento> findAllenamentiByDate(Date inizio, Date fine, Integer idStagione);
    
    public Allenamento findById(Integer idAllenamento);
    
    public Allenamento insertUpdateAllenamento(Allenamento allenamento);
    
    public List<Allenamento> findAllenamentiStagione(Integer idStagione);
    
    public List<Allenamento> findByMeseAnno(Integer idStagione, Integer mese, Integer anno);
    
    public List<Allenamento> ricercaAvanzataAllenamenti(RicercaAllenamento re);
    
}
