/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.interfaces;

import it.elbuild.soccertactics.lib.entities.Avversario;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Ale
 */
@Local
public interface AvversariManager {
    
    public List<Avversario> findAvversariStagione(Integer idStagione);
    
    public Avversario insertUpdateAvversario(Avversario avversario);
    
    public Avversario findById(Integer idAvversario);
    
}
