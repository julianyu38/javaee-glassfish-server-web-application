/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.email.utils;


import java.io.File;
import java.io.InputStream;
import javax.faces.context.FacesContext;

/**
 *
 * @author Eve
 */
public class EmailText {
    
    public static String getTestoFromFile(String path){
               String t= FileTextUtil.getContents(new File(path)).replace("'", "''");
               
               t=t.replace("{", "'{'");
                  t=t.replace("}", "'}'");
                  
                  
                  t=t.replace("*'{'", "{");
                  t=t.replace("'}'*", "}");
               
               
               return t;
    }
    
    public static String getTesto(String path){
                InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream(path);
               String t= FileTextUtil.getContents(stream).replace("'", "''");
               
               t=t.replace("{", "'{'");
                  t=t.replace("}", "'}'");
                  
                  
                  t=t.replace("*'{'", "{");
                  t=t.replace("'}'*", "}");
               
               
               return t;
    }
    
    public static String getTesto(InputStream stream){
               String t= FileTextUtil.getContents(stream).replace("'", "''");
               
               t=t.replace("{", "'{'");
               t=t.replace("}", "'}'");
                  
               t=t.replace("*'{'", "{");
               t=t.replace("'}'*", "}");
               
               
               return t;
    }
    
}
