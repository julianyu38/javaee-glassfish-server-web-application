/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.esercizio.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ale
 */
public class ElementoEx {
    
//    public static String IMMAGINE = ""; // nome della classe dell'immagine 
    public static String LINEA = "linea"; 
    public static String CURVA = "curva"; 
    public static String LINEDASHED = "linedashed"; 
    public static String IMGCUSTOM = "imgcustom";
    
    private String tipo;
    private String file; // per tenere il nome delle immagini dei portieri
    private List<Point> punti;

    public ElementoEx(String tipo, List<Point> punti) {
        this.tipo = tipo;
        this.punti = punti;
    }
    
    public ElementoEx(String tipo, Double x, Double y) {
        this.tipo = tipo;
        punti = new ArrayList<Point>();
        punti.add(new Point(x, y));
    }
    
    public ElementoEx(String tipo, Double x0, Double y0, Double x1, Double y1) {
        this.tipo = tipo;
        punti = new ArrayList<Point>();
        punti.add(new Point(x0, y0));
        punti.add(new Point(x1, y1));
    }
    
    public ElementoEx(String tipo, Double x0, Double y0, Double x1, Double y1, Double x2, Double y2) {
        this.tipo = tipo;
        punti = new ArrayList<Point>();
        punti.add(new Point(x0, y0));
        punti.add(new Point(x1, y1));
        punti.add(new Point(x2, y2));
    }

    public ElementoEx(String tipo, Double x0, Double y0, String file) {
        this.tipo = tipo;
        this.file = file;
        punti = new ArrayList<Point>();
        punti.add(new Point(x0, y0));
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List<Point> getPunti() {
        return punti;
    }

    public void setPunti(List<Point> punti) {
        this.punti = punti;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    
}
