/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "RESOCONTO_P_G")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ResocontoPG.findAll", query = "SELECT r FROM ResocontoPG r"),
    @NamedQuery(name = "ResocontoPG.findById", query = "SELECT r FROM ResocontoPG r WHERE r.id = :id"),
    @NamedQuery(name = "ResocontoPG.findByIdPartitaGiocatore", query = "SELECT r FROM ResocontoPG r WHERE r.idPartitaGiocatore = :idPartitaGiocatore"),
    @NamedQuery(name = "ResocontoPG.findByIdTecnicaMa", query = "SELECT r FROM ResocontoPG r WHERE r.idTecnicaMa = :idTecnicaMa"),
    @NamedQuery(name = "ResocontoPG.findByTempoI", query = "SELECT r FROM ResocontoPG r WHERE r.tempoI = :tempoI"),
    @NamedQuery(name = "ResocontoPG.findByTempoIi", query = "SELECT r FROM ResocontoPG r WHERE r.tempoIi = :tempoIi"),
    @NamedQuery(name = "ResocontoPG.findByTotale", query = "SELECT r FROM ResocontoPG r WHERE r.totale = :totale")})
public class ResocontoPG implements Serializable {
    
    // dettagli resoconto partita giocatore
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ID_PARTITA_GIOCATORE")
    private Integer idPartitaGiocatore;
    @Column(name = "ID_TECNICA_MA")
    private Integer idTecnicaMa;
    @Column(name = "TEMPO_I")
    private Integer tempoI;
    @Column(name = "TEMPO_II")
    private Integer tempoIi;
    @Column(name = "TOTALE")
    private Integer totale;
    @Column(name = "ORDINE")
    private Integer ordine;
    
    @OneToOne
    @JoinColumn(name="ID_TECNICA_MA", referencedColumnName="ID", insertable=false, updatable=false)
    private TecnicaMa tecnicaMa;

    public ResocontoPG() {
    }

    public ResocontoPG(Integer idPartitaGiocatore, TecnicaMa tecnicaMa, int ordine) {
        this.idPartitaGiocatore = idPartitaGiocatore;
        this.tecnicaMa = tecnicaMa;
        idTecnicaMa = tecnicaMa.getId();
        this.ordine = ordine;
    }

    public ResocontoPG(Integer id) {
        this.id = id;
    }
    
    public void addTempoI(){
        if(tempoI==null){
            tempoI = 1;
        }else{
            tempoI++;
        }
    }
    
    public void removeTempoI(){
        if(tempoI!=null && tempoI >0){
            tempoI--;
        }
    }
    
    public void addTempoII(){
        if(tempoIi==null){
            tempoIi = 1;
        }else{
            tempoIi++;
        }
    }
    
    public void removeTempoII(){
        if(tempoIi!=null && tempoIi >0){
            tempoIi--;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPartitaGiocatore() {
        return idPartitaGiocatore;
    }

    public void setIdPartitaGiocatore(Integer idPartitaGiocatore) {
        this.idPartitaGiocatore = idPartitaGiocatore;
    }

    public Integer getIdTecnicaMa() {
        return idTecnicaMa;
    }

    public void setIdTecnicaMa(Integer idTecnicaMa) {
        this.idTecnicaMa = idTecnicaMa;
    }

    public Integer getTempoI() {
        return tempoI;
    }

    public void setTempoI(Integer tempoI) {
        this.tempoI = tempoI;
    }

    public Integer getTempoIi() {
        return tempoIi;
    }

    public void setTempoIi(Integer tempoIi) {
        this.tempoIi = tempoIi;
    }

    public Integer getTotale() {
        totale = 0;
        if(tempoI!=null){
            totale = totale +tempoI;
        }
        if(tempoIi!=null){
            totale = totale + tempoIi;
        }
        return totale;
    }

    public void setTotale(Integer totale) {
        this.totale = totale;
    }

    public TecnicaMa getTecnicaMa() {
        return tecnicaMa;
    }

    public void setTecnicaMa(TecnicaMa tecnicaMa) {
        this.tecnicaMa = tecnicaMa;
    }

    public Integer getOrdine() {
        return ordine;
    }

    public void setOrdine(Integer ordine) {
        this.ordine = ordine;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ResocontoPG)) {
            return false;
        }
        ResocontoPG other = (ResocontoPG) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.ResocontoPG[ id=" + id + " ]";
    }
    
}
