/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "SUBSCRIPTION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Subscription.findAll", query = "SELECT s FROM Subscription s WHERE s.active = '1' ORDER BY s.dataInserimento DESC"),
    @NamedQuery(name = "Subscription.findById", query = "SELECT s FROM Subscription s WHERE s.id = :id AND s.active = '1'"),
    // trova abbonamenti attivi dell'utente (anche di tipo free)
    @NamedQuery(name = "Subscription.findAbbonamentiCorrentiUserPacchetto", query = "SELECT s FROM Subscription s WHERE s.userId = :userId AND s.pacchettoId = :pacchettoId AND s.validUntil> :data AND s.active = '1'"),
    @NamedQuery(name = "Subscription.findAbbonamentiCorrentiUser", query = "SELECT s FROM Subscription s WHERE s.userId = :userId AND s.validUntil> :data AND s.active = '1'"),
    // trova abbonamneti attivi e non free per inviare eventualmente mail rinnovo abbonamneto
    @NamedQuery(name = "Subscription.findAbbonamentiAttivi", query = "SELECT s FROM Subscription s WHERE s.validUntil> :data AND s.active = '1'"),
    @NamedQuery(name = "Subscription.findByUserId", query = "SELECT s FROM Subscription s WHERE s.userId = :userId AND s.active = '1'"),
    @NamedQuery(name = "Subscription.findByPacchettoId", query = "SELECT s FROM Subscription s WHERE s.pacchettoId = :pacchettoId AND s.active = '1'"),
    @NamedQuery(name = "Subscription.findByValidFrom", query = "SELECT s FROM Subscription s WHERE s.validFrom = :validFrom AND s.active = '1'"),
    @NamedQuery(name = "Subscription.findByValidUntil", query = "SELECT s FROM Subscription s WHERE s.validUntil = :validUntil AND s.active = '1'"),
    @NamedQuery(name = "Subscription.findByNextReset", query = "SELECT s FROM Subscription s WHERE s.nextReset = :nextReset AND s.active = '1'"),
    @NamedQuery(name = "Subscription.findByMaxUsers", query = "SELECT s FROM Subscription s WHERE s.maxUsers = :maxUsers AND s.active = '1'"),
    @NamedQuery(name = "Subscription.findByCurrentUsers", query = "SELECT s FROM Subscription s WHERE s.currentUsers = :currentUsers AND s.active = '1'"),
    @NamedQuery(name = "Subscription.findByActive", query = "SELECT s FROM Subscription s WHERE s.active = :active")})
public class Subscription implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "USER_ID")
    private Integer userId;
    @Column(name = "PACCHETTO_ID")
    private Integer pacchettoId;
    @Column(name = "VALID_FROM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validFrom;
    @Column(name = "VALID_UNTIL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validUntil;
    @Column(name = "NEXT_RESET")
    @Temporal(TemporalType.TIMESTAMP)
    private Date nextReset;
    @Column(name = "MAX_USERS")
    private Integer maxUsers;
    @Column(name = "CURRENT_USERS")
    private Integer currentUsers;
    @Column(name = "ACTIVE")
    private Boolean active;
    @Column(name = "DATA_INSERIMENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInserimento;
    
    @OneToOne
    @JoinColumn(name="PACCHETTO_ID", referencedColumnName="ID", insertable=false, updatable=false)
    private Pacchetto pacchetto;
    
    @OneToOne
    @JoinColumn(name="USER_ID", referencedColumnName="ID", insertable=false, updatable=false)
    private Utente utente;

    public Subscription() {
    }

    public Subscription(Integer id) {
        this.id = id;
    }
    
    public Subscription(Integer idUtente , Pacchetto p, int MAX_USERS, Date date) {
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("CET"), Locale.ITALIAN);
        c.setTime(date);
        userId = idUtente;
        pacchettoId = p.getId();
        pacchetto = p;
        validFrom = c.getTime();
        maxUsers = MAX_USERS;
        active = true;
        dataInserimento = Calendar.getInstance(TimeZone.getTimeZone("CET")).getTime();
        // abbonamento sempre annuale
        c.add(Calendar.YEAR, 1);
        validUntil = c.getTime();
        nextReset = c.getTime();
    }
    
    public boolean accessoAreaTecnica(){
        return !pacchetto.getNome().equals("DISEGNO ESERCIZI");
    }
    
    public boolean accessoAreaGestionale(){
        return pacchetto.getNome().equals("VERSIONE COMPLETA");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPacchettoId() {
        return pacchettoId;
    }

    public void setPacchettoId(Integer pacchettoId) {
        this.pacchettoId = pacchettoId;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    public Date getNextReset() {
        return nextReset;
    }

    public void setNextReset(Date nextReset) {
        this.nextReset = nextReset;
    }

    public Integer getMaxUsers() {
        return maxUsers;
    }

    public void setMaxUsers(Integer maxUsers) {
        this.maxUsers = maxUsers;
    }

    public Integer getCurrentUsers() {
        return currentUsers;
    }

    public void setCurrentUsers(Integer currentUsers) {
        this.currentUsers = currentUsers;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Pacchetto getPacchetto() {
        return pacchetto;
    }

    public void setPacchetto(Pacchetto pacchetto) {
        this.pacchetto = pacchetto;
    }

    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    public Date getDataInserimento() {
        return dataInserimento;
    }

    public void setDataInserimento(Date dataInserimento) {
        this.dataInserimento = dataInserimento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Subscription)) {
            return false;
        }
        Subscription other = (Subscription) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.Subscription[ id=" + id + " ]";
    }
    
}
