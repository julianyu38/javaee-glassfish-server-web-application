 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "PARTITA_GIOCATORE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PartitaGiocatore.findAll", query = "SELECT p FROM PartitaGiocatore p"),
    @NamedQuery(name = "PartitaGiocatore.findById", query = "SELECT p FROM PartitaGiocatore p WHERE p.id = :id"),
    @NamedQuery(name = "PartitaGiocatore.findByIdPartita", query = "SELECT p FROM PartitaGiocatore p WHERE p.idPartita = :idPartita"),
    @NamedQuery(name = "PartitaGiocatore.findByIdGiocatore", query = "SELECT p FROM PartitaGiocatore p WHERE p.idGiocatore = :idGiocatore"),
    @NamedQuery(name = "PartitaGiocatore.findByColGf", query = "SELECT p FROM PartitaGiocatore p WHERE p.colGf = :colGf"),
    @NamedQuery(name = "PartitaGiocatore.findByColGs", query = "SELECT p FROM PartitaGiocatore p WHERE p.colGs = :colGs"),
    @NamedQuery(name = "PartitaGiocatore.findByColTp", query = "SELECT p FROM PartitaGiocatore p WHERE p.colTp = :colTp"),
    @NamedQuery(name = "PartitaGiocatore.findByColTf", query = "SELECT p FROM PartitaGiocatore p WHERE p.colTf = :colTf"),
    @NamedQuery(name = "PartitaGiocatore.findByColA", query = "SELECT p FROM PartitaGiocatore p WHERE p.colA = :colA"),
    @NamedQuery(name = "PartitaGiocatore.findByColE", query = "SELECT p FROM PartitaGiocatore p WHERE p.colE = :colE"),
    @NamedQuery(name = "PartitaGiocatore.findByColPp", query = "SELECT p FROM PartitaGiocatore p WHERE p.colPp = :colPp"),
    @NamedQuery(name = "PartitaGiocatore.findByColPr", query = "SELECT p FROM PartitaGiocatore p WHERE p.colPr = :colPr"),
    @NamedQuery(name = "PartitaGiocatore.findByColFf", query = "SELECT p FROM PartitaGiocatore p WHERE p.colFf = :colFf"),
    @NamedQuery(name = "PartitaGiocatore.findByColFs", query = "SELECT p FROM PartitaGiocatore p WHERE p.colFs = :colFs"),
    @NamedQuery(name = "PartitaGiocatore.findByColMing", query = "SELECT p FROM PartitaGiocatore p WHERE p.colMing = :colMing"),
    @NamedQuery(name = "PartitaGiocatore.findByColVoto", query = "SELECT p FROM PartitaGiocatore p WHERE p.colVoto = :colVoto")})
public class PartitaGiocatore implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ID_PARTITA")
    private Integer idPartita;
    @Column(name = "ID_GIOCATORE")
    private Integer idGiocatore;
    @Column(name = "COL_GF")
    private Integer colGf;
    @Column(name = "COL_GS")
    private Integer colGs;
    @Column(name = "COL_TP")
    private Integer colTp;
    @Column(name = "COL_TF")
    private Integer colTf;
    @Column(name = "COL_A")
    private Integer colA;
    @Column(name = "COL_E")
    private Integer colE;
    @Column(name = "COL_PP")
    private Integer colPp;
    @Column(name = "COL_PR")
    private Integer colPr;
    @Column(name = "COL_FF")
    private Integer colFf;
    @Column(name = "COL_FS")
    private Integer colFs;
    @Column(name = "COL_MING")
    private Integer colMing;
    @Column(name = "COL_VOTO")
    private Double colVoto;
    
    @OneToOne
    @JoinColumn(name="ID_GIOCATORE", referencedColumnName="ID", insertable=false, updatable=false)
    private Giocatore giocatore;
    
    @OneToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
    @JoinColumn(name = "ID_PARTITA_GIOCATORE", referencedColumnName = "ID")   
    @OrderBy("ordine")
    private List<ResocontoPG> resoconti;
    
    public PartitaGiocatore() {
    }

    public PartitaGiocatore(Giocatore giocatore) {
        this.giocatore = giocatore;
        idGiocatore = giocatore.getId();
    }

    public PartitaGiocatore(Integer id) {
        this.id = id;
    }
    
    public void addResoconto(TecnicaMa tecnica){
        if(resoconti == null){
            resoconti = new ArrayList<>();
        }
        resoconti.add(new ResocontoPG(id, tecnica, resoconti.size()));
    }
    
    public void addResoconti(List<TecnicaMa> tecniche){
        int i = 0;
        if(resoconti == null){
            resoconti = new ArrayList<ResocontoPG>();
        }
        for(TecnicaMa t : tecniche){
            resoconti.add(new ResocontoPG(id, t, i));
            i++;
        }
    }
    
    public boolean inserisciTitoloCategoriaI(ResocontoPG r){
        int index = resoconti.indexOf(r);
        if(index == 0 || !resoconti.get(index).getTecnicaMa().getCategoria1().equals(resoconti.get(index-1).getTecnicaMa().getCategoria1())){
            return true;
        }else{
            return false;
        }
    }
    
    public boolean inserisciTitoloCategoriaII(ResocontoPG r){
        int index = resoconti.indexOf(r);
        if(index == 0 || !resoconti.get(index).getTecnicaMa().getCategoria2().equals(resoconti.get(index-1).getTecnicaMa().getCategoria2())){
            return true;
        }else{
            return false;
        }
    }
    
    public int colonneCategoriaII(ResocontoPG r){
        int index = resoconti.indexOf(r)+1;
        int count = 1;
        while(index<resoconti.size() && resoconti.get(index).getTecnicaMa().getCategoria2().equals(resoconti.get(index-1).getTecnicaMa().getCategoria2())){
            count++;
            index++;
        }
        return count;
    }
    
    public boolean inserisciTitoloCategoriaIII(ResocontoPG r){
        int index = resoconti.indexOf(r);
        if(index == 0 || !resoconti.get(index).getTecnicaMa().getCategoria3().equals(resoconti.get(index-1).getTecnicaMa().getCategoria3())){
            return true;
        }else{
            return false;
        }
    }
    
    public int colonneCategoriaIII(ResocontoPG r){
        int index = resoconti.indexOf(r)+1;
        int count = 1;
        while(index<resoconti.size() && resoconti.get(index).getTecnicaMa().getCategoria3().equals(resoconti.get(index-1).getTecnicaMa().getCategoria3())){
            count++;
            index++;
        }
        return count;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPartita() {
        return idPartita;
    }

    public void setIdPartita(Integer idPartita) {
        this.idPartita = idPartita;
    }

    public Integer getIdGiocatore() {
        return idGiocatore;
    }

    public void setIdGiocatore(Integer idGiocatore) {
        this.idGiocatore = idGiocatore;
    }

    public Integer getColGf() {
        return colGf;
    }

    public void setColGf(Integer colGf) {
        this.colGf = colGf;
    }

    public Integer getColGs() {
        return colGs;
    }

    public void setColGs(Integer colGs) {
        this.colGs = colGs;
    }

    public Integer getColTp() {
        return colTp;
    }

    public void setColTp(Integer colTp) {
        this.colTp = colTp;
    }

    public Integer getColTf() {
        return colTf;
    }

    public void setColTf(Integer colTf) {
        this.colTf = colTf;
    }

    public Integer getColA() {
        return colA;
    }

    public void setColA(Integer colA) {
        this.colA = colA;
    }

    public Integer getColE() {
        return colE;
    }

    public void setColE(Integer colE) {
        this.colE = colE;
    }

    public Integer getColPp() {
        return colPp;
    }

    public void setColPp(Integer colPp) {
        this.colPp = colPp;
    }

    public Integer getColPr() {
        return colPr;
    }

    public void setColPr(Integer colPr) {
        this.colPr = colPr;
    }

    public Integer getColFf() {
        return colFf;
    }

    public void setColFf(Integer colFf) {
        this.colFf = colFf;
    }

    public Integer getColFs() {
        return colFs;
    }

    public void setColFs(Integer colFs) {
        this.colFs = colFs;
    }

    public Integer getColMing() {
        return colMing;
    }

    public void setColMing(Integer colMing) {
        this.colMing = colMing;
    }

    public Double getColVoto() {
        return colVoto;
    }

    public void setColVoto(Double colVoto) {
        this.colVoto = colVoto;
    }

    public Giocatore getGiocatore() {
        return giocatore;
    }

    public void setGiocatore(Giocatore giocatore) {
        this.giocatore = giocatore;
    }

    public List<ResocontoPG> getResoconti() {
        return resoconti;
    }

    public void setResoconti(List<ResocontoPG> resoconti) {
        this.resoconti = resoconti;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PartitaGiocatore)) {
            return false;
        }
        PartitaGiocatore other = (PartitaGiocatore) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.PartitaGiocatore[ id=" + id + " ]";
    }
    
}
