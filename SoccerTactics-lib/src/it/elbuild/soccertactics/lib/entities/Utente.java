/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "UTENTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Utente.findAll", query = "SELECT u FROM Utente u WHERE u.ruolo != :ruolo"),
    @NamedQuery(name = "Utente.findById", query = "SELECT u FROM Utente u WHERE u.id = :id AND u.ruolo != :ruolo"),
    @NamedQuery(name = "Utente.findByCF", query = "SELECT u FROM Utente u WHERE u.cf = :cf AND u.ruolo != :ruolo"),
    @NamedQuery(name = "Utente.findByNome", query = "SELECT u FROM Utente u WHERE u.nome = :nome"),
    @NamedQuery(name = "Utente.findByCognome", query = "SELECT u FROM Utente u WHERE u.cognome = :cognome"),
    @NamedQuery(name = "Utente.findByEmail", query = "SELECT u FROM Utente u WHERE u.email = :email AND u.ruolo != :ruolo"),
    @NamedQuery(name = "Utente.findByPassword", query = "SELECT u FROM Utente u WHERE u.password = :password"),
    @NamedQuery(name = "Utente.findByRuolo", query = "SELECT u FROM Utente u WHERE u.ruolo = :ruolo"),
    @NamedQuery(name = "Utente.findByMansione", query = "SELECT u FROM Utente u WHERE u.mansione = :mansione")})
public class Utente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "DATA_INSERIMENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInserimento;
    @Column(name = "NOME")
    private String nome;
    @Size(max = 256)
    @Column(name = "COGNOME")
    private String cognome;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 256)
    @Column(name = "EMAIL")
    private String email;
    @Size(max = 256)
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "PSW_UTENTE")
    private String pswUtente;
    @Size(max = 256)
    @Column(name = "RUOLO")
    private String ruolo;
    @Size(max = 256)
    @Column(name = "MANSIONE")
    private String mansione;
    @Column(name = "ID_SOCIETA", insertable=false, updatable=false)
    private Integer idSocieta;
    @Column(name = "ID_STAGIONE")
    private Integer idStagione;
    
    @Size(max = 256)
    @Column(name = "RAGIONE_SOCIALE")
    private String ragioneSociale;
    @Size(max = 256)
    @Column(name = "LUOGO_NASCITA")
    private String luogoNascita;
    @Column(name = "DATA_NASCITA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataNascita;
    @Size(max = 256)
    @Column(name = "LUOGO_RESIDENZA")
    private String luogoResidenza;
    @Size(max = 50)
    @Column(name = "CAP")
    private String cap;
    @Size(max = 50)
    @Column(name = "PROVINCIA")
    private String pv;
    @Size(max = 256)
    @Column(name = "INDIRIZZO")
    private String indirizzo;
    @Size(max = 50)
    @Column(name = "NUM_CIVICO")
    private String numCivico;
    @Size(max = 256)
    @Column(name = "CODICE_FISCALE")
    private String cf;
    @Size(max = 100)
    @Column(name = "PARTITA_IVA")
    private String piva;
    
    
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="ID_SOCIETA", referencedColumnName="ID", updatable=true)
    private Societa societa; 
    
    @OneToOne
    @JoinColumn(name="ID_STAGIONE", referencedColumnName="ID", insertable=false, updatable=false)
    private Stagione stagione;

    public Utente() {
        societa = new Societa();
    }

    public Utente(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRuolo() {
        return ruolo;
    }

    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }

    public String getMansione() {
        return mansione;
    }

    public void setMansione(String mansione) {
        this.mansione = mansione;
    }

    public Integer getIdSocieta() {
        return idSocieta;
    }

    public void setIdSocieta(Integer idSocieta) {
        this.idSocieta = idSocieta;
    }

    public Integer getIdStagione() {
        return idStagione;
    }

    public void setIdStagione(Integer idStagione) {
        this.idStagione = idStagione;
    }

    public Stagione getStagione() {
        return stagione;
    }

    public void setStagione(Stagione stagione) {
        this.stagione = stagione;
    }

    public Date getDataInserimento() {
        return dataInserimento;
    }

    public void setDataInserimento(Date dataInserimento) {
        this.dataInserimento = dataInserimento;
    }

    public Societa getSocieta() {
        return societa;
    }

    public void setSocieta(Societa societa) {
        this.societa = societa;
    }

    public String getRagioneSociale() {
        return ragioneSociale;
    }

    public void setRagioneSociale(String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
    }

    public String getLuogoNascita() {
        return luogoNascita;
    }

    public void setLuogoNascita(String luogoNascita) {
        this.luogoNascita = luogoNascita;
    }

    public Date getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(Date dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getLuogoResidenza() {
        return luogoResidenza;
    }

    public void setLuogoResidenza(String luogoResidenza) {
        this.luogoResidenza = luogoResidenza;
    }

    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getPv() {
        return pv;
    }

    public void setPv(String pv) {
        this.pv = pv;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getNumCivico() {
        return numCivico;
    }

    public void setNumCivico(String numCivico) {
        this.numCivico = numCivico;
    }

    public String getCf() {
        return cf;
    }

    public void setCf(String cf) {
        this.cf = cf;
    }

    public String getPswUtente() {
        return pswUtente;
    }

    public void setPswUtente(String pswUtente) {
        this.pswUtente = pswUtente;
    }

    public String getPiva() {
        return piva;
    }

    public void setPiva(String piva) {
        this.piva = piva;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Utente)) {
            return false;
        }
        Utente other = (Utente) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.Utente[ id=" + id + " ]";
    }
    
}
