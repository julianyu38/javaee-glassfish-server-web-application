/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "PARTITA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Partita.findAll", query = "SELECT p FROM Partita p"),
    @NamedQuery(name = "Partita.findById", query = "SELECT p FROM Partita p WHERE p.id = :id"),
    @NamedQuery(name = "Partita.findByIdAvversario", query = "SELECT p FROM Partita p WHERE p.idAvversario = :idAvversario"),
    @NamedQuery(name = "Partita.findByIdStagione", query = "SELECT p FROM Partita p WHERE p.idStagione = :idStagione"),
    @NamedQuery(name = "Partita.findByDate", query = "SELECT p FROM Partita p WHERE (p.dataInizio BETWEEN :inizio AND :fine) AND p.idStagione = :idStagione")})
public class Partita implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "DATA_INIZIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInizio;
    @Column(name = "DATA_FINE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFine;
    @Column(name = "ID_AVVERSARIO")
    private Integer idAvversario;
    @Column(name = "ID_STAGIONE")
    private Integer idStagione;
    @Column(name = "LUOGO")
    private String luogo;
    @Column(name = "GOL_FATTI_AZIONE")
    private Integer golFattiAzione;
    @Column(name = "GOL_FATTI_PALLA")
    private Integer golFattiPalla;
    @Column(name = "GOL_SUBITI_AZIONE")
    private Integer golSubitiAzione;
    @Column(name = "GOL_SUBITI_PALLA")
    private Integer golSubitiPalla;
    @Column(name = "POSSESSO_PALLA_I_TEMPO")
    private Integer possessoPallaI;
    @Column(name = "POSSESSO_PALLA_II_TEMPO")
    private Integer possessoPallaII;
    @Column(name = "POSSESSO_PALLA_TOT")
    private Integer possessoPallaTot;
    @Column(name = "GOL_TIRI")
    private Double golTiri;
    @Column(name = "GOL_TIRI_AVVERSARI")
    private Double golTiriAvversari;
    @Column(name = "RESOCONTO_INSERITO")
    private Boolean resocontoInserito;    
    
    @OneToOne
    @JoinColumn(name = "ID_AVVERSARIO", referencedColumnName="ID", insertable=false, updatable=false)
    private Avversario avversario;

    @OneToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
    @JoinColumn(name = "ID_PARTITA", referencedColumnName = "ID")   
    private List<PartitaGiocatore> giocatori;
    
    @OneToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
    @JoinColumn(name = "ID_PARTITA", referencedColumnName = "ID")   
    private List<PartitaAvversario> avversari;
        
    @Transient
    private Date dataI;
    
    @Transient
    private String oraI;
            
    public Partita() {
        giocatori = new ArrayList<PartitaGiocatore>();
        resocontoInserito = false;
    }

    public Partita(Integer id) {
        this.id = id;
    }
    
     // per recuperare le variabili transient nella modifica del corso
    public void impostaOrariTransient(){
        if(dataInizio!=null){
            Calendar inizio = Calendar.getInstance(TimeZone.getTimeZone("CET"), Locale.ITALIAN);
            inizio.setTime(dataInizio);            
            dataI = inizio.getTime();
            oraI = inizio.get(Calendar.HOUR_OF_DAY)+":"+inizio.get(Calendar.MINUTE);            
        }
    }
    
    // per impostare data termine e data inizio dopo inserimento o modifica corso
     public boolean impostaDate(){
         Calendar data = null;
         if(dataI!=null && oraI!=null && oraI.length()>0){
             data = Calendar.getInstance(TimeZone.getTimeZone("CET"), Locale.ITALIAN);
             data.setTime(dataI);
             String[] om = oraI.split(":");
             data.set(Calendar.HOUR_OF_DAY, Integer.parseInt(om[0]));
             data.set(Calendar.MINUTE, Integer.parseInt(om[1]));
             dataInizio = data.getTime();
             
             data.add(Calendar.MINUTE, 90);
             dataFine = data.getTime();
         }   
         
         // controllo date data inizio precedente a data fine 
        if(dataInizio!=null && dataFine!=null){
            return dataInizio.before(dataFine) ? true : false;
        }else{
            return false;
        }
     }
     
     public void addGiocatore(Giocatore giocatore){
        if(giocatori == null){
            giocatori = new ArrayList<PartitaGiocatore>();
        }
        if(giocatore!=null && giocatoriPartitaContains(giocatore.getId())==null){
            giocatori.add(new PartitaGiocatore(giocatore));
        }
    }
    
    public void removeGiocatore(Giocatore giocatore){
        Integer index = giocatoriPartitaContains(giocatore.getId());
        if(index!=null){
            giocatori.remove(giocatori.get(index));
        }
    }
    
    public Integer giocatoriPartitaContains(Integer idGiocatore){
        if(giocatori!=null){
            for(PartitaGiocatore pg : giocatori){
                if(pg.getIdGiocatore().equals(idGiocatore)){
                    return giocatori.indexOf(pg);
                }
            }
        }
        return null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataInizio() {
        return dataInizio;
    }

    public void setDataInizio(Date dataInizio) {
        this.dataInizio = dataInizio;
    }

    public Date getDataFine() {
        return dataFine;
    }

    public void setDataFine(Date dataFine) {
        this.dataFine = dataFine;
    }

    public Integer getIdAvversario() {
        return idAvversario;
    }

    public void setIdAvversario(Integer idAvversario) {
        this.idAvversario = idAvversario;
    }

    public Integer getIdStagione() {
        return idStagione;
    }

    public void setIdStagione(Integer idStagione) {
        this.idStagione = idStagione;
    }
    
    public Avversario getAvversario() {
        return avversario;
    }

    public void setAvversario(Avversario avversario) {
        this.avversario = avversario;
    }

    public Date getDataI() {
        return dataI;
    }

    public void setDataI(Date dataI) {
        this.dataI = dataI;
    }

    public String getOraI() {
        return oraI;
    }

    public void setOraI(String oraI) {
        this.oraI = oraI;
    }

    public Integer getGolFattiAzione() {
        return golFattiAzione;
    }

    public void setGolFattiAzione(Integer golFattiAzione) {
        this.golFattiAzione = golFattiAzione;
    }

    public Integer getGolFattiPalla() {
        return golFattiPalla;
    }

    public void setGolFattiPalla(Integer golFattiPalla) {
        this.golFattiPalla = golFattiPalla;
    }

    public Integer getGolSubitiAzione() {
        return golSubitiAzione;
    }

    public void setGolSubitiAzione(Integer golSubitiAzione) {
        this.golSubitiAzione = golSubitiAzione;
    }

    public Integer getGolSubitiPalla() {
        return golSubitiPalla;
    }

    public void setGolSubitiPalla(Integer golSubitiPalla) {
        this.golSubitiPalla = golSubitiPalla;
    }

    public Integer getPossessoPallaI() {
        return possessoPallaI;
    }

    public void setPossessoPallaI(Integer possessoPallaI) {
        this.possessoPallaI = possessoPallaI;
    }

    public Integer getPossessoPallaII() {
        return possessoPallaII;
    }

    public void setPossessoPallaII(Integer possessoPallaII) {
        this.possessoPallaII = possessoPallaII;
    }

    public Integer getPossessoPallaTot() {
        return possessoPallaTot;
    }

    public void setPossessoPallaTot(Integer possessoPallaTot) {
        this.possessoPallaTot = possessoPallaTot;
    }

    public Double getGolTiri() {
        return golTiri;
    }

    public void setGolTiri(Double golTiri) {
        this.golTiri = golTiri;
    }

    public Double getGolTiriAvversari() {
        return golTiriAvversari;
    }

    public void setGolTiriAvversari(Double golTiriAvversari) {
        this.golTiriAvversari = golTiriAvversari;
    }

    public Boolean getResocontoInserito() {
        return resocontoInserito;
    }

    public void setResocontoInserito(Boolean resocontoInserito) {
        this.resocontoInserito = resocontoInserito;
    }

    public List<PartitaGiocatore> getGiocatori() {
        return giocatori;
    }

    public void setGiocatori(List<PartitaGiocatore> giocatori) {
        this.giocatori = giocatori;
    }

    public String getLuogo() {
        return luogo;
    }

    public void setLuogo(String luogo) {
        this.luogo = luogo;
    }

    public List<PartitaAvversario> getAvversari() {
        return avversari;
    }

    public void setAvversari(List<PartitaAvversario> avversari) {
        this.avversari = avversari;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Partita)) {
            return false;
        }
        Partita other = (Partita) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.Partita[ id=" + id + " ]";
    }
    
}
