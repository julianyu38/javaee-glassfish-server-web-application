/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities.fatture;

import it.elbuild.soccertactics.lib.entities.Ordine;
import it.elbuild.soccertactics.lib.entities.Utente;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Eve
 */
@Entity
@XmlRootElement
@NamedQueries({})

@DiscriminatorValue("FatturaSemplice")

public class FatturaSemplice extends Fattura implements Serializable {

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "documento_trasporto")
    private DocumentoTrasporto documentoTrasporto;

    public FatturaSemplice() {
        super();
        documentoTrasporto = null;

    }

    public FatturaSemplice(double iva, Ordine o, Utente u) {
        super(iva, o, u);
        documentoTrasporto = null;
    }

    public FatturaSemplice(FatturaSemplice f) {
        super(f);
        documentoTrasporto = f.documentoTrasporto;
    }

    public FatturaSemplice(Fattura f) {
        super(f);
        documentoTrasporto = null;
    }

    public FatturaSemplice(Integer id) {
        this();
        this.id = id;
    }

    public void creaDocumentoTrasporto(int numero) {
        if (documentoTrasporto == null) {
            documentoTrasporto = new DocumentoTrasporto(this, numero);
        }
    }

    @Override
    public String getClasse() {
        return "FatturaSemplice";
    }

    /**
     * @return the documentoTrasporto
     */
    public DocumentoTrasporto getDocumentoTrasporto() {
        return documentoTrasporto;
    }

    /**
     * @param documentoTrasporto the documentoTrasporto to set
     */
    public void setDocumentoTrasporto(DocumentoTrasporto documentoTrasporto) {
        this.documentoTrasporto = documentoTrasporto;
    }

}
