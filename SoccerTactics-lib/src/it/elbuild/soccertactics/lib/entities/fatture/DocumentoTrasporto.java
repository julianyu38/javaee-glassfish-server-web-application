/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities.fatture;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sauroago
 */
@Entity
@Table(name = "documenti_trasporto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DocumentoTrasporto.findAll", query = "SELECT f FROM DocumentoTrasporto f"),
    @NamedQuery(name = "DocumentoTrasporto.findById", query = "SELECT f FROM DocumentoTrasporto f WHERE f.id = :id"),
     @NamedQuery(name = "DocumentoTrasporto.findByNumero", query = "SELECT f FROM DocumentoTrasporto f WHERE f.numero = :numero"),
   @NamedQuery(name = "DocumentoTrasporto.findLastNumero", query = "SELECT MAX(f.numero) FROM DocumentoTrasporto f WHERE f.stato='emesso'")
})

@DiscriminatorValue("DocumentoTrasporto")

public class DocumentoTrasporto extends DocumentoFatturazione implements Serializable{
   
    
    @Column(name = "traportato_da")
    private String trasportatoDa;
    
    @Column(name = "causale_trasporto")
    private String causaleTrasporto;
    
    @Column(name = "aspetto")
    private String aspetto;
    
    @Column(name = "colli")
    private Integer colli;
    
    @Column(name = "peso")
    private Double peso;
      
    @Column(name = "resa")
    private String resa;
    
    @Column(name = "vettore")
    private String vettore;
    
    @Column(name = "data_ritiro")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataRitiro;
    
    @Column(name = "note_trasporto")
    private String noteTrasporto;
  
    
    
    
  
    
 
    
    @OneToOne(mappedBy = "documentoTrasporto", cascade=CascadeType.DETACH)
    private FatturaSemplice fattura;
    
    
    
       @Transient
   private Integer oraRitiro;
   @Transient
   private Integer minutiRitiro;

    
   
   
   
public DocumentoTrasporto(){
   
    
        stato="bozza";
   trasportatoDa="vettore";
    causaleTrasporto="";
    aspetto="";
    colli=1;
    peso=0.0;
    resa="";
    vettore="";
    dataRitiro=new Date(System.currentTimeMillis());
    noteTrasporto="";
    oraRitiro=0;
    minutiRitiro=0;
  
   
}

    public DocumentoTrasporto(FatturaSemplice fattura, int numero) {
        
        super(fattura);
         stato="bozza";
   trasportatoDa="vettore";
    causaleTrasporto="";
    aspetto="";
    colli=1;
    peso=0.0;
    resa="";
    vettore="";
    dataRitiro=new Date(System.currentTimeMillis());
    noteTrasporto="";
    oraRitiro=0;
    minutiRitiro=0;
        this.fattura=fattura;
        this.numero=numero;
    }

    private DocumentoTrasporto(FatturaSemplice fattura) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
public void emetti(){
    creaNomePDF();
       if (dataRitiro!=null)
        dataRitiro.setTime(dataRitiro.getTime()+oraRitiro*60*60*1000+minutiRitiro*60*1000);
        stato="emesso";
data=new Date(System.currentTimeMillis());
    }

    /**
     * @return the trasportatoDa
     */
    public String getTrasportatoDa() {
        return trasportatoDa;
    }

    /**
     * @param trasportatoDa the trasportatoDa to set
     */
    public void setTrasportatoDa(String trasportatoDa) {
        this.trasportatoDa = trasportatoDa;
    }

    /**
     * @return the causaleTrasporto
     */
    public String getCausaleTrasporto() {
        return causaleTrasporto;
    }

    /**
     * @param causaleTrasporto the causaleTrasporto to set
     */
    public void setCausaleTrasporto(String causaleTrasporto) {
        this.causaleTrasporto = causaleTrasporto;
    }

    /**
     * @return the aspetto
     */
    public String getAspetto() {
        return aspetto;
    }

    /**
     * @param aspetto the aspetto to set
     */
    public void setAspetto(String aspetto) {
        this.aspetto = aspetto;
    }

    /**
     * @return the colli
     */
    public Integer getColli() {
        return colli;
    }

    /**
     * @param colli the colli to set
     */
    public void setColli(Integer colli) {
        this.colli = colli;
    }

    /**
     * @return the peso
     */
    public Double getPeso() {
        return peso;
    }

    /**
     * @param peso the peso to set
     */
    public void setPeso(Double peso) {
        this.peso = peso;
    }

    /**
     * @return the resa
     */
    public String getResa() {
        return resa;
    }

    /**
     * @param resa the resa to set
     */
    public void setResa(String resa) {
        this.resa = resa;
    }

    /**
     * @return the vettore
     */
    public String getVettore() {
        return vettore;
    }

    /**
     * @param vettore the vettore to set
     */
    public void setVettore(String vettore) {
        this.vettore = vettore;
    }

    /**
     * @return the dataRitiro
     */
    public Date getDataRitiro() {
        return dataRitiro;
    }

    /**
     * @param dataRitiro the dataRitiro to set
     */
    public void setDataRitiro(Date dataRitiro) {
        this.dataRitiro = dataRitiro;
    }

    /**
     * @return the noteTrasporto
     */
    public String getNoteTrasporto() {
        return noteTrasporto;
    }

    /**
     * @param noteTrasporto the noteTrasporto to set
     */
    public void setNoteTrasporto(String noteTrasporto) {
        this.noteTrasporto = noteTrasporto;
    }

   


    /**
     * @return the numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * @return the stato
     */
    public String getStato() {
        return stato;
    }

    /**
     * @param stato the stato to set
     */
    public void setStato(String stato) {
        this.stato = stato;
    }

    /**
     * @return the fattura
     */
    public FatturaSemplice getFattura() {
        return fattura;
    }

    /**
     * @param fattura the fattura to set
     */
    public void setFattura(FatturaSemplice fattura) {
        this.fattura = fattura;
    }

    /**
     * @return the oraRitiro
     */
    public Integer getOraRitiro() {
        return oraRitiro;
    }

    /**
     * @param oraRitiro the oraRitiro to set
     */
    public void setOraRitiro(Integer oraRitiro) {
        this.oraRitiro = oraRitiro;
    }

    /**
     * @return the minutiRitiro
     */
    public Integer getMinutiRitiro() {
        return minutiRitiro;
    }

    /**
     * @param minutiRitiro the minutiRitiro to set
     */
    public void setMinutiRitiro(Integer minutiRitiro) {
        this.minutiRitiro = minutiRitiro;
    }

    /**
     * @return the nomePDF
     */
    public String getNomePDF() {
        return nomePDF;
    }

    /**
     * @param nomePDF the nomePDF to set
     */
    public void setNomePDF(String nomePDF) {
        this.nomePDF = nomePDF;
    }

    public void creaNomePDF() {
         nomePDF="DTRASPORTO-"+numero+"-"+fattura.getAnno()+"-"+fattura.getIntestatarioBreve()+".pdf";
    }

   


  

   
         
         

                 
  
   
   
}
