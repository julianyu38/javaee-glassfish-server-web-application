/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities.fatture;


import it.elbuild.soccertactics.lib.entities.Utente;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Eve
 */
@Entity
@Table(name = "intestatari")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IntestatarioFattura.findAll", query = "SELECT r FROM IntestatarioFattura r"),
    @NamedQuery(name = "IntestatarioFattura.findById", query = "SELECT r FROM IntestatarioFattura r WHERE r.id = :id")})
public class IntestatarioFattura implements Serializable {

    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "nome", nullable = false)
    private String nome;
    @Column(name = "cognome", nullable = false)
    private String cognome;
    @Column(name = "ragione_sociale", nullable = true)
    private String ragioneSociale;
    @Column(name = "luogo_nascita", nullable = true)
    private String luogoNascita;
    @Column(name = "data_nascita", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataNascita;
    @Column(name = "residenza", nullable = true)
    private String luogoResidenza;
    @Column(name = "cap", nullable = true)
    private String cap;
    @Column(name = "pv", nullable = true)
    private String pv;
    @Column(name = "indirizzo", nullable = true)
    private String indirizzo;
    @Column(name = "num_civico", nullable = true)
    private String numCivico;
    @Column(name = "codice_fiscale", nullable = true)
    private String codiceFiscale;
    @Column(name = "partita_iva", nullable = true)
    private String partitaIva;
    

    public IntestatarioFattura() {
        
    }

    public IntestatarioFattura(Utente u) {
        if (u != null) {
            nome = u.getNome();
            cognome = u.getCognome();
            ragioneSociale = u.getRagioneSociale();
            luogoNascita = u.getLuogoNascita();
            dataNascita = u.getDataNascita();
            luogoResidenza = u.getLuogoResidenza();
            cap = u.getCap();
            pv = u.getPv();
            indirizzo = u.getIndirizzo();
            numCivico = u.getNumCivico();
            codiceFiscale = u.getCf();
            partitaIva = u.getPiva();
        }
    }

    public IntestatarioFattura(IntestatarioFattura i) {
        nome = i.getNome();
        cognome = i.getCognome();
        ragioneSociale = i.getRagioneSociale();
        luogoNascita = i.getLuogoNascita();
        dataNascita = i.getDataNascita();
        luogoResidenza = i.getLuogoResidenza();
        cap = i.getCap();
        pv = i.getPv();
        indirizzo = i.getIndirizzo();
        numCivico = i.getNumCivico();
        codiceFiscale = i.getCodiceFiscale();
        partitaIva = i.getPartitaIva();

    }

    public void leggiDati() {
//        if (utente != null) {
//            nome = utente.getNome();
//            cognome = utente.getCognome();
//            ragioneSociale = utente.getNomeFatt();
//            codiceFiscale = utente.getCfFatt();
//            partitaIva = utente.getPivaFatt();
//            if (ragioneSociale == null || ragioneSociale.isEmpty()) {
//                destinatarioNome = utente.getNome() + " " + utente.getCognome();
//            } else {
//                destinatarioNome = ragioneSociale;
//            }
//            destinatarioIndirizzo = utente.getIndirizzoFatt();
//            destinatarioCitta = utente.getCapFatt() + " " + utente.getCittaFatt() + " - " + utente.getProvinciaFatt();
//        }
    }

    public IntestatarioFattura(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IntestatarioFattura)) {
            return false;
        }
        IntestatarioFattura other = (IntestatarioFattura) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.isfatturazione.lib.users.entities.IntestatarioFattura[ id=" + id + " ]";
    }

    public String getNome() {
        return nome == null ? "" : nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome == null ? "" : cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getRagioneSociale() {
        return ragioneSociale == null ? "" : ragioneSociale;
    }

    public void setRagioneSociale(String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
    }

    public String getLuogoNascita() {
        return luogoNascita;
    }

    public void setLuogoNascita(String luogoNascita) {
        this.luogoNascita = luogoNascita;
    }

    public Date getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(Date dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getLuogoResidenza() {
        return luogoResidenza;
    }

    public void setLuogoResidenza(String luogoResidenza) {
        this.luogoResidenza = luogoResidenza;
    }

    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getPv() {
        return pv;
    }

    public void setPv(String pv) {
        this.pv = pv;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getNumCivico() {
        return numCivico;
    }

    public void setNumCivico(String numCivico) {
        this.numCivico = numCivico;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public String getPartitaIva() {
        return partitaIva;
    }

    public void setPartitaIva(String partitaIva) {
        this.partitaIva = partitaIva;
    }

}
