/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities.fatture;

import it.elbuild.soccertactics.lib.entities.Ordine;
import it.elbuild.soccertactics.lib.entities.Utente;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.annotations.ConversionValue;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.ObjectTypeConverter;

/**
 *
 * @author Eve
 */
@Entity
@Table(name = "documenti_fatturazione")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DocumentoFatturazione.findAll", query = "SELECT f FROM DocumentoFatturazione f"),
    @NamedQuery(name = "DocumentoFatturazione.findById", query = "SELECT f FROM DocumentoFatturazione f WHERE f.id = :id"),
    @NamedQuery(name = "DocumentoFatturazione.findByNumero", query = "SELECT f FROM DocumentoFatturazione f WHERE f.numero = :numero"),
    @NamedQuery(name = "DocumentoFatturazione.findByIntestatario", query = "SELECT f FROM DocumentoFatturazione f WHERE f.intestatario = :intestatario"),
    @NamedQuery(name = "DocumentoFatturazione.findByData", query = "SELECT f FROM DocumentoFatturazione f WHERE f.data = :data"),
    @NamedQuery(name = "DocumentoFatturazione.findByDataCreazione", query = "SELECT f FROM DocumentoFatturazione f WHERE f.dataCreazione = :dataCreazione"),
    @NamedQuery(name = "DocumentoFatturazione.findBySpeseSpedizione", query = "SELECT f FROM DocumentoFatturazione f WHERE f.speseSpedizione = :speseSpedizione"),
    @NamedQuery(name = "DocumentoFatturazione.findByIva", query = "SELECT f FROM DocumentoFatturazione f WHERE f.iva = :iva"),
    @NamedQuery(name = "DocumentoFatturazione.findByModalitaPagamento", query = "SELECT f FROM DocumentoFatturazione f WHERE f.modalitaPagamento = :modalitaPagamento"),
    @NamedQuery(name = "DocumentoFatturazione.findByBancaAppoggio", query = "SELECT f FROM DocumentoFatturazione f WHERE f.bancaAppoggio = :bancaAppoggio"),
    @NamedQuery(name = "DocumentoFatturazione.findByScadenza", query = "SELECT f FROM DocumentoFatturazione f WHERE f.scadenza = :scadenza"),
    @NamedQuery(name = "DocumentoFatturazione.findLastNumero", query = "SELECT MAX(f.numero) FROM DocumentoFatturazione f WHERE f.stato='emessa'")
})

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name = "tipo_documento",
        discriminatorType = DiscriminatorType.STRING
)
@DiscriminatorValue("DocumentoFatturazione")

public class DocumentoFatturazione implements Serializable {

    private static final long serialVersionUID = 1L;
//    @Transient
//    private final double IVA = 22.0;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    protected Integer id;
    @Basic(optional = false)
    @Column(name = "numero", nullable = false)
    protected int numero;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "intestatario", nullable = false)
    protected IntestatarioFattura intestatario;
    @Basic(optional = false)
    @Column(name = "data", nullable = false)
    @Temporal(TemporalType.DATE)
    protected Date data;
    @Basic(optional = false)
    @Column(name = "creazione", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    protected Date dataCreazione;
    @Column(name = "ultima_modifica", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    protected Date dataUltimaModifica;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "spese_spedizione", precision = 22)
    protected Double speseSpedizione;
    @Column(name = "iva", precision = 22)
    protected Double iva;
    @Column(name = "modalita_pagamento")
    protected String modalitaPagamento;
    @Column(name = "banca_appoggio")
    protected String bancaAppoggio;
    @Column(name = "scadenza")
    @Temporal(TemporalType.DATE)
    protected Date scadenza;
    @ObjectTypeConverter(
            name = "stringToBooleanConverter",
            dataType = java.lang.String.class,
            objectType = java.lang.Boolean.class,
            conversionValues = {
                @ConversionValue(dataValue = "NO", objectValue = "False"),
                @ConversionValue(dataValue = "SI", objectValue = "True")})
    @Convert("stringToBooleanConverter")
    @Column(name = "pagato")
    private Boolean pagato;
    @Convert("stringToBooleanConverter")
    @Column(name = "usa_piva")
    private Boolean usaPiva;
    @Column(name = "riferimento_ordine")
    protected String riferimentoOrdine;
    @Column(name = "stato", length = 100)
    protected String stato;
    @OneToMany(mappedBy = "documentoFatturazione", cascade = CascadeType.ALL)
    @OrderBy("ordine")
    @OrderColumn(name = "ordine")
//     @OneToMany(cascade=CascadeType.MERGE)
//     @JoinColumn(name="fattura_id", referencedColumnName="id")
    protected List<VoceFattura> voci;
    @Transient
    protected Double imponibile;
    @Column(name = "nome_pdf")
    protected String nomePDF;
    @Column(name = "note")
    protected String note;
    @Column(name = "spese_incasso")
    protected Double speseIncasso;
    @Column(name = "sconto")
    private Double sconto;

    public DocumentoFatturazione(double IVA, Ordine o, Utente user) {

        iva = IVA;
        voci = new ArrayList<VoceFattura>();
        VoceFattura v = new VoceFattura(this);

            //v.setCodiceArticolo(o.getProduct().getProductSku());
        v.setDescrizione(o.getDescrizione());

        v.setQuantita(1);
        v.setPrezzoUnitario(o.getTotale());
//        v.setCodiceArticolo(a.getNome()); 
        voci.add(v);

//        if (voci.size() < 4) {
//            for (int x = voci.size(); x < 4; x++) {
//                voci.add(new VoceFattura(this));
//            }
//        }

        this.speseSpedizione = 0.0;
        intestatario = new IntestatarioFattura(user);
        data = o.getDataOrdine();
        scadenza = new Date(System.currentTimeMillis());
        dataCreazione = new Date(System.currentTimeMillis());
        dataUltimaModifica = new Date(System.currentTimeMillis());
        stato = "bozza";
        speseIncasso = 0.0;
        usaPiva = true;
        pagato = true;
        modalitaPagamento = "paypal";
        sconto = 0.0;
        SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy");
        riferimentoOrdine = sdf.format(o.getDataOrdine()) + o.getId().toString();
    }

    public void annulla() {
        voci = new ArrayList<VoceFattura>();
        voci.add(new VoceFattura(this));
        voci.add(new VoceFattura(this));
        voci.add(new VoceFattura(this));
        voci.add(new VoceFattura(this));
        speseIncasso = 0.0;
        sconto = 0.0;
        calcolaTotale();
        speseSpedizione = 0.0;
    }

    public DocumentoFatturazione() {

        iva = 22.0;
        voci = new ArrayList<VoceFattura>();
        voci.add(new VoceFattura(this));
        voci.add(new VoceFattura(this));
        voci.add(new VoceFattura(this));
        voci.add(new VoceFattura(this));
        imponibile = 0.0;
        this.speseSpedizione = 0.0;
        intestatario = new IntestatarioFattura();
        data = new Date(System.currentTimeMillis());
        scadenza = new Date(System.currentTimeMillis());
        dataCreazione = new Date(System.currentTimeMillis());
        dataUltimaModifica = new Date(System.currentTimeMillis());
        stato = "bozza";
        speseIncasso = 0.0;
        usaPiva = true;
        pagato = false;
        sconto = 0.0;
    }

    public DocumentoFatturazione(DocumentoFatturazione f) {
        sconto = f.sconto;
        bancaAppoggio = f.bancaAppoggio;
        data = f.data;
        dataCreazione = f.dataCreazione;
        dataUltimaModifica = f.dataUltimaModifica;
        //id=f.id;
        imponibile = f.imponibile;
        intestatario = new IntestatarioFattura(f.intestatario);
        iva = f.iva;
        modalitaPagamento = f.modalitaPagamento;
        note = f.note;
        numero = f.numero;
        riferimentoOrdine = f.riferimentoOrdine;
        scadenza = f.scadenza;
        speseIncasso = f.speseIncasso;
        speseSpedizione = f.speseSpedizione;
        voci = new ArrayList<VoceFattura>();

        for (VoceFattura v : f.voci) {
            VoceFattura voce = new VoceFattura(v);
            voce.setDocumentoFatturazione(this);
            voci.add(voce);
        }

        stato = f.stato;
        usaPiva = f.usaPiva;
        pagato = f.pagato;
    }

    public DocumentoFatturazione(Integer id) {
        this();
        this.id = id;
    }

    public String getEtichetta() {
        String s = "fattura ";
        if ("emessa".equals(stato)) {
            s = s + "n. " + numero + " ";
        }
        if (intestatario != null) {
            s = s + " intestata a " + intestatario.getCognome();;
        }
        if ("bozza".equals(stato) && (intestatario == null || intestatario.getCognome()== null || "".equals(intestatario.getCognome()))) {
            s = "nuova fattura";
        }
        return s;
    }

    public String getClasse() {
        return "DocumentoFatturazione";
    }

    public void aggiungiVoce() {
        if (voci == null) {
            voci = new ArrayList<VoceFattura>();
        }
        voci.add(new VoceFattura(this));
    }

    public void rimuoviVoce(VoceFattura v) {
        if (voci != null && voci.contains(v)) {
            voci.remove(v);
        }
    }

    public Double getImportoIva() {
        if (iva == null) {
            iva = 0.0;
        }        
//        if (imponibile != null && iva > 0) {
//            
//            BigDecimal netto = BigDecimal.valueOf(imponibile);
//            netto.setScale(6, RoundingMode.CEILING);
//            BigDecimal tasse = netto.multiply(BigDecimal.valueOf(iva)).divide(BigDecimal.valueOf(100));
////                BigDecimal tasseRound = tasse.setScale(2, RoundingMode.HALF_UP);
//
//            BigDecimal totround = FormazioneUtils.round(netto.add(tasse), new BigDecimal("0.10"), RoundingMode.HALF_UP);
//            if (totround.compareTo(netto.add(tasse)) < 0) {
//                totround = netto.add(tasse.setScale(6)).setScale(2, RoundingMode.HALF_UP);
//            }
//            tasse = totround.subtract(BigDecimal.valueOf(imponibile)).setScale(2, RoundingMode.HALF_UP);
//            System.out.println("getImportoIva() imponibile " + imponibile + " tasse " + tasse.doubleValue() + " totale" + totround.doubleValue());
//
//
//            return tasse.doubleValue();
//        } else {
            return 0.0;
//        }
    }

    public void calcolaTotale() {
        imponibile = speseSpedizione + speseIncasso - sconto;;
        for (VoceFattura voce : voci) {
            if (voce.getImporto() != null) {
                imponibile = imponibile + voce.getImporto();
            }
        }
    }

    public void leggiDati() {
        if (intestatario == null) {
            intestatario = new IntestatarioFattura();
        }
        intestatario.leggiDati();
    }

    public void emetti() throws Exception {
        if (intestatario == null) {
            leggiDati();
        }
//        data = new Date(System.currentTimeMillis());
        creaNomePDF();
        stato = "emessa";

    }

    public boolean isModalitaPagamentoBonifico() {
        if (modalitaPagamento != null && modalitaPagamento.toLowerCase().startsWith("bonifico")) {
            return true;
        } else {
            return false;
        }
    }

    public Double getTotale() {
        return imponibile + this.getImportoIva();
    }

    public Double getTotaleCalcolato() {
        this.calcolaTotale();
        return imponibile + this.getImportoIva();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public IntestatarioFattura getIntestatario() {
        return intestatario;
    }

    public void setIntestatario(IntestatarioFattura intestatario) {
        this.intestatario = intestatario;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Date getDataCreazione() {
        return dataCreazione;
    }

    public void setDataCreazione(Date dataCreazione) {
        this.dataCreazione = dataCreazione;
    }

    public Double getSpeseSpedizione() {
        return speseSpedizione;
    }

    public void setSpeseSpedizione(Double speseSpedizione) {
        this.speseSpedizione = speseSpedizione;
    }

    public Double getIva() {
        return iva;
    }

    public void setIva(Double iva) {
        this.iva = iva;
    }

    public String getModalitaPagamento() {
        return modalitaPagamento;
    }

    public void setModalitaPagamento(String modalitaPagamento) {
        this.modalitaPagamento = modalitaPagamento;
    }

    public String getBancaAppoggio() {
        return bancaAppoggio;
    }

    public void setBancaAppoggio(String bancaAppoggio) {
        this.bancaAppoggio = bancaAppoggio;
    }

    public Date getScadenza() {
        return scadenza;
    }

    public void setScadenza(Date scadenza) {
        this.scadenza = scadenza;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fattura)) {
            return false;
        }
        DocumentoFatturazione other = (DocumentoFatturazione) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "net.interstudio.isformazione.lib.entity.fatture.Fatture[ id=" + id + " ]";
    }

    public void salvaDatiTransient() {

    }

    public void leggiDatiTransient() {

    }

    /**
     * @return the voci
     */
    public List<VoceFattura> getVoci() {
        return voci;
    }

    /**
     * @param voci the voci to set
     */
    public void setVoci(List<VoceFattura> voci) {
        this.voci = voci;
    }

    /**
     * @return the riferimentoOrdine
     */
    public String getRiferimentoOrdine() {
        return riferimentoOrdine;
    }

    /**
     * @param riferimentoOrdine the riferimentoOrdine to set
     */
    public void setRiferimentoOrdine(String riferimentoOrdine) {
        this.riferimentoOrdine = riferimentoOrdine;
    }

    /**
     * @return the importo
     */
    public Double getImporto() {
        return imponibile;
    }

    /**
     * @param importo the importo to set
     */
    public void setImporto(Double importo) {
        this.imponibile = importo;
    }

    /**
     * @return the imponibile
     */
    public Double getImponibile() {
        return imponibile;
    }

    /**
     * @param imponibile the imponibile to set
     */
    public void setImponibile(Double imponibile) {
        this.imponibile = imponibile;
    }

    /**
     * @return the stato
     */
    public String getStato() {
        return stato;
    }

    /**
     * @param stato the stato to set
     */
    public void setStato(String stato) {
        this.stato = stato;
    }

    /**
     * @return the dataUltimaModifica
     */
    public Date getDataUltimaModifica() {
        return dataUltimaModifica;
    }

    /**
     * @param dataUltimaModifica the dataUltimaModifica to set
     */
    public void setDataUltimaModifica(Date dataUltimaModifica) {
        this.dataUltimaModifica = dataUltimaModifica;
    }

    //servono solo per in nome del pdf non vengono chiamati nelle pagine
    public String getAnno() {
        String s = "";
        if (data != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(data);
            s = c.get(Calendar.YEAR) + "";
        }
        return s;
    }

    public String getIntestatarioBreve() {
        String s = "";

        if (intestatario != null) {
            if (intestatario.getRagioneSociale() != null && !intestatario.getRagioneSociale().trim().isEmpty()) {
                s = intestatario.getRagioneSociale().replace(" ", "_");
            } else if (intestatario.getNome() != null && intestatario.getCognome() != null) {
                s = intestatario.getNome().replace(" ", "_") + "_" + intestatario.getCognome().replace(" ", "_");
            }
        }

        return s;
    }

    public void creaNomePDF() {
        nomePDF = "RICEVUTA-" + numero + "-" + getAnno() + "-" + getIntestatarioBreve() + ".pdf";
    }

    /**
     * @return the nomePDF
     */
    public String getNomePDF() {
        return nomePDF;
    }

    /**
     * @param nomePDF the nomePDF to set
     */
    public void setNomePDF(String nomePDF) {
        this.nomePDF = nomePDF;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the speseIncasso
     */
    public Double getSpeseIncasso() {
        return speseIncasso;
    }

    /**
     * @param speseIncasso the speseIncasso to set
     */
    public void setSpeseIncasso(Double speseIncasso) {
        this.speseIncasso = speseIncasso;
    }

    /**
     * @return the pagato
     */
    public Boolean getPagato() {
        return pagato;
    }

    /**
     * @param pagato the pagato to set
     */
    public void setPagato(Boolean pagato) {
        this.pagato = pagato;
    }

    /**
     * @return the usaPiva
     */
    public Boolean getUsaPiva() {
        return usaPiva;
    }

    /**
     * @param usaPiva the usaPiva to set
     */
    public void setUsaPiva(Boolean usaPiva) {
        this.usaPiva = usaPiva;
    }

    /**
     * @return the sconto
     */
    public Double getSconto() {
        return sconto;
    }

    /**
     * @param sconto the sconto to set
     */
    public void setSconto(Double sconto) {
        this.sconto = sconto;
    }

    public String getEtichettaNumero() {
        return numero + "/"+getAnno();
    }

}
