/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities.fatture;


import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Eve
 */
@Entity
@Table(name = "voci_fattura")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VoceFattura.findAll", query = "SELECT v FROM VoceFattura v"),
    @NamedQuery(name = "VoceFattura.findById", query = "SELECT v FROM VoceFattura v WHERE v.id = :id"),
    @NamedQuery(name = "VoceFattura.findByCodiceArticolo", query = "SELECT v FROM VoceFattura v WHERE v.codiceArticolo = :codiceArticolo"),
    @NamedQuery(name = "VoceFattura.findByQuantita", query = "SELECT v FROM VoceFattura v WHERE v.quantita = :ta"),
    @NamedQuery(name = "VoceFattura.findByPrezzoUnitario", query = "SELECT v FROM VoceFattura v WHERE v.prezzoUnitario = :prezzoUnitario")})
public class VoceFattura implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "codice_articolo", length = 100)
    private String codiceArticolo;
    @Lob
    @Column(name = "descrizione", length = 65535)
    private String descrizione;
    @Column(name = "quantita")
    private Integer quantita;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "prezzo_unitario", precision = 22)
    private Double prezzoUnitario;
    @Column(name = "sconto_importo")
    private Double scontoImporto;
   
    
     @Column(name = "ordine")
    private Integer ordine;
    
     @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "documento_fatturazione_id")
    private DocumentoFatturazione documentoFatturazione;

     @Transient
     private boolean scontoPercentuale;
     
      @Column(name = "sconto")
     private Double sconto;
     
    public VoceFattura() {
        
     quantita=0;
     sconto=0.0;
     prezzoUnitario=0.0;
     scontoPercentuale=false;
    }
    
    
     public VoceFattura(VoceFattura v) {
        codiceArticolo=v.codiceArticolo;
        descrizione=v.descrizione;
      
     quantita=v.quantita;
     sconto=v.sconto;
     scontoImporto=v.scontoImporto;
     prezzoUnitario=v.prezzoUnitario;
     scontoPercentuale=false;
     //FIXME non ci metto fattura e id al momento non mi servono...ma poi fattura serve mai??
    }
    
    
    public VoceFattura(DocumentoFatturazione f){
        this();
        codiceArticolo="";
        descrizione="";
        
        this.documentoFatturazione=f;
        scontoPercentuale=false;
    }

    
    public void scegliPercentuale(boolean b){
        scontoPercentuale=b;
    }
    
    public VoceFattura(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodiceArticolo() {
        return codiceArticolo;
    }

    public void setCodiceArticolo(String codiceArticolo) {
        this.codiceArticolo = codiceArticolo;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public Integer getQuantita() {
        return quantita;
    }

    public void setQuantita(Integer ta) {
        this.quantita = ta;
    }

    public Double getPrezzoUnitario() {
        return prezzoUnitario;
    }

    public void setPrezzoUnitario(Double prezzoUnitario) {
        this.prezzoUnitario = prezzoUnitario;
    }

    public Double getSconto() {
        return sconto;
    }

    public void setSconto(Double sconto) {
        this.sconto = sconto;
    }

    public Double getImporto() {
         double totale=0.0;
         
         if (prezzoUnitario==null)prezzoUnitario=0.0;
         if (quantita==null)quantita=0;
        
        
         totale=prezzoUnitario*quantita;
        
        
            if (scontoPercentuale || (!sconto.equals(0.0) && scontoImporto.equals(0.0))){
            if (sconto==null)sconto=0.0;
            
            scontoImporto=(totale/100*sconto);
            
            }
            else {
                if (scontoImporto==null) scontoImporto=0.0;
                sconto=scontoImporto==0 || totale==0 ? 0 : scontoImporto/totale*100;
            }
           
            totale=totale-scontoImporto;
            
            
        return totale;
    }
    
       public Double calcolaImportoEuro(double tasso) {
        return getImporto()*tasso;
    }
       
       
       

    
   
    
   

//    public Fattura getFattura() {
//        return fattura;
//    }
//
//    public void setFattura(Fattura fatturaId) {
//        this.fattura = fatturaId;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VoceFattura)) {
            return false;
        }
        VoceFattura other = (VoceFattura) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        
        if ((this.codiceArticolo == null ? other.codiceArticolo == null : this.codiceArticolo.equals(other.codiceArticolo)) && 
                (this.descrizione == null ? other.descrizione == null : this.descrizione.equals(other.descrizione)) &&
                this.prezzoUnitario==other.prezzoUnitario && 
                this.quantita==other.quantita &&
                this.scontoImporto==other.scontoImporto)
            return true;
        else return false;
    
    }

    @Override
    public String toString() {
        return "it.elbuild.isfatturazione.lib.entities.VociFattura[ id=" + id + " ]";
    }

    
      
      
    /**
     * @return the documentoFatturazione
     */
    public DocumentoFatturazione getDocumentoFatturazione() {
        return documentoFatturazione;
    }

    /**
     * @param documentoFatturazione the documentoFatturazione to set
     */
    public void setDocumentoFatturazione(DocumentoFatturazione documentoFatturazione) {
        this.documentoFatturazione = documentoFatturazione;
    }

    /**
     * @return the scontoImporto
     */
    public Double getScontoImporto() {
        return scontoImporto;
    }

    /**
     * @param scontoImporto the scontoImporto to set
     */
    public void setScontoImporto(Double scontoImporto) {
        this.scontoImporto = scontoImporto;
    }

    /**
     * @return the scontoPercentuale
     */
    public boolean isScontoPercentuale() {
        return scontoPercentuale;
    }

    /**
     * @param scontoPercentuale the scontoPercentuale to set
     */
    public void setScontoPercentuale(boolean scontoPercentuale) {
        this.scontoPercentuale = scontoPercentuale;
    }

    /**
     * @return the ordine
     */
    public Integer getOrdine() {
        return ordine;
    }

    /**
     * @param ordine the ordine to set
     */
    public void setOrdine(Integer ordine) {
        this.ordine = ordine;
    }

  
}
