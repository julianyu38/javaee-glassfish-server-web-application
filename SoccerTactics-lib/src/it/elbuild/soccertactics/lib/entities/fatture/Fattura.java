/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities.fatture;

import it.elbuild.soccertactics.lib.entities.Ordine;
import it.elbuild.soccertactics.lib.entities.Utente;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Eve
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fattura.findAll", query = "SELECT f FROM Fattura f"),
    @NamedQuery(name = "Fattura.findAllOrderByEditTime", query = "SELECT f FROM Fattura f ORDER BY f.dataUltimaModifica DESC"),
    @NamedQuery(name = "Fattura.findById", query = "SELECT f FROM Fattura f WHERE f.id = :id"),
    @NamedQuery(name = "Fattura.findByNumero", query = "SELECT f FROM Fattura f WHERE f.numero = :numero AND f.stato='emessa'"),
    @NamedQuery(name = "Fattura.findByNumeroInDates", query = "SELECT f FROM Fattura f WHERE f.numero = :numero AND f.stato='emessa' AND f.data BETWEEN :start AND :end"),
    @NamedQuery(name = "Fattura.findByIntestatario", query = "SELECT f FROM Fattura f WHERE f.intestatario = :intestatario"),
    @NamedQuery(name = "Fattura.findByData", query = "SELECT f FROM Fattura f WHERE f.data = :data"),
    @NamedQuery(name = "Fattura.findFattureDate", query = "SELECT f FROM Fattura f WHERE f.data >= :start AND f.data <= :stop"),
    @NamedQuery(name = "Fattura.findByDataCreazione", query = "SELECT f FROM Fattura f WHERE f.dataCreazione = :dataCreazione"),
    @NamedQuery(name = "Fattura.findBySpeseSpedizione", query = "SELECT f FROM Fattura f WHERE f.speseSpedizione = :speseSpedizione"),
    @NamedQuery(name = "Fattura.findByIva", query = "SELECT f FROM Fattura f WHERE f.iva = :iva"),
    @NamedQuery(name = "Fattura.findByModalitaPagamento", query = "SELECT f FROM Fattura f WHERE f.modalitaPagamento = :modalitaPagamento"),
    @NamedQuery(name = "Fattura.findByBancaAppoggio", query = "SELECT f FROM Fattura f WHERE f.bancaAppoggio = :bancaAppoggio"),
    @NamedQuery(name = "Fattura.findByScadenza", query = "SELECT f FROM Fattura f WHERE f.scadenza = :scadenza"),
    @NamedQuery(name = "Fattura.findLastNumero", query = "SELECT MAX(f.numero) FROM Fattura f WHERE f.stato='emessa' AND f.data BETWEEN :start AND :end")
})

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name = "tipo_fattura",
        discriminatorType = DiscriminatorType.STRING
)

@DiscriminatorValue("Fattura")

public class Fattura extends DocumentoFatturazione implements Serializable {

    public Fattura() {
        super();

    }

    public Fattura(double iva, Ordine o, Utente u) {
        super(iva, o, u);

    }

    public Fattura(Fattura f) {
        super(f);
    }

    public Fattura(Integer id) {
        this();
        this.id = id;
    }

    @Override
    public String getClasse() {
        return "Fattura";
    }

}
