/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.entities.fatture.Fattura;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "ORDINE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ordine.findAll", query = "SELECT o FROM Ordine o order by o.dataOrdine DESC"),
    @NamedQuery(name = "Ordine.findById", query = "SELECT o FROM Ordine o WHERE o.id = :id"),
    @NamedQuery(name = "Ordine.findByDataOrdine", query = "SELECT o FROM Ordine o WHERE o.dataOrdine = :dataOrdine"),
    @NamedQuery(name = "Ordine.findByIdOrdine", query = "SELECT o FROM Ordine o WHERE o.idOrdine = :idOrdine"),
    @NamedQuery(name = "Ordine.findByIdUtente", query = "SELECT o FROM Ordine o WHERE o.idUtente = :idUtente"),
    @NamedQuery(name = "Ordine.findByMetodoPagamento", query = "SELECT o FROM Ordine o WHERE o.metodoPagamento = :metodoPagamento"),
    @NamedQuery(name = "Ordine.findByStatoOrdine", query = "SELECT o FROM Ordine o WHERE o.statoOrdine = :statoOrdine"),
    @NamedQuery(name = "Ordine.findByStatoPaypal", query = "SELECT o FROM Ordine o WHERE o.statoPaypal = :statoPaypal"),
    @NamedQuery(name = "Ordine.findByTotale", query = "SELECT o FROM Ordine o WHERE o.totale = :totale"),
    @NamedQuery(name = "Ordine.findByDescrizione", query = "SELECT o FROM Ordine o WHERE o.descrizione = :descrizione")})
public class Ordine implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "DATA_ORDINE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataOrdine;
    @Size(max = 255)
    @Column(name = "ID_ORDINE")
    private String idOrdine;
    @Column(name = "ID_UTENTE")
    private Integer idUtente;
    @Size(max = 255)
    @Column(name = "METODO_PAGAMENTO")
    private String metodoPagamento;
    @Size(max = 255)
    @Column(name = "STATO_ORDINE")
    private String statoOrdine;
    @Size(max = 255)
    @Column(name = "STATO_PAYPAL")
    private String statoPaypal;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TOTALE")
    private Double totale;
    @Size(max = 400)
    @Column(name = "DESCRIZIONE")
    private String descrizione;
    @Column(name = "PACCHETTO_ABB")
    private Integer idPackAbb;
    @Column(name = "PACCHETTO_ES")
    private Integer idPackEs;
    
    @OneToOne
    @JoinColumn(name="ID_UTENTE", referencedColumnName="ID", insertable=false, updatable=false)
    private Utente utente;  
    
    @OneToOne
    @JoinColumn(name="PACCHETTO_ABB", referencedColumnName="ID", insertable=false, updatable=false)
    private Pacchetto packAbb; 
    
    @OneToOne
    @JoinColumn(name="PACCHETTO_ES", referencedColumnName="ID", insertable=false, updatable=false)
    private Pacchetto packEs; 
    
    @Column(name = "FATTURA_ID", insertable = false, updatable = false)
    private Integer fatturaId;    
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "FATTURA_ID")
    private Fattura fattura;
    
    public Ordine() {
    }

    public Ordine(Integer id) {
        this.id = id;
    }
    
    public Ordine(String statoOrdine, String descrizione, Double totale, Integer idUtente, Pacchetto abb, Pacchetto es) {
        this.statoOrdine = statoOrdine;
        this.dataOrdine = new java.util.Date(System.currentTimeMillis());
        this.metodoPagamento = "paypal";
        this.totale = totale;
        this.descrizione = descrizione;
        this.idUtente = idUtente;
        if(abb!=null){
            idPackAbb = abb.getId();
            packAbb = abb;
        }
        if(es!=null){
            idPackEs = es.getId();
            packEs = es;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataOrdine() {
        return dataOrdine;
    }

    public void setDataOrdine(Date dataOrdine) {
        this.dataOrdine = dataOrdine;
    }

    public String getIdOrdine() {
        return idOrdine;
    }

    public void setIdOrdine(String idOrdine) {
        this.idOrdine = idOrdine;
    }

    public Integer getIdUtente() {
        return idUtente;
    }

    public void setIdUtente(Integer idUtente) {
        this.idUtente = idUtente;
    }

    public String getMetodoPagamento() {
        return metodoPagamento;
    }

    public void setMetodoPagamento(String metodoPagamento) {
        this.metodoPagamento = metodoPagamento;
    }

    public String getStatoOrdine() {
        return statoOrdine;
    }

    public void setStatoOrdine(String statoOrdine) {
        this.statoOrdine = statoOrdine;
    }

    public String getStatoPaypal() {
        return statoPaypal;
    }

    public void setStatoPaypal(String statoPaypal) {
        this.statoPaypal = statoPaypal;
    }

    public Double getTotale() {
        return totale;
    }

    public void setTotale(Double totale) {
        this.totale = totale;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public Integer getIdPackAbb() {
        return idPackAbb;
    }

    public void setIdPackAbb(Integer idPackAbb) {
        this.idPackAbb = idPackAbb;
    }

    public Integer getIdPackEs() {
        return idPackEs;
    }

    public void setIdPackEs(Integer idPackEs) {
        this.idPackEs = idPackEs;
    }

    public Pacchetto getPackAbb() {
        return packAbb;
    }

    public void setPackAbb(Pacchetto packAbb) {
        this.packAbb = packAbb;
    }

    public Pacchetto getPackEs() {
        return packEs;
    }

    public void setPackEs(Pacchetto packEs) {
        this.packEs = packEs;
    }

    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    public Integer getFatturaId() {
        return fatturaId;
    }

    public void setFatturaId(Integer fatturaId) {
        this.fatturaId = fatturaId;
    }

    public Fattura getFattura() {
        return fattura;
    }

    public void setFattura(Fattura fattura) {
        this.fattura = fattura;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ordine)) {
            return false;
        }
        Ordine other = (Ordine) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.Ordine[ id=" + id + " ]";
    }
    
}
