/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.esercizio.model.ElementoEx;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "ESERCIZIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Esercizio.findAll", query = "SELECT e FROM Esercizio e WHERE e.eliminato = '0' "),
    @NamedQuery(name = "Esercizio.findById", query = "SELECT e FROM Esercizio e WHERE e.id = :id AND e.eliminato = '0' "),
    @NamedQuery(name = "Esercizio.findByIdUtente", query = "SELECT e FROM Esercizio e WHERE e.idUtente = :idUtente AND e.eliminato = '0' ORDER BY e.id DESC"),
    @NamedQuery(name = "Esercizio.findByIdUtenti", query = "SELECT e FROM Esercizio e WHERE e.eliminato = '0' AND e.idUtente IN :idUList  ORDER BY e.id DESC"),
    @NamedQuery(name = "Esercizio.findByTitolo", query = "SELECT e FROM Esercizio e WHERE e.titolo = :titolo AND e.eliminato = '0' ")})
public class Esercizio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "TITOLO")
    private String titolo;
    @Column(name = "MODELLO")
    private String modello;
    @Column(name = "IMMAGINE")
    private String immagine;
    @Column(name = "FILE_CARICATO")
    private String fileCaricato;
    @Column(name = "ID_UTENTE")
    private Integer idUtente;
    @Column(name = "PUBBLICO")
    private Boolean pubblico;
    @Column(name = "DESCRIZIONE")
    private String descrizione;
    @Column(name = "OB_ATT_I")
    private String obAttI;
    @Column(name = "OB_ATT_II")
    private String obAttII;
    @Column(name = "OB_DIF_I")
    private String obDifI;
    @Column(name = "OB_DIF_II")
    private String obDifII; 
    @Column(name = "OB_DIF_III")
    private String obDifIII; 
    @Column(name = "OB_COORD")
    private String obCoord;
    @Column(name = "OB_COND")
    private String obCond;    
    @Column(name = "OB_TAT_I")
    private String obTatI;
    @Column(name = "OB_TAT_II")
    private String obTatII;
    @Column(name = "TUTTI")
    private Boolean tutti;
    @Column(name = "PRIMA_SQUADRA")
    private Boolean primaSquadra;
    @Column(name = "JUNIORES")
    private Boolean juniores;
    @Column(name = "ALLIEVI")
    private Boolean allievi;
    @Column(name = "GIOVANISSIMI")
    private Boolean giovanissimi;
    @Column(name = "ESORDIENTI")
    private Boolean esordienti;
    @Column(name = "PULCINI")
    private Boolean pulcini;
    @Column(name = "PRIMI_CALCI")
    private Boolean primiCalci;
    @Column(name = "GENERALE")
    private Boolean generale;
    @Column(name = "PRE_ALLENAMENTO")
    private Boolean preAllenamento;
    @Column(name = "PORTIERI")
    private Integer portieri;
    @Column(name = "RIPETIZIONI")
    private Integer ripetizioni;
    @Column(name = "DURATA")
    private Integer durata;
    @Column(name = "STAZIONI_GRUPPI")
    private Integer stazioniGruppi;
    @Column(name = "RECUPERO")
    private Integer recupero;
    @Column(name = "SERIE")
    private Integer serie;
    
    @Column(name = "SFONDO")
    private String sfondo;
    @Column(name = "ELIMINATO")
    private Boolean eliminato;
    
    // CAMPI METODO
    @Column(name = "ANALITICO")
    private Boolean analitico;
    @Column(name = "GLOBALE")
    private Boolean globale;
    @Column(name = "FINALIZZATO")
    private Boolean finalizzato;
    @Column(name = "SITUAZIONALE")
    private Boolean situazionale;
    @Column(name = "GIOCO_INIZIALE")
    private Boolean giocoIniziale;
    @Column(name = "GIOCO_TEMA")
    private Boolean giocoTema;
    @Column(name = "PERCORSO_STAZIONI")
    private Boolean percorsoStazioni;
    @Column(name = "LAVORO_GRUPPO")
    private Boolean lavoroGruppo;
    @Column(name = "ATLETICO_MOTORIO")
    private Boolean atleticoMotorio;
    @Column(name = "RECUPERO_INFORTUNIO")
    private Boolean recuperoInfortunio;
    
    //campi obiettivi secondari solo per visualizzazione non incidono su ricerca nè su mesociclo
    @Column(name = "DIFESA_2")
    private String difesa2;
    @Column(name = "DIFESA_2_II")
    private String difesa2II;
    @Column(name = "DIFESA_2_III")
    private String difesa2III;
    @Column(name = "ATTACCO_2")
    private String attacco2;
    @Column(name = "ATTACCO_2_II")
    private String attacco2II;
    @Column(name = "COORDINATIVA")
    private String coordinativa;
    @Column(name = "CONDIZIONALE")
    private String condizionale;
    
    @OneToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
    @JoinColumn(name = "ID_ESERCIZIO", referencedColumnName = "ID")   
    private List<Materiale> materiali;
    
    @Transient
    private List<ElementoEx> elementi;

    @OneToOne
    @JoinColumn(name="ID_UTENTE", referencedColumnName="ID", insertable=false, updatable=false)
    private Utente utente;
    
    public Esercizio() {
        pubblico = false;
        elementi = new ArrayList<ElementoEx>();
        materiali = new ArrayList<Materiale>();
        sfondo = "background";
        eliminato = false;
    }

    public Esercizio(Integer id) {
        this.id = id;
    }
    
    public Esercizio(Esercizio e, Utente u){
        titolo = e.getTitolo()!=null ? e.getTitolo()+" - copia" : "Copia";
        modello = e.getModello();
        immagine = e.getImmagine();
        fileCaricato = e.getFileCaricato();
        idUtente = e.getIdUtente();
        pubblico = e.getPubblico();
        descrizione = e.getDescrizione();
        obAttI = e.getObTatI();
        obAttII = e.getObTatII();
        obDifI = e.getObDifI();
        obDifII = e.getObDifII();
        obDifIII = e.getObDifIII();
        difesa2 = e.getDifesa2();
        difesa2II = e.getDifesa2II();
        difesa2III = e.getDifesa2III();
        obAttI = e.getObAttI();
        obAttII = e.getObAttII();                
        attacco2 = e.getAttacco2();
        attacco2II = e.getAttacco2II();
        obTatI = e.getObTatI();
        obTatII = e.getObTatII();
        obCoord = e.getObCoord();        
        coordinativa = e.getCoordinativa();
        obCond = e.getObCond();  
        condizionale = e.getCondizionale();  
        
        tutti = e.getTutti();
        primaSquadra = e.getPrimaSquadra();
        juniores = e.getJuniores();
        allievi = e.getAllievi();
        giovanissimi = e.getGiovanissimi();
        esordienti = e.getEsordienti();
        pulcini = e.getPulcini();
        primiCalci = e.getPrimiCalci();
        generale = e.getGenerale();
        preAllenamento = e.getPreAllenamento();
        portieri = e.getPortieri();
        ripetizioni = e.getRipetizioni();
        durata = e.getDurata();
        stazioniGruppi = e.getStazioniGruppi();
        recupero = e.getRecupero();
        serie = e.getSerie();
        sfondo = e.getSfondo();
        utente = u;        
        materiali = new ArrayList<>();
        if(e.getMateriali()!=null && !e.getMateriali().isEmpty()){            
            for (Materiale m : e.getMateriali()) {
                materiali.add(new Materiale(m.getNome(), m.getQuantita()));
            }
        }
        eliminato = false;
        
        analitico = e.getAnalitico();
        globale = e.getGlobale();
        finalizzato = e.getFinalizzato();
        situazionale = e.getSituazionale();
        giocoIniziale = e.getGiocoIniziale();
        giocoTema = e.getGiocoTema();
        percorsoStazioni = e.getPercorsoStazioni();
        lavoroGruppo = e.getLavoroGruppo();
        atleticoMotorio = e.getAtleticoMotorio();
        recuperoInfortunio = e.getRecuperoInfortunio();
           
    }
    
    public void addMateriale(){
        if(materiali == null){
            materiali = new ArrayList<Materiale>();
        }
        materiali.add(new Materiale());
    }
    
    public void removeMateriale(Materiale m){
        materiali.remove(m);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getModello() {
        return modello;
    }

    public void setModello(String modello) {
        this.modello = modello;
    }

    public Integer getIdUtente() {
        return idUtente;
    }

    public void setIdUtente(Integer idUtente) {
        this.idUtente = idUtente;
    }

    public Boolean getPubblico() {
        return pubblico;
    }

    public void setPubblico(Boolean pubblico) {
        this.pubblico = pubblico;
    }

    public List<ElementoEx> getElementi() {
        return elementi;
    }

    public void setElementi(List<ElementoEx> elementi) {
        this.elementi = elementi;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }
    
    public String getObTatI() {
        return obTatI;
    }

    public void setObTatI(String obTatI) {
        this.obTatI = obTatI;
    }

    public String getObTatII() {
        return obTatII;
    }

    public void setObTatII(String obTatII) {
        this.obTatII = obTatII;
    }
    
    public Boolean getTutti() {
        return tutti;
    }

    public void setTutti(Boolean tutti) {
        this.tutti = tutti;
    }

    public Boolean getPrimaSquadra() {
        return primaSquadra;
    }

    public void setPrimaSquadra(Boolean primaSquadra) {
        this.primaSquadra = primaSquadra;
    }

    public Boolean getJuniores() {
        return juniores;
    }

    public void setJuniores(Boolean juniores) {
        this.juniores = juniores;
    }

    public Boolean getAllievi() {
        return allievi;
    }

    public void setAllievi(Boolean allievi) {
        this.allievi = allievi;
    }

    public Boolean getGiovanissimi() {
        return giovanissimi;
    }

    public void setGiovanissimi(Boolean giovanissimi) {
        this.giovanissimi = giovanissimi;
    }

    public Boolean getEsordienti() {
        return esordienti;
    }

    public void setEsordienti(Boolean esordienti) {
        this.esordienti = esordienti;
    }

    public Boolean getPulcini() {
        return pulcini;
    }

    public void setPulcini(Boolean pulcini) {
        this.pulcini = pulcini;
    }

    public Boolean getPrimiCalci() {
        return primiCalci;
    }

    public void setPrimiCalci(Boolean primiCalci) {
        this.primiCalci = primiCalci;
    }

    public Boolean getGenerale() {
        return generale;
    }

    public void setGenerale(Boolean generale) {
        this.generale = generale;
    }

    public Integer getPortieri() {
        return portieri;
    }

    public void setPortieri(Integer portieri) {
        this.portieri = portieri;
    }

    public Integer getRipetizioni() {
        return ripetizioni;
    }

    public void setRipetizioni(Integer ripetizioni) {
        this.ripetizioni = ripetizioni;
    }

    public Integer getDurata() {
        return durata;
    }

    public void setDurata(Integer durata) {
        this.durata = durata;
    }

    public Integer getStazioniGruppi() {
        return stazioniGruppi;
    }

    public void setStazioniGruppi(Integer stazioniGruppi) {
        this.stazioniGruppi = stazioniGruppi;
    }

    public Integer getRecupero() {
        return recupero;
    }

    public void setRecupero(Integer recupero) {
        this.recupero = recupero;
    }

    public Integer getSerie() {
        return serie;
    }

    public void setSerie(Integer serie) {
        this.serie = serie;
    }

    public String getImmagine() {
        return immagine;
    }

    public void setImmagine(String immagine) {
        this.immagine = immagine;
    }

    public String getFileCaricato() {
        return fileCaricato;
    }

    public void setFileCaricato(String fileCaricato) {
        this.fileCaricato = fileCaricato;
    }

    public List<Materiale> getMateriali() {
        return materiali;
    }

    public void setMateriali(List<Materiale> materiali) {
        this.materiali = materiali;
    }

    public String getSfondo() {
        return sfondo;
    }

    public void setSfondo(String sfondo) {
        this.sfondo = sfondo;
    }

    public String getObAttI() {
        return obAttI;
    }

    public void setObAttI(String obAttI) {
        this.obAttI = obAttI;
    }

    public String getObAttII() {
        return obAttII;
    }

    public void setObAttII(String obAttII) {
        this.obAttII = obAttII;
    }

    public String getObDifI() {
        return obDifI;
    }

    public void setObDifI(String obDifI) {
        this.obDifI = obDifI;
    }

    public String getObDifII() {
        return obDifII;
    }

    public void setObDifII(String obDifII) {
        this.obDifII = obDifII;
    }

    public String getObCoord() {
        return obCoord;
    }

    public void setObCoord(String obCoord) {
        this.obCoord = obCoord;
    }

    public String getObCond() {
        return obCond;
    }

    public void setObCond(String obCond) {
        this.obCond = obCond;
    }

    public Boolean getPreAllenamento() {
        return preAllenamento;
    }

    public void setPreAllenamento(Boolean preAllenamento) {
        this.preAllenamento = preAllenamento;
    }

    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    public Boolean isEliminato() {
        return eliminato;
    }

    public void setEliminato(Boolean eliminato) {
        this.eliminato = eliminato;
    }

    public Boolean getAnalitico() {
        return analitico;
    }

    public void setAnalitico(Boolean analitico) {
        this.analitico = analitico;
    }

    public Boolean getGlobale() {
        return globale;
    }

    public void setGlobale(Boolean globale) {
        this.globale = globale;
    }

    public Boolean getFinalizzato() {
        return finalizzato;
    }

    public void setFinalizzato(Boolean finalizzato) {
        this.finalizzato = finalizzato;
    }

    public Boolean getSituazionale() {
        return situazionale;
    }

    public void setSituazionale(Boolean situazionale) {
        this.situazionale = situazionale;
    }

    public Boolean getGiocoIniziale() {
        return giocoIniziale;
    }

    public void setGiocoIniziale(Boolean giocoIniziale) {
        this.giocoIniziale = giocoIniziale;
    }

    public Boolean getGiocoTema() {
        return giocoTema;
    }

    public void setGiocoTema(Boolean giocoTema) {
        this.giocoTema = giocoTema;
    }

    public Boolean getPercorsoStazioni() {
        return percorsoStazioni;
    }

    public void setPercorsoStazioni(Boolean percorsoStazioni) {
        this.percorsoStazioni = percorsoStazioni;
    }

    public Boolean getLavoroGruppo() {
        return lavoroGruppo;
    }

    public void setLavoroGruppo(Boolean lavoroGruppo) {
        this.lavoroGruppo = lavoroGruppo;
    }

    public Boolean getAtleticoMotorio() {
        return atleticoMotorio;
    }

    public void setAtleticoMotorio(Boolean atleticoMotorio) {
        this.atleticoMotorio = atleticoMotorio;
    }

    public Boolean getRecuperoInfortunio() {
        return recuperoInfortunio;
    }

    public void setRecuperoInfortunio(Boolean recuperoInfortunio) {
        this.recuperoInfortunio = recuperoInfortunio;
    }

    public String getDifesa2() {
        return difesa2;
    }

    public void setDifesa2(String difesa2) {
        this.difesa2 = difesa2;
    }

    public String getDifesa2II() {
        return difesa2II;
    }

    public void setDifesa2II(String difesa2II) {
        this.difesa2II = difesa2II;
    }

    public String getAttacco2() {
        return attacco2;
    }

    public void setAttacco2(String attacco2) {
        this.attacco2 = attacco2;
    }

    public String getAttacco2II() {
        return attacco2II;
    }

    public void setAttacco2II(String attacco2II) {
        this.attacco2II = attacco2II;
    }

    public String getCoordinativa() {
        return coordinativa;
    }

    public void setCoordinativa(String coordinativa) {
        this.coordinativa = coordinativa;
    }

    public String getCondizionale() {
        return condizionale;
    }

    public void setCondizionale(String condizionale) {
        this.condizionale = condizionale;
    }

    public String getObDifIII() {
        return obDifIII;
    }

    public void setObDifIII(String obDifIII) {
        this.obDifIII = obDifIII;
    }

    public String getDifesa2III() {
        return difesa2III;
    }

    public void setDifesa2III(String difesa2III) {
        this.difesa2III = difesa2III;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Esercizio)) {
            return false;
        }
        Esercizio other = (Esercizio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.Esercizio[ id=" + id + " ]";
    }
    
}
