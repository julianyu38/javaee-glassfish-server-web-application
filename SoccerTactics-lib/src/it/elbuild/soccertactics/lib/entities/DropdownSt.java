/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "DROPDOWN_ST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DropdownSt.findAll", query = "SELECT d FROM DropdownSt d"),
    @NamedQuery(name = "DropdownSt.findById", query = "SELECT d FROM DropdownSt d WHERE d.id = :id"),
    @NamedQuery(name = "DropdownSt.findByNome", query = "SELECT d FROM DropdownSt d WHERE d.nome = :nome"),
    @NamedQuery(name = "DropdownSt.findByCategoria", query = "SELECT d FROM DropdownSt d WHERE d.categoria = :categoria")})
public class DropdownSt implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "NOME")
    private String nome;
    @Column(name = "CATEGORIA")
    private String categoria;

    public DropdownSt() {
    }

    public DropdownSt(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DropdownSt)) {
            return false;
        }
        DropdownSt other = (DropdownSt) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.DropdownSt[ id=" + id + " ]";
    }
    
}
