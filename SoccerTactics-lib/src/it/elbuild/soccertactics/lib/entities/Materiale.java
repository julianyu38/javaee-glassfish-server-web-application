/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "MATERIALE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Materiale.findAll", query = "SELECT m FROM Materiale m"),
    @NamedQuery(name = "Materiale.findById", query = "SELECT m FROM Materiale m WHERE m.id = :id"),
    @NamedQuery(name = "Materiale.findByIdEsercizio", query = "SELECT m FROM Materiale m WHERE m.idEsercizio = :idEsercizio"),
    @NamedQuery(name = "Materiale.findByNome", query = "SELECT m FROM Materiale m WHERE m.nome = :nome"),
    @NamedQuery(name = "Materiale.findByQuantita", query = "SELECT m FROM Materiale m WHERE m.quantita = :quantita")})
public class Materiale implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ID_ESERCIZIO")
    private Integer idEsercizio;
    @Size(max = 256)
    @Column(name = "NOME")
    private String nome;
    @Column(name = "QUANTITA")
    private Integer quantita;

    public Materiale() {
    }

    public Materiale(Integer id) {
        this.id = id;
    }

    public Materiale(String nome, Integer quantita) {
        this.nome = nome;
        this.quantita = quantita;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEsercizio() {
        return idEsercizio;
    }

    public void setIdEsercizio(Integer idEsercizio) {
        this.idEsercizio = idEsercizio;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQuantita() {
        return quantita;
    }

    public void setQuantita(Integer quantita) {
        this.quantita = quantita;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Materiale)) {
            return false;
        }
        Materiale other = (Materiale) object;
        if( (this.id==null || (id!=null && other.id != null && this.id.equals(other.id)))&& 
            ((this.nome==null && other.nome==null) || (this.nome!=null && other.nome!=null && this.nome.equals(other.nome))) && 
            ((this.idEsercizio==null && other.idEsercizio==null) || (this.idEsercizio!=null && other.idEsercizio!=null && this.idEsercizio.equals(other.idEsercizio))) &&
            ((this.quantita==null && other.quantita==null) || (this.quantita!=null && other.quantita!=null && this.quantita.equals(other.quantita))) ){
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.Materiale[ id=" + id + " ]";
    }
    
}
