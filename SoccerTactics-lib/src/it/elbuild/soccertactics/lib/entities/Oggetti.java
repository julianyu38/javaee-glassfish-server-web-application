/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "OGGETTI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Oggetti.findAll", query = "SELECT o FROM Oggetti o ORDER BY o.nome ASC"),
    @NamedQuery(name = "Oggetti.findById", query = "SELECT o FROM Oggetti o WHERE o.id = :id"),
    @NamedQuery(name = "Oggetti.findByPath", query = "SELECT o FROM Oggetti o WHERE o.path = :path")})
public class Oggetti implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "PATH")
    private String path;
    @Column(name = "CATEGORIA") // default o custom (per decidere se è colorabile o meno)
    private String categoria;
    @Column(name = "TIPO")
    private String tipo;
    @Column(name = "NOME")
    private String nome;

    public Oggetti() {
    }

    public Oggetti(Integer id) {
        this.id = id;
    }
    
    public String pathCustomImg(String color){
        if(path.contains("MISTER")){
            return "../images/es/portieri/"+path+".png";
        }else{
            return "../images/es/portieri/"+path+"_"+color+".png";
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Oggetti)) {
            return false;
        }
        Oggetti other = (Oggetti) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.Oggetti[ id=" + id + " ]";
    }
    
}
