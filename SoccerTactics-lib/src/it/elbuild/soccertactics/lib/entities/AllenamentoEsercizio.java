/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "ALLENAMENTO_ESERCIZIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AllenamentoEsercizio.findAll", query = "SELECT a FROM AllenamentoEsercizio a"),
    @NamedQuery(name = "AllenamentoEsercizio.findById", query = "SELECT a FROM AllenamentoEsercizio a WHERE a.id = :id"),
    @NamedQuery(name = "AllenamentoEsercizio.findByIdAllenamento", query = "SELECT a FROM AllenamentoEsercizio a WHERE a.idAllenamento = :idAllenamento"),
    @NamedQuery(name = "AllenamentoEsercizio.findByIdEsercizio", query = "SELECT a FROM AllenamentoEsercizio a WHERE a.idEsercizio = :idEsercizio"),
    @NamedQuery(name = "AllenamentoEsercizio.findByFase", query = "SELECT a FROM AllenamentoEsercizio a WHERE a.fase = :fase")})
public class AllenamentoEsercizio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ID_ALLENAMENTO")
    private Integer idAllenamento;
    @Column(name = "ID_ESERCIZIO")
    private Integer idEsercizio;
    @Column(name = "FASE")
    private String fase;
    
    @OneToOne
    @JoinColumn(name="ID_ESERCIZIO", referencedColumnName="ID", insertable=false, updatable=false)
    private Esercizio esercizio;

    public AllenamentoEsercizio() {
    }

    public AllenamentoEsercizio(Integer id) {
        this.id = id;
    }

    public AllenamentoEsercizio(String fase, Esercizio esercizio) {
        this.fase = fase;
        this.esercizio = esercizio;
        this.idEsercizio = esercizio.getId();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdAllenamento() {
        return idAllenamento;
    }

    public void setIdAllenamento(Integer idAllenamento) {
        this.idAllenamento = idAllenamento;
    }

    public Integer getIdEsercizio() {
        return idEsercizio;
    }

    public void setIdEsercizio(Integer idEsercizio) {
        this.idEsercizio = idEsercizio;
    }

    public String getFase() {
        return fase;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }

    public Esercizio getEsercizio() {
        return esercizio;
    }

    public void setEsercizio(Esercizio esercizio) {
        this.esercizio = esercizio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AllenamentoEsercizio)) {
            return false;
        }
        AllenamentoEsercizio other = (AllenamentoEsercizio) object;
        if( (this.id==null || (id!=null && other.id != null && this.id.equals(other.id)))&& 
            ((this.fase==null && other.fase==null) || (this.fase!=null && other.fase!=null && this.fase.equals(other.fase))) && 
            ((this.idEsercizio==null && other.idEsercizio==null) || (this.idEsercizio!=null && other.idEsercizio!=null && this.idEsercizio.equals(other.idEsercizio))) &&
            ((this.idAllenamento==null && other.idAllenamento==null) || (this.idAllenamento!=null && other.idAllenamento!=null && this.idAllenamento.equals(other.idAllenamento))) ){
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.AllenamentoEsercizio[ id=" + id + " ]";
    }
    
}
