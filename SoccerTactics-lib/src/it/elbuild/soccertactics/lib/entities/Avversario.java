/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "AVVERSARIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Avversario.findAll", query = "SELECT a FROM Avversario a"),
    @NamedQuery(name = "Avversario.findById", query = "SELECT a FROM Avversario a WHERE a.id = :id"),
    @NamedQuery(name = "Avversario.findByIdStagione", query = "SELECT a FROM Avversario a WHERE a.idStagione = :idStagione"),
    @NamedQuery(name = "Avversario.findByNome", query = "SELECT a FROM Avversario a WHERE a.nome = :nome")})
public class Avversario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "NOME")
    private String nome;
    @Column(name = "ID_STAGIONE")
    private Integer idStagione;
    @Column(name = "STEMMA")
    private String stemma;
    
    @OneToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
    @JoinColumn(name = "ID_AVVERSARIO", referencedColumnName = "ID")   
    private List<GiocatoreAvversario> giocatori;

    public Avversario() {
        giocatori = new ArrayList<GiocatoreAvversario>();        
    }
    
    public void addGiocatore(){
        if(giocatori==null){
            giocatori = new ArrayList<GiocatoreAvversario>();        
        }
        giocatori.add(new GiocatoreAvversario());
    }
    
    public void removeGiocatore(GiocatoreAvversario giocatore){
        if(giocatori.contains(giocatore)){
            giocatori.remove(giocatore);      
        }
    }

    public Avversario(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<GiocatoreAvversario> getGiocatori() {
        return giocatori;
    }

    public void setGiocatori(List<GiocatoreAvversario> giocatori) {
        this.giocatori = giocatori;
    }

    public Integer getIdStagione() {
        return idStagione;
    }

    public void setIdStagione(Integer idStagione) {
        this.idStagione = idStagione;
    }

    public String getStemma() {
        return stemma;
    }

    public void setStemma(String stemma) {
        this.stemma = stemma;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Avversario)) {
            return false;
        }
        Avversario other = (Avversario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.Avversario[ id=" + id + " ]";
    }
    
}
