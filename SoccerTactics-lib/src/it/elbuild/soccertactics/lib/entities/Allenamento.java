/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.utils.Constants;
import it.elbuild.soccertactics.lib.utils.MaterialeAllenamento;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "ALLENAMENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Allenamento.findAll", query = "SELECT a FROM Allenamento a where a.eliminato = '0' "),
    @NamedQuery(name = "Allenamento.findById", query = "SELECT a FROM Allenamento a WHERE a.id = :id AND a.eliminato = '0'"),
    @NamedQuery(name = "Allenamento.findByTitolo", query = "SELECT a FROM Allenamento a WHERE a.titolo = :titolo"),
    @NamedQuery(name = "Allenamento.findByDescrizione", query = "SELECT a FROM Allenamento a WHERE a.descrizione = :descrizione"),
    @NamedQuery(name = "Allenamento.findByMeseAnno", query = "SELECT a FROM Allenamento a WHERE a.eliminato = '0' AND a.idStagione = :idStagione AND FUNC('MONTH', a.dataInizio) = :mese AND FUNC('YEAR', a.dataInizio) = :anno ORDER BY a.dataInizio DESC"),
    @NamedQuery(name = "Allenamento.findByIdStagione", query = "SELECT a FROM Allenamento a WHERE a.eliminato = '0' AND a.idStagione = :idStagione ORDER BY a.dataInizio DESC"),
    @NamedQuery(name = "Allenamento.findByDate", query = "SELECT a FROM Allenamento a WHERE a.eliminato = '0' AND a.idStagione = :idStagione AND a.dataInizio >= :inizio AND a.dataInizio <= :fine ")})
public class Allenamento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ID_STAGIONE")
    private Integer idStagione;
    @Column(name = "TITOLO")
    private String titolo;
    @Column(name = "DATA_INIZIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInizio;
    @Column(name = "DATA_FINE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFine;
    @Column(name = "OB_TEC")
    private String obTec;
    @Column(name = "OB_FIS")
    private String obFis;
    @Column(name = "OB_TAT")
    private String obTat;
    @Column(name = "OB_PSI")
    private String obPsi;
    @Column(name = "DESCRIZIONE")
    private String descrizione;
    @Column(name = "ELIMINATO")
    private Boolean eliminato;
    
    @OneToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
    @JoinColumn(name = "ID_ALLENAMENTO", referencedColumnName = "ID")   
    private List<AllenamentoEsercizio> esercizi;
        
    @ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(
      name="ALLENAMENTO_GIOCATORE",
      joinColumns={ @JoinColumn(name="ID_ALLENAMENTO", referencedColumnName="ID") },
      inverseJoinColumns={ @JoinColumn(name="ID_GIOCATORE", referencedColumnName="ID", unique=true) }
    )
    private List<Giocatore> giocatori;
    
    @Transient
    private Date dataI;
    
    @Transient
    private String oraI;
    
    @Transient
    private String oraF;
    
    @Transient
    private List<AllenamentoEsercizio> fase1;
    @Transient
    private List<AllenamentoEsercizio> fase2;
    @Transient
    private List<AllenamentoEsercizio> fase3;
    @Transient
    private List<AllenamentoEsercizio> fase4;
    
    @Transient
    List<MaterialeAllenamento> materiali;

    public Allenamento() {
        esercizi = new ArrayList<AllenamentoEsercizio>();
        fase1 = new ArrayList<AllenamentoEsercizio>();
        fase2 = new ArrayList<AllenamentoEsercizio>();
        fase3 = new ArrayList<AllenamentoEsercizio>();
        fase4 = new ArrayList<AllenamentoEsercizio>();
        giocatori = new ArrayList<Giocatore>();
        eliminato = false;
    }

    public Allenamento(Integer id) {
        this.id = id;
    }
    
    // per recuperare le variabili transient nella modifica del corso
    public void impostaOrariTransient(){
        if(dataInizio!=null){
            Calendar inizio = Calendar.getInstance(TimeZone.getTimeZone("CET"), Locale.ITALIAN);
            inizio.setTime(dataInizio);            
            dataI = inizio.getTime();
            oraI = inizio.get(Calendar.HOUR_OF_DAY)+":"+inizio.get(Calendar.MINUTE);            
        }
        if(dataFine!=null){
            Calendar fine = Calendar.getInstance(TimeZone.getTimeZone("CET"), Locale.ITALIAN);
            fine.setTime(dataFine);            
            oraF = fine.get(Calendar.HOUR_OF_DAY)+":"+fine.get(Calendar.MINUTE);            
        }
    }
    
    // per impostare data termine e data inizio dopo inserimento o modifica corso
     public boolean impostaDate(){
         Calendar data = null;
         if(dataI!=null && oraI!=null && oraI.length()>0 && oraF!=null && oraF.length()>0){
             data = Calendar.getInstance(TimeZone.getTimeZone("CET"), Locale.ITALIAN);
             data.setTime(dataI);
             String[] om = oraI.split(":");
             data.set(Calendar.HOUR_OF_DAY, Integer.parseInt(om[0]));
             data.set(Calendar.MINUTE, Integer.parseInt(om[1]));
             dataInizio = data.getTime();
             
             om = oraF.split(":");
             data.set(Calendar.HOUR_OF_DAY, Integer.parseInt(om[0]));
             data.set(Calendar.MINUTE, Integer.parseInt(om[1]));
             dataFine = data.getTime();
         }   
         
         // controllo date data inizio precedente a data fine 
        if(dataInizio!=null && dataFine!=null){
            return dataInizio.before(dataFine) ? true : false;
        }else{
            return false;
        }
     }
     
     public void updateListEsercizi(){
         fase1 = new ArrayList<AllenamentoEsercizio>(); 
         fase2 = new ArrayList<AllenamentoEsercizio>(); 
         fase3 = new ArrayList<AllenamentoEsercizio>(); 
         fase4 = new ArrayList<AllenamentoEsercizio>(); 
         if(esercizi!=null){
             for(AllenamentoEsercizio ae : esercizi){
                 if(ae.getFase().equals(Constants.FasiAllenamento.FASE1)){
                     fase1.add(ae);
                 }else if(ae.getFase().equals(Constants.FasiAllenamento.FASE2)){
                     fase2.add(ae);
                 }else if(ae.getFase().equals(Constants.FasiAllenamento.FASE3)){
                     fase3.add(ae);
                 }else if(ae.getFase().equals(Constants.FasiAllenamento.FASE4)){
                     fase4.add(ae);
                 }
             }
         }
     }
    
    public void addEsercizio(Esercizio esercizio, String fase){
        if(esercizi == null){
            esercizi = new ArrayList<AllenamentoEsercizio>();
        }
        if(esercizio != null){
            AllenamentoEsercizio ae = new AllenamentoEsercizio(fase, esercizio);
            esercizi.add(ae);
            if (ae.getFase().equals(Constants.FasiAllenamento.FASE1)) {
                fase1.add(ae);
            } else if (ae.getFase().equals(Constants.FasiAllenamento.FASE2)) {
                fase2.add(ae);
            } else if (ae.getFase().equals(Constants.FasiAllenamento.FASE3)) {
                fase3.add(ae);
            } else if (ae.getFase().equals(Constants.FasiAllenamento.FASE4)) {
                fase4.add(ae);
            }
        }
    }
    
    public void removeEsercizio(AllenamentoEsercizio esercizio){
        if(esercizi.contains(esercizio)){
            esercizi.remove(esercizio);
            if (esercizio.getFase().equals(Constants.FasiAllenamento.FASE1)) {
                fase1.remove(esercizio);
            } else if (esercizio.getFase().equals(Constants.FasiAllenamento.FASE2)) {
                fase2.remove(esercizio);
            } else if (esercizio.getFase().equals(Constants.FasiAllenamento.FASE3)) {
                fase3.remove(esercizio);
            } else if (esercizio.getFase().equals(Constants.FasiAllenamento.FASE4)) {
                fase4.remove(esercizio);
            }
        }
    }
    
    public void updateMaterialiAllenamento(){
        materiali = new ArrayList<MaterialeAllenamento>();
        if (esercizi != null) {
            Integer indexMa = null;
            for (AllenamentoEsercizio ae : esercizi) {
                if (ae.getEsercizio().getMateriali() != null) {
                    for (Materiale m : ae.getEsercizio().getMateriali()) {
                        indexMa = containsMateriale(m.getNome());
                        if(indexMa==null){
                            materiali.add(new MaterialeAllenamento(m.getNome(), m.getQuantita()));
                        }else{
                            materiali.get(indexMa).setMaxQuantita(m.getQuantita());
                        }
                    }
                }
            }
        }
    }
    
    public Integer containsMateriale(String materiale){
        if(materiali!=null){
            for(MaterialeAllenamento m : materiali){
                if(m.getMateriale()!=null && m.getMateriale().equals(materiale)){
                    return materiali.indexOf(m);
                }
            }
        }
        return null;
    }
    
    public void addGiocatore(Giocatore giocatore){
        if(giocatori == null){
            giocatori = new ArrayList<Giocatore>();
        }
        if(giocatore!=null && !giocatori.contains(giocatore)){
            giocatori.add(giocatore);
        }
    }
    
    public void removeGiocatore(Giocatore giocatore){
        if(giocatori.contains(giocatore)){
            giocatori.remove(giocatore);
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public Date getDataInizio() {
        return dataInizio;
    }

    public void setDataInizio(Date dataInizio) {
        this.dataInizio = dataInizio;
    }

    public Date getDataFine() {
        return dataFine;
    }

    public void setDataFine(Date dataFine) {
        this.dataFine = dataFine;
    }

    public String getObTec() {
        return obTec;
    }

    public void setObTec(String obTec) {
        this.obTec = obTec;
    }

    public String getObFis() {
        return obFis;
    }

    public void setObFis(String obFis) {
        this.obFis = obFis;
    }

    public String getObTat() {
        return obTat;
    }

    public void setObTat(String obTat) {
        this.obTat = obTat;
    }

    public String getObPsi() {
        return obPsi;
    }

    public void setObPsi(String obPsi) {
        this.obPsi = obPsi;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public List<AllenamentoEsercizio> getEsercizi() {
        return esercizi;
    }

    public void setEsercizi(List<AllenamentoEsercizio> esercizi) {
        this.esercizi = esercizi;
    }
   
    public List<Giocatore> getGiocatori() {
        return giocatori;
    }

    public void setGiocatori(List<Giocatore> giocatori) {
        this.giocatori = giocatori;
    }

    public Boolean getEliminato() {
        return eliminato;
    }

    public void setEliminato(Boolean eliminato) {
        this.eliminato = eliminato;
    }

    public Date getDataI() {
        return dataI;
    }

    public void setDataI(Date dataI) {
        this.dataI = dataI;
    }

    public String getOraI() {
        return oraI;
    }

    public void setOraI(String oraI) {
        this.oraI = oraI;
    }

    public String getOraF() {
        return oraF;
    }

    public void setOraF(String oraF) {
        this.oraF = oraF;
    }

    public Integer getIdStagione() {
        return idStagione;
    }

    public void setIdStagione(Integer idStagione) {
        this.idStagione = idStagione;
    }

    public List<AllenamentoEsercizio> getFase1() {
        return fase1;
    }

    public void setFase1(List<AllenamentoEsercizio> fase1) {
        this.fase1 = fase1;
    }

    public List<AllenamentoEsercizio> getFase2() {
        return fase2;
    }

    public void setFase2(List<AllenamentoEsercizio> fase2) {
        this.fase2 = fase2;
    }

    public List<AllenamentoEsercizio> getFase3() {
        return fase3;
    }

    public void setFase3(List<AllenamentoEsercizio> fase3) {
        this.fase3 = fase3;
    }

    public List<AllenamentoEsercizio> getFase4() {
        return fase4;
    }

    public void setFase4(List<AllenamentoEsercizio> fase4) {
        this.fase4 = fase4;
    }

    public List<MaterialeAllenamento> getMateriali() {
        return materiali;
    }

    public void setMateriali(List<MaterialeAllenamento> materiali) {
        this.materiali = materiali;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Allenamento)) {
            return false;
        }
        Allenamento other = (Allenamento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.Allenamento[ id=" + id + " ]";
    }
    
}
