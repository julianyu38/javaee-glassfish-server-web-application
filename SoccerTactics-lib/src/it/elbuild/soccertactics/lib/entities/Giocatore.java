/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "GIOCATORE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Giocatore.findAll", query = "SELECT g FROM Giocatore g"),
    @NamedQuery(name = "Giocatore.findById", query = "SELECT g FROM Giocatore g WHERE g.id = :id"),
    @NamedQuery(name = "Giocatore.findByIdStagione", query = "SELECT g FROM Giocatore g WHERE g.idStagione = :idStagione ORDER BY g.gruppo"),
    @NamedQuery(name = "Giocatore.findByNome", query = "SELECT g FROM Giocatore g WHERE g.nome = :nome"),
    @NamedQuery(name = "Giocatore.findByCognome", query = "SELECT g FROM Giocatore g WHERE g.cognome = :cognome"),
    @NamedQuery(name = "Giocatore.findByFoto", query = "SELECT g FROM Giocatore g WHERE g.foto = :foto"),
    @NamedQuery(name = "Giocatore.findByRuolo", query = "SELECT g FROM Giocatore g WHERE g.ruolo = :ruolo"),
    @NamedQuery(name = "Giocatore.findByPiede", query = "SELECT g FROM Giocatore g WHERE g.piede = :piede"),
    @NamedQuery(name = "Giocatore.findByDataNascita", query = "SELECT g FROM Giocatore g WHERE g.dataNascita = :dataNascita"),
    @NamedQuery(name = "Giocatore.findByLuogoNascita", query = "SELECT g FROM Giocatore g WHERE g.luogoNascita = :luogoNascita"),
    @NamedQuery(name = "Giocatore.findByResidenzaRegione", query = "SELECT g FROM Giocatore g WHERE g.residenzaRegione = :residenzaRegione"),
    @NamedQuery(name = "Giocatore.findByResidenzaProvincia", query = "SELECT g FROM Giocatore g WHERE g.residenzaProvincia = :residenzaProvincia"),
    @NamedQuery(name = "Giocatore.findByResidenzaCitta", query = "SELECT g FROM Giocatore g WHERE g.residenzaCitta = :residenzaCitta"),
    @NamedQuery(name = "Giocatore.findByResidenzaVia", query = "SELECT g FROM Giocatore g WHERE g.residenzaVia = :residenzaVia"),
    @NamedQuery(name = "Giocatore.findByResidenzaCap", query = "SELECT g FROM Giocatore g WHERE g.residenzaCap = :residenzaCap"),
    @NamedQuery(name = "Giocatore.findByCellulare", query = "SELECT g FROM Giocatore g WHERE g.cellulare = :cellulare"),
    @NamedQuery(name = "Giocatore.findByPeso", query = "SELECT g FROM Giocatore g WHERE g.peso = :peso"),
    @NamedQuery(name = "Giocatore.findByAltezza", query = "SELECT g FROM Giocatore g WHERE g.altezza = :altezza"),
    @NamedQuery(name = "Giocatore.findByAltezzaSeduto", query = "SELECT g FROM Giocatore g WHERE g.altezzaSeduto = :altezzaSeduto"),
    @NamedQuery(name = "Giocatore.findByTorace", query = "SELECT g FROM Giocatore g WHERE g.torace = :torace"),
    @NamedQuery(name = "Giocatore.findByCosciaMaxDx", query = "SELECT g FROM Giocatore g WHERE g.cosciaMaxDx = :cosciaMaxDx"),
    @NamedQuery(name = "Giocatore.findByCosciaMaxSx", query = "SELECT g FROM Giocatore g WHERE g.cosciaMaxSx = :cosciaMaxSx"),
    @NamedQuery(name = "Giocatore.findByCosciaMediaDx", query = "SELECT g FROM Giocatore g WHERE g.cosciaMediaDx = :cosciaMediaDx"),
    @NamedQuery(name = "Giocatore.findByCosciaMediaSx", query = "SELECT g FROM Giocatore g WHERE g.cosciaMediaSx = :cosciaMediaSx"),
    @NamedQuery(name = "Giocatore.findByCosciaMinDx", query = "SELECT g FROM Giocatore g WHERE g.cosciaMinDx = :cosciaMinDx"),
    @NamedQuery(name = "Giocatore.findByCosciaMinSx", query = "SELECT g FROM Giocatore g WHERE g.cosciaMinSx = :cosciaMinSx"),
    @NamedQuery(name = "Giocatore.findByGambaSx", query = "SELECT g FROM Giocatore g WHERE g.gambaSx = :gambaSx"),
    @NamedQuery(name = "Giocatore.findByGambaDx", query = "SELECT g FROM Giocatore g WHERE g.gambaDx = :gambaDx")})
public class Giocatore implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ID_STAGIONE")
    private Integer idStagione;
    @Column(name = "GRUPPO")
    private String gruppo;
    @Column(name = "NOME")
    private String nome;
    @Column(name = "COGNOME")
    private String cognome;
    @Column(name = "FOTO")
    private String foto;
    @Column(name = "RUOLO")
    private String ruolo;
    @Column(name = "PIEDE")
    private String piede;
    @Column(name = "DATA_NASCITA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataNascita;
    @Column(name = "LUOGO_NASCITA")
    private String luogoNascita;
    @Column(name = "RESIDENZA_REGIONE")
    private String residenzaRegione;
    @Column(name = "RESIDENZA_PROVINCIA")
    private String residenzaProvincia;
    @Column(name = "RESIDENZA_CITTA")
    private String residenzaCitta;
    @Column(name = "RESIDENZA_VIA")
    private String residenzaVia;
    @Column(name = "RESIDENZA_CAP")
    private String residenzaCap;
    @Column(name = "CELLULARE")
    private String cellulare;
    @Column(name = "PESO")
    private Double peso;
    @Column(name = "ALTEZZA")
    private Double altezza;
    @Column(name = "ALTEZZA_SEDUTO")
    private Double altezzaSeduto;
    @Column(name = "TORACE")
    private Double torace;
    @Column(name = "COSCIA_MAX_DX")
    private Double cosciaMaxDx;
    @Column(name = "COSCIA_MAX_SX")
    private Double cosciaMaxSx;
    @Column(name = "COSCIA_MEDIA_DX")
    private Double cosciaMediaDx;
    @Column(name = "COSCIA_MEDIA_SX")
    private Double cosciaMediaSx;
    @Column(name = "COSCIA_MIN_DX")
    private Double cosciaMinDx;
    @Column(name = "COSCIA_MIN_SX")
    private Double cosciaMinSx;
    @Column(name = "GAMBA_SX")
    private Double gambaSx;
    @Column(name = "GAMBA_DX")
    private Double gambaDx;
    @Column(name = "EMAIL")
    private String email;
    
    @Column(name = "POSTURA_ATT_DX")
    private Double posturaAttDx;
    @Column(name = "POSTURA_ATT_SX")
    private Double posturaAttSx;
    @Column(name = "PRESA_SX")
    private Double presaSx;
    @Column(name = "PRESA_DX")
    private Double presaDx;
    @Column(name = "RACCOLTA_SX")
    private Double raccoltaSx;
    @Column(name = "RACCOLTA_DX")
    private Double raccoltaDx;
    @Column(name = "TUFFO_STRIS_SX")
    private Double tuffoStrisSx;
    @Column(name = "TUFFO_STRIS_DX")
    private Double tuffoStrisDx;
    @Column(name = "TUFFO_TRAIET_ALT_SX")
    private Double tuffoTraietAltSx;
    @Column(name = "TUFFO_TRAIET_ALT_DX")
    private Double tuffoTraietAltDx;
    @Column(name = "UNOVSUNO_SX")
    private Double unoVSunoSx;
    @Column(name = "UNOVSUNO_DX")
    private Double unoVSunoDx;
    @Column(name = "USCITA_BASSA_SX")
    private Double uscitaBassaSx;
    @Column(name = "USCITA_BASSA_DX")
    private Double uscitaBassaDx;
    @Column(name = "USCITA_ALTA_SX")
    private Double uscitaAltaSx;
    @Column(name = "USCITA_ALTA_DX")
    private Double uscitaAltaDx;
    @Column(name = "GEST_PRES_GARA")
    private Double gestPresGara;
    @Column(name = "COMUNICAZIONE")
    private Double comunicazione;
    @Column(name = "PROBLEM_SOLVING")
    private Double problemSolving;
    @Column(name = "SICUREZZA_INSICUREZZA")
    private Double sicurezzaInsicurezza;
     @Column(name = "EFFICACIA")
    private Double efficacia;
    @Column(name = "REAZ_EVENT_ERRORE")
    private Double reazEventErrore;
    @Column(name = "PARTECIPAZIONE_GIOCO")
    private Double partecipazioneGioco;
    @Column(name = "VALUTAZIONE_CAP_COORD")
    private String valCapCoord;
    @Column(name = "VALUTAZIONE_CAP_CONDIZ")
    private String valCapCondiz;
    @Column(name = "NOTE")
    private String note;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ID_STAGIONE", referencedColumnName="ID", insertable=false, updatable=false)
    private Stagione stagione;

    public Giocatore() {
    }

    public Giocatore(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getRuolo() {
        return ruolo;
    }

    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }

    public String getPiede() {
        return piede;
    }

    public void setPiede(String piede) {
        this.piede = piede;
    }

    public Date getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(Date dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getLuogoNascita() {
        return luogoNascita;
    }

    public void setLuogoNascita(String luogoNascita) {
        this.luogoNascita = luogoNascita;
    }

    public String getResidenzaRegione() {
        return residenzaRegione;
    }

    public void setResidenzaRegione(String residenzaRegione) {
        this.residenzaRegione = residenzaRegione;
    }

    public String getResidenzaProvincia() {
        return residenzaProvincia;
    }

    public void setResidenzaProvincia(String residenzaProvincia) {
        this.residenzaProvincia = residenzaProvincia;
    }

    public String getResidenzaCitta() {
        return residenzaCitta;
    }

    public void setResidenzaCitta(String residenzaCitta) {
        this.residenzaCitta = residenzaCitta;
    }

    public String getResidenzaVia() {
        return residenzaVia;
    }

    public void setResidenzaVia(String residenzaVia) {
        this.residenzaVia = residenzaVia;
    }

    public String getResidenzaCap() {
        return residenzaCap;
    }

    public void setResidenzaCap(String residenzaCap) {
        this.residenzaCap = residenzaCap;
    }

    public String getCellulare() {
        return cellulare;
    }

    public void setCellulare(String cellulare) {
        this.cellulare = cellulare;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public Double getAltezza() {
        return altezza;
    }

    public void setAltezza(Double altezza) {
        this.altezza = altezza;
    }

    public Double getAltezzaSeduto() {
        return altezzaSeduto;
    }

    public void setAltezzaSeduto(Double altezzaSeduto) {
        this.altezzaSeduto = altezzaSeduto;
    }

    public Double getTorace() {
        return torace;
    }

    public void setTorace(Double torace) {
        this.torace = torace;
    }

    public Double getCosciaMaxDx() {
        return cosciaMaxDx;
    }

    public void setCosciaMaxDx(Double cosciaMaxDx) {
        this.cosciaMaxDx = cosciaMaxDx;
    }

    public Double getCosciaMaxSx() {
        return cosciaMaxSx;
    }

    public void setCosciaMaxSx(Double cosciaMaxSx) {
        this.cosciaMaxSx = cosciaMaxSx;
    }

    public Double getCosciaMediaDx() {
        return cosciaMediaDx;
    }

    public void setCosciaMediaDx(Double cosciaMediaDx) {
        this.cosciaMediaDx = cosciaMediaDx;
    }

    public Double getCosciaMediaSx() {
        return cosciaMediaSx;
    }

    public void setCosciaMediaSx(Double cosciaMediaSx) {
        this.cosciaMediaSx = cosciaMediaSx;
    }

    public Double getCosciaMinDx() {
        return cosciaMinDx;
    }

    public void setCosciaMinDx(Double cosciaMinDx) {
        this.cosciaMinDx = cosciaMinDx;
    }

    public Double getCosciaMinSx() {
        return cosciaMinSx;
    }

    public void setCosciaMinSx(Double cosciaMinSx) {
        this.cosciaMinSx = cosciaMinSx;
    }

    public Double getGambaSx() {
        return gambaSx;
    }

    public void setGambaSx(Double gambaSx) {
        this.gambaSx = gambaSx;
    }

    public Double getGambaDx() {
        return gambaDx;
    }

    public void setGambaDx(Double gambaDx) {
        this.gambaDx = gambaDx;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIdStagione() {
        return idStagione;
    }

    public void setIdStagione(Integer idStagione) {
        this.idStagione = idStagione;
    }

    public String getGruppo() {
        return gruppo;
    }

    public void setGruppo(String gruppo) {
        this.gruppo = gruppo;
    }

    public Double getPosturaAttDx() {
        return posturaAttDx;
    }

    public void setPosturaAttDx(Double posturaAttDx) {
        this.posturaAttDx = posturaAttDx;
    }

    public Double getPosturaAttSx() {
        return posturaAttSx;
    }

    public void setPosturaAttSx(Double posturaAttSx) {
        this.posturaAttSx = posturaAttSx;
    }

    public Double getPresaSx() {
        return presaSx;
    }

    public void setPresaSx(Double presaSx) {
        this.presaSx = presaSx;
    }

    public Double getPresaDx() {
        return presaDx;
    }

    public void setPresaDx(Double presaDx) {
        this.presaDx = presaDx;
    }

    public Double getRaccoltaSx() {
        return raccoltaSx;
    }

    public void setRaccoltaSx(Double raccoltaSx) {
        this.raccoltaSx = raccoltaSx;
    }

    public Double getRaccoltaDx() {
        return raccoltaDx;
    }

    public void setRaccoltaDx(Double raccoltaDx) {
        this.raccoltaDx = raccoltaDx;
    }

    public Double getTuffoStrisSx() {
        return tuffoStrisSx;
    }

    public void setTuffoStrisSx(Double tuffoStrisSx) {
        this.tuffoStrisSx = tuffoStrisSx;
    }

    public Double getTuffoStrisDx() {
        return tuffoStrisDx;
    }

    public void setTuffoStrisDx(Double tuffoStrisDx) {
        this.tuffoStrisDx = tuffoStrisDx;
    }

    public Double getTuffoTraietAltSx() {
        return tuffoTraietAltSx;
    }

    public void setTuffoTraietAltSx(Double tuffoTraietAltSx) {
        this.tuffoTraietAltSx = tuffoTraietAltSx;
    }

    public Double getTuffoTraietAltDx() {
        return tuffoTraietAltDx;
    }

    public void setTuffoTraietAltDx(Double tuffoTraietAltDx) {
        this.tuffoTraietAltDx = tuffoTraietAltDx;
    }

    public Double getUnoVSunoSx() {
        return unoVSunoSx;
    }

    public void setUnoVSunoSx(Double unoVSunoSx) {
        this.unoVSunoSx = unoVSunoSx;
    }

    public Double getUnoVSunoDx() {
        return unoVSunoDx;
    }

    public void setUnoVSunoDx(Double unoVSunoDx) {
        this.unoVSunoDx = unoVSunoDx;
    }

    public Double getUscitaBassaSx() {
        return uscitaBassaSx;
    }

    public void setUscitaBassaSx(Double uscitaBassaSx) {
        this.uscitaBassaSx = uscitaBassaSx;
    }

    public Double getUscitaBassaDx() {
        return uscitaBassaDx;
    }

    public void setUscitaBassaDx(Double uscitaBassaDx) {
        this.uscitaBassaDx = uscitaBassaDx;
    }

    public Double getUscitaAltaSx() {
        return uscitaAltaSx;
    }

    public void setUscitaAltaSx(Double uscitaAltaSx) {
        this.uscitaAltaSx = uscitaAltaSx;
    }

    public Double getUscitaAltaDx() {
        return uscitaAltaDx;
    }

    public void setUscitaAltaDx(Double uscitaAltaDx) {
        this.uscitaAltaDx = uscitaAltaDx;
    }

    public Double getGestPresGara() {
        return gestPresGara;
    }

    public void setGestPresGara(Double gestPresGara) {
        this.gestPresGara = gestPresGara;
    }

    public Double getComunicazione() {
        return comunicazione;
    }

    public void setComunicazione(Double comunicazione) {
        this.comunicazione = comunicazione;
    }

    public Double getProblemSolving() {
        return problemSolving;
    }

    public void setProblemSolving(Double problemSolving) {
        this.problemSolving = problemSolving;
    }

    public Double getSicurezzaInsicurezza() {
        return sicurezzaInsicurezza;
    }

    public void setSicurezzaInsicurezza(Double sicurezzaInsicurezza) {
        this.sicurezzaInsicurezza = sicurezzaInsicurezza;
    }

    public Double getEfficacia() {
        return efficacia;
    }

    public void setEfficacia(Double efficacia) {
        this.efficacia = efficacia;
    }

    public Double getReazEventErrore() {
        return reazEventErrore;
    }

    public void setReazEventErrore(Double reazEventErrore) {
        this.reazEventErrore = reazEventErrore;
    }

    public Double getPartecipazioneGioco() {
        return partecipazioneGioco;
    }

    public void setPartecipazioneGioco(Double partecipazioneGioco) {
        this.partecipazioneGioco = partecipazioneGioco;
    }

    public String getValCapCoord() {
        return valCapCoord;
    }

    public void setValCapCoord(String valCapCoord) {
        this.valCapCoord = valCapCoord;
    }

    public String getValCapCondiz() {
        return valCapCondiz;
    }

    public void setValCapCondiz(String valCapCondiz) {
        this.valCapCondiz = valCapCondiz;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Stagione getStagione() {
        return stagione;
    }

    public void setStagione(Stagione stagione) {
        this.stagione = stagione;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Giocatore)) {
            return false;
        }
        Giocatore other = (Giocatore) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.Giocatore[ id=" + id + " ]";
    }
    
}
