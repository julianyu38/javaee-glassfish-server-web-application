/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import it.elbuild.soccertactics.lib.utils.FormatUtils;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.omg.CORBA.MARSHAL;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "STAGIONE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Stagione.findAll", query = "SELECT s FROM Stagione s"),
    @NamedQuery(name = "Stagione.findById", query = "SELECT s FROM Stagione s WHERE s.id = :id"),
    @NamedQuery(name = "Stagione.findByIdUtente", query = "SELECT s FROM Stagione s WHERE s.idUtente = :idUtente"),
    @NamedQuery(name = "Stagione.findByNome", query = "SELECT s FROM Stagione s WHERE s.nome = :nome"),
    @NamedQuery(name = "Stagione.findByInizio", query = "SELECT s FROM Stagione s WHERE s.inizio = :inizio"),
    @NamedQuery(name = "Stagione.findByFine", query = "SELECT s FROM Stagione s WHERE s.fine = :fine")})
public class Stagione implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ID_UTENTE")
    private Integer idUtente;
    @Column(name = "NOME")
    private String nome;
    @Column(name = "INIZIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inizio;
    @Column(name = "FINE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fine;    
    @Column(name = "TECNICA_ATTACCO")
    private Double attacco;
    @Column(name = "TECNICA_DIFESA")
    private Double difesa;
    @Column(name = "COORDINATIVO")
    private Double coordinativo;
    @Column(name = "CONDIZIONALE")
    private Double condizionale;
    @Column(name = "TATTICO")
    private Double tattico;

    public Stagione() {
    }

    public Stagione(Integer id) {
        this.id = id;
    }
    
    public boolean programmaInserito(){
        return attacco!=null && difesa!=null && coordinativo!=null && condizionale!=null && tattico!=null 
                && (getAttacco()+getCondizionale()+getCoordinativo()+getDifesa()+getTattico())==100;
    }
    
    public void azzeraPercentuali(){
        attacco = 0d;
        difesa = 0d;
        coordinativo = 0d;
        condizionale = 0d;
        tattico = 0d;
    }
    
    public void aggiungiPercentuale(double  at, double df, double co, double cn){
        attacco = attacco + at;
        difesa = difesa + df;
        coordinativo = coordinativo + co;
        condizionale = condizionale + cn;
    }
    
    public void calcolaPercentuali(double num){
        attacco = FormatUtils.roundDouble(attacco/num);
        difesa = FormatUtils.roundDouble(difesa/num);
        coordinativo = FormatUtils.roundDouble(coordinativo/num);
        condizionale = FormatUtils.roundDouble(condizionale/num);
        tattico = 100-attacco-difesa-coordinativo-condizionale;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUtente() {
        return idUtente;
    }

    public void setIdUtente(Integer idUtente) {
        this.idUtente = idUtente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getInizio() {
        return inizio;
    }

    public void setInizio(Date inizio) {
        this.inizio = inizio;
    }

    public Date getFine() {
        return fine;
    }

    public void setFine(Date fine) {
        this.fine = fine;
    }

    public Double getAttacco() {
        return attacco;
    }

    public void setAttacco(Double attacco) {
        this.attacco = attacco;
    }

    public Double getDifesa() {
        return difesa;
    }

    public void setDifesa(Double difesa) {
        this.difesa = difesa;
    }

    public Double getCoordinativo() {
        return coordinativo;
    }

    public void setCoordinativo(Double coordinativo) {
        this.coordinativo = coordinativo;
    }

    public Double getCondizionale() {
        return condizionale;
    }

    public void setCondizionale(Double condizionale) {
        this.condizionale = condizionale;
    }

    public Double getTattico() {
        return tattico;
    }

    public void setTattico(Double tattico) {
        this.tattico = tattico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Stagione)) {
            return false;
        }
        Stagione other = (Stagione) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.Stagione[ id=" + id + " ]";
    }
    
}
