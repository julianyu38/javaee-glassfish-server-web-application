/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "GIOCATORE_AVVERSARIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GiocatoreAvversario.findAll", query = "SELECT g FROM GiocatoreAvversario g"),
    @NamedQuery(name = "GiocatoreAvversario.findById", query = "SELECT g FROM GiocatoreAvversario g WHERE g.id = :id"),
    @NamedQuery(name = "GiocatoreAvversario.findByNome", query = "SELECT g FROM GiocatoreAvversario g WHERE g.nome = :nome"),
    @NamedQuery(name = "GiocatoreAvversario.findByCognome", query = "SELECT g FROM GiocatoreAvversario g WHERE g.cognome = :cognome"),
    @NamedQuery(name = "GiocatoreAvversario.findByNazionalita", query = "SELECT g FROM GiocatoreAvversario g WHERE g.nazionalita = :nazionalita"),
    @NamedQuery(name = "GiocatoreAvversario.findByDataNascita", query = "SELECT g FROM GiocatoreAvversario g WHERE g.dataNascita = :dataNascita"),
    @NamedQuery(name = "GiocatoreAvversario.findByRuolo", query = "SELECT g FROM GiocatoreAvversario g WHERE g.ruolo = :ruolo"),
    @NamedQuery(name = "GiocatoreAvversario.findByGiudizio", query = "SELECT g FROM GiocatoreAvversario g WHERE g.giudizio = :giudizio"),
    @NamedQuery(name = "GiocatoreAvversario.findByIdAvversario", query = "SELECT g FROM GiocatoreAvversario g WHERE g.idAvversario = :idAvversario")})
public class GiocatoreAvversario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 256)
    @Column(name = "NOME")
    private String nome;
    @Size(max = 256)
    @Column(name = "COGNOME")
    private String cognome;
    @Size(max = 256)
    @Column(name = "NAZIONALITA")
    private String nazionalita;
    @Column(name = "DATA_NASCITA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataNascita;
    @Size(max = 256)
    @Column(name = "RUOLO")
    private String ruolo;
    @Size(max = 256)
    @Column(name = "GIUDIZIO")
    private String giudizio;
    @Column(name = "ID_AVVERSARIO")
    private Integer idAvversario;

    public GiocatoreAvversario() {
    }

    public GiocatoreAvversario(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getNazionalita() {
        return nazionalita;
    }

    public void setNazionalita(String nazionalita) {
        this.nazionalita = nazionalita;
    }

    public Date getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(Date dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getRuolo() {
        return ruolo;
    }

    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }

    public String getGiudizio() {
        return giudizio;
    }

    public void setGiudizio(String giudizio) {
        this.giudizio = giudizio;
    }

    public Integer getIdAvversario() {
        return idAvversario;
    }

    public void setIdAvversario(Integer idAvversario) {
        this.idAvversario = idAvversario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GiocatoreAvversario)) {
            return false;
        }
        GiocatoreAvversario other = (GiocatoreAvversario) object;
        if( (this.id==null || (id!=null && other.id != null && this.id.equals(other.id)))&& 
            ((this.nome==null && other.nome==null) || (this.nome!=null && other.nome!=null && this.nome.equals(other.nome))) && 
            ((this.cognome==null && other.cognome==null) || (this.cognome!=null && other.cognome!=null && this.cognome.equals(other.cognome))) &&
            ((this.nazionalita==null && other.nazionalita==null) || (this.nazionalita!=null && other.nazionalita!=null && this.nazionalita.equals(other.nazionalita))) &&
            ((this.dataNascita==null && other.dataNascita==null) || (this.dataNascita!=null && other.dataNascita!=null && this.dataNascita.equals(other.dataNascita))) &&
            ((this.ruolo==null && other.ruolo==null) || (this.ruolo!=null && other.ruolo!=null && this.ruolo.equals(other.ruolo))) &&
            ((this.giudizio==null && other.giudizio==null) || (this.giudizio!=null && other.giudizio!=null && this.giudizio.equals(other.giudizio))) ){
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.GiocatoreAvversario[ id=" + id + " ]";
    }
    
}
