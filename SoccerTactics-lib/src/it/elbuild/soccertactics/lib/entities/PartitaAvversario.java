/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "PARTITA_AVVERSARIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PartitaAvversario.findAll", query = "SELECT p FROM PartitaAvversario p"),
    @NamedQuery(name = "PartitaAvversario.findById", query = "SELECT p FROM PartitaAvversario p WHERE p.id = :id"),
    @NamedQuery(name = "PartitaAvversario.findByIdPartita", query = "SELECT p FROM PartitaAvversario p WHERE p.idPartita = :idPartita"),
    @NamedQuery(name = "PartitaAvversario.findByIdGiocatoreAvversario", query = "SELECT p FROM PartitaAvversario p WHERE p.idGiocatoreAvversario = :idGiocatoreAvversario"),
    @NamedQuery(name = "PartitaAvversario.findByColGf", query = "SELECT p FROM PartitaAvversario p WHERE p.colGf = :colGf"),
    @NamedQuery(name = "PartitaAvversario.findByColGs", query = "SELECT p FROM PartitaAvversario p WHERE p.colGs = :colGs"),
    @NamedQuery(name = "PartitaAvversario.findByColTp", query = "SELECT p FROM PartitaAvversario p WHERE p.colTp = :colTp"),
    @NamedQuery(name = "PartitaAvversario.findByColTf", query = "SELECT p FROM PartitaAvversario p WHERE p.colTf = :colTf"),
    @NamedQuery(name = "PartitaAvversario.findByColA", query = "SELECT p FROM PartitaAvversario p WHERE p.colA = :colA"),
    @NamedQuery(name = "PartitaAvversario.findByColE", query = "SELECT p FROM PartitaAvversario p WHERE p.colE = :colE"),
    @NamedQuery(name = "PartitaAvversario.findByColPp", query = "SELECT p FROM PartitaAvversario p WHERE p.colPp = :colPp"),
    @NamedQuery(name = "PartitaAvversario.findByColPr", query = "SELECT p FROM PartitaAvversario p WHERE p.colPr = :colPr"),
    @NamedQuery(name = "PartitaAvversario.findByColFf", query = "SELECT p FROM PartitaAvversario p WHERE p.colFf = :colFf"),
    @NamedQuery(name = "PartitaAvversario.findByColFs", query = "SELECT p FROM PartitaAvversario p WHERE p.colFs = :colFs"),
    @NamedQuery(name = "PartitaAvversario.findByColMing", query = "SELECT p FROM PartitaAvversario p WHERE p.colMing = :colMing"),
    @NamedQuery(name = "PartitaAvversario.findByColVoto", query = "SELECT p FROM PartitaAvversario p WHERE p.colVoto = :colVoto")})
public class PartitaAvversario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ID_PARTITA")
    private Integer idPartita;
    @Column(name = "ID_GIOCATORE_AVVERSARIO")
    private Integer idGiocatoreAvversario;
    @Column(name = "COL_GF")
    private Integer colGf;
    @Column(name = "COL_GS")
    private Integer colGs;
    @Column(name = "COL_TP")
    private Integer colTp;
    @Column(name = "COL_TF")
    private Integer colTf;
    @Column(name = "COL_A")
    private Integer colA;
    @Column(name = "COL_E")
    private Integer colE;
    @Column(name = "COL_PP")
    private Integer colPp;
    @Column(name = "COL_PR")
    private Integer colPr;
    @Column(name = "COL_FF")
    private Integer colFf;
    @Column(name = "COL_FS")
    private Integer colFs;
    @Column(name = "COL_MING")
    private Integer colMing;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "COL_VOTO")
    private Double colVoto;
    
    @OneToOne
    @JoinColumn(name="ID_GIOCATORE_AVVERSARIO", referencedColumnName="ID", insertable=false, updatable=false)
    private GiocatoreAvversario giocatoreAvversario;

    public PartitaAvversario() {
    }

    public PartitaAvversario(Integer id) {
        this.id = id;
    }

    public PartitaAvversario(GiocatoreAvversario giocatoreAvversario) {
        this.idGiocatoreAvversario = giocatoreAvversario.getId();
        this.giocatoreAvversario = giocatoreAvversario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPartita() {
        return idPartita;
    }

    public void setIdPartita(Integer idPartita) {
        this.idPartita = idPartita;
    }

    public Integer getIdGiocatoreAvversario() {
        return idGiocatoreAvversario;
    }

    public void setIdGiocatoreAvversario(Integer idGiocatoreAvversario) {
        this.idGiocatoreAvversario = idGiocatoreAvversario;
    }

    public Integer getColGf() {
        return colGf;
    }

    public void setColGf(Integer colGf) {
        this.colGf = colGf;
    }

    public Integer getColGs() {
        return colGs;
    }

    public void setColGs(Integer colGs) {
        this.colGs = colGs;
    }

    public Integer getColTp() {
        return colTp;
    }

    public void setColTp(Integer colTp) {
        this.colTp = colTp;
    }

    public Integer getColTf() {
        return colTf;
    }

    public void setColTf(Integer colTf) {
        this.colTf = colTf;
    }

    public Integer getColA() {
        return colA;
    }

    public void setColA(Integer colA) {
        this.colA = colA;
    }

    public Integer getColE() {
        return colE;
    }

    public void setColE(Integer colE) {
        this.colE = colE;
    }

    public Integer getColPp() {
        return colPp;
    }

    public void setColPp(Integer colPp) {
        this.colPp = colPp;
    }

    public Integer getColPr() {
        return colPr;
    }

    public void setColPr(Integer colPr) {
        this.colPr = colPr;
    }

    public Integer getColFf() {
        return colFf;
    }

    public void setColFf(Integer colFf) {
        this.colFf = colFf;
    }

    public Integer getColFs() {
        return colFs;
    }

    public void setColFs(Integer colFs) {
        this.colFs = colFs;
    }

    public Integer getColMing() {
        return colMing;
    }

    public void setColMing(Integer colMing) {
        this.colMing = colMing;
    }

    public GiocatoreAvversario getGiocatoreAvversario() {
        return giocatoreAvversario;
    }

    public void setGiocatoreAvversario(GiocatoreAvversario giocatoreAvversario) {
        this.giocatoreAvversario = giocatoreAvversario;
    }

    public Double getColVoto() {
        return colVoto;
    }

    public void setColVoto(Double colVoto) {
        this.colVoto = colVoto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PartitaAvversario)) {
            return false;
        }
        PartitaAvversario other = (PartitaAvversario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.PartitaAvversario[ id=" + id + " ]";
    }
    
}
