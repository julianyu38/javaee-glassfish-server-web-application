/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "SOCIETA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Societa.findAll", query = "SELECT s FROM Societa s"),
    @NamedQuery(name = "Societa.findById", query = "SELECT s FROM Societa s WHERE s.id = :id"),
    @NamedQuery(name = "Societa.findByStemma", query = "SELECT s FROM Societa s WHERE s.stemma = :stemma"),
    @NamedQuery(name = "Societa.findBySedeSociale", query = "SELECT s FROM Societa s WHERE s.sedeSociale = :sedeSociale"),
    @NamedQuery(name = "Societa.findByColoreIMaglia", query = "SELECT s FROM Societa s WHERE s.coloreIMaglia = :coloreIMaglia"),
    @NamedQuery(name = "Societa.findByColoreIiMaglia", query = "SELECT s FROM Societa s WHERE s.coloreIiMaglia = :coloreIiMaglia"),
    @NamedQuery(name = "Societa.findByCampoAllenamento", query = "SELECT s FROM Societa s WHERE s.campoAllenamento = :campoAllenamento"),
    @NamedQuery(name = "Societa.findByCampoGara", query = "SELECT s FROM Societa s WHERE s.campoGara = :campoGara"),
    @NamedQuery(name = "Societa.findByPresidente", query = "SELECT s FROM Societa s WHERE s.presidente = :presidente")})
public class Societa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "STEMMA")
    private String stemma;
    @Column(name = "NOME")
    private String nome;
    @Column(name = "SEDE_SOCIALE")
    private String sedeSociale;
    @Column(name = "COLORE_I_MAGLIA")
    private String coloreIMaglia;
    @Column(name = "COLORE_II_MAGLIA")
    private String coloreIiMaglia;
    @Column(name = "CAMPO_ALLENAMENTO")
    private String campoAllenamento;
    @Column(name = "CAMPO_GARA")
    private String campoGara;
    @Column(name = "PRESIDENTE")
    private String presidente;

    public Societa() {
    }

    public Societa(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStemma() {
        return stemma;
    }

    public void setStemma(String stemma) {
        this.stemma = stemma;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSedeSociale() {
        return sedeSociale;
    }

    public void setSedeSociale(String sedeSociale) {
        this.sedeSociale = sedeSociale;
    }

    public String getColoreIMaglia() {
        return coloreIMaglia;
    }

    public void setColoreIMaglia(String coloreIMaglia) {
        this.coloreIMaglia = coloreIMaglia;
    }

    public String getColoreIiMaglia() {
        return coloreIiMaglia;
    }

    public void setColoreIiMaglia(String coloreIiMaglia) {
        this.coloreIiMaglia = coloreIiMaglia;
    }

    public String getCampoAllenamento() {
        return campoAllenamento;
    }

    public void setCampoAllenamento(String campoAllenamento) {
        this.campoAllenamento = campoAllenamento;
    }

    public String getCampoGara() {
        return campoGara;
    }

    public void setCampoGara(String campoGara) {
        this.campoGara = campoGara;
    }

    public String getPresidente() {
        return presidente;
    }

    public void setPresidente(String presidente) {
        this.presidente = presidente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Societa)) {
            return false;
        }
        Societa other = (Societa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.Societa[ id=" + id + " ]";
    }
    
}
