/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "TECNICA_MA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TecnicaMa.findAll", query = "SELECT t FROM TecnicaMa t "),
    @NamedQuery(name = "TecnicaMa.findById", query = "SELECT t FROM TecnicaMa t WHERE t.id = :id"),
    @NamedQuery(name = "TecnicaMa.findByCategoria1", query = "SELECT t FROM TecnicaMa t WHERE t.categoria1 = :categoria1"),
    @NamedQuery(name = "TecnicaMa.findByCategoria2", query = "SELECT t FROM TecnicaMa t WHERE t.categoria2 = :categoria2"),
    @NamedQuery(name = "TecnicaMa.findByCategoria3", query = "SELECT t FROM TecnicaMa t WHERE t.categoria3 = :categoria3"),
    @NamedQuery(name = "TecnicaMa.findByTecnica", query = "SELECT t FROM TecnicaMa t WHERE t.tecnica = :tecnica")})
public class TecnicaMa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "CATEGORIA_1")
    private String categoria1;
    @Column(name = "CATEGORIA_2")
    private String categoria2;
    @Column(name = "CATEGORIA_3")
    private String categoria3;
    @Column(name = "TECNICA")
    private String tecnica;

    public TecnicaMa() {
    }

    public TecnicaMa(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoria1() {
        return categoria1;
    }

    public void setCategoria1(String categoria1) {
        this.categoria1 = categoria1;
    }

    public String getCategoria2() {
        return categoria2;
    }

    public void setCategoria2(String categoria2) {
        this.categoria2 = categoria2;
    }

    public String getCategoria3() {
        return categoria3==null ? "" : categoria3;
    }

    public void setCategoria3(String categoria3) {
        this.categoria3 = categoria3;
    }

    public String getTecnica() {
        return tecnica==null ? "" : tecnica;
    }

    public void setTecnica(String tecnica) {
        this.tecnica = tecnica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TecnicaMa)) {
            return false;
        }
        TecnicaMa other = (TecnicaMa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.TecnicaMa[ id=" + id + " ]";
    }
    
}
