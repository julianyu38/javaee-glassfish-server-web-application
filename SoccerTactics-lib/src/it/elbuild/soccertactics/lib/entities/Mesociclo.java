/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "MESOCICLO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mesociclo.findAll", query = "SELECT m FROM Mesociclo m ORDER BY m.data ASC"),
    
    @NamedQuery(name = "Mesociclo.findById", query = "SELECT m FROM Mesociclo m WHERE m.id = :id"),
    @NamedQuery(name = "Mesociclo.findByIdStagioneIdUtente", query = "SELECT m FROM Mesociclo m WHERE m.idStagione = :idStagione AND m.idUtente = :idUtente ORDER BY m.data ASC"),
    @NamedQuery(name = "Mesociclo.findByIdStagioneIdUtenteMese", query = "SELECT m FROM Mesociclo m WHERE m.idStagione = :idStagione AND m.idUtente = :idUtente AND m.mese = :mese AND m.anno = :anno ORDER BY m.data ASC"),
    @NamedQuery(name = "Mesociclo.findByIdStagione", query = "SELECT m FROM Mesociclo m WHERE m.idStagione = :idStagione")})
public class Mesociclo implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private static final SimpleDateFormat sdf = new SimpleDateFormat("MMMMMMMMMMMMMMM", Locale.ITALIAN);
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ID_UTENTE")
    private Integer idUtente;
    @Column(name = "ID_STAGIONE")
    private Integer idStagione;
    @Column(name = "MESE")
    private Integer mese;
    @Column(name = "ANNO")
    private Integer anno;
    @Column(name = "DATA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;
    @Column(name = "TECNICA_ATTACCO")
    private Double attacco;
    @Column(name = "TECNICA_DIFESA")
    private Double difesa;
    @Column(name = "COORDINATIVO")
    private Double coordinativo;
    @Column(name = "CONDIZIONALE")
    private Double condizionale;
    @Column(name = "TATTICO")
    private Double tattico;

    public Mesociclo() {
    }

    public Mesociclo(Integer id) {
        this.id = id;
    }

    public void initTotali(){
        attacco = 0d;
        difesa = 0d;
        coordinativo = 0d;
        condizionale = 0d;
        tattico = 0d;
    }
    
    public Mesociclo(Utente utente, Calendar calendar) {
        idUtente = utente.getId();
        idStagione = utente.getStagione().getId();
        data = calendar.getTime();
        mese = calendar.get(Calendar.MONTH);
        anno = calendar.get(Calendar.YEAR);
        attacco = utente.getStagione().getAttacco();
        difesa = utente.getStagione().getDifesa();
        coordinativo = utente.getStagione().getCoordinativo();
        condizionale = utente.getStagione().getCondizionale();
        tattico = utente.getStagione().getTattico();
    }
    
//    public String getNomeGiorno(){
//        if(giorno==null){
//            return "";
//        }else if(giorno.equals(Calendar.MONDAY)){
//            return "Lunedì";
//        }else if(giorno.equals(Calendar.TUESDAY)){
//            return "Martedì";
//        }else if(giorno.equals(Calendar.WEDNESDAY)){
//            return "Mercoledì";
//        }else if(giorno.equals(Calendar.THURSDAY)){
//            return "Giovedì";
//        }else if(giorno.equals(Calendar.FRIDAY)){
//            return "Venerdì";
//        }else if(giorno.equals(Calendar.SATURDAY)){
//            return "Sabato";
//        }else if(giorno.equals(Calendar.SUNDAY)){
//            return "Domenica";
//        }
//        return "";
//    }
//    
    public void initTotale(){
        attacco = 0d;
        difesa = 0d;
        coordinativo = 0d;
        condizionale = 0d;
        tattico = 0d;
    }
    
    public Double calcolaTotale(){
        Double totale = 0d;
        totale = attacco!=null ? attacco+totale : totale;
        totale = difesa!=null ? difesa+totale : totale;
        totale = coordinativo!=null ? coordinativo+totale : totale;
        totale = condizionale!=null ? condizionale+totale : totale;
        totale = tattico!=null ? tattico+totale : totale;
        return totale;
    }
    
    public boolean controllaPercentuali(){
        return attacco!=null && difesa!=null && coordinativo!=null && condizionale!=null && tattico!=null 
                && (getAttacco()+getCondizionale()+getCoordinativo()+getDifesa()+getTattico())==100;
    }

    public String nomeMese(){
        return data!=null ? sdf.format(data) : "";
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUtente() {
        return idUtente;
    }

    public void setIdUtente(Integer idUtente) {
        this.idUtente = idUtente;
    }

    public Integer getIdStagione() {
        return idStagione;
    }

    public void setIdStagione(Integer idStagione) {
        this.idStagione = idStagione;
    }

    public Integer getMese() {
        return mese;
    }

    public void setMese(Integer mese) {
        this.mese = mese;
    }

    public Integer getAnno() {
        return anno;
    }

    public void setAnno(Integer anno) {
        this.anno = anno;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Double getAttacco() {
        return attacco;
    }

    public void setAttacco(Double attacco) {
        this.attacco = attacco;
    }

    public Double getDifesa() {
        return difesa;
    }

    public void setDifesa(Double difesa) {
        this.difesa = difesa;
    }

    public Double getCoordinativo() {
        return coordinativo;
    }

    public void setCoordinativo(Double coordinativo) {
        this.coordinativo = coordinativo;
    }

    public Double getCondizionale() {
        return condizionale;
    }

    public void setCondizionale(Double condizionale) {
        this.condizionale = condizionale;
    }

    public Double getTattico() {
        return tattico;
    }

    public void setTattico(Double tattico) {
        this.tattico = tattico;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mesociclo)) {
            return false;
        }
        Mesociclo other = (Mesociclo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.Mesociclo[ id=" + id + " ]";
    }
    
}
