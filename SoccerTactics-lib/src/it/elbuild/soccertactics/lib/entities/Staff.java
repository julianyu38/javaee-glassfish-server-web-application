/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ale
 */
@Entity
@Table(name = "STAFF")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Staff.findAll", query = "SELECT s FROM Staff s"),
    @NamedQuery(name = "Staff.findById", query = "SELECT s FROM Staff s WHERE s.id = :id"),
    @NamedQuery(name = "Staff.findByIdStagione", query = "SELECT s FROM Staff s WHERE s.idStagione = :idStagione"),
    @NamedQuery(name = "Staff.findByCategoria", query = "SELECT s FROM Staff s WHERE s.categoria = :categoria"),
    @NamedQuery(name = "Staff.findByMansione", query = "SELECT s FROM Staff s WHERE s.mansione = :mansione"),
    @NamedQuery(name = "Staff.findByNome", query = "SELECT s FROM Staff s WHERE s.nome = :nome"),
    @NamedQuery(name = "Staff.findByCognome", query = "SELECT s FROM Staff s WHERE s.cognome = :cognome"),
    @NamedQuery(name = "Staff.findByCellulare", query = "SELECT s FROM Staff s WHERE s.cellulare = :cellulare"),
    @NamedQuery(name = "Staff.findByEmail", query = "SELECT s FROM Staff s WHERE s.email = :email"),
    @NamedQuery(name = "Staff.findByResidenzaRegione", query = "SELECT s FROM Staff s WHERE s.residenzaRegione = :residenzaRegione"),
    @NamedQuery(name = "Staff.findByResidenzaProvincia", query = "SELECT s FROM Staff s WHERE s.residenzaProvincia = :residenzaProvincia"),
    @NamedQuery(name = "Staff.findByResidenzaCitta", query = "SELECT s FROM Staff s WHERE s.residenzaCitta = :residenzaCitta"),
    @NamedQuery(name = "Staff.findByResidenzaVia", query = "SELECT s FROM Staff s WHERE s.residenzaVia = :residenzaVia"),
    @NamedQuery(name = "Staff.findByResidenzaCap", query = "SELECT s FROM Staff s WHERE s.residenzaCap = :residenzaCap")})
public class Staff implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ID_STAGIONE")
    private Integer idStagione;
    @Column(name = "CATEGORIA")
    private String categoria;
    @Column(name = "MANSIONE")
    private String mansione;
    @Column(name = "FOTO")
    private String foto;
    @Column(name = "NOME")
    private String nome;
    @Column(name = "COGNOME")
    private String cognome;
    @Column(name = "CELLULARE")
    private String cellulare;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "RESIDENZA_REGIONE")
    private String residenzaRegione;
    @Column(name = "RESIDENZA_PROVINCIA")
    private String residenzaProvincia;
    @Column(name = "RESIDENZA_CITTA")
    private String residenzaCitta;
    @Column(name = "RESIDENZA_VIA")
    private String residenzaVia;
    @Column(name = "RESIDENZA_CAP")
    private String residenzaCap;

    public Staff() {
    }

    public Staff(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getMansione() {
        return mansione;
    }

    public void setMansione(String mansione) {
        this.mansione = mansione;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getCellulare() {
        return cellulare;
    }

    public void setCellulare(String cellulare) {
        this.cellulare = cellulare;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getResidenzaRegione() {
        return residenzaRegione;
    }

    public void setResidenzaRegione(String residenzaRegione) {
        this.residenzaRegione = residenzaRegione;
    }

    public String getResidenzaProvincia() {
        return residenzaProvincia;
    }

    public void setResidenzaProvincia(String residenzaProvincia) {
        this.residenzaProvincia = residenzaProvincia;
    }

    public String getResidenzaCitta() {
        return residenzaCitta;
    }

    public void setResidenzaCitta(String residenzaCitta) {
        this.residenzaCitta = residenzaCitta;
    }

    public String getResidenzaVia() {
        return residenzaVia;
    }

    public void setResidenzaVia(String residenzaVia) {
        this.residenzaVia = residenzaVia;
    }

    public String getResidenzaCap() {
        return residenzaCap;
    }

    public void setResidenzaCap(String residenzaCap) {
        this.residenzaCap = residenzaCap;
    }

    public Integer getIdStagione() {
        return idStagione;
    }

    public void setIdStagione(Integer idStagione) {
        this.idStagione = idStagione;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Staff)) {
            return false;
        }
        Staff other = (Staff) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.elbuild.soccertactics.lib.entities.Staff[ id=" + id + " ]";
    }
    
}
