/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Ale
 */
public class MesocicloAllenamento {
    
    private Integer idUtente;
    private Integer idStagione;
    private Integer settimana;
    private Integer giorno;
    private Integer mese;
    private Integer anno;
    private Date data;
    private Map<String, Integer> mapTecnicaTot;

    public MesocicloAllenamento() {
    }

    public String getNomeGiorno(){
        if(giorno==null){
            return "";
        }else if(giorno.equals(Calendar.MONDAY)){
            return "Lunedì";
        }else if(giorno.equals(Calendar.TUESDAY)){
            return "Martedì";
        }else if(giorno.equals(Calendar.WEDNESDAY)){
            return "Mercoledì";
        }else if(giorno.equals(Calendar.THURSDAY)){
            return "Giovedì";
        }else if(giorno.equals(Calendar.FRIDAY)){
            return "Venerdì";
        }else if(giorno.equals(Calendar.SATURDAY)){
            return "Sabato";
        }else if(giorno.equals(Calendar.SUNDAY)){
            return "Domenica";
        }
        return "";
    }
    
    public void initTotale(){
        if(mapTecnicaTot!=null){
            mapTecnicaTot.clear();
        }else{
            mapTecnicaTot = new HashMap<>();
        }
    }
    
    public Integer calcolaTotale(){
        int totale = 0;
        Integer value;
        for (Map.Entry<String, Integer> entrySet : mapTecnicaTot.entrySet()) {
            value = entrySet.getValue();
            if(value!=null){
                totale = totale+value;
            }
        }
        return totale;
    }
    
    public void addDurataObiettivo(String ob, Integer durata){
        if(durata!=null){
            if(mapTecnicaTot.containsKey(ob)){
                int tot = mapTecnicaTot.get(ob);
                tot = tot+durata;
                mapTecnicaTot.put(ob, tot);
            }else{
                mapTecnicaTot.put(ob, durata);
            }            
        }
    }

    public Integer getIdUtente() {
        return idUtente;
    }

    public void setIdUtente(Integer idUtente) {
        this.idUtente = idUtente;
    }

    public Integer getIdStagione() {
        return idStagione;
    }

    public void setIdStagione(Integer idStagione) {
        this.idStagione = idStagione;
    }

    public Integer getSettimana() {
        return settimana;
    }

    public void setSettimana(Integer settimana) {
        this.settimana = settimana;
    }

    public Integer getGiorno() {
        return giorno;
    }

    public void setGiorno(Integer giorno) {
        this.giorno = giorno;
    }

    public Integer getMese() {
        return mese;
    }

    public void setMese(Integer mese) {
        this.mese = mese;
    }

    public Integer getAnno() {
        return anno;
    }

    public void setAnno(Integer anno) {
        this.anno = anno;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Map<String, Integer> getMapTecnicaTot() {
        return mapTecnicaTot;
    }

    public void setMapTecnicaTot(Map<String, Integer> mapTecnicaTot) {
        this.mapTecnicaTot = mapTecnicaTot;
    }
   

}
