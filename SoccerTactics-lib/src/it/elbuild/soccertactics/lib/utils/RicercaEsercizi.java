/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.utils;

import it.elbuild.soccertactics.lib.entities.Esercizio;
import it.elbuild.soccertactics.lib.entities.Esercizio_;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Ale
 */
public class RicercaEsercizi {
    
    private String titolo;
    private Integer idUtente;
    
//    private Boolean pubblico;
    private String obDifI;
    private String obDifII;
    private String obDifIII;
    private String obAttI;
    private String obAttII;
    private String obTatI;
    private String obCoord;
    private String obCond; 
    private Boolean tutti;
    private Boolean primaSquadra;
    private Boolean juniores;
    private Boolean allievi;
    private Boolean giovanissimi;
    private Boolean esordienti;
    private Boolean pulcini;
    private Boolean primiCalci;
    private Boolean generale;
    private Boolean preAllenamento;
    private Integer portieri;
    private Integer ripetizioni;
    private Integer durata;
    private Integer stazioniGruppi;
    private Integer recupero;
    private Integer serie;
    
    // campi metodo
    private Boolean analitico;
    private Boolean globale;
    private Boolean finalizzato;
    private Boolean situazionale;
    private Boolean giocoIniziale;
    private Boolean giocoTema;
    private Boolean percorsoStazioni;
    private Boolean lavoroGruppo;
    private Boolean atleticoMotorio;
    private Boolean recuperoInfortunio;
    
    private List<Predicate> predicati;
    
    private List<Integer> uids; // per ricerca da archivio esercizi
   
    public void buildPredicates(CriteriaBuilder cb, Root<Esercizio> i){
        
        predicati = new ArrayList<Predicate>();
        
        //esercizio non eliminato 
        Predicate p = cb.isFalse(i.get(Esercizio_.eliminato));
        predicati.add(p);
        
        if(titolo!=null && !titolo.equals("")){
            p = cb.like(cb.lower(i.get(Esercizio_.titolo)), "%" + titolo.toLowerCase() + "%");
            predicati.add(p);
        }
        
        if(idUtente!=null){
            p = cb.equal(i.get(Esercizio_.idUtente), idUtente);
            predicati.add(p);
        }
        
//        if(pubblico!=null){
//            p = cb.equal(i.get(Esercizio_.pubblico), pubblico);
//            predicati.add(p);
//        }
        
        
        if(obDifI!=null && !obDifI.equals("")){
            p = cb.equal(i.get(Esercizio_.obDifI), obDifI);
            predicati.add(p);
        }
        
        if(obDifII!=null && !obDifII.equals("")){
            p = cb.equal(i.get(Esercizio_.obDifII), obDifII);
            predicati.add(p);
        }
        
        if(obDifIII!=null && !obDifIII.equals("")){
            p = cb.equal(i.get(Esercizio_.obDifIII), obDifIII);
            predicati.add(p);
        }
        
        if(obAttI!=null && !obAttI.equals("")){
            p = cb.equal(i.get(Esercizio_.obAttI), obAttI);
            predicati.add(p);
        }
        
        if(obAttII!=null && !obAttII.equals("")){
            p = cb.equal(i.get(Esercizio_.obAttII), obAttII);
            predicati.add(p);
        }
        if(obTatI!=null && !obTatI.equals("")){
            p = cb.equal(i.get(Esercizio_.obTatI), obTatI);
            predicati.add(p);
        }
        
        if(obCoord!=null && !obCoord.equals("")){
            p = cb.equal(i.get(Esercizio_.obCoord), obCoord);
            predicati.add(p);
        }
        
        if(obCond!=null && !obCond.equals("")){
            p = cb.equal(i.get(Esercizio_.obCond), obCond);
            predicati.add(p);
        }
        
        if(tutti!=null && tutti){
            p = cb.equal(i.get(Esercizio_.tutti), tutti);
            predicati.add(p);
        }
        
        if(primaSquadra!=null && primaSquadra){
            p = cb.equal(i.get(Esercizio_.primaSquadra), primaSquadra);
            predicati.add(p);
        }
        
        if(juniores!=null && juniores){
            p = cb.equal(i.get(Esercizio_.juniores), juniores);
            predicati.add(p);
        }
       
        if(allievi!=null && allievi){
            p = cb.equal(i.get(Esercizio_.allievi), allievi);
            predicati.add(p);
        }
        
        if(giovanissimi!=null && giovanissimi){
            p = cb.equal(i.get(Esercizio_.giovanissimi), giovanissimi);
            predicati.add(p);
        }
        
        if(esordienti!=null && esordienti){
            p = cb.equal(i.get(Esercizio_.esordienti), esordienti);
            predicati.add(p);
        }
        
        if(pulcini!=null && pulcini){
            p = cb.equal(i.get(Esercizio_.pulcini), pulcini);
            predicati.add(p);
        }
        
        if(primiCalci!=null && primiCalci){
            p = cb.equal(i.get(Esercizio_.primiCalci), primiCalci);
            predicati.add(p);
        }
        
        if(generale!=null && generale){
            p = cb.equal(i.get(Esercizio_.generale), generale);
            predicati.add(p);
        }
        
        if(preAllenamento!=null && preAllenamento){
            p = cb.equal(i.get(Esercizio_.preAllenamento), preAllenamento);
            predicati.add(p);
        }        
        
        if(portieri!=null){
            p = cb.equal(i.get(Esercizio_.portieri), portieri);
            predicati.add(p);
        }
        
        if(ripetizioni!=null){
            p = cb.equal(i.get(Esercizio_.ripetizioni), ripetizioni);
            predicati.add(p);
        }
        
        if(durata!=null){
            p = cb.equal(i.get(Esercizio_.durata), durata);
            predicati.add(p);
        }
        
        if(stazioniGruppi!=null){
            p = cb.equal(i.get(Esercizio_.stazioniGruppi), stazioniGruppi);
            predicati.add(p);
        }
        
        if(recupero!=null){
            p = cb.equal(i.get(Esercizio_.recupero), recupero);
            predicati.add(p);
        }
        
        if(serie!=null){
            p = cb.equal(i.get(Esercizio_.serie), serie);
            predicati.add(p);
        }
        
        if(analitico!=null && analitico){
            p = cb.equal(i.get(Esercizio_.analitico), analitico);
            predicati.add(p);
        }
        if(globale!=null && globale){
            p = cb.equal(i.get(Esercizio_.globale), globale);
            predicati.add(p);
        }
        if(finalizzato!=null && finalizzato){
            p = cb.equal(i.get(Esercizio_.finalizzato), finalizzato);
            predicati.add(p);
        }
        if(situazionale!=null && situazionale){
            p = cb.equal(i.get(Esercizio_.situazionale), situazionale);
            predicati.add(p);
        }
        if(giocoIniziale!=null && giocoIniziale){
            p = cb.equal(i.get(Esercizio_.giocoIniziale), giocoIniziale);
            predicati.add(p);
        }
        if(giocoTema!=null && giocoTema){
            p = cb.equal(i.get(Esercizio_.giocoTema), giocoTema);
            predicati.add(p);
        }
        if(percorsoStazioni!=null && percorsoStazioni){
            p = cb.equal(i.get(Esercizio_.percorsoStazioni), percorsoStazioni);
            predicati.add(p);
        }
        if(lavoroGruppo!=null && lavoroGruppo){
            p = cb.equal(i.get(Esercizio_.lavoroGruppo), lavoroGruppo);
            predicati.add(p);
        }
        if(atleticoMotorio!=null && atleticoMotorio){
            p = cb.equal(i.get(Esercizio_.atleticoMotorio), atleticoMotorio);
            predicati.add(p);
        }
        if(recuperoInfortunio!=null && recuperoInfortunio){
            p = cb.equal(i.get(Esercizio_.recuperoInfortunio), recuperoInfortunio);
            predicati.add(p);
        }
        
        if(uids!=null && !uids.isEmpty()){
            p = i.get(Esercizio_.idUtente).in(uids);
            predicati.add(p);
        }
    }
    
    public void resetCriteri() {
        titolo = null;
        idUtente = null;
//    pubblico = null;
        obDifI = null;
        obDifII = null;
        obDifIII = null;
        obAttI = null;
        obAttII = null;
        obTatI = null;
        obCoord = null;
        obCond = null;
        tutti = null;
        primaSquadra = null;
        juniores = null;
        allievi = null;
        giovanissimi = null;
        esordienti = null;
        pulcini = null;
        primiCalci = null;
        generale = null;
        preAllenamento = null;
        portieri = null;
        ripetizioni = null;
        durata = null;
        stazioniGruppi = null;
        recupero = null;
        serie = null;
        analitico = null;
        globale = null;
        finalizzato = null;
        situazionale = null;
        giocoIniziale = null;
        giocoTema = null;
        percorsoStazioni = null;
        lavoroGruppo = null;
        atleticoMotorio = null;
        recuperoInfortunio = null;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public Integer getIdUtente() {
        return idUtente;
    }

    public void setIdUtente(Integer idUtente) {
        this.idUtente = idUtente;
    }

    public String getObDifI() {
        return obDifI;
    }

    public void setObDifI(String obDifI) {
        this.obDifI = obDifI;
    }

    public String getObDifII() {
        return obDifII;
    }

    public void setObDifII(String obDifII) {
        this.obDifII = obDifII;
    }

    public String getObAttI() {
        return obAttI;
    }

    public void setObAttI(String obAttI) {
        this.obAttI = obAttI;
    }

    public String getObAttII() {
        return obAttII;
    }

    public void setObAttII(String obAttII) {
        this.obAttII = obAttII;
    }

    public String getObTatI() {
        return obTatI;
    }

    public void setObTatI(String obTatI) {
        this.obTatI = obTatI;
    }

    public String getObCoord() {
        return obCoord;
    }

    public void setObCoord(String obCoord) {
        this.obCoord = obCoord;
    }

    public String getObCond() {
        return obCond;
    }

    public void setObCond(String obCond) {
        this.obCond = obCond;
    }

    public Boolean getTutti() {
        return tutti;
    }

    public void setTutti(Boolean tutti) {
        this.tutti = tutti;
    }

    public Boolean getPrimaSquadra() {
        return primaSquadra;
    }

    public void setPrimaSquadra(Boolean primaSquadra) {
        this.primaSquadra = primaSquadra;
    }

    public Boolean getJuniores() {
        return juniores;
    }

    public void setJuniores(Boolean juniores) {
        this.juniores = juniores;
    }

    public Boolean getAllievi() {
        return allievi;
    }

    public void setAllievi(Boolean allievi) {
        this.allievi = allievi;
    }

    public Boolean getGiovanissimi() {
        return giovanissimi;
    }

    public void setGiovanissimi(Boolean giovanissimi) {
        this.giovanissimi = giovanissimi;
    }

    public Boolean getEsordienti() {
        return esordienti;
    }

    public void setEsordienti(Boolean esordienti) {
        this.esordienti = esordienti;
    }

    public Boolean getPulcini() {
        return pulcini;
    }

    public void setPulcini(Boolean pulcini) {
        this.pulcini = pulcini;
    }

    public Boolean getPrimiCalci() {
        return primiCalci;
    }

    public void setPrimiCalci(Boolean primiCalci) {
        this.primiCalci = primiCalci;
    }

    public Boolean getGenerale() {
        return generale;
    }

    public void setGenerale(Boolean generale) {
        this.generale = generale;
    }

    public Integer getPortieri() {
        return portieri;
    }

    public void setPortieri(Integer portieri) {
        this.portieri = portieri;
    }

    public Integer getRipetizioni() {
        return ripetizioni;
    }

    public void setRipetizioni(Integer ripetizioni) {
        this.ripetizioni = ripetizioni;
    }

    public Integer getDurata() {
        return durata;
    }

    public void setDurata(Integer durata) {
        this.durata = durata;
    }

    public Integer getStazioniGruppi() {
        return stazioniGruppi;
    }

    public void setStazioniGruppi(Integer stazioniGruppi) {
        this.stazioniGruppi = stazioniGruppi;
    }

    public Integer getRecupero() {
        return recupero;
    }

    public void setRecupero(Integer recupero) {
        this.recupero = recupero;
    }

    public Integer getSerie() {
        return serie;
    }

    public void setSerie(Integer serie) {
        this.serie = serie;
    }

    public List<Predicate> getPredicati() {
        return predicati;
    }

    public void setPredicati(List<Predicate> predicati) {
        this.predicati = predicati;
    }

    public Boolean getPreAllenamento() {
        return preAllenamento;
    }

    public void setPreAllenamento(Boolean preAllenamento) {
        this.preAllenamento = preAllenamento;
    }

    public Boolean getAnalitico() {
        return analitico;
    }

    public void setAnalitico(Boolean analitico) {
        this.analitico = analitico;
    }

    public Boolean getGlobale() {
        return globale;
    }

    public void setGlobale(Boolean globale) {
        this.globale = globale;
    }

    public Boolean getFinalizzato() {
        return finalizzato;
    }

    public void setFinalizzato(Boolean finalizzato) {
        this.finalizzato = finalizzato;
    }

    public Boolean getSituazionale() {
        return situazionale;
    }

    public void setSituazionale(Boolean situazionale) {
        this.situazionale = situazionale;
    }

    public Boolean getGiocoIniziale() {
        return giocoIniziale;
    }

    public void setGiocoIniziale(Boolean giocoIniziale) {
        this.giocoIniziale = giocoIniziale;
    }

    public Boolean getGiocoTema() {
        return giocoTema;
    }

    public void setGiocoTema(Boolean giocoTema) {
        this.giocoTema = giocoTema;
    }

    public Boolean getPercorsoStazioni() {
        return percorsoStazioni;
    }

    public void setPercorsoStazioni(Boolean percorsoStazioni) {
        this.percorsoStazioni = percorsoStazioni;
    }

    public Boolean getLavoroGruppo() {
        return lavoroGruppo;
    }

    public void setLavoroGruppo(Boolean lavoroGruppo) {
        this.lavoroGruppo = lavoroGruppo;
    }

    public Boolean getAtleticoMotorio() {
        return atleticoMotorio;
    }

    public void setAtleticoMotorio(Boolean atleticoMotorio) {
        this.atleticoMotorio = atleticoMotorio;
    }

    public Boolean getRecuperoInfortunio() {
        return recuperoInfortunio;
    }

    public void setRecuperoInfortunio(Boolean recuperoInfortunio) {
        this.recuperoInfortunio = recuperoInfortunio;
    }

    public String getObDifIII() {
        return obDifIII;
    }

    public void setObDifIII(String obDifIII) {
        this.obDifIII = obDifIII;
    }

    public List<Integer> getUids() {
        return uids;
    }

    public void setUids(List<Integer> uids) {
        this.uids = uids;
    }
    
}
