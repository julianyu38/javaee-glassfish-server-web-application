/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.utils;

import java.math.BigDecimal;

/**
 *
 * @author Ale
 */
public class FormatUtils {
    
    public static Double roundDouble(Double num){
       if(num==null){
           return 0d;
       }
       BigDecimal bg = new BigDecimal(num); 
       bg = bg.setScale(2, BigDecimal.ROUND_HALF_UP); 
       return bg.doubleValue();
   }
    
}
