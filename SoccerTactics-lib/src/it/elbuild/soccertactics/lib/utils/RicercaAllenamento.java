/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package it.elbuild.soccertactics.lib.utils;

import it.elbuild.soccertactics.lib.entities.Allenamento;
import it.elbuild.soccertactics.lib.entities.Allenamento_;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Ale
 */
public class RicercaAllenamento {
    
    private Integer idStagione;
    private String titolo;
    private Date datada;
    private Date dataa;
    private String obTec;
    private String obFis;
    private String obTat;
    private String obPsi;
    
    private List<Predicate> predicati;
    
    public void buildPredicates(CriteriaBuilder cb, Root<Allenamento> i){
        
        predicati = new ArrayList<Predicate>();
        
        //esercizio non eliminato 
        Predicate p = cb.isFalse(i.get(Allenamento_.eliminato));
        predicati.add(p);
        
        if(idStagione!=null){
            p = cb.equal(i.get(Allenamento_.idStagione), idStagione);
            predicati.add(p);
        }
        
        if(titolo!=null && !titolo.equals("")){
            p = cb.like(cb.lower(i.get(Allenamento_.titolo)), "%" + titolo.toLowerCase() + "%");
            predicati.add(p);
        }
        
        if(datada!=null && dataa!=null){
            if(datada.before(dataa)){
                p = cb.greaterThanOrEqualTo(i.get(Allenamento_.dataInizio), datada);
                predicati.add(p);
                p = cb.lessThanOrEqualTo(i.get(Allenamento_.dataInizio), dataa);
                predicati.add(p);
            }else{
                datada = null;
                dataa = null;
            }
        }else if(datada!=null){
            p = cb.greaterThanOrEqualTo(i.get(Allenamento_.dataInizio), datada);
            predicati.add(p);
        }else if(dataa!=null){
            p = cb.lessThanOrEqualTo(i.get(Allenamento_.dataInizio), dataa);
            predicati.add(p);
        }
        
        if(obTec!=null && !obTec.equals("")){
            p = cb.like(cb.lower(i.get(Allenamento_.obTec)), "%" + obTec.toLowerCase() + "%");
            predicati.add(p);
        }
        if(obFis!=null && !obFis.equals("")){
            p = cb.like(cb.lower(i.get(Allenamento_.obFis)), "%" + obFis.toLowerCase() + "%");
            predicati.add(p);
        }
        if(obTat!=null && !obTat.equals("")){
            p = cb.like(cb.lower(i.get(Allenamento_.obTat)), "%" + obTat.toLowerCase() + "%");
            predicati.add(p);
        }
        if(obPsi!=null && !obPsi.equals("")){
            p = cb.like(cb.lower(i.get(Allenamento_.obPsi)), "%" + obPsi.toLowerCase() + "%");
            predicati.add(p);
        }        
    }
    
    public void resetCriteri() {
        titolo = null;
        datada = null;
        dataa = null;
        obTec = null;
        obFis = null;
        obTat = null;
        obPsi = null;
     }

    public Integer getIdStagione() {
        return idStagione;
    }

    public void setIdStagione(Integer idStagione) {
        this.idStagione = idStagione;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public Date getDatada() {
        return datada;
    }

    public void setDatada(Date datada) {
        this.datada = datada;
    }

    public Date getDataa() {
        return dataa;
    }

    public void setDataa(Date dataa) {
        this.dataa = dataa;
    }

    public String getObTec() {
        return obTec;
    }

    public void setObTec(String obTec) {
        this.obTec = obTec;
    }

    public String getObFis() {
        return obFis;
    }

    public void setObFis(String obFis) {
        this.obFis = obFis;
    }

    public String getObTat() {
        return obTat;
    }

    public void setObTat(String obTat) {
        this.obTat = obTat;
    }

    public String getObPsi() {
        return obPsi;
    }

    public void setObPsi(String obPsi) {
        this.obPsi = obPsi;
    }

    public List<Predicate> getPredicati() {
        return predicati;
    }

    public void setPredicati(List<Predicate> predicati) {
        this.predicati = predicati;
    }
    
}
