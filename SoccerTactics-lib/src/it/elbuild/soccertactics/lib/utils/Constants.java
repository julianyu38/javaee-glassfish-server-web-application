/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Ale
 */
public class Constants {
    
    public class ServletConstants{
        public static final String JPEG = "jpeg";
        public static final String JPG = "jpg";
        public static final String PNG = "png";
        public static final String GIF = "gif";
        
        public static final String IMAGE_FORMAT_ERROR = "1";
        public static final String GENERIC_ERROR = "2";
        public static final String MISSING_PARAMETER = "3";
        
        public static final String FILE_PATH = "file.path";
        
        public static final String URL_IMMAGINI = "url.immagini";
        
         // PARAMETRI PASSATI A SERVLET APPUNTAMENTI  
        public static final String DATA_INIZIO = "inizio";
        public static final String DATA_FINE = "fine";
        public static final String VISUALIZZAZIONE = "visualizzazione";
        public static final String STAGIONE = "stagione";
        // classi visualizzazione eventi
        public static final String EVENT_WARNING = "event-warning";
        public static final String EVENT_SUCCESS = "event-success";
        public static final String EVENT_IMPORTANT = "event-important";
    }
    
    public class EmailConstants{
        public static final String MAIL_MITTENTE = "mail.mittente";
        public static final String MAIL_ADMIN = "mail.mittente";
    }
    
    public class Ruoli{
        public static final String STAFF = "staff";
        public static final String ADMIN = "admin";
        public static final String BLOCKED = "blocked";
        public static final String DELETED = "deleted";
    }
    
    public class Conf{
        public static final String PATH_SCHEMI = "file.path.schemi";
        public static final String UPDATE_URL = "url.update.disegno";
        public static final String UPDATE_SFONDO_URL = "url.update.sfondo.disegno";
        
        public static final String PAYPAL_USERNAME = "paypal.username";
        public static final String PAYPAL_PASSWORD = "paypal.password";
        public static final String PAYPAL_SIGNATURE = "paypal.signature";
        public static final String PAYPAL_ENVIRONMENT = "paypal.environment";
        public static final String PAYPAL_RETURNURL = "paypal.returnurl";
        public static final String PAYPAL_CANCELURL = "paypal.cancelurl";
        
        public static final String PATH_RICEVUTE = "file.path.ricevute";
    }
    
    public class FasiAllenamento{
        public static final String FASE1 = "ATTIVAZIONE MOTORIA";
        public static final String FASE2 = "FASE CENTRALE - ANALITICA";
        public static final String FASE3 = "FASE CENTRALE - SITUAZIONALE";
        public static final String FASE4 = "FASE FINALE - LAVORO CON SQUADRA/SCARICO";
    }
    
    public static List<String> MATERIALI_ESERCIZIO = initListaMateriali();
    
    private static List<String> initListaMateriali(){
        List<String> lista = new ArrayList<String>();
        lista.add("Bosu");
        lista.add("Cerchi");
        lista.add("Cinesini");
        lista.add("Coni");
        lista.add("Corda");
        lista.add("Ostacoli alti");
        lista.add("Ostacoli bassi");
        lista.add("Paletti orizzontali");
        lista.add("Paletti verticali");
        lista.add("Palle ritmiche");
        lista.add("Palline da tennis");
        lista.add("Plinto");
        lista.add("Porticine");
        lista.add("Porte regolari");
        lista.add("Reti grandi");
        lista.add("Reti piccole");
        lista.add("Sagome gonfiabili");
        lista.add("Sagome rigide");
        lista.add("Scaletta");
        lista.add("Smimmy");
        lista.add("Step");
        lista.add("Swiss ball");
        lista.add("Tavolette propriocettive");
        lista.add("Teloni");
        lista.add("Tubi");
        return lista;
    }
    
    public class ObiettiviEs{
        public static final String TECNICA_DIFESA = "TECNICA DIFENSIVA";
        public static final String TECNICA_ATTACCO = "TECNICA OFFENSIVA";
        public static final String OBIETTIVO_TATTICO = "OBIETTIVO TATTICO";
        public static final String OBIETTIVO_COORDINATIVO = "OBIETTIVO COORDINATIVO";
        public static final String OBIETTIVO_CONDIZIONALE = "OBIETTIVO CONDIZIONALE";
    }
    
//    messo tutto nel javascript
//    public class PngPortieri{
//        public static final String f = "f";         // fronte 
//        public static final String fdx = "fdx";     // fronte destro
//        public static final String fsx = "fsx";     // fronte sinistro
//        public static final String dfdx = "dfdx";   // diagonale fronte dx
//        public static final String dfsx = "dfsx";   
//        public static final String ldx = "ldx";     // laterale dx
//        public static final String lsx = "lsx";
//        public static final String drdx = "drdx";   // diagonale retro dx
//        public static final String drsx = "drsx";
//        public static final String r = "r";
//        public static final String rdx = "rdx";     // retro dx
//        public static final String rsx = "rsx";
//
//        public List<String> POS_A = Arrays.asList(f, dfdx, ldx, drdx, r, drsx, lsx, dfsx);
//        public List<String> POS_C1 = Arrays.asList(f, dfdx, ldx, lsx, dfsx);
//        public List<String> POS_C2DX = Arrays.asList(f, ldx);
//        public List<String> POS_C2SX = Arrays.asList(f, lsx);
//        public List<String> POS_D1 = Arrays.asList(f, dfdx, ldx, drdx, r, drsx, lsx, dfsx);
//        public List<String> POS_D3 = Arrays.asList(f, dfdx, ldx, lsx, dfsx);
//        public List<String> POS_D4DX = Arrays.asList(f, dfdx);
//        public List<String> POS_D4SX = Arrays.asList(f, dfsx);
//        public List<String> POS_E = Arrays.asList(fdx,fsx, dfdx, ldx, drdx, rdx, rsx, drsx, lsx, dfsx);
//        public List<String> POS_F = Arrays.asList(fdx,fsx, dfdx, ldx, drdx, rdx, rsx, drsx, lsx, dfsx);
//        public List<String> POS_G = Arrays.asList(f, dfdx, ldx, lsx, dfsx);
//        public List<String> POS_H = Arrays.asList(f, dfdx, ldx, lsx, dfsx);
//        public List<String> POS_IDX = Arrays.asList(f, dfdx, ldx, drdx, r, drsx, lsx, dfsx);
//        public List<String> POS_ISX = Arrays.asList(f, dfdx, ldx, drdx, r, drsx, lsx, dfsx);
//        public List<String> POS_L = Arrays.asList(f, dfdx, ldx, drdx, r, drsx, lsx, dfsx);
//        public List<String> POS_M = Arrays.asList(f, dfdx, ldx, drdx, r, drsx, lsx, dfsx);
//        public List<String> POS_MISTER1 = Arrays.asList(f, dfdx, ldx, drdx, r, drsx, lsx, dfsx);
//        public List<String> POS_MISTER2 = Arrays.asList(f, dfdx, ldx, drdx, r, drsx, lsx, dfsx);
//        
//    }
    
    public class TipoPacchetto{
        public static final String ESERCIZI = "ESERCIZI";
        public static final String ABBONAMENTO = "ABBONAMENTO";        
    }
    
    public static class StatiOrdine{
        public static String CREATED = "CREATED";
        public static String PAYMENT_PENDING = "PAYMENT_PENDING";
        public static String GETEC_FAILED = "GETEC_FAILED";
        public static String CANCELLED = "CANCELLED";
        public static String AUTHORIZED = "AUTHORIZED";
        public static String NOT_AUTHORIZED = "NOT_AUTHORIZED";
    }
    
}
