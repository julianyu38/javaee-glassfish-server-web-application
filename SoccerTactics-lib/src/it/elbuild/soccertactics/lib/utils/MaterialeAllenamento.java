/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.elbuild.soccertactics.lib.utils;

import java.io.Serializable;

/**
 *
 * @author Ale
 */
public class MaterialeAllenamento implements Serializable{
    
    private String materiale;
    private Integer quantita;

    public MaterialeAllenamento(String materiale, Integer quantita) {
        this.materiale = materiale;
        this.quantita = quantita;
    }
    
    public void setMaxQuantita(Integer q){
        if(q!=null){
            quantita = quantita!=null && q>quantita? q : quantita;
        }
    }

//    public void addQuantita(Integer q){
//        if(q!=null){
//            quantita = quantita!=null ? (quantita + q) : q;
//        }
//    }
    
    public String getMateriale() {
        return materiale;
    }

    public void setMateriale(String materiale) {
        this.materiale = materiale;
    }

    public Integer getQuantita() {
        return quantita;
    }

    public void setQuantita(Integer quantita) {
        this.quantita = quantita;
    }    
}
